if ((bowser.safari && bowser.version < 9) || (bowser.ie && bowser.version < 10)) {
  window.location.href = "/browser_not_supported";
}

angular.module('loomioApp', ['ngNewRouter', 'pascalprecht.translate', 'ngSanitize', 'hc.marked', 'angularFileUpload', 'mentio', 'ngAnimate', 'angular-inview', 'ui.gravatar', 'duScroll', 'angular-clipboard', 'checklist-model', 'monospaced.elastic', 'angularMoment', 'offClick', 'ngMaterial', 'angulartics', 'angulartics.google.tagmanager', 'vcRecaptcha', 'ngAnimate']);

window.Loomio.routes = [
  {
    path: '/dashboard',
    component: 'dashboardPage'
  }, {
    path: '/dashboard/:filter',
    component: 'dashboardPage'
  }, {
    path: '/polls',
    component: 'pollsPage'
  }, {
    path: '/polls/:filter',
    component: 'pollsPage'
  }, {
    path: '/inbox',
    component: 'inboxPage'
  }, {
    path: '/groups',
    component: 'groupsPage'
  }, {
    path: '/explore',
    component: 'explorePage'
  }, {
    path: '/profile',
    component: 'profilePage'
  }, {
    path: '/email_preferences',
    component: 'emailSettingsPage'
  }, {
    path: '/d/:key',
    component: 'threadPage'
  }, {
    path: '/d/:key/:stub',
    component: 'threadPage'
  }, {
    path: '/d/:key/comment/:comment',
    component: 'threadPage'
  }, {
    path: '/d/:key/proposal/:proposal',
    component: 'threadPage'
  }, {
    path: '/d/:key/proposal/:proposal/:outcome',
    component: 'threadPage'
  }, {
    path: '/m/:key/',
    component: 'proposalRedirect'
  }, {
    path: '/m/:key/:stub',
    component: 'proposalRedirect'
  }, {
    path: '/m/:key/votes/new',
    component: 'proposalRedirect'
  }, {
    path: '/p/new',
    component: 'startPollPage'
  }, {
    path: '/p/new/:poll_type',
    component: 'startPollPage'
  }, {
    path: '/p/:key/',
    component: 'pollPage'
  }, {
    path: '/p/:key/:stub',
    component: 'pollPage'
  }, {
    path: '/g/:key/memberships',
    component: 'membershipsPage'
  }, {
    path: '/g/:key/memberships/:username',
    component: 'membershipsPage'
  }, {
    path: '/g/:key/membership_requests',
    component: 'membershipRequestsPage'
  }, {
    path: '/g/:key/previous_proposals',
    component: 'previousProposalsPage'
  }, {
    path: '/g/:key/previous_polls',
    component: 'previousPollsPage'
  }, {
    path: '/g/new',
    component: 'startGroupPage'
  }, {
    path: '/g/:key',
    component: 'groupPage'
  }, {
    path: '/g/:key/:stub',
    component: 'groupPage'
  }, {
    path: '/u/:key',
    component: 'userPage'
  }, {
    path: '/u/:key/:stub',
    component: 'userPage'
  }, {
    path: '/apps/authorized',
    component: 'authorizedAppsPage'
  }, {
    path: '/apps/registered',
    component: 'registeredAppsPage'
  }, {
    path: '/apps/registered/:id',
    component: 'registeredAppPage'
  }, {
    path: '/apps/registered/:id/:stub',
    component: 'registeredAppPage'
  }, {
    path: '/slack/install',
    component: 'installSlackPage'
  }
];

if (document.location.protocol.match(/https/) && (navigator.serviceWorker != null)) {
  navigator.serviceWorker.register(document.location.origin + '/service-worker.js', {
    scope: './'
  });
}

angular.module('loomioApp').config(function($analyticsProvider) {
  $analyticsProvider.firstPageview(true);
  return $analyticsProvider.withAutoBase(true);
});

angular.module('loomioApp').config(function($animateProvider) {
  if (window.Loomio.environment === 'test') {
    return $animateProvider.classNameFilter(/no-elements-shall-animate-on-test/);
  } else {
    return $animateProvider.classNameFilter(/animated/);
  }
});

angular.module('loomioApp').config(function($compileProvider) {
  if (window.Loomio.environment === 'production') {
    return $compileProvider.debugInfoEnabled(false);
  }
});

angular.module('loomioApp').config(function($locationProvider) {
  return $locationProvider.html5Mode(true);
});

angular.module('loomioApp').config(function(markedProvider) {
  var _parse, customRenderer;
  customRenderer = function(opts) {
    var _super, cook, renderer;
    _super = new marked.Renderer(opts);
    renderer = _.clone(_super);
    cook = function(text) {
      text = emojione.shortnameToImage(text);
      text = text.replace(/\[\[@([a-zA-Z0-9]+)\]\]/g, "<a class='lmo-user-mention' href='/u/$1'>@$1</a>");
      return text;
    };
    renderer.paragraph = function(text) {
      return _super.paragraph(cook(text));
    };
    renderer.listitem = function(text) {
      return _super.listitem(cook(text));
    };
    renderer.tablecell = function(text, flags) {
      return _super.tablecell(cook(text), flags);
    };
    renderer.heading = function(text, level) {
      return _super.heading(emojione.shortnameToImage(text), level, text);
    };
    renderer.link = function(href, title, text) {
      return _super.link(href, title, text).replace('<a ', '<a rel="noopener noreferrer" target="_blank" ');
    };
    return renderer;
  };
  _parse = marked.parse;
  marked.parse = function(src, opt, callback) {
    src = src.replace(/<img[^>]+\>/ig, "");
    src = src.replace(/<script[^>]+\>/ig, "");
    return _parse(src, opt, callback);
  };
  return markedProvider.setRenderer(customRenderer({
    gfm: true,
    sanitize: true,
    breaks: true
  }));
});

angular.module('loomioApp').config(function($mdThemingProvider) {
  return $mdThemingProvider.theme('default').primaryPalette('orange', {
    'default': '400'
  }).accentPalette('cyan', {
    "default": '500'
  });
});

angular.module('loomioApp').config(function($mdDateLocaleProvider) {
  return $mdDateLocaleProvider.formatDate = function(date) {
    return moment(date).format('D MMMM YYYY');
  };
});

angular.module('loomioApp').config(function($provide) {
  return $provide.decorator('mentioMenuDirective', function($delegate) {
    var directive;
    directive = _.first($delegate);
    directive.compile = function() {
      return function(scope, elem) {
        var modal;
        directive.link.apply(this, arguments);
        if (modal = scope.parentMentio.targetElement[0].closest('.modal')) {
          return modal.appendChild(elem[0]);
        }
      };
    };
    return $delegate;
  });
});

angular.module('loomioApp').config(function($translateProvider) {
  return $translateProvider.useUrlLoader("/api/v1/translations").useSanitizeValueStrategy('escapeParameters').preferredLanguage(window.Loomio.currentUserLocale);
});

angular.module('loomioApp').filter('timeFromNowInWords', function() {
  return function(date, excludeAgo) {
    return moment(date).fromNow(excludeAgo);
  };
});

angular.module('loomioApp').filter('exactDateWithTime', function() {
  return function(date) {
    return moment(date).format('dddd MMMM Do [at] h:mm a');
  };
});

angular.module('loomioApp').filter('filterModel', function() {
  return function(records, fragment) {
    if (records == null) {
      records = [];
    }
    return _.filter(records, function(record) {
      return _.some(_.map(record.constructor.searchableFields, function(field) {
        if (typeof record[field] === 'function') {
          field = record[field]();
        }
        if ((field != null) && (field.search != null)) {
          return ~field.search(new RegExp(fragment, 'i'));
        }
      }));
    });
  };
});

angular.module('loomioApp').filter('limitByFn', function() {
  return function(items, f, args) {
    if (items) {
      return items.slice(0, f(args));
    }
  };
});

angular.module('loomioApp').filter('truncate', function() {
  return function(string, length, separator) {
    if (length == null) {
      length = 100;
    }
    if (separator == null) {
      separator = ' ';
    }
    return _.trunc(string, {
      length: length,
      separator: separator
    });
  };
});

emojione.unicodeAlt = false;

emojione.imagePathPNG = "/img/emojis/emojione/";

emojione.ascii = true;

_.fromPairs = function(pairs) {
  var index, length, pair, result;
  index = -1;
  length = pairs === null ? 0 : pairs.length;
  result = {};
  while (++index < length) {
    pair = pairs[index];
    result[pair[0]] = pair[1];
  }
  return result;
};

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

angular.module('loomioApp').factory('AttachmentModel', function(BaseModel, AppConfig) {
  var AttachmentModel;
  return AttachmentModel = (function(superClass) {
    extend(AttachmentModel, superClass);

    function AttachmentModel() {
      return AttachmentModel.__super__.constructor.apply(this, arguments);
    }

    AttachmentModel.singular = 'attachment';

    AttachmentModel.plural = 'attachments';

    AttachmentModel.indices = ['id', 'commentId'];

    AttachmentModel.serializableAttributes = AppConfig.permittedParams.attachment;

    AttachmentModel.prototype.relationships = function() {
      this.belongsTo('author', {
        from: 'users',
        by: 'authorId'
      });
      return this.belongsTo('comment', {
        from: 'comments',
        by: 'commentId'
      });
    };

    AttachmentModel.prototype.formattedFilesize = function() {
      var denom, size;
      if (isNaN(this.filesize)) {
        return "(invalid file size)";
      }
      if (this.filesize < 1000) {
        denom = "bytes";
        size = this.filesize;
      } else if (this.filesize < Math.pow(1000, 2)) {
        denom = "kB";
        size = this.filesize / 1000;
      } else if (this.filesize < Math.pow(1000, 3)) {
        denom = "MB";
        size = this.filesize / Math.pow(1000, 2);
      } else {
        denom = "GB";
        size = this.filesize / Math.pow(1000, 3);
      }
      return "(" + (size.toFixed(1)) + " " + denom + ")";
    };

    return AttachmentModel;

  })(BaseModel);
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

angular.module('loomioApp').factory('AttachmentRecordsInterface', function(BaseRecordsInterface, AttachmentModel) {
  var AttachmentRecordsInterface;
  return AttachmentRecordsInterface = (function(superClass) {
    extend(AttachmentRecordsInterface, superClass);

    function AttachmentRecordsInterface() {
      return AttachmentRecordsInterface.__super__.constructor.apply(this, arguments);
    }

    AttachmentRecordsInterface.prototype.model = AttachmentModel;

    AttachmentRecordsInterface.prototype.upload = function(file, progress) {
      return this.remote.upload('', file, {
        data: {
          'attachment[filename]': file.name.replace(/[^a-z0-9_\-\.]/gi, '_')
        },
        fileFormDataName: 'attachment[file]'
      }, progress);
    };

    return AttachmentRecordsInterface;

  })(BaseRecordsInterface);
});

angular.module('loomioApp').factory('BaseModel', function() {
  return AngularRecordStore.BaseModelFn();
});

angular.module('loomioApp').factory('BaseRecordsInterface', function(RestfulClient, $q) {
  return AngularRecordStore.BaseRecordsInterfaceFn(RestfulClient, $q);
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

angular.module('loomioApp').factory('CommentModel', function(DraftableModel, AppConfig) {
  var CommentModel;
  return CommentModel = (function(superClass) {
    extend(CommentModel, superClass);

    function CommentModel() {
      return CommentModel.__super__.constructor.apply(this, arguments);
    }

    CommentModel.singular = 'comment';

    CommentModel.plural = 'comments';

    CommentModel.indices = ['discussionId', 'authorId'];

    CommentModel.serializableAttributes = AppConfig.permittedParams.comment;

    CommentModel.draftParent = 'discussion';

    CommentModel.draftPayloadAttributes = ['body', 'attachment_ids'];

    CommentModel.prototype.afterConstruction = function() {
      return this.newAttachmentIds = _.clone(this.attachmentIds) || [];
    };

    CommentModel.prototype.defaultValues = function() {
      return {
        usesMarkdown: true,
        discussionId: null,
        body: '',
        likerIds: [],
        attachmentIds: [],
        mentionedUsernames: []
      };
    };

    CommentModel.prototype.relationships = function() {
      this.belongsTo('author', {
        from: 'users'
      });
      this.belongsTo('discussion');
      this.belongsTo('parent', {
        from: 'comments',
        by: 'parentId'
      });
      return this.hasMany('versions', {
        sortBy: 'createdAt'
      });
    };

    CommentModel.prototype.serialize = function() {
      var data;
      data = this.baseSerialize();
      data.comment.attachment_ids = this.newAttachmentIds;
      return data;
    };

    CommentModel.prototype.group = function() {
      return this.discussion().group();
    };

    CommentModel.prototype.isMostRecent = function() {
      return _.last(this.discussion().comments()) === this;
    };

    CommentModel.prototype.isReply = function() {
      return this.parentId != null;
    };

    CommentModel.prototype.hasContext = function() {
      return !!this.body;
    };

    CommentModel.prototype.parent = function() {
      return this.recordStore.comments.find(this.parentId);
    };

    CommentModel.prototype.likers = function() {
      return this.recordStore.users.find(this.likerIds);
    };

    CommentModel.prototype.newAttachments = function() {
      return this.recordStore.attachments.find(this.newAttachmentIds);
    };

    CommentModel.prototype.attachments = function() {
      return this.recordStore.attachments.find({
        attachableId: this.id,
        attachableType: 'Comment'
      });
    };

    CommentModel.prototype.authorName = function() {
      return this.author().name;
    };

    CommentModel.prototype.authorUsername = function() {
      return this.author().username;
    };

    CommentModel.prototype.authorAvatar = function() {
      return this.author().avatarOrInitials();
    };

    CommentModel.prototype.addLiker = function(user) {
      return this.likerIds.push(user.id);
    };

    CommentModel.prototype.removeLiker = function(user) {
      return this.removeLikerId(user.id);
    };

    CommentModel.prototype.removeLikerId = function(id) {
      return this.likerIds = _.without(this.likerIds, id);
    };

    CommentModel.prototype.cookedBody = function() {
      var cooked;
      cooked = this.body;
      _.each(this.mentionedUsernames, function(username) {
        return cooked = cooked.replace(RegExp("@" + username, "g"), "[[@" + username + "]]");
      });
      return cooked;
    };

    CommentModel.prototype.edited = function() {
      return this.versionsCount > 1;
    };

    CommentModel.prototype.attributeForVersion = function(attr, version) {
      if (!version) {
        return '';
      }
      if (version.changes[attr]) {
        return version.changes[attr][1];
      } else {
        return this.attributeForVersion(attr, this.recordStore.versions.find(version.previousId));
      }
    };

    return CommentModel;

  })(DraftableModel);
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

angular.module('loomioApp').factory('CommentRecordsInterface', function(BaseRecordsInterface, CommentModel) {
  var CommentRecordsInterface;
  return CommentRecordsInterface = (function(superClass) {
    extend(CommentRecordsInterface, superClass);

    function CommentRecordsInterface() {
      return CommentRecordsInterface.__super__.constructor.apply(this, arguments);
    }

    CommentRecordsInterface.prototype.model = CommentModel;

    CommentRecordsInterface.prototype.like = function(user, comment, success) {
      return this.remote.postMember(comment.id, "like");
    };

    CommentRecordsInterface.prototype.unlike = function(user, comment, success) {
      return this.remote.postMember(comment.id, "unlike");
    };

    return CommentRecordsInterface;

  })(BaseRecordsInterface);
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

angular.module('loomioApp').factory('CommunityModel', function(BaseModel, AppConfig) {
  var CommunityModel;
  return CommunityModel = (function(superClass) {
    extend(CommunityModel, superClass);

    function CommunityModel() {
      return CommunityModel.__super__.constructor.apply(this, arguments);
    }

    CommunityModel.singular = 'community';

    CommunityModel.plural = 'communities';

    CommunityModel.serializableAttributes = AppConfig.permittedParams.community;

    CommunityModel.prototype.defaultValues = function() {
      return {
        customFields: {}
      };
    };

    CommunityModel.prototype.relationships = function() {
      this.belongsTo('poll');
      this.belongsTo('user');
      return this.belongsTo('identity', {
        from: 'identities'
      });
    };

    CommunityModel.prototype.displayName = function() {
      switch (this.communityType) {
        case 'facebook':
          return this.customFields.facebook_group_name;
        case 'slack':
          return (this.identity().customFields.slack_team_name) + " - #" + this.customFields.slack_channel_name;
      }
    };

    return CommunityModel;

  })(BaseModel);
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

angular.module('loomioApp').factory('CommunityRecordsInterface', function(BaseRecordsInterface, CommunityModel) {
  var CommunityRecordsInterface;
  return CommunityRecordsInterface = (function(superClass) {
    extend(CommunityRecordsInterface, superClass);

    function CommunityRecordsInterface() {
      return CommunityRecordsInterface.__super__.constructor.apply(this, arguments);
    }

    CommunityRecordsInterface.prototype.model = CommunityModel;

    return CommunityRecordsInterface;

  })(BaseRecordsInterface);
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

angular.module('loomioApp').factory('ContactMessageModel', function(BaseModel, AppConfig) {
  var ContactMessageModel;
  return ContactMessageModel = (function(superClass) {
    extend(ContactMessageModel, superClass);

    function ContactMessageModel() {
      return ContactMessageModel.__super__.constructor.apply(this, arguments);
    }

    ContactMessageModel.singular = 'contactMessage';

    ContactMessageModel.plural = 'contactMessages';

    ContactMessageModel.serializableAttributes = AppConfig.permittedParams.contact_message;

    return ContactMessageModel;

  })(BaseModel);
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

angular.module('loomioApp').factory('ContactMessageRecordsInterface', function(BaseRecordsInterface, ContactMessageModel) {
  var ContactMessageRecordsInterface;
  return ContactMessageRecordsInterface = (function(superClass) {
    extend(ContactMessageRecordsInterface, superClass);

    function ContactMessageRecordsInterface() {
      return ContactMessageRecordsInterface.__super__.constructor.apply(this, arguments);
    }

    ContactMessageRecordsInterface.prototype.model = ContactMessageModel;

    return ContactMessageRecordsInterface;

  })(BaseRecordsInterface);
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

angular.module('loomioApp').factory('ContactModel', function(BaseModel) {
  var ContactModal;
  return ContactModal = (function(superClass) {
    extend(ContactModal, superClass);

    function ContactModal() {
      return ContactModal.__super__.constructor.apply(this, arguments);
    }

    ContactModal.singular = 'contact';

    ContactModal.plural = 'contacts';

    return ContactModal;

  })(BaseModel);
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

angular.module('loomioApp').factory('ContactRecordsInterface', function(BaseRecordsInterface, ContactModel) {
  var ContactRecordsInterface;
  return ContactRecordsInterface = (function(superClass) {
    extend(ContactRecordsInterface, superClass);

    function ContactRecordsInterface() {
      return ContactRecordsInterface.__super__.constructor.apply(this, arguments);
    }

    ContactRecordsInterface.prototype.model = ContactModel;

    ContactRecordsInterface.prototype.fetchInvitables = function(fragment, groupKey) {
      return this.fetch({
        params: {
          q: fragment,
          group_key: groupKey
        }
      });
    };

    return ContactRecordsInterface;

  })(BaseRecordsInterface);
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

angular.module('loomioApp').factory('DidNotVoteModel', function(BaseModel) {
  var DidNotVoteModel;
  return DidNotVoteModel = (function(superClass) {
    extend(DidNotVoteModel, superClass);

    function DidNotVoteModel() {
      return DidNotVoteModel.__super__.constructor.apply(this, arguments);
    }

    DidNotVoteModel.singular = 'didNotVote';

    DidNotVoteModel.plural = 'didNotVotes';

    DidNotVoteModel.indices = ['id', 'proposalId'];

    DidNotVoteModel.prototype.relationships = function() {
      this.belongsTo('user');
      return this.belongsTo('proposal');
    };

    return DidNotVoteModel;

  })(BaseModel);
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

angular.module('loomioApp').factory('DidNotVoteRecordsInterface', function(BaseRecordsInterface, DidNotVoteModel) {
  var DidNotVoteRecordsInterface;
  return DidNotVoteRecordsInterface = (function(superClass) {
    extend(DidNotVoteRecordsInterface, superClass);

    function DidNotVoteRecordsInterface() {
      return DidNotVoteRecordsInterface.__super__.constructor.apply(this, arguments);
    }

    DidNotVoteRecordsInterface.prototype.model = DidNotVoteModel;

    DidNotVoteRecordsInterface.prototype.fetchByProposal = function(proposalKey, options) {
      if (options == null) {
        options = {};
      }
      return this.fetch({
        params: {
          motion_id: proposalKey,
          per: options['per']
        }
      });
    };

    return DidNotVoteRecordsInterface;

  })(BaseRecordsInterface);
});

var bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
  extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

angular.module('loomioApp').factory('DiscussionModel', function(DraftableModel, AppConfig) {
  var DiscussionModel;
  return DiscussionModel = (function(superClass) {
    extend(DiscussionModel, superClass);

    function DiscussionModel() {
      this.move = bind(this.move, this);
      this.saveVolume = bind(this.saveVolume, this);
      this.privateDefaultValue = bind(this.privateDefaultValue, this);
      this.defaultValues = bind(this.defaultValues, this);
      return DiscussionModel.__super__.constructor.apply(this, arguments);
    }

    DiscussionModel.singular = 'discussion';

    DiscussionModel.plural = 'discussions';

    DiscussionModel.uniqueIndices = ['id', 'key'];

    DiscussionModel.indices = ['groupId', 'authorId'];

    DiscussionModel.draftParent = 'group';

    DiscussionModel.draftPayloadAttributes = ['title', 'description'];

    DiscussionModel.serializableAttributes = AppConfig.permittedParams.discussion;

    DiscussionModel.prototype.afterConstruction = function() {
      if (this.isNew()) {
        this["private"] = this.privateDefaultValue();
      }
      return this.newAttachmentIds = _.clone(this.attachmentIds) || [];
    };

    DiscussionModel.prototype.defaultValues = function() {
      return {
        "private": null,
        usesMarkdown: true,
        lastSequenceId: 0,
        firstSequenceId: 0,
        lastItemAt: null,
        title: '',
        description: ''
      };
    };

    DiscussionModel.prototype.serialize = function() {
      var data;
      data = this.baseSerialize();
      data.discussion.attachment_ids = this.newAttachmentIds;
      return data;
    };

    DiscussionModel.prototype.privateDefaultValue = function() {
      if (this.group()) {
        switch (this.group().discussionPrivacyOptions) {
          case 'private_only':
            return true;
          case 'public_or_private':
            return true;
          case 'public_only':
            return false;
        }
      } else {
        return null;
      }
    };

    DiscussionModel.prototype.relationships = function() {
      this.hasMany('comments', {
        sortBy: 'createdAt'
      });
      this.hasMany('events', {
        sortBy: 'sequenceId'
      });
      this.hasMany('proposals', {
        sortBy: 'createdAt',
        sortDesc: true
      });
      this.hasMany('polls', {
        sortBy: 'createdAt',
        sortDesc: true
      });
      this.hasMany('versions', {
        sortBy: 'createdAt'
      });
      this.belongsTo('group');
      return this.belongsTo('author', {
        from: 'users'
      });
    };

    DiscussionModel.prototype.translationOptions = function() {
      return {
        title: this.title,
        groupName: this.groupName()
      };
    };

    DiscussionModel.prototype.authorName = function() {
      if (this.author()) {
        return this.author().name;
      }
    };

    DiscussionModel.prototype.groupName = function() {
      if (this.group()) {
        return this.group().name;
      }
    };

    DiscussionModel.prototype.activeProposals = function() {
      return _.filter(this.proposals(), function(proposal) {
        return proposal.isActive();
      });
    };

    DiscussionModel.prototype.closedProposals = function() {
      return _.reject(this.proposals(), function(proposal) {
        return proposal.isActive();
      });
    };

    DiscussionModel.prototype.anyClosedProposals = function() {
      return _.some(this.closedProposals());
    };

    DiscussionModel.prototype.activeProposal = function() {
      return _.first(this.activeProposals());
    };

    DiscussionModel.prototype.hasActiveProposal = function() {
      return this.activeProposal() != null;
    };

    DiscussionModel.prototype.activePolls = function() {
      return _.filter(this.polls(), function(poll) {
        return poll.isActive();
      });
    };

    DiscussionModel.prototype.hasActivePoll = function() {
      return _.any(this.activePolls());
    };

    DiscussionModel.prototype.hasDecision = function() {
      return this.hasActiveProposal() || this.hasActivePoll();
    };

    DiscussionModel.prototype.closedPolls = function() {
      return _.filter(this.polls(), function(poll) {
        return !poll.isActive();
      });
    };

    DiscussionModel.prototype.activePoll = function() {
      return _.first(this.activePolls());
    };

    DiscussionModel.prototype.isUnread = function() {
      return !this.isDismissed() && (this.discussionReaderId != null) && ((this.lastReadAt == null) || this.unreadActivityCount() > 0);
    };

    DiscussionModel.prototype.isDismissed = function() {
      return (this.discussionReaderId != null) && (this.dismissedAt != null) && this.dismissedAt.isSameOrAfter(this.lastActivityAt);
    };

    DiscussionModel.prototype.hasUnreadActivity = function() {
      return this.isUnread() && this.unreadActivityCount() > 0;
    };

    DiscussionModel.prototype.hasContext = function() {
      return !!this.description;
    };

    DiscussionModel.prototype.isImportant = function() {
      return this.starred || this.hasDecision();
    };

    DiscussionModel.prototype.unreadActivityCount = function() {
      return this.salientItemsCount - this.readSalientItemsCount;
    };

    DiscussionModel.prototype.requireReloadFor = function(event) {
      if (!event || event.discussionId !== this.id || event.sequenceId) {
        return false;
      }
      return _.find(this.events(), function(e) {
        return e.kind === 'new_comment' && e.eventable.id === event.eventable.id;
      });
    };

    DiscussionModel.prototype.minLoadedSequenceId = function() {
      var item;
      item = _.min(this.events(), function(event) {
        return event.sequenceId || Number.MAX_VALUE;
      });
      return item.sequenceId;
    };

    DiscussionModel.prototype.maxLoadedSequenceId = function() {
      var item;
      item = _.max(this.events(), function(event) {
        return event.sequenceId || 0;
      });
      return item.sequenceId;
    };

    DiscussionModel.prototype.allEventsLoaded = function() {
      return this.recordStore.events.find({
        discussionId: this.id
      }).length === this.itemsCount;
    };

    DiscussionModel.prototype.membership = function() {
      return this.recordStore.memberships.find({
        userId: AppConfig.currentUserId,
        groupId: this.groupId
      })[0];
    };

    DiscussionModel.prototype.membershipVolume = function() {
      if (this.membership()) {
        return this.membership().volume;
      }
    };

    DiscussionModel.prototype.volume = function() {
      return this.discussionReaderVolume || this.membershipVolume();
    };

    DiscussionModel.prototype.saveVolume = function(volume, applyToAll) {
      if (applyToAll == null) {
        applyToAll = false;
      }
      if (applyToAll) {
        return this.membership().saveVolume(volume);
      } else {
        if (volume != null) {
          this.discussionReaderVolume = volume;
        }
        return this.remote.patchMember(this.keyOrId(), 'set_volume', {
          volume: this.discussionReaderVolume
        });
      }
    };

    DiscussionModel.prototype.isMuted = function() {
      return this.volume() === 'mute';
    };

    DiscussionModel.prototype.saveStar = function() {
      return this.remote.patchMember(this.keyOrId(), this.starred ? 'star' : 'unstar');
    };

    DiscussionModel.prototype.update = function(attrs) {
      if (attrs.lastReadSequenceId < this.lastReadSequenceId) {
        delete attrs.lastReadSequenceId;
      }
      if (attrs.readSalientItemsCount < this.readSalientItemsCount) {
        delete attrs.readSalientItemsCount;
      }
      return this.baseUpdate(attrs);
    };

    DiscussionModel.prototype.markAsRead = function(sequenceId) {
      if (isNaN(sequenceId)) {
        sequenceId = this.lastSequenceId;
      }
      if ((this.discussionReaderId != null) && (_.isNull(this.lastReadAt) || this.lastReadSequenceId < sequenceId)) {
        this.remote.patchMember(this.keyOrId(), 'mark_as_read', {
          sequence_id: sequenceId
        });
        return this.update({
          lastReadAt: moment(),
          lastReadSequenceId: sequenceId
        });
      }
    };

    DiscussionModel.prototype.dismiss = function() {
      this.remote.patchMember(this.keyOrId(), 'dismiss');
      return this.update({
        dismissedAt: moment()
      });
    };

    DiscussionModel.prototype.move = function() {
      return this.remote.patchMember(this.keyOrId(), 'move', {
        group_id: this.groupId
      });
    };

    DiscussionModel.prototype.edited = function() {
      return this.versionsCount > 1;
    };

    DiscussionModel.prototype.newAttachments = function() {
      return this.recordStore.attachments.find(this.newAttachmentIds);
    };

    DiscussionModel.prototype.attachments = function() {
      return this.recordStore.attachments.find({
        attachableId: this.id,
        attachableType: 'Discussion'
      });
    };

    DiscussionModel.prototype.hasAttachments = function() {
      return _.some(this.attachments());
    };

    DiscussionModel.prototype.attributeForVersion = function(attr, version) {
      if (!version) {
        return '';
      }
      if (version.changes[attr]) {
        return version.changes[attr][1];
      } else {
        return this.attributeForVersion(attr, this.recordStore.versions.find(version.previousId));
      }
    };

    DiscussionModel.prototype.cookedDescription = function() {
      var cooked;
      cooked = this.description;
      _.each(this.mentionedUsernames, function(username) {
        return cooked = cooked.replace(RegExp("@" + username, "g"), "[[@" + username + "]]");
      });
      return cooked;
    };

    return DiscussionModel;

  })(DraftableModel);
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

angular.module('loomioApp').factory('DiscussionRecordsInterface', function(BaseRecordsInterface, DiscussionModel) {
  var DiscussionRecordsInterface;
  return DiscussionRecordsInterface = (function(superClass) {
    extend(DiscussionRecordsInterface, superClass);

    function DiscussionRecordsInterface() {
      return DiscussionRecordsInterface.__super__.constructor.apply(this, arguments);
    }

    DiscussionRecordsInterface.prototype.model = DiscussionModel;

    DiscussionRecordsInterface.prototype.fetchByGroup = function(groupKey, options) {
      if (options == null) {
        options = {};
      }
      options['group_id'] = groupKey;
      return this.fetch({
        params: options
      });
    };

    DiscussionRecordsInterface.prototype.fetchDashboard = function(options) {
      if (options == null) {
        options = {};
      }
      return this.fetch({
        path: 'dashboard',
        params: options
      });
    };

    DiscussionRecordsInterface.prototype.fetchInbox = function(options) {
      if (options == null) {
        options = {};
      }
      return this.fetch({
        path: 'inbox',
        params: {
          from: options['from'] || 0,
          per: options['per'] || 100,
          since: options['since'] || moment().startOf('day').subtract(6, 'week').toDate(),
          timeframe_for: options['timeframe_for'] || 'last_activity_at'
        }
      });
    };

    return DiscussionRecordsInterface;

  })(BaseRecordsInterface);
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

angular.module('loomioApp').factory('DraftModel', function(BaseModel, AppConfig) {
  var DraftModel;
  return DraftModel = (function(superClass) {
    extend(DraftModel, superClass);

    function DraftModel() {
      return DraftModel.__super__.constructor.apply(this, arguments);
    }

    DraftModel.singular = 'draft';

    DraftModel.plural = 'drafts';

    DraftModel.uniqueIndices = ['id'];

    DraftModel.serializableAttributes = AppConfig.permittedParams.draft;

    DraftModel.prototype.updateFrom = function(model) {
      var payloadField;
      payloadField = _.snakeCase(model.constructor.serializationRoot || model.constructor.singular);
      this.payload[payloadField] = _.pick(model.serialize()[payloadField], model.constructor.draftPayloadAttributes);
      return this.remote.post((this.draftableType.toLowerCase()) + "/" + this.draftableId, this.serialize());
    };

    return DraftModel;

  })(BaseModel);
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

angular.module('loomioApp').factory('DraftRecordsInterface', function(BaseRecordsInterface, DraftModel) {
  var DraftRecordsInterface;
  return DraftRecordsInterface = (function(superClass) {
    extend(DraftRecordsInterface, superClass);

    function DraftRecordsInterface() {
      return DraftRecordsInterface.__super__.constructor.apply(this, arguments);
    }

    DraftRecordsInterface.prototype.model = DraftModel;

    DraftRecordsInterface.prototype.findOrBuildFor = function(model) {
      return _.first(this.find({
        draftableType: _.capitalize(model.constructor.singular),
        draftableId: model.id
      })) || this.build({
        draftableType: _.capitalize(model.constructor.singular),
        draftableId: model.id,
        payload: {}
      });
    };

    DraftRecordsInterface.prototype.fetchFor = function(model) {
      return this.remote.get(model.constructor.singular + "/" + model.id);
    };

    return DraftRecordsInterface;

  })(BaseRecordsInterface);
});

var bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
  extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

angular.module('loomioApp').factory('DraftableModel', function(BaseModel) {
  var DraftableModel;
  return DraftableModel = (function(superClass) {
    extend(DraftableModel, superClass);

    function DraftableModel() {
      this.fetchDraft = bind(this.fetchDraft, this);
      return DraftableModel.__super__.constructor.apply(this, arguments);
    }

    DraftableModel.draftParent = 'undefined';

    DraftableModel.prototype.draftParent = function() {
      return this[this.constructor.draftParent]();
    };

    DraftableModel.prototype.draft = function() {
      var parent;
      if (!(parent = this.draftParent())) {
        return;
      }
      return this.recordStore.drafts.findOrBuildFor(parent);
    };

    DraftableModel.prototype.fetchDraft = function() {
      var parent;
      if (!(parent = this.draftParent())) {
        return;
      }
      return this.recordStore.drafts.fetchFor(parent);
    };

    DraftableModel.prototype.restoreDraft = function() {
      var draft, payloadField;
      if (!(draft = this.draft())) {
        return;
      }
      payloadField = _.snakeCase(this.constructor.serializationRoot || this.constructor.singular);
      return this.update(_.omit(draft.payload[payloadField], _.isNull));
    };

    DraftableModel.prototype.resetDraft = function() {
      var draft;
      if (!(draft = this.draft())) {
        return;
      }
      return draft.updateFrom(this.recordStore[this.constructor.plural].build());
    };

    DraftableModel.prototype.updateDraft = function() {
      var draft;
      if (!(draft = this.draft())) {
        return;
      }
      return draft.updateFrom(this);
    };

    return DraftableModel;

  })(BaseModel);
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

angular.module('loomioApp').factory('EventModel', function(BaseModel) {
  var EventModel;
  return EventModel = (function(superClass) {
    extend(EventModel, superClass);

    function EventModel() {
      return EventModel.__super__.constructor.apply(this, arguments);
    }

    EventModel.singular = 'event';

    EventModel.plural = 'events';

    EventModel.indices = ['id', 'discussionId'];

    EventModel.eventTypeMap = {
      group: 'groups',
      discussion: 'discussions',
      motion: 'proposals',
      comment: 'comments',
      comment_vote: 'comments',
      membership: 'memberships',
      membership_request: 'membershipRequests'
    };

    EventModel.prototype.relationships = function() {
      this.belongsTo('actor', {
        from: 'users'
      });
      this.belongsTo('version');
      return this.hasMany('notifications');
    };

    EventModel.prototype["delete"] = function() {
      return this.deleted = true;
    };

    EventModel.prototype.actorName = function() {
      return this.actor().name;
    };

    EventModel.prototype.model = function() {
      return this.recordStore[this.constructor.eventTypeMap[this.eventable.type]].find(this.eventable.id);
    };

    EventModel.prototype.beforeRemove = function() {
      return _.invoke(this.notifications(), 'remove');
    };

    return EventModel;

  })(BaseModel);
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

angular.module('loomioApp').factory('EventRecordsInterface', function(BaseRecordsInterface, EventModel) {
  var EventRecordsInterface;
  return EventRecordsInterface = (function(superClass) {
    extend(EventRecordsInterface, superClass);

    function EventRecordsInterface() {
      return EventRecordsInterface.__super__.constructor.apply(this, arguments);
    }

    EventRecordsInterface.prototype.model = EventModel;

    EventRecordsInterface.prototype.fetchByDiscussion = function(discussionKey, options) {
      if (options == null) {
        options = {};
      }
      options['discussion_key'] = discussionKey;
      return this.fetch({
        params: options
      });
    };

    EventRecordsInterface.prototype.findByDiscussionAndSequenceId = function(discussion, sequenceId) {
      return this.collection.chain().find({
        discussionId: discussion.id
      }).find({
        sequenceId: sequenceId
      }).data()[0];
    };

    return EventRecordsInterface;

  })(BaseRecordsInterface);
});

var bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
  extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

angular.module('loomioApp').factory('GroupModel', function(DraftableModel, AppConfig) {
  var GroupModel;
  return GroupModel = (function(superClass) {
    extend(GroupModel, superClass);

    function GroupModel() {
      this.uploadPhoto = bind(this.uploadPhoto, this);
      this.archive = bind(this.archive, this);
      this.publish = bind(this.publish, this);
      return GroupModel.__super__.constructor.apply(this, arguments);
    }

    GroupModel.singular = 'group';

    GroupModel.plural = 'groups';

    GroupModel.uniqueIndices = ['id', 'key'];

    GroupModel.indices = ['parentId'];

    GroupModel.serializableAttributes = AppConfig.permittedParams.group;

    GroupModel.draftParent = 'draftParent';

    GroupModel.draftPayloadAttributes = ['name', 'description'];

    GroupModel.prototype.draftParent = function() {
      return this.parent() || this.recordStore.users.find(AppConfig.currentUserId);
    };

    GroupModel.prototype.defaultValues = function() {
      return {
        parentId: null,
        name: '',
        description: '',
        groupPrivacy: 'closed',
        discussionPrivacyOptions: 'private_only',
        membershipGrantedUpon: 'approval',
        membersCanAddMembers: true,
        membersCanEditDiscussions: true,
        membersCanEditComments: true,
        membersCanRaiseMotions: true,
        membersCanVote: true,
        membersCanStartDiscussions: true,
        membersCanCreateSubgroups: false,
        motionsCanBeEdited: false
      };
    };

    GroupModel.prototype.relationships = function() {
      this.hasMany('discussions');
      this.hasMany('proposals');
      this.hasMany('polls');
      this.hasMany('membershipRequests');
      this.hasMany('memberships');
      this.hasMany('invitations');
      this.hasMany('subgroups', {
        from: 'groups',
        "with": 'parentId',
        of: 'id'
      });
      return this.belongsTo('parent', {
        from: 'groups'
      });
    };

    GroupModel.prototype.parentOrSelf = function() {
      if (this.isParent()) {
        return this;
      } else {
        return this.parent();
      }
    };

    GroupModel.prototype.group = function() {
      return this;
    };

    GroupModel.prototype.shareableInvitation = function() {
      return this.recordStore.invitations.find({
        singleUse: false,
        groupId: this.id
      })[0];
    };

    GroupModel.prototype.closedProposals = function() {
      return _.filter(this.proposals(), function(proposal) {
        return proposal.isClosed();
      });
    };

    GroupModel.prototype.closedPolls = function() {
      return _.filter(this.polls(), function(poll) {
        return !poll.isActive();
      });
    };

    GroupModel.prototype.activePolls = function() {
      return _.filter(this.polls(), function(poll) {
        return poll.isActive();
      });
    };

    GroupModel.prototype.hasPreviousProposals = function() {
      return _.some(this.closedProposals());
    };

    GroupModel.prototype.pendingMembershipRequests = function() {
      return _.filter(this.membershipRequests(), function(membershipRequest) {
        return membershipRequest.isPending();
      });
    };

    GroupModel.prototype.hasPendingMembershipRequests = function() {
      return _.some(this.pendingMembershipRequests());
    };

    GroupModel.prototype.hasPendingMembershipRequestFrom = function(user) {
      return _.some(this.pendingMembershipRequests(), function(request) {
        return request.requestorId === user.id;
      });
    };

    GroupModel.prototype.previousMembershipRequests = function() {
      return _.filter(this.membershipRequests(), function(membershipRequest) {
        return !membershipRequest.isPending();
      });
    };

    GroupModel.prototype.hasPreviousMembershipRequests = function() {
      return _.some(this.previousMembershipRequests());
    };

    GroupModel.prototype.pendingInvitations = function() {
      return _.filter(this.invitations(), function(invitation) {
        return invitation.isPending() && invitation.singleUse;
      });
    };

    GroupModel.prototype.hasPendingInvitations = function() {
      return _.some(this.pendingInvitations());
    };

    GroupModel.prototype.organisationDiscussions = function() {
      return this.recordStore.discussions.find({
        groupId: {
          $in: this.organisationIds()
        },
        discussionReaderId: {
          $ne: null
        }
      });
    };

    GroupModel.prototype.organisationIds = function() {
      return _.pluck(this.subgroups(), 'id').concat(this.id);
    };

    GroupModel.prototype.organisationSubdomain = function() {
      if (this.isSubgroup()) {
        return this.parent().subdomain;
      } else {
        return this.subdomain;
      }
    };

    GroupModel.prototype.memberships = function() {
      return this.recordStore.memberships.find({
        groupId: this.id
      });
    };

    GroupModel.prototype.membershipFor = function(user) {
      return _.find(this.memberships(), function(membership) {
        return membership.userId === user.id;
      });
    };

    GroupModel.prototype.members = function() {
      return this.recordStore.users.find({
        id: {
          $in: this.memberIds()
        }
      });
    };

    GroupModel.prototype.adminMemberships = function() {
      return _.filter(this.memberships(), function(membership) {
        return membership.admin;
      });
    };

    GroupModel.prototype.admins = function() {
      var adminIds;
      adminIds = _.map(this.adminMemberships(), function(membership) {
        return membership.userId;
      });
      return this.recordStore.users.find({
        id: {
          $in: adminIds
        }
      });
    };

    GroupModel.prototype.coordinatorsIncludes = function(user) {
      return _.some(this.recordStore.memberships.where({
        groupId: this.id,
        userId: user.id
      }));
    };

    GroupModel.prototype.memberIds = function() {
      return _.map(this.memberships(), function(membership) {
        return membership.userId;
      });
    };

    GroupModel.prototype.adminIds = function() {
      return _.map(this.adminMemberships(), function(membership) {
        return membership.userId;
      });
    };

    GroupModel.prototype.parentName = function() {
      if (this.parent() != null) {
        return this.parent().name;
      }
    };

    GroupModel.prototype.privacyIsOpen = function() {
      return this.groupPrivacy === 'open';
    };

    GroupModel.prototype.privacyIsClosed = function() {
      return this.groupPrivacy === 'closed';
    };

    GroupModel.prototype.privacyIsSecret = function() {
      return this.groupPrivacy === 'secret';
    };

    GroupModel.prototype.allowPublicDiscussions = function() {
      if (this.privacyIsClosed() && this.isNew()) {
        return true;
      } else {
        return this.discussionPrivacyOptions !== 'private_only';
      }
    };

    GroupModel.prototype.isSubgroup = function() {
      return this.parentId != null;
    };

    GroupModel.prototype.isArchived = function() {
      return this.archivedAt != null;
    };

    GroupModel.prototype.isParent = function() {
      return this.parentId == null;
    };

    GroupModel.prototype.logoUrl = function() {
      if (this.logoUrlMedium) {
        return this.logoUrlMedium;
      } else if (this.isSubgroup()) {
        return this.parent().logoUrl();
      } else {
        return '/img/default-logo-medium.png';
      }
    };

    GroupModel.prototype.coverUrl = function(size) {
      if (this.isSubgroup() && !this.hasCustomCover) {
        return this.parent().coverUrl(size);
      } else {
        return this.coverUrls[size] || this.coverUrls.small;
      }
    };

    GroupModel.prototype.publish = function(identifier, channel) {
      return this.remote.postMember(this.key, 'publish', {
        make_announcement: this.makeAnnouncement,
        identifier: identifier,
        channel: channel
      });
    };

    GroupModel.prototype.archive = function() {
      return this.remote.patchMember(this.key, 'archive').then((function(_this) {
        return function() {
          _this.remove();
          return _.each(_this.memberships(), function(m) {
            return m.remove();
          });
        };
      })(this));
    };

    GroupModel.prototype.uploadPhoto = function(file, kind) {
      return this.remote.upload(this.key + "/upload_photo/" + kind, file, {}, function() {});
    };

    GroupModel.prototype.hasSubscription = function() {
      return this.subscriptionKind != null;
    };

    GroupModel.prototype.noInvitationsSent = function() {
      return this.membershipsCount < 2 && this.invitationsCount < 2;
    };

    GroupModel.prototype.isSubgroupOfSecretParent = function() {
      return this.isSubgroup() && this.parent().privacyIsSecret();
    };

    return GroupModel;

  })(DraftableModel);
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

angular.module('loomioApp').factory('GroupRecordsInterface', function(BaseRecordsInterface, GroupModel) {
  var GroupRecordsInterface;
  return GroupRecordsInterface = (function(superClass) {
    extend(GroupRecordsInterface, superClass);

    function GroupRecordsInterface() {
      return GroupRecordsInterface.__super__.constructor.apply(this, arguments);
    }

    GroupRecordsInterface.prototype.model = GroupModel;

    GroupRecordsInterface.prototype.fetchByParent = function(parentGroup) {
      return this.fetch({
        path: parentGroup.id + "/subgroups"
      });
    };

    GroupRecordsInterface.prototype.fetchExploreGroups = function(query, options) {
      if (options == null) {
        options = {};
      }
      options['q'] = query;
      return this.fetch({
        params: options
      });
    };

    GroupRecordsInterface.prototype.getExploreResultsCount = function(query, options) {
      if (options == null) {
        options = {};
      }
      options['q'] = query;
      return this.fetch({
        path: 'count_explore_results',
        params: options
      });
    };

    return GroupRecordsInterface;

  })(BaseRecordsInterface);
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

angular.module('loomioApp').factory('IdentityModel', function(BaseModel) {
  var IdentityModel;
  return IdentityModel = (function(superClass) {
    extend(IdentityModel, superClass);

    function IdentityModel() {
      return IdentityModel.__super__.constructor.apply(this, arguments);
    }

    IdentityModel.singular = 'identity';

    IdentityModel.plural = 'identities';

    return IdentityModel;

  })(BaseModel);
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

angular.module('loomioApp').factory('IdentityRecordsInterface', function(BaseRecordsInterface, IdentityModel) {
  var IdentityRecordsInterface;
  return IdentityRecordsInterface = (function(superClass) {
    extend(IdentityRecordsInterface, superClass);

    function IdentityRecordsInterface() {
      return IdentityRecordsInterface.__super__.constructor.apply(this, arguments);
    }

    IdentityRecordsInterface.prototype.model = IdentityModel;

    IdentityRecordsInterface.prototype.performCommand = function(id, command) {
      return this.remote.getMember(id, command);
    };

    return IdentityRecordsInterface;

  })(BaseRecordsInterface);
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

angular.module('loomioApp').factory('InvitationFormModel', function(DraftableModel, AppConfig) {
  var InvitationFormModel;
  return InvitationFormModel = (function(superClass) {
    extend(InvitationFormModel, superClass);

    function InvitationFormModel() {
      return InvitationFormModel.__super__.constructor.apply(this, arguments);
    }

    InvitationFormModel.singular = 'invitationForm';

    InvitationFormModel.plural = 'invitationForms';

    InvitationFormModel.draftParent = 'group';

    InvitationFormModel.serializableFields = ['emails', 'message'];

    InvitationFormModel.prototype.defaultValues = function() {
      return {
        emails: "",
        message: ""
      };
    };

    InvitationFormModel.prototype.relationships = function() {
      return this.belongsTo('group');
    };

    return InvitationFormModel;

  })(DraftableModel);
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

angular.module('loomioApp').factory('InvitationFormRecordsInterface', function(BaseRecordsInterface, InvitationFormModel) {
  var InvitationFormRecordsInterface;
  return InvitationFormRecordsInterface = (function(superClass) {
    extend(InvitationFormRecordsInterface, superClass);

    function InvitationFormRecordsInterface() {
      return InvitationFormRecordsInterface.__super__.constructor.apply(this, arguments);
    }

    InvitationFormRecordsInterface.prototype.model = InvitationFormModel;

    return InvitationFormRecordsInterface;

  })(BaseRecordsInterface);
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

angular.module('loomioApp').factory('InvitationModel', function(BaseModel, AppConfig) {
  var InvitationModel;
  return InvitationModel = (function(superClass) {
    extend(InvitationModel, superClass);

    function InvitationModel() {
      return InvitationModel.__super__.constructor.apply(this, arguments);
    }

    InvitationModel.singular = 'invitation';

    InvitationModel.plural = 'invitations';

    InvitationModel.indices = ['groupId'];

    InvitationModel.serializableAttributes = AppConfig.permittedParams.invitation;

    InvitationModel.draftPayloadAttributes = ['emails', 'message'];

    InvitationModel.prototype.relationships = function() {
      return this.belongsTo('group');
    };

    InvitationModel.prototype.isPending = function() {
      return (this.cancelledAt == null) && (this.acceptedAt == null);
    };

    return InvitationModel;

  })(BaseModel);
});

var bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
  extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

angular.module('loomioApp').factory('InvitationRecordsInterface', function(BaseRecordsInterface, InvitationModel) {
  var InvitationRecordsInterface;
  return InvitationRecordsInterface = (function(superClass) {
    extend(InvitationRecordsInterface, superClass);

    function InvitationRecordsInterface() {
      this.sendByEmail = bind(this.sendByEmail, this);
      return InvitationRecordsInterface.__super__.constructor.apply(this, arguments);
    }

    InvitationRecordsInterface.prototype.model = InvitationModel;

    InvitationRecordsInterface.prototype.sendByEmail = function(invitationForm) {
      return this.remote.create(_.merge(invitationForm.serialize(), {
        group_id: invitationForm.groupId
      }));
    };

    InvitationRecordsInterface.prototype.fetchPendingByGroup = function(groupKey, options) {
      if (options == null) {
        options = {};
      }
      options['group_key'] = groupKey;
      return this.remote.get('/pending', options);
    };

    InvitationRecordsInterface.prototype.fetchShareableInvitationByGroupId = function(groupId, options) {
      if (options == null) {
        options = {};
      }
      options['group_id'] = groupId;
      return this.remote.get('/shareable', options);
    };

    return InvitationRecordsInterface;

  })(BaseRecordsInterface);
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

angular.module('loomioApp').factory('MembershipModel', function(BaseModel, AppConfig) {
  var MembershipModel;
  return MembershipModel = (function(superClass) {
    extend(MembershipModel, superClass);

    function MembershipModel() {
      return MembershipModel.__super__.constructor.apply(this, arguments);
    }

    MembershipModel.singular = 'membership';

    MembershipModel.plural = 'memberships';

    MembershipModel.indices = ['id', 'userId', 'groupId'];

    MembershipModel.searchableFields = ['userName', 'userUsername'];

    MembershipModel.serializableAttributes = AppConfig.permittedParams.membership;

    MembershipModel.prototype.relationships = function() {
      this.belongsTo('group');
      this.belongsTo('user');
      return this.belongsTo('inviter', {
        from: 'users'
      });
    };

    MembershipModel.prototype.userName = function() {
      return this.user().name;
    };

    MembershipModel.prototype.userUsername = function() {
      return this.user().username;
    };

    MembershipModel.prototype.groupName = function() {
      return this.group().name;
    };

    MembershipModel.prototype.saveVolume = function(volume, applyToAll) {
      if (applyToAll == null) {
        applyToAll = false;
      }
      return this.remote.patchMember(this.keyOrId(), 'set_volume', {
        volume: volume,
        apply_to_all: applyToAll,
        unsubscribe_token: this.user().unsubscribeToken
      }).then((function(_this) {
        return function() {
          if (applyToAll) {
            _.each(_this.user().allThreads(), function(thread) {
              return thread.update({
                discussionReaderVolume: null
              });
            });
            return _.each(_this.user().memberships(), function(membership) {
              return membership.update({
                volume: volume
              });
            });
          } else {
            return _.each(_this.group().discussions(), function(discussion) {
              return discussion.update({
                discussionReaderVolume: null
              });
            });
          }
        };
      })(this));
    };

    MembershipModel.prototype.isMuted = function() {
      return this.volume === 'mute';
    };

    MembershipModel.prototype.beforeRemove = function() {
      return _.invoke(this.recordStore.events.find({
        'eventable.type': 'membership',
        'eventable.id': this.id
      }), 'remove');
    };

    return MembershipModel;

  })(BaseModel);
});

var bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
  extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

angular.module('loomioApp').factory('MembershipRecordsInterface', function(BaseRecordsInterface, MembershipModel) {
  var MembershipRecordsInterface;
  return MembershipRecordsInterface = (function(superClass) {
    extend(MembershipRecordsInterface, superClass);

    function MembershipRecordsInterface() {
      this.saveExperience = bind(this.saveExperience, this);
      return MembershipRecordsInterface.__super__.constructor.apply(this, arguments);
    }

    MembershipRecordsInterface.prototype.model = MembershipModel;

    MembershipRecordsInterface.prototype.joinGroup = function(group) {
      return this.remote.post('join_group', {
        group_id: group.id
      });
    };

    MembershipRecordsInterface.prototype.fetchMyMemberships = function() {
      return this.fetch({
        path: 'my_memberships'
      });
    };

    MembershipRecordsInterface.prototype.fetchByNameFragment = function(fragment, groupKey, limit) {
      if (limit == null) {
        limit = 5;
      }
      return this.fetch({
        path: 'autocomplete',
        params: {
          q: fragment,
          group_key: groupKey,
          per: limit
        }
      });
    };

    MembershipRecordsInterface.prototype.fetchInvitables = function(fragment, groupKey, limit) {
      if (limit == null) {
        limit = 5;
      }
      return this.fetch({
        path: 'invitables',
        params: {
          q: fragment,
          group_key: groupKey,
          per: limit
        }
      });
    };

    MembershipRecordsInterface.prototype.fetchByGroup = function(groupKey, options) {
      if (options == null) {
        options = {};
      }
      return this.fetch({
        params: {
          group_key: groupKey,
          per: options['per'] || 30
        }
      });
    };

    MembershipRecordsInterface.prototype.fetchByUser = function(userKey, options) {
      if (options == null) {
        options = {};
      }
      return this.fetch({
        path: 'for_user',
        params: {
          user_key: userKey,
          per: options['per'] || 30
        }
      });
    };

    MembershipRecordsInterface.prototype.addUsersToSubgroup = function(arg) {
      var groupId, userIds;
      groupId = arg.groupId, userIds = arg.userIds;
      return this.remote.post('add_to_subgroup', {
        group_id: groupId,
        user_ids: userIds
      });
    };

    MembershipRecordsInterface.prototype.makeAdmin = function(membership) {
      return this.remote.postMember(membership.id, "make_admin");
    };

    MembershipRecordsInterface.prototype.removeAdmin = function(membership) {
      return this.remote.postMember(membership.id, "remove_admin");
    };

    MembershipRecordsInterface.prototype.saveExperience = function(experience, membership) {
      return this.remote.postMember(membership.id, "save_experience", {
        experience: experience
      });
    };

    return MembershipRecordsInterface;

  })(BaseRecordsInterface);
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

angular.module('loomioApp').factory('MembershipRequestModel', function(BaseModel, AppConfig) {
  var MembershipRequestModel;
  return MembershipRequestModel = (function(superClass) {
    extend(MembershipRequestModel, superClass);

    function MembershipRequestModel() {
      return MembershipRequestModel.__super__.constructor.apply(this, arguments);
    }

    MembershipRequestModel.singular = 'membershipRequest';

    MembershipRequestModel.plural = 'membershipRequests';

    MembershipRequestModel.indices = ['id', 'groupId'];

    MembershipRequestModel.serializableAttributes = AppConfig.permittedParams.membership_request;

    MembershipRequestModel.prototype.initialize = function(data) {
      this.baseInitialize(data);
      if (!this.byExistingUser()) {
        return this.fakeUser = {
          name: this.name,
          email: this.email,
          avatarKind: 'initials',
          avatarInitials: _.map(this.name.split(' '), function(t) {
            return t[0];
          }).join('')
        };
      }
    };

    MembershipRequestModel.prototype.relationships = function() {
      this.belongsTo('group');
      this.belongsTo('requestor', {
        from: 'users'
      });
      return this.belongsTo('responder', {
        from: 'users'
      });
    };

    MembershipRequestModel.prototype.actor = function() {
      if (this.byExistingUser()) {
        return this.requestor();
      } else {
        return this.fakeUser;
      }
    };

    MembershipRequestModel.prototype.byExistingUser = function() {
      return this.requestorId != null;
    };

    MembershipRequestModel.prototype.isPending = function() {
      return this.respondedAt == null;
    };

    MembershipRequestModel.prototype.formattedResponse = function() {
      return _.capitalize(this.response);
    };

    MembershipRequestModel.prototype.charsLeft = function() {
      return 250 - (this.introduction || '').toString().length;
    };

    MembershipRequestModel.prototype.overCharLimit = function() {
      return this.charsLeft() < 0;
    };

    return MembershipRequestModel;

  })(BaseModel);
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

angular.module('loomioApp').factory('MembershipRequestRecordsInterface', function(BaseRecordsInterface, MembershipRequestModel) {
  var MembershipRequestRecordsInterface;
  return MembershipRequestRecordsInterface = (function(superClass) {
    extend(MembershipRequestRecordsInterface, superClass);

    function MembershipRequestRecordsInterface() {
      return MembershipRequestRecordsInterface.__super__.constructor.apply(this, arguments);
    }

    MembershipRequestRecordsInterface.prototype.model = MembershipRequestModel;

    MembershipRequestRecordsInterface.prototype.fetchMyPendingByGroup = function(groupKey, options) {
      if (options == null) {
        options = {};
      }
      options['group_key'] = groupKey;
      return this.remote.get('/my_pending', options);
    };

    MembershipRequestRecordsInterface.prototype.fetchPendingByGroup = function(groupKey, options) {
      if (options == null) {
        options = {};
      }
      options['group_key'] = groupKey;
      return this.remote.get('/pending', options);
    };

    MembershipRequestRecordsInterface.prototype.fetchPreviousByGroup = function(groupKey, options) {
      if (options == null) {
        options = {};
      }
      options['group_key'] = groupKey;
      return this.remote.get('/previous', options);
    };

    MembershipRequestRecordsInterface.prototype.approve = function(membershipRequest) {
      return this.remote.postMember(membershipRequest.id, 'approve', {
        group_key: membershipRequest.group().key
      });
    };

    MembershipRequestRecordsInterface.prototype.ignore = function(membershipRequest) {
      return this.remote.postMember(membershipRequest.id, 'ignore', {
        group_key: membershipRequest.group().key
      });
    };

    return MembershipRequestRecordsInterface;

  })(BaseRecordsInterface);
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

angular.module('loomioApp').factory('NotificationModel', function(BaseModel, $translate) {
  var NotificationModel;
  return NotificationModel = (function(superClass) {
    extend(NotificationModel, superClass);

    function NotificationModel() {
      return NotificationModel.__super__.constructor.apply(this, arguments);
    }

    NotificationModel.singular = 'notification';

    NotificationModel.plural = 'notifications';

    NotificationModel.prototype.relationships = function() {
      this.belongsTo('event');
      this.belongsTo('user');
      return this.belongsTo('actor', {
        from: 'users'
      });
    };

    NotificationModel.prototype.content = function() {
      return $translate.instant("notifications." + this.kind, this.translationValues);
    };

    NotificationModel.prototype.actionPath = function() {
      switch (this.kind()) {
        case 'motion_closed':
        case 'motion_closed_by_user':
          return 'outcome';
        case 'invitation_accepted':
          return this.actor().username;
      }
    };

    return NotificationModel;

  })(BaseModel);
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

angular.module('loomioApp').factory('NotificationRecordsInterface', function(BaseRecordsInterface, NotificationModel) {
  var NotificationRecordsInterface;
  return NotificationRecordsInterface = (function(superClass) {
    extend(NotificationRecordsInterface, superClass);

    function NotificationRecordsInterface() {
      return NotificationRecordsInterface.__super__.constructor.apply(this, arguments);
    }

    NotificationRecordsInterface.prototype.model = NotificationModel;

    NotificationRecordsInterface.prototype.viewed = function() {
      var any;
      any = false;
      _.each(this.collection.find({
        viewed: {
          $ne: true
        }
      }), (function(_this) {
        return function(n) {
          any = true;
          return n.update({
            viewed: true
          });
        };
      })(this));
      if (any) {
        return this.remote.post('viewed');
      }
    };

    return NotificationRecordsInterface;

  })(BaseRecordsInterface);
});

var bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
  extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

angular.module('loomioApp').factory('OauthApplicationModel', function(BaseModel, AppConfig) {
  var OauthApplicationModel;
  return OauthApplicationModel = (function(superClass) {
    extend(OauthApplicationModel, superClass);

    function OauthApplicationModel() {
      this.uploadLogo = bind(this.uploadLogo, this);
      return OauthApplicationModel.__super__.constructor.apply(this, arguments);
    }

    OauthApplicationModel.singular = 'oauthApplication';

    OauthApplicationModel.plural = 'oauthApplications';

    OauthApplicationModel.serializationRoot = 'oauth_application';

    OauthApplicationModel.serializableAttributes = AppConfig.permittedParams.oauth_application;

    OauthApplicationModel.prototype.defaultValues = function() {
      return {
        logoUrl: '/img/default-logo-medium.png'
      };
    };

    OauthApplicationModel.prototype.redirectUriArray = function() {
      return this.redirectUri.split("\n");
    };

    OauthApplicationModel.prototype.revokeAccess = function() {
      return this.remote.postMember(this.id, 'revoke_access');
    };

    OauthApplicationModel.prototype.uploadLogo = function(file) {
      return this.remote.upload(this.id + "/upload_logo", file);
    };

    return OauthApplicationModel;

  })(BaseModel);
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

angular.module('loomioApp').factory('OauthApplicationRecordsInterface', function(BaseRecordsInterface, OauthApplicationModel) {
  var OauthApplicationRecordsInterface;
  return OauthApplicationRecordsInterface = (function(superClass) {
    extend(OauthApplicationRecordsInterface, superClass);

    function OauthApplicationRecordsInterface() {
      return OauthApplicationRecordsInterface.__super__.constructor.apply(this, arguments);
    }

    OauthApplicationRecordsInterface.prototype.model = OauthApplicationModel;

    OauthApplicationRecordsInterface.prototype.fetchOwned = function(options) {
      if (options == null) {
        options = {};
      }
      return this.fetch({
        path: 'owned',
        params: options
      });
    };

    OauthApplicationRecordsInterface.prototype.fetchAuthorized = function(options) {
      if (options == null) {
        options = {};
      }
      return this.fetch({
        path: 'authorized',
        params: options
      });
    };

    return OauthApplicationRecordsInterface;

  })(BaseRecordsInterface);
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

angular.module('loomioApp').factory('OutcomeModel', function(DraftableModel, AppConfig, MentionLinkService) {
  var OutcomeModel;
  return OutcomeModel = (function(superClass) {
    extend(OutcomeModel, superClass);

    function OutcomeModel() {
      return OutcomeModel.__super__.constructor.apply(this, arguments);
    }

    OutcomeModel.singular = 'outcome';

    OutcomeModel.plural = 'outcomes';

    OutcomeModel.indices = ['pollId', 'authorId'];

    OutcomeModel.serializableAttributes = AppConfig.permittedParams.outcome;

    OutcomeModel.draftParent = 'poll';

    OutcomeModel.draftPayloadAttributes = ['statement'];

    OutcomeModel.prototype.defaultValues = function() {
      return {
        statement: '',
        customFields: {}
      };
    };

    OutcomeModel.prototype.relationships = function() {
      this.belongsTo('author', {
        from: 'users'
      });
      return this.belongsTo('poll');
    };

    OutcomeModel.prototype.group = function() {
      if (this.poll()) {
        return this.poll().group();
      }
    };

    OutcomeModel.prototype.communitySize = function() {
      return this.poll().communitySize();
    };

    OutcomeModel.prototype.announcementSize = function() {
      return this.communitySize();
    };

    OutcomeModel.prototype.notifyAction = function() {
      return 'publish';
    };

    return OutcomeModel;

  })(DraftableModel);
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

angular.module('loomioApp').factory('OutcomeRecordsInterface', function(BaseRecordsInterface, OutcomeModel) {
  var OutcomeRecordsInterface;
  return OutcomeRecordsInterface = (function(superClass) {
    extend(OutcomeRecordsInterface, superClass);

    function OutcomeRecordsInterface() {
      return OutcomeRecordsInterface.__super__.constructor.apply(this, arguments);
    }

    OutcomeRecordsInterface.prototype.model = OutcomeModel;

    return OutcomeRecordsInterface;

  })(BaseRecordsInterface);
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

angular.module('loomioApp').factory('PollCommunityRecordsInterface', function(BaseRecordsInterface) {
  var PollCommunityRecordsInterface;
  return PollCommunityRecordsInterface = (function(superClass) {
    extend(PollCommunityRecordsInterface, superClass);

    function PollCommunityRecordsInterface() {
      return PollCommunityRecordsInterface.__super__.constructor.apply(this, arguments);
    }

    PollCommunityRecordsInterface.prototype.model = {
      plural: 'poll_communities'
    };

    PollCommunityRecordsInterface.prototype.revoke = function(poll, community, options) {
      if (options == null) {
        options = {};
      }
      options['poll_id'] = poll.id;
      options['community_id'] = community.id;
      return this.remote["delete"]('', {
        params: options
      }).then(function() {
        return community.remove();
      });
    };

    return PollCommunityRecordsInterface;

  })(BaseRecordsInterface);
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

angular.module('loomioApp').factory('PollDidNotVoteModel', function(AppConfig, BaseModel) {
  var PollDidNotVoteModel;
  return PollDidNotVoteModel = (function(superClass) {
    extend(PollDidNotVoteModel, superClass);

    function PollDidNotVoteModel() {
      return PollDidNotVoteModel.__super__.constructor.apply(this, arguments);
    }

    PollDidNotVoteModel.singular = 'poll_did_not_vote';

    PollDidNotVoteModel.plural = 'poll_did_not_votes';

    PollDidNotVoteModel.indices = ['pollId', 'userId'];

    PollDidNotVoteModel.serializableAttributes = AppConfig.permittedParams.pollDidNotVote;

    PollDidNotVoteModel.prototype.relationships = function() {
      this.belongsTo('user');
      return this.belongsTo('poll');
    };

    return PollDidNotVoteModel;

  })(BaseModel);
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

angular.module('loomioApp').factory('PollDidNotVoteRecordsInterface', function(BaseRecordsInterface, PollDidNotVoteModel) {
  var PollDidNotVoteRecordsInterface;
  return PollDidNotVoteRecordsInterface = (function(superClass) {
    extend(PollDidNotVoteRecordsInterface, superClass);

    function PollDidNotVoteRecordsInterface() {
      return PollDidNotVoteRecordsInterface.__super__.constructor.apply(this, arguments);
    }

    PollDidNotVoteRecordsInterface.prototype.model = PollDidNotVoteModel;

    return PollDidNotVoteRecordsInterface;

  })(BaseRecordsInterface);
});

var bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
  extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

angular.module('loomioApp').factory('PollModel', function(DraftableModel, AppConfig, MentionLinkService) {
  var PollModel;
  return PollModel = (function(superClass) {
    extend(PollModel, superClass);

    function PollModel() {
      this.toggleSubscription = bind(this.toggleSubscription, this);
      this.addOptions = bind(this.addOptions, this);
      this.publish = bind(this.publish, this);
      this.close = bind(this.close, this);
      return PollModel.__super__.constructor.apply(this, arguments);
    }

    PollModel.singular = 'poll';

    PollModel.plural = 'polls';

    PollModel.indices = ['discussionId', 'authorId'];

    PollModel.serializableAttributes = AppConfig.permittedParams.poll;

    PollModel.draftParent = 'draftParent';

    PollModel.draftPayloadAttributes = ['title', 'details'];

    PollModel.prototype.draftParent = function() {
      return this.discussion() || this.author();
    };

    PollModel.prototype.afterConstruction = function() {
      return this.newAttachmentIds = _.clone(this.attachmentIds) || [];
    };

    PollModel.prototype.importance = function(now) {
      if (this.closedAt != null) {
        return Math.abs(this.closedAt - now);
      } else {
        return 0.0001 * Math.abs(this.closingAt - now);
      }
    };

    PollModel.prototype.defaultValues = function() {
      return {
        discussionId: null,
        title: '',
        details: '',
        closingAt: moment().add(3, 'days').startOf('hour'),
        pollOptionNames: [],
        pollOptionIds: [],
        customFields: {}
      };
    };

    PollModel.prototype.serialize = function() {
      var data;
      data = this.baseSerialize();
      data.poll.attachment_ids = this.newAttachmentIds;
      return data;
    };

    PollModel.prototype.relationships = function() {
      this.belongsTo('author', {
        from: 'users'
      });
      this.belongsTo('discussion');
      this.hasMany('pollOptions');
      this.hasMany('stances', {
        sortBy: 'createdAt',
        sortDesc: true
      });
      this.hasMany('pollDidNotVotes');
      this.hasMany('communities');
      return this.hasMany('visitors');
    };

    PollModel.prototype.group = function() {
      if (this.discussion()) {
        return this.discussion().group();
      } else {
        return this.recordStore.groups.find(this.groupId);
      }
    };

    PollModel.prototype.userVoters = function() {
      return this.recordStore.users.find(_.pluck(this.stances(), 'userId'));
    };

    PollModel.prototype.visitorVoters = function() {
      return this.recordStore.visitors.find(_.pluck(this.stances(), 'visitorId'));
    };

    PollModel.prototype.newAttachments = function() {
      return this.recordStore.attachments.find(this.newAttachmentIds);
    };

    PollModel.prototype.attachments = function() {
      return this.recordStore.attachments.find({
        attachableId: this.id,
        attachableType: 'Poll'
      });
    };

    PollModel.prototype.hasAttachments = function() {
      return _.some(this.attachments());
    };

    PollModel.prototype.communitySize = function() {
      return this.membersCount() + (this.visitorsCount || 0);
    };

    PollModel.prototype.membersCount = function() {
      if (this.group()) {
        return this.group().membershipsCount;
      } else {
        return 1;
      }
    };

    PollModel.prototype.announcementSize = function(action) {
      switch (action || this.notifyAction()) {
        case 'publish':
          return this.communitySize();
        case 'edit':
          return this.stancesCount;
        default:
          return 0;
      }
    };

    PollModel.prototype.percentVoted = function() {
      if (this.communitySize() > 0) {
        return (100 * this.stancesCount / this.communitySize()).toFixed(0);
      }
    };

    PollModel.prototype.undecidedCount = function() {
      return this.undecidedUserCount + this.undecidedVisitorCount;
    };

    PollModel.prototype.undecidedUsers = function() {
      if (this.isActive()) {
        if (this.group()) {
          return _.difference(this.group().members(), this.userVoters());
        } else {
          return _.difference([this.author()], this.userVoters());
        }
      } else {
        return this.recordStore.users.find(_.pluck(this.pollDidNotVotes(), 'userId'));
      }
    };

    PollModel.prototype.undecidedVisitors = function() {
      return _.difference(this.visitors(), this.visitorVoters());
    };

    PollModel.prototype.firstOption = function() {
      return _.first(this.pollOptions());
    };

    PollModel.prototype.outcome = function() {
      return this.recordStore.outcomes.find({
        pollId: this.id,
        latest: true
      })[0];
    };

    PollModel.prototype.clearStaleStances = function() {
      var existing;
      existing = [];
      return _.each(this.uniqueStances('-createdAt'), function(stance) {
        if (_.contains(existing, stance.participant())) {
          return stance.remove();
        } else {
          return existing.push(stance.participant());
        }
      });
    };

    PollModel.prototype.uniqueStances = function(order, limit) {
      return _.slice(_.sortBy(this.recordStore.stances.find({
        pollId: this.id,
        latest: true
      }), order), 0, limit);
    };

    PollModel.prototype.cookedDetails = function() {
      return MentionLinkService.cook(this.mentionedUsernames, this.details);
    };

    PollModel.prototype.isActive = function() {
      return this.closedAt == null;
    };

    PollModel.prototype.isClosed = function() {
      return this.closedAt != null;
    };

    PollModel.prototype.goal = function() {
      return this.customFields.goal || this.communitySize();
    };

    PollModel.prototype.close = function() {
      return this.remote.postMember(this.key, 'close');
    };

    PollModel.prototype.publish = function(community, message) {
      return this.remote.postMember(this.key, 'publish', {
        community_id: community.id,
        message: message
      }).then((function(_this) {
        return function() {
          return _this.published = true;
        };
      })(this));
    };

    PollModel.prototype.addOptions = function() {
      return this.remote.postMember(this.key, 'add_options', {
        poll_option_names: this.pollOptionNames
      });
    };

    PollModel.prototype.createVisitors = function() {
      this.processing = true;
      return this.remote.postMember(this.key, 'create_visitors', {
        emails: this.customFields.pending_emails.join(',')
      })["finally"]((function(_this) {
        return function() {
          return _this.processing = false;
        };
      })(this));
    };

    PollModel.prototype.toggleSubscription = function() {
      return this.remote.postMember(this.key, 'toggle_subscription');
    };

    PollModel.prototype.enableCommunities = function() {
      return (this.group() && this.group().features.enable_communities) || (this.author() && this.author().experiences.enable_communities);
    };

    PollModel.prototype.notifyAction = function() {
      if (this.isNew()) {
        return 'publish';
      } else {
        return 'edit';
      }
    };

    return PollModel;

  })(DraftableModel);
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

angular.module('loomioApp').factory('PollOptionModel', function(BaseModel) {
  var PollOptionModel;
  return PollOptionModel = (function(superClass) {
    extend(PollOptionModel, superClass);

    function PollOptionModel() {
      return PollOptionModel.__super__.constructor.apply(this, arguments);
    }

    PollOptionModel.singular = 'pollOption';

    PollOptionModel.plural = 'pollOptions';

    PollOptionModel.indices = ['pollId'];

    PollOptionModel.prototype.relationships = function() {
      this.belongsTo('poll');
      return this.hasMany('stanceChoices');
    };

    PollOptionModel.prototype.stances = function() {
      return _.compact(_.map(this.stanceChoices(), function(stanceChoice) {
        return stanceChoice.stance();
      }));
    };

    PollOptionModel.prototype.beforeRemove = function() {
      return _.each(this.stances(), function(stance) {
        return stance.remove();
      });
    };

    return PollOptionModel;

  })(BaseModel);
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

angular.module('loomioApp').factory('PollOptionRecordsInterface', function(BaseRecordsInterface, PollOptionModel) {
  var PollOptionRecordsInterface;
  return PollOptionRecordsInterface = (function(superClass) {
    extend(PollOptionRecordsInterface, superClass);

    function PollOptionRecordsInterface() {
      return PollOptionRecordsInterface.__super__.constructor.apply(this, arguments);
    }

    PollOptionRecordsInterface.prototype.model = PollOptionModel;

    return PollOptionRecordsInterface;

  })(BaseRecordsInterface);
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

angular.module('loomioApp').factory('PollRecordsInterface', function(BaseRecordsInterface, PollModel) {
  var PollRecordsInterface;
  return PollRecordsInterface = (function(superClass) {
    extend(PollRecordsInterface, superClass);

    function PollRecordsInterface() {
      return PollRecordsInterface.__super__.constructor.apply(this, arguments);
    }

    PollRecordsInterface.prototype.model = PollModel;

    PollRecordsInterface.prototype.fetchFor = function(model, options) {
      if (options == null) {
        options = {};
      }
      options[model.constructor.singular + "_key"] = model.key;
      return this.search(options);
    };

    PollRecordsInterface.prototype.search = function(options) {
      if (options == null) {
        options = {};
      }
      return this.fetch({
        path: 'search',
        params: options
      });
    };

    PollRecordsInterface.prototype.searchResultsCount = function(options) {
      if (options == null) {
        options = {};
      }
      return this.fetch({
        path: 'search_results_count',
        params: options
      });
    };

    PollRecordsInterface.prototype.fetchByGroup = function(groupKey, options) {
      if (options == null) {
        options = {};
      }
      return this.search(_.merge(options, {
        group_key: groupKey
      }));
    };

    return PollRecordsInterface;

  })(BaseRecordsInterface);
});

var bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
  extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

angular.module('loomioApp').factory('ProposalModel', function(BaseModel, AppConfig, DraftableModel) {
  var ProposalModel;
  return ProposalModel = (function(superClass) {
    extend(ProposalModel, superClass);

    function ProposalModel() {
      this.updateOutcome = bind(this.updateOutcome, this);
      this.createOutcome = bind(this.createOutcome, this);
      this.close = bind(this.close, this);
      return ProposalModel.__super__.constructor.apply(this, arguments);
    }

    ProposalModel.singular = 'proposal';

    ProposalModel.plural = 'proposals';

    ProposalModel.uniqueIndices = ['id', 'key'];

    ProposalModel.indices = ['discussionId'];

    ProposalModel.serializationRoot = 'motion';

    ProposalModel.serializableAttributes = AppConfig.permittedParams.motion;

    ProposalModel.draftParent = 'discussion';

    ProposalModel.draftPayloadAttributes = ['name', 'description'];

    ProposalModel.prototype.afterConstruction = function() {
      return this.newAttachmentIds = _.clone(this.attachmentIds) || [];
    };

    ProposalModel.prototype.defaultValues = function() {
      return {
        description: '',
        outcome: '',
        voteCounts: {
          yes: 0,
          no: 0,
          abstain: 0,
          block: 0
        },
        closingAt: moment().add(3, 'days').startOf('hour')
      };
    };

    ProposalModel.prototype.serialize = function() {
      var data;
      data = this.baseSerialize();
      data.motion.attachment_ids = this.newAttachmentIds;
      return data;
    };

    ProposalModel.prototype.relationships = function() {
      this.hasMany('votes', {
        sortBy: 'createdAt',
        sortDesc: true
      });
      this.hasMany('didNotVotes');
      this.belongsTo('author', {
        from: 'users'
      });
      return this.belongsTo('discussion');
    };

    ProposalModel.prototype.positionVerbs = ['agree', 'abstain', 'disagree', 'block'];

    ProposalModel.prototype.positions = ['yes', 'abstain', 'no', 'block'];

    ProposalModel.prototype.closingSoon = function() {
      return this.isActive() && this.closingAt < moment().add(24, 'hours').toDate();
    };

    ProposalModel.prototype.canBeEdited = function() {
      return this.isNew() || !this.hasVotes();
    };

    ProposalModel.prototype.hasVotes = function() {
      return this.votes().length > 0;
    };

    ProposalModel.prototype.group = function() {
      return this.discussion().group();
    };

    ProposalModel.prototype.voters = function() {
      return this.recordStore.users.find(this.voterIds());
    };

    ProposalModel.prototype.voterIds = function() {
      return _.pluck(this.votes(), 'authorId');
    };

    ProposalModel.prototype.authorName = function() {
      return this.author().name;
    };

    ProposalModel.prototype.isActive = function() {
      return !this.isClosed();
    };

    ProposalModel.prototype.isClosed = function() {
      return (this.closedAt != null) || ((this.closingAt != null) && this.closingAt.isBefore());
    };

    ProposalModel.prototype.uniqueVotesByUserId = function() {
      var votesByUserId;
      votesByUserId = {};
      _.each(_.sortBy(this.votes(), 'createdAt'), function(vote) {
        return votesByUserId[vote.authorId] = vote;
      });
      return votesByUserId;
    };

    ProposalModel.prototype.uniqueVotes = function() {
      return _.values(this.uniqueVotesByUserId());
    };

    ProposalModel.prototype.numberVoted = function() {
      return this.uniqueVotes().length;
    };

    ProposalModel.prototype.percentVoted = function() {
      if (this.votersCount === 0 || this.membersCount === 0) {
        return 0;
      }
      return (100 * this.votersCount / this.membersCount).toFixed(0);
    };

    ProposalModel.prototype.lastVoteByUser = function(user) {
      return this.uniqueVotesByUserId()[user.id];
    };

    ProposalModel.prototype.userHasVoted = function(user) {
      return this.lastVoteByUser(user) != null;
    };

    ProposalModel.prototype.close = function() {
      return this.remote.postMember(this.id, "close");
    };

    ProposalModel.prototype.hasOutcome = function() {
      return _.some(this.outcome);
    };

    ProposalModel.prototype.hasContext = function() {
      return !!this.description;
    };

    ProposalModel.prototype.undecidedMembers = function() {
      if (this.isActive()) {
        return _.difference(this.group().members(), this.voters());
      } else {
        return this.recordStore.users.find(_.pluck(this.didNotVotes(), 'userId'));
      }
    };

    ProposalModel.prototype.undecidedUsernames = function() {
      return _.pluck(this.undecidedMembers(), 'username');
    };

    ProposalModel.prototype.hasUndecidedMembers = function() {
      return this.membersCount > this.votersCount;
    };

    ProposalModel.prototype.createOutcome = function() {
      return this.remote.postMember(this.id, "create_outcome", {
        motion: {
          outcome: this.outcome
        }
      });
    };

    ProposalModel.prototype.updateOutcome = function() {
      return this.remote.postMember(this.id, "update_outcome", {
        motion: {
          outcome: this.outcome
        }
      });
    };

    ProposalModel.prototype.fetchUndecidedMembers = function() {
      if (this.isActive()) {
        return this.recordStore.memberships.fetchByGroup(this.group().key, {
          per: 500
        });
      } else {
        return this.recordStore.didNotVotes.fetchByProposal(this.key, {
          per: 500
        });
      }
    };

    ProposalModel.prototype.cookedDescription = function() {
      var cooked;
      cooked = this.description;
      _.each(this.mentionedUsernames, function(username) {
        return cooked = cooked.replace(RegExp("@" + username, "g"), "[[@" + username + "]]");
      });
      return cooked;
    };

    ProposalModel.prototype.newAttachments = function() {
      return this.recordStore.attachments.find(this.newAttachmentIds);
    };

    ProposalModel.prototype.attachments = function() {
      return this.recordStore.attachments.find({
        attachableId: this.id,
        attachableType: 'Motion'
      });
    };

    return ProposalModel;

  })(DraftableModel);
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

angular.module('loomioApp').factory('ProposalRecordsInterface', function(BaseRecordsInterface, ProposalModel) {
  var ProposalRecordsInterface;
  return ProposalRecordsInterface = (function(superClass) {
    extend(ProposalRecordsInterface, superClass);

    function ProposalRecordsInterface() {
      return ProposalRecordsInterface.__super__.constructor.apply(this, arguments);
    }

    ProposalRecordsInterface.prototype.model = ProposalModel;

    ProposalRecordsInterface.prototype.fetchByDiscussion = function(discussion) {
      return this.fetch({
        params: {
          discussion_key: discussion.key
        },
        cacheKey: "proposalsFor" + discussion.key
      });
    };

    ProposalRecordsInterface.prototype.fetchClosedByGroup = function(groupKey, options) {
      if (options == null) {
        options = {};
      }
      options['group_key'] = groupKey;
      return this.fetch({
        path: 'closed',
        params: options
      });
    };

    return ProposalRecordsInterface;

  })(BaseRecordsInterface);
});

angular.module('loomioApp').factory('RecordStore', function() {
  return AngularRecordStore.RecordStoreFn();
});

angular.module('loomioApp').factory('Records', function(RecordStore, RecordStoreDatabaseName, AttachmentRecordsInterface, CommentRecordsInterface, DiscussionRecordsInterface, EventRecordsInterface, GroupRecordsInterface, MembershipRecordsInterface, MembershipRequestRecordsInterface, NotificationRecordsInterface, ProposalRecordsInterface, UserRecordsInterface, VoteRecordsInterface, DidNotVoteRecordsInterface, SearchResultRecordsInterface, ContactRecordsInterface, InvitationRecordsInterface, InvitationFormRecordsInterface, VersionRecordsInterface, DraftRecordsInterface, TranslationRecordsInterface, OauthApplicationRecordsInterface, SessionRecordsInterface, RegistrationRecordsInterface, PollRecordsInterface, PollOptionRecordsInterface, StanceRecordsInterface, StanceChoiceRecordsInterface, OutcomeRecordsInterface, PollDidNotVoteRecordsInterface, VisitorRecordsInterface, IdentityRecordsInterface, CommunityRecordsInterface, PollCommunityRecordsInterface) {
  var db, recordStore;
  db = new loki(RecordStoreDatabaseName);
  recordStore = new RecordStore(db);
  recordStore.addRecordsInterface(AttachmentRecordsInterface);
  recordStore.addRecordsInterface(CommentRecordsInterface);
  recordStore.addRecordsInterface(DiscussionRecordsInterface);
  recordStore.addRecordsInterface(EventRecordsInterface);
  recordStore.addRecordsInterface(GroupRecordsInterface);
  recordStore.addRecordsInterface(MembershipRecordsInterface);
  recordStore.addRecordsInterface(MembershipRequestRecordsInterface);
  recordStore.addRecordsInterface(NotificationRecordsInterface);
  recordStore.addRecordsInterface(ProposalRecordsInterface);
  recordStore.addRecordsInterface(UserRecordsInterface);
  recordStore.addRecordsInterface(VoteRecordsInterface);
  recordStore.addRecordsInterface(DidNotVoteRecordsInterface);
  recordStore.addRecordsInterface(SearchResultRecordsInterface);
  recordStore.addRecordsInterface(ContactRecordsInterface);
  recordStore.addRecordsInterface(InvitationRecordsInterface);
  recordStore.addRecordsInterface(InvitationFormRecordsInterface);
  recordStore.addRecordsInterface(TranslationRecordsInterface);
  recordStore.addRecordsInterface(VersionRecordsInterface);
  recordStore.addRecordsInterface(DraftRecordsInterface);
  recordStore.addRecordsInterface(OauthApplicationRecordsInterface);
  recordStore.addRecordsInterface(SessionRecordsInterface);
  recordStore.addRecordsInterface(RegistrationRecordsInterface);
  recordStore.addRecordsInterface(PollRecordsInterface);
  recordStore.addRecordsInterface(PollOptionRecordsInterface);
  recordStore.addRecordsInterface(StanceRecordsInterface);
  recordStore.addRecordsInterface(StanceChoiceRecordsInterface);
  recordStore.addRecordsInterface(OutcomeRecordsInterface);
  recordStore.addRecordsInterface(PollDidNotVoteRecordsInterface);
  recordStore.addRecordsInterface(VisitorRecordsInterface);
  recordStore.addRecordsInterface(IdentityRecordsInterface);
  recordStore.addRecordsInterface(CommunityRecordsInterface);
  recordStore.addRecordsInterface(PollCommunityRecordsInterface);
  return recordStore;
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

angular.module('loomioApp').factory('RegistrationModel', function(BaseModel) {
  var RegistrationModel;
  return RegistrationModel = (function(superClass) {
    extend(RegistrationModel, superClass);

    function RegistrationModel() {
      return RegistrationModel.__super__.constructor.apply(this, arguments);
    }

    RegistrationModel.singular = 'registration';

    RegistrationModel.plural = 'registrations';

    RegistrationModel.serializableAttributes = ['name', 'email', 'password', 'passwordConfirmation', 'recaptcha'];

    RegistrationModel.serializationRoot = 'user';

    return RegistrationModel;

  })(BaseModel);
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

angular.module('loomioApp').factory('RegistrationRecordsInterface', function(BaseRecordsInterface, RegistrationModel) {
  var RegistrationRecordsInterface;
  return RegistrationRecordsInterface = (function(superClass) {
    extend(RegistrationRecordsInterface, superClass);

    function RegistrationRecordsInterface() {
      return RegistrationRecordsInterface.__super__.constructor.apply(this, arguments);
    }

    RegistrationRecordsInterface.prototype.model = RegistrationModel;

    return RegistrationRecordsInterface;

  })(BaseRecordsInterface);
});

angular.module('loomioApp').factory('RestfulClient', function($http, $upload) {
  return AngularRecordStore.RestfulClientFn($http, $upload);
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

angular.module('loomioApp').factory('SearchResultModel', function(BaseModel) {
  var SearchResultModel;
  return SearchResultModel = (function(superClass) {
    extend(SearchResultModel, superClass);

    function SearchResultModel() {
      return SearchResultModel.__super__.constructor.apply(this, arguments);
    }

    SearchResultModel.singular = 'searchResult';

    SearchResultModel.plural = 'searchResults';

    SearchResultModel.apiEndPoint = 'search';

    return SearchResultModel;

  })(BaseModel);
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

angular.module('loomioApp').factory('SearchResultRecordsInterface', function(BaseRecordsInterface, SearchResultModel) {
  var SearchResultRecordsInterface;
  return SearchResultRecordsInterface = (function(superClass) {
    extend(SearchResultRecordsInterface, superClass);

    function SearchResultRecordsInterface() {
      return SearchResultRecordsInterface.__super__.constructor.apply(this, arguments);
    }

    SearchResultRecordsInterface.prototype.model = SearchResultModel;

    SearchResultRecordsInterface.prototype.fetchByFragment = function(fragment) {
      return this.fetch({
        params: {
          q: fragment,
          per: 5
        }
      });
    };

    return SearchResultRecordsInterface;

  })(BaseRecordsInterface);
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

angular.module('loomioApp').factory('SessionModel', function(BaseModel) {
  var SessionModel;
  return SessionModel = (function(superClass) {
    extend(SessionModel, superClass);

    function SessionModel() {
      return SessionModel.__super__.constructor.apply(this, arguments);
    }

    SessionModel.singular = 'session';

    SessionModel.plural = 'sessions';

    SessionModel.serializableAttributes = ['type', 'email', 'password', 'rememberMe'];

    SessionModel.serializationRoot = 'user';

    return SessionModel;

  })(BaseModel);
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

angular.module('loomioApp').factory('SessionRecordsInterface', function(BaseRecordsInterface, SessionModel) {
  var SessionRecordsInterface;
  return SessionRecordsInterface = (function(superClass) {
    extend(SessionRecordsInterface, superClass);

    function SessionRecordsInterface() {
      return SessionRecordsInterface.__super__.constructor.apply(this, arguments);
    }

    SessionRecordsInterface.prototype.model = SessionModel;

    return SessionRecordsInterface;

  })(BaseRecordsInterface);
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

angular.module('loomioApp').factory('StanceChoiceModel', function(BaseModel, AppConfig) {
  var StanceChoiceModel;
  return StanceChoiceModel = (function(superClass) {
    extend(StanceChoiceModel, superClass);

    function StanceChoiceModel() {
      return StanceChoiceModel.__super__.constructor.apply(this, arguments);
    }

    StanceChoiceModel.singular = 'stanceChoice';

    StanceChoiceModel.plural = 'stanceChoices';

    StanceChoiceModel.indices = ['pollOptionId', 'stanceId'];

    StanceChoiceModel.serializableAttributes = AppConfig.permittedParams.stanceChoices;

    StanceChoiceModel.prototype.defaultValues = function() {
      return {
        score: 1
      };
    };

    StanceChoiceModel.prototype.relationships = function() {
      this.belongsTo('pollOption');
      return this.belongsTo('stance');
    };

    StanceChoiceModel.prototype.poll = function() {
      if (this.stance()) {
        return this.stance().poll();
      }
    };

    return StanceChoiceModel;

  })(BaseModel);
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

angular.module('loomioApp').factory('StanceChoiceRecordsInterface', function(BaseRecordsInterface, StanceChoiceModel) {
  var StanceChoiceRecordsInterface;
  return StanceChoiceRecordsInterface = (function(superClass) {
    extend(StanceChoiceRecordsInterface, superClass);

    function StanceChoiceRecordsInterface() {
      return StanceChoiceRecordsInterface.__super__.constructor.apply(this, arguments);
    }

    StanceChoiceRecordsInterface.prototype.model = StanceChoiceModel;

    return StanceChoiceRecordsInterface;

  })(BaseRecordsInterface);
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

angular.module('loomioApp').factory('StanceModel', function(DraftableModel, AppConfig, MentionLinkService) {
  var StanceModel;
  return StanceModel = (function(superClass) {
    extend(StanceModel, superClass);

    function StanceModel() {
      return StanceModel.__super__.constructor.apply(this, arguments);
    }

    StanceModel.singular = 'stance';

    StanceModel.plural = 'stances';

    StanceModel.indices = ['pollId', 'participantId'];

    StanceModel.serializableAttributes = AppConfig.permittedParams.stance;

    StanceModel.draftParent = 'poll';

    StanceModel.draftPayloadAttributes = ['reason'];

    StanceModel.prototype.defaultValues = function() {
      return {
        reason: '',
        visitorAttributes: {}
      };
    };

    StanceModel.prototype.relationships = function() {
      this.belongsTo('poll');
      return this.hasMany('stanceChoices');
    };

    StanceModel.prototype.participant = function() {
      return this.recordStore.users.find(this.userId) || this.recordStore.visitors.find(this.visitorId);
    };

    StanceModel.prototype.author = function() {
      return this.participant();
    };

    StanceModel.prototype.stanceChoice = function() {
      return _.first(this.stanceChoices());
    };

    StanceModel.prototype.pollOption = function() {
      if (this.stanceChoice()) {
        return this.stanceChoice().pollOption();
      }
    };

    StanceModel.prototype.pollOptionId = function() {
      return (this.pollOption() || {}).id;
    };

    StanceModel.prototype.pollOptions = function() {
      return this.recordStore.pollOptions.find(this.pollOptionIds());
    };

    StanceModel.prototype.stanceChoiceNames = function() {
      return _.pluck(this.pollOptions(), 'name');
    };

    StanceModel.prototype.pollOptionIds = function() {
      return _.pluck(this.stanceChoices(), 'pollOptionId');
    };

    StanceModel.prototype.choose = function(optionIds) {
      _.each(this.recordStore.stanceChoices.find({
        stanceId: this.id
      }), function(stanceChoice) {
        return stanceChoice.remove();
      });
      _.each(_.flatten([optionIds]), (function(_this) {
        return function(optionId) {
          return _this.recordStore.stanceChoices.create({
            pollOptionId: parseInt(optionId),
            stanceId: _this.id
          });
        };
      })(this));
      return this;
    };

    StanceModel.prototype.votedFor = function(option) {
      return _.contains(_.pluck(this.pollOptions(), 'id'), option.id);
    };

    return StanceModel;

  })(DraftableModel);
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

angular.module('loomioApp').factory('StanceRecordsInterface', function(AppConfig, BaseRecordsInterface, StanceModel) {
  var StanceRecordsInterface;
  return StanceRecordsInterface = (function(superClass) {
    extend(StanceRecordsInterface, superClass);

    function StanceRecordsInterface() {
      return StanceRecordsInterface.__super__.constructor.apply(this, arguments);
    }

    StanceRecordsInterface.prototype.model = StanceModel;

    StanceRecordsInterface.prototype.fetchMyStances = function(groupKey, options) {
      if (options == null) {
        options = {};
      }
      options['group_id'] = groupKey;
      return this.fetch({
        path: 'my_stances',
        params: options
      });
    };

    StanceRecordsInterface.prototype.fetchMyStancesByDiscussion = function(discussionKey, options) {
      if (options == null) {
        options = {};
      }
      options['discussion_id'] = discussionKey;
      return this.fetch({
        path: 'my_stances',
        params: options
      });
    };

    return StanceRecordsInterface;

  })(BaseRecordsInterface);
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

angular.module('loomioApp').factory('TranslationModel', function(BaseModel) {
  var TranslationModel;
  return TranslationModel = (function(superClass) {
    extend(TranslationModel, superClass);

    function TranslationModel() {
      return TranslationModel.__super__.constructor.apply(this, arguments);
    }

    TranslationModel.singular = 'translation';

    TranslationModel.plural = 'translations';

    TranslationModel.indices = ['id'];

    return TranslationModel;

  })(BaseModel);
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

angular.module('loomioApp').factory('TranslationRecordsInterface', function(BaseRecordsInterface, TranslationModel) {
  var TranslationRecordsInterface;
  return TranslationRecordsInterface = (function(superClass) {
    extend(TranslationRecordsInterface, superClass);

    function TranslationRecordsInterface() {
      return TranslationRecordsInterface.__super__.constructor.apply(this, arguments);
    }

    TranslationRecordsInterface.prototype.model = TranslationModel;

    TranslationRecordsInterface.prototype.fetchTranslation = function(translatable, language) {
      return this.fetch({
        path: 'inline',
        params: {
          model: translatable.constructor.singular,
          id: translatable.id,
          to: language
        }
      });
    };

    return TranslationRecordsInterface;

  })(BaseRecordsInterface);
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

angular.module('loomioApp').factory('UserModel', function(BaseModel, AppConfig) {
  var UserModel;
  return UserModel = (function(superClass) {
    extend(UserModel, superClass);

    function UserModel() {
      return UserModel.__super__.constructor.apply(this, arguments);
    }

    UserModel.singular = 'user';

    UserModel.plural = 'users';

    UserModel.apiEndPoint = 'profile';

    UserModel.serializableAttributes = AppConfig.permittedParams.user;

    UserModel.prototype.relationships = function() {
      this.hasMany('memberships');
      this.hasMany('notifications');
      this.hasMany('contacts');
      this.hasMany('versions');
      this.hasMany('identities');
      return this.hasMany('communities');
    };

    UserModel.prototype.identityFor = function(type) {
      return _.detect(this.identities(), function(i) {
        return i.identityType === type;
      });
    };

    UserModel.prototype.membershipFor = function(group) {
      return _.first(this.recordStore.memberships.find({
        groupId: group.id,
        userId: this.id
      }));
    };

    UserModel.prototype.adminMemberships = function() {
      return _.filter(this.memberships(), function(m) {
        return m.admin;
      });
    };

    UserModel.prototype.groupIds = function() {
      return _.map(this.memberships(), 'groupId');
    };

    UserModel.prototype.groups = function() {
      var groups;
      groups = _.filter(this.recordStore.groups.find({
        id: {
          $in: this.groupIds()
        }
      }), function(group) {
        return !group.isArchived();
      });
      return _.sortBy(groups, 'fullName');
    };

    UserModel.prototype.adminGroups = function() {
      return _.invoke(this.adminMemberships(), 'group');
    };

    UserModel.prototype.adminGroupIds = function() {
      return _.invoke(this.adminMemberships(), 'groupId');
    };

    UserModel.prototype.parentGroups = function() {
      return _.filter(this.groups(), function(group) {
        return group.isParent();
      });
    };

    UserModel.prototype.hasAnyGroups = function() {
      return this.groups().length > 0;
    };

    UserModel.prototype.hasMultipleGroups = function() {
      return this.groups().length > 1;
    };

    UserModel.prototype.allThreads = function() {
      return _.flatten(_.map(this.groups(), function(group) {
        return group.discussions();
      }));
    };

    UserModel.prototype.orphanSubgroups = function() {
      return _.filter(this.groups(), (function(_this) {
        return function(group) {
          return group.isSubgroup() && !_this.isMemberOf(group.parent());
        };
      })(this));
    };

    UserModel.prototype.orphanParents = function() {
      return _.uniq(_.map(this.orphanSubgroups(), (function(_this) {
        return function(group) {
          return group.parent();
        };
      })(this)));
    };

    UserModel.prototype.isAuthorOf = function(object) {
      return this.id === object.authorId;
    };

    UserModel.prototype.isAdminOf = function(group) {
      return _.contains(group.adminIds(), this.id);
    };

    UserModel.prototype.isMemberOf = function(group) {
      return _.contains(group.memberIds(), this.id);
    };

    UserModel.prototype.firstName = function() {
      if (this.name) {
        return _.first(this.name.split(' '));
      }
    };

    UserModel.prototype.lastName = function() {
      return this.name.split(' ').slice(1).join(' ');
    };

    UserModel.prototype.saveVolume = function(volume, applyToAll) {
      return this.remote.post('set_volume', {
        volume: volume,
        apply_to_all: applyToAll,
        unsubscribe_token: this.unsubscribeToken
      }).then((function(_this) {
        return function() {
          if (!applyToAll) {
            return;
          }
          _.each(_this.allThreads(), function(thread) {
            return thread.update({
              discussionReaderVolume: null
            });
          });
          return _.each(_this.memberships(), function(membership) {
            return membership.update({
              volume: volume
            });
          });
        };
      })(this));
    };

    UserModel.prototype.hasExperienced = function(key, group) {
      if (group && this.isMemberOf(group)) {
        return this.membershipFor(group).experiences[key];
      } else {
        return this.experiences[key];
      }
    };

    UserModel.prototype.hasProfilePhoto = function() {
      return this.avatarKind !== 'initials';
    };

    UserModel.prototype.belongsToPayingGroup = function() {
      return _.any(this.groups(), function(group) {
        return group.subscriptionKind === 'paid';
      });
    };

    return UserModel;

  })(BaseModel);
});

var bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
  extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

angular.module('loomioApp').factory('UserRecordsInterface', function(BaseRecordsInterface, UserModel, RestfulClient) {
  var UserRecordsInterface;
  return UserRecordsInterface = (function(superClass) {
    extend(UserRecordsInterface, superClass);

    function UserRecordsInterface() {
      this.saveExperience = bind(this.saveExperience, this);
      this.deactivate = bind(this.deactivate, this);
      this.changePassword = bind(this.changePassword, this);
      this.uploadAvatar = bind(this.uploadAvatar, this);
      this.updateProfile = bind(this.updateProfile, this);
      return UserRecordsInterface.__super__.constructor.apply(this, arguments);
    }

    UserRecordsInterface.prototype.model = UserModel;

    UserRecordsInterface.prototype.updateProfile = function(user) {
      return this.remote.post('update_profile', _.merge(user.serialize(), {
        unsubscribe_token: user.unsubscribeToken
      }));
    };

    UserRecordsInterface.prototype.uploadAvatar = function(file) {
      return this.remote.upload('upload_avatar', file);
    };

    UserRecordsInterface.prototype.changePassword = function(user) {
      return this.remote.post('change_password', user.serialize());
    };

    UserRecordsInterface.prototype.deactivate = function(user) {
      return this.remote.post('deactivate', user.serialize());
    };

    UserRecordsInterface.prototype.saveExperience = function(experience) {
      return this.remote.post('save_experience', {
        experience: experience
      });
    };

    UserRecordsInterface.prototype.emailStatus = function(email) {
      return this.fetch({
        path: 'email_status',
        params: {
          email: email
        }
      });
    };

    return UserRecordsInterface;

  })(BaseRecordsInterface);
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

angular.module('loomioApp').factory('VersionModel', function(BaseModel) {
  var VersionModel;
  return VersionModel = (function(superClass) {
    extend(VersionModel, superClass);

    function VersionModel() {
      return VersionModel.__super__.constructor.apply(this, arguments);
    }

    VersionModel.singular = 'version';

    VersionModel.plural = 'versions';

    VersionModel.indices = ['discussionId'];

    VersionModel.prototype.relationships = function() {
      this.belongsTo('discussion');
      this.belongsTo('comment');
      this.belongsTo('proposal');
      this.belongsTo('poll');
      return this.belongsTo('author', {
        from: 'users',
        by: 'whodunnit'
      });
    };

    VersionModel.prototype.editedAttributeNames = function() {
      return _.filter(_.keys(this.changes).sort(), function(key) {
        return _.include(['title', 'name', 'description', 'closing_at', 'private', 'attachment_ids'], key);
      });
    };

    VersionModel.prototype.attributeEdited = function(name) {
      return _.include(_.keys(this.changes), name);
    };

    VersionModel.prototype.model = function() {
      return this.discussion() || this.comment();
    };

    VersionModel.prototype.isCurrent = function() {
      return this.id === _.last(this.model().versions())['id'];
    };

    VersionModel.prototype.isOriginal = function() {
      return this.id === _.first(this.model().versions())['id'];
    };

    VersionModel.prototype.authorOrEditorName = function() {
      if (this.isOriginal()) {
        return this.model().authorName();
      } else {
        return this.author().name;
      }
    };

    return VersionModel;

  })(BaseModel);
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

angular.module('loomioApp').factory('VersionRecordsInterface', function(BaseRecordsInterface, VersionModel) {
  var VersionRecordsInterface;
  return VersionRecordsInterface = (function(superClass) {
    extend(VersionRecordsInterface, superClass);

    function VersionRecordsInterface() {
      return VersionRecordsInterface.__super__.constructor.apply(this, arguments);
    }

    VersionRecordsInterface.prototype.model = VersionModel;

    VersionRecordsInterface.prototype.fetchByDiscussion = function(discussionKey, options) {
      if (options == null) {
        options = {};
      }
      return this.fetch({
        params: {
          model: 'discussion',
          discussion_id: discussionKey
        }
      });
    };

    VersionRecordsInterface.prototype.fetchByComment = function(commentId, options) {
      if (options == null) {
        options = {};
      }
      return this.fetch({
        params: {
          model: 'comment',
          comment_id: commentId
        }
      });
    };

    return VersionRecordsInterface;

  })(BaseRecordsInterface);
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

angular.module('loomioApp').factory('VisitorModel', function(AppConfig, BaseModel) {
  var VisitorModel;
  return VisitorModel = (function(superClass) {
    extend(VisitorModel, superClass);

    function VisitorModel() {
      return VisitorModel.__super__.constructor.apply(this, arguments);
    }

    VisitorModel.singular = 'visitor';

    VisitorModel.plural = 'visitors';

    VisitorModel.serializableAttributes = AppConfig.permittedParams.visitor;

    VisitorModel.prototype.relationships = function() {
      return this.belongsTo('poll');
    };

    VisitorModel.prototype.invite = function(poll) {
      this.processing = true;
      return this.remote.post('', _.merge(this.serialize(), {
        poll_id: poll.id
      }))["finally"]((function(_this) {
        return function() {
          return _this.processing = false;
        };
      })(this));
    };

    VisitorModel.prototype.remind = function(poll) {
      this.processing = true;
      return this.remote.postMember(this.id, 'remind', {
        poll_id: poll.id
      })["finally"]((function(_this) {
        return function() {
          return _this.processing = false;
        };
      })(this));
    };

    return VisitorModel;

  })(BaseModel);
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

angular.module('loomioApp').factory('VisitorRecordsInterface', function(BaseRecordsInterface, VisitorModel) {
  var VisitorRecordsInterface;
  return VisitorRecordsInterface = (function(superClass) {
    extend(VisitorRecordsInterface, superClass);

    function VisitorRecordsInterface() {
      return VisitorRecordsInterface.__super__.constructor.apply(this, arguments);
    }

    VisitorRecordsInterface.prototype.model = VisitorModel;

    return VisitorRecordsInterface;

  })(BaseRecordsInterface);
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

angular.module('loomioApp').factory('VoteModel', function(BaseModel, AppConfig) {
  var VoteModel;
  return VoteModel = (function(superClass) {
    extend(VoteModel, superClass);

    function VoteModel() {
      return VoteModel.__super__.constructor.apply(this, arguments);
    }

    VoteModel.singular = 'vote';

    VoteModel.plural = 'votes';

    VoteModel.indices = ['id', 'proposalId'];

    VoteModel.serializableAttributes = AppConfig.permittedParams.vote;

    VoteModel.prototype.defaultValues = function() {
      return {
        statement: ''
      };
    };

    VoteModel.prototype.relationships = function() {
      this.belongsTo('author', {
        from: 'users'
      });
      return this.belongsTo('proposal');
    };

    VoteModel.prototype.group = function() {
      return this.proposal().group();
    };

    VoteModel.prototype.authorName = function() {
      return this.author().name;
    };

    VoteModel.prototype.positionVerb = function() {
      switch (this.position) {
        case 'yes':
          return 'agree';
        case 'no':
          return 'disagree';
        default:
          return this.position;
      }
    };

    VoteModel.prototype.hasStatement = function() {
      return (this.statement != null) && this.statement.toString().length > 0;
    };

    VoteModel.prototype.anyPosition = function() {
      return this.position;
    };

    VoteModel.prototype.isAgree = function() {
      return this.position === 'yes';
    };

    VoteModel.prototype.isDisagree = function() {
      return this.position === 'no';
    };

    VoteModel.prototype.isAbstain = function() {
      return this.position === 'abstain';
    };

    VoteModel.prototype.isBlock = function() {
      return this.position === 'block';
    };

    VoteModel.prototype.charsLeft = function() {
      return 250 - (this.statement || '').toString().length;
    };

    VoteModel.prototype.overCharLimit = function() {
      return this.charsLeft() < 0;
    };

    return VoteModel;

  })(BaseModel);
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

angular.module('loomioApp').factory('VoteRecordsInterface', function(BaseRecordsInterface, VoteModel) {
  var VoteRecordsInterface;
  return VoteRecordsInterface = (function(superClass) {
    extend(VoteRecordsInterface, superClass);

    function VoteRecordsInterface() {
      return VoteRecordsInterface.__super__.constructor.apply(this, arguments);
    }

    VoteRecordsInterface.prototype.model = VoteModel;

    VoteRecordsInterface.prototype.fetchMyVotes = function(model) {
      var params;
      params = {};
      params[model.constructor.singular + "_id"] = model.id;
      return this.fetch({
        path: 'my_votes',
        params: params
      });
    };

    VoteRecordsInterface.prototype.fetchByProposal = function(proposal) {
      return this.fetch({
        params: {
          motion_id: proposal.id
        }
      });
    };

    return VoteRecordsInterface;

  })(BaseRecordsInterface);
});

var bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

angular.module('loomioApp').factory('AbilityService', function(AppConfig, Session) {
  var AbilityService;
  return new (AbilityService = (function() {
    function AbilityService() {
      this.isLoggedIn = bind(this.isLoggedIn, this);
    }

    AbilityService.prototype.isLoggedIn = function() {
      return this.isUser() && (Session.user().restricted == null);
    };

    AbilityService.prototype.isVisitor = function() {
      return AppConfig.currentVisitorId != null;
    };

    AbilityService.prototype.isUser = function() {
      return AppConfig.currentUserId != null;
    };

    AbilityService.prototype.canAddComment = function(thread) {
      return Session.user().isMemberOf(thread.group());
    };

    AbilityService.prototype.canRespondToComment = function(comment) {
      return Session.user().isMemberOf(comment.group());
    };

    AbilityService.prototype.canStartProposal = function(thread) {
      return thread && !thread.hasActiveProposal() && (this.canAdministerGroup(thread.group()) || (Session.user().isMemberOf(thread.group()) && thread.group().membersCanRaiseMotions));
    };

    AbilityService.prototype.canStartPoll = function(group) {
      return group && (this.canAdministerGroup(group) || Session.user().isMemberOf(group) && group.membersCanRaiseMotions);
    };

    AbilityService.prototype.canEditThread = function(thread) {
      return this.canAdministerGroup(thread.group()) || Session.user().isMemberOf(thread.group()) && (Session.user().isAuthorOf(thread) || thread.group().membersCanEditDiscussions);
    };

    AbilityService.prototype.canMoveThread = function(thread) {
      return this.canAdministerGroup(thread.group()) || Session.user().isAuthorOf(thread);
    };

    AbilityService.prototype.canDeleteThread = function(thread) {
      return this.canAdministerGroup(thread.group()) || Session.user().isAuthorOf(thread);
    };

    AbilityService.prototype.canChangeThreadVolume = function(thread) {
      return Session.user().isMemberOf(thread.group());
    };

    AbilityService.prototype.canChangeGroupVolume = function(group) {
      return Session.user().isMemberOf(group);
    };

    AbilityService.prototype.canVoteOn = function(proposal) {
      return proposal.isActive() && Session.user().isMemberOf(proposal.group()) && (this.canAdministerGroup(proposal.group()) || proposal.group().membersCanVote);
    };

    AbilityService.prototype.canCloseOrExtendProposal = function(proposal) {
      return proposal.isActive() && (this.canAdministerGroup(proposal.group()) || Session.user().isAuthorOf(proposal));
    };

    AbilityService.prototype.canEditProposal = function(proposal) {
      return proposal.isActive() && proposal.canBeEdited() && (this.canAdministerGroup(proposal.group()) || (Session.user().isMemberOf(proposal.group()) && Session.user().isAuthorOf(proposal)));
    };

    AbilityService.prototype.canCreateOutcomeFor = function(proposal) {
      return this.canSetOutcomeFor(proposal) && !proposal.hasOutcome();
    };

    AbilityService.prototype.canUpdateOutcomeFor = function(proposal) {
      return this.canSetOutcomeFor(proposal) && proposal.hasOutcome();
    };

    AbilityService.prototype.canSetOutcomeFor = function(proposal) {
      return (proposal != null) && proposal.isClosed() && (Session.user().isAuthorOf(proposal) || this.canAdministerGroup(proposal.group()));
    };

    AbilityService.prototype.canAdministerGroup = function(group) {
      return Session.user().isAdminOf(group);
    };

    AbilityService.prototype.canManageGroupSubscription = function(group) {
      return this.canAdministerGroup(group) && (group.subscriptionKind != null) && group.subscriptionKind !== 'trial' && group.subscriptionPaymentMethod !== 'manual';
    };

    AbilityService.prototype.isCreatorOf = function(group) {
      return Session.user().id === group.creatorId;
    };

    AbilityService.prototype.canStartThread = function(group) {
      return this.canAdministerGroup(group) || (Session.user().isMemberOf(group) && group.membersCanStartDiscussions);
    };

    AbilityService.prototype.canAddMembers = function(group) {
      return this.canAdministerGroup(group) || (Session.user().isMemberOf(group) && group.membersCanAddMembers);
    };

    AbilityService.prototype.canCreateSubgroups = function(group) {
      return group.isParent() && (this.canAdministerGroup(group) || (Session.user().isMemberOf(group) && group.membersCanCreateSubgroups));
    };

    AbilityService.prototype.canEditGroup = function(group) {
      return this.canAdministerGroup(group);
    };

    AbilityService.prototype.canArchiveGroup = function(group) {
      return this.canAdministerGroup(group);
    };

    AbilityService.prototype.canEditComment = function(comment) {
      return Session.user().isMemberOf(comment.group()) && Session.user().isAuthorOf(comment) && (comment.isMostRecent() || comment.group().membersCanEditComments);
    };

    AbilityService.prototype.canDeleteComment = function(comment) {
      return (Session.user().isMemberOf(comment.group()) && Session.user().isAuthorOf(comment)) || this.canAdministerGroup(comment.group());
    };

    AbilityService.prototype.canRemoveMembership = function(membership) {
      return membership.group().memberIds().length > 1 && (!membership.admin || membership.group().adminIds().length > 1) && (membership.user() === Session.user() || this.canAdministerGroup(membership.group()));
    };

    AbilityService.prototype.canDeactivateUser = function() {
      return _.all(Session.user().memberships(), function(membership) {
        return !membership.admin || membership.group().hasMultipleAdmins;
      });
    };

    AbilityService.prototype.canManageMembershipRequests = function(group) {
      return (group.membersCanAddMembers && Session.user().isMemberOf(group)) || this.canAdministerGroup(group);
    };

    AbilityService.prototype.canViewGroup = function(group) {
      return !group.privacyIsSecret() || Session.user().isMemberOf(group);
    };

    AbilityService.prototype.canViewPrivateContent = function(group) {
      return Session.user().isMemberOf(group);
    };

    AbilityService.prototype.canCreateContentFor = function(group) {
      return Session.user().isMemberOf(group);
    };

    AbilityService.prototype.canViewMemberships = function(group) {
      return Session.user().isMemberOf(group);
    };

    AbilityService.prototype.canViewPreviousProposals = function(group) {
      return this.canViewGroup(group);
    };

    AbilityService.prototype.canViewPreviousPolls = function(group) {
      return this.canViewGroup(group);
    };

    AbilityService.prototype.canJoinGroup = function(group) {
      return (group.membershipGrantedUpon === 'request') && this.canViewGroup(group) && !Session.user().isMemberOf(group);
    };

    AbilityService.prototype.canRequestMembership = function(group) {
      return (group.membershipGrantedUpon === 'approval') && this.canViewGroup(group) && !Session.user().isMemberOf(group);
    };

    AbilityService.prototype.canTranslate = function(model) {
      return (AppConfig.inlineTranslation.isAvailable != null) && _.contains(AppConfig.inlineTranslation.supportedLangs, Session.user().locale) && Session.user().locale !== model.author().locale;
    };

    AbilityService.prototype.canSubscribeToPoll = function(poll) {
      if (poll.group()) {
        return this.canViewGroup(poll.group());
      } else {
        return this.canAdministerPoll() || _.contains(this.poll().voters(), Session.user());
      }
    };

    AbilityService.prototype.canSharePoll = function(poll) {
      return this.canEditPoll(poll);
    };

    AbilityService.prototype.canEditPoll = function(poll) {
      return poll.isActive() && this.canAdministerPoll(poll);
    };

    AbilityService.prototype.canDeletePoll = function(poll) {
      return this.canAdministerPoll(poll);
    };

    AbilityService.prototype.canSetPollOutcome = function(poll) {
      return poll.isClosed() && this.canAdministerPoll(poll);
    };

    AbilityService.prototype.canAdministerPoll = function(poll) {
      if (poll.group()) {
        return this.canAdministerGroup(poll.group()) || (Session.user().isMemberOf(poll.group()) && Session.user().isAuthorOf(poll));
      } else {
        return Session.user().isAuthorOf(poll);
      }
    };

    AbilityService.prototype.canClosePoll = function(poll) {
      return this.canEditPoll(poll);
    };

    AbilityService.prototype.requireLoginFor = function(page) {
      if (this.isLoggedIn()) {
        return false;
      }
      switch (page) {
        case 'emailSettingsPage':
          return Session.user().restricted == null;
        case 'groupsPage':
        case 'dashboardPage':
        case 'inboxPage':
        case 'profilePage':
        case 'authorizedAppsPage':
        case 'registeredAppsPage':
        case 'registeredAppPage':
        case 'pollsPage':
        case 'startPollPage':
        case 'startGroupPage':
          return true;
        default:
          return false;
      }
    };

    return AbilityService;

  })());
});

angular.module('loomioApp').factory('AhoyService', function($rootScope, $window) {
  var AhoyService;
  return new (AhoyService = (function() {
    function AhoyService() {}

    AhoyService.prototype.init = function() {
      if (typeof ahoy === "undefined" || ahoy === null) {
        return;
      }
      ahoy.trackClicks();
      ahoy.trackSubmits();
      ahoy.trackChanges();
      $rootScope.$on('currentComponent', (function(_this) {
        return function() {
          return _this.track('$view', {
            page: $window.location.pathname,
            url: $window.location.href,
            title: document.title
          });
        };
      })(this));
      return $rootScope.$on('modalOpened', (function(_this) {
        return function(_, modal) {
          return _this.track('modalOpened', {
            name: modal.templateUrl.match(/(\w+)\.html$/)[1]
          });
        };
      })(this));
    };

    AhoyService.prototype.track = function(event, options) {
      if (typeof ahoy !== "undefined" && ahoy !== null) {
        return ahoy.track(event, options);
      }
    };

    return AhoyService;

  })());
});

angular.module('loomioApp').factory('AppConfig', function() {
  var configData;
  configData = (typeof window !== "undefined" && window !== null) && (window.Loomio != null) ? window.Loomio : {
    bootData: {},
    permittedParams: {}
  };
  configData.pluginConfig = function(name) {
    return _.find(configData.plugins.installed, function(p) {
      return p.name === name;
    });
  };
  configData.timeZone = moment.tz.guess();
  return configData;
});

angular.module('loomioApp').factory('AttachmentService', function(Records) {
  var AttachmentService;
  return new (AttachmentService = (function() {
    function AttachmentService() {}

    AttachmentService.prototype.listenForAttachments = function(scope, model) {
      scope.$on('disableAttachmentForm', function() {
        return scope.submitIsDisabled = true;
      });
      scope.$on('enableAttachmentForm', function() {
        return scope.submitIsDisabled = false;
      });
      return scope.$on('attachmentRemoved', function(event, attachment) {
        var ids;
        ids = model.newAttachmentIds;
        ids.splice(ids.indexOf(attachment.id), 1);
        if (!_.contains(model.attachmentIds, attachment.id)) {
          return attachment.destroy();
        }
      });
    };

    AttachmentService.prototype.listenForPaste = function(scope) {
      return scope.handlePaste = function(event) {
        var data, file, item;
        data = event.clipboardData;
        if (!(item = _.first(_.filter(data.items, function(item) {
          return item.getAsFile();
        })))) {
          return;
        }
        event.preventDefault();
        file = new File([item.getAsFile()], data.getData('text/plain') || Date.now(), {
          lastModified: moment(),
          type: item.type
        });
        return scope.$broadcast('attachmentPasted', file);
      };
    };

    AttachmentService.prototype.cleanupAfterUpdate = function(model, singular) {
      return Records.attachments.find({
        attachableId: model.id,
        attachableType: _.capitalize(singular)
      }).filter(function(attachment) {
        return !_.contains(model.attachment_ids, attachment.id);
      }).map(function(attachment) {
        return attachment.remove();
      });
    };

    return AttachmentService;

  })());
});

angular.module('loomioApp').factory('AuthService', function($window, Records, RestfulClient) {
  var AuthService;
  return new (AuthService = (function() {
    function AuthService() {}

    AuthService.prototype.emailStatus = function(user) {
      return Records.users.emailStatus(user.email).then((function(_this) {
        return function(data) {
          return _this.applyEmailStatus(user, _.first(data.users));
        };
      })(this));
    };

    AuthService.prototype.applyEmailStatus = function(user, data) {
      var keys;
      keys = ['name', 'email', 'avatar_kind', 'avatar_initials', 'gravatar_md5', 'avatar_url', 'has_password', 'email_status'];
      user.update(_.mapKeys(_.pick(data, keys), function(v, k) {
        return _.camelCase(k);
      }));
      return user;
    };

    AuthService.prototype.signIn = function(user) {
      return Records.sessions.build({
        email: user.email,
        password: user.password
      }).save().then(function() {
        return $window.location.reload();
      });
    };

    AuthService.prototype.signUp = function(user) {
      return Records.registrations.build({
        email: user.email,
        name: user.name,
        recaptcha: user.recaptcha
      }).save().then(function() {
        return user.sentLoginLink = true;
      });
    };

    AuthService.prototype.confirmOauth = function() {
      return Records.registrations.remote.post('oauth').then(function() {
        return $window.location.reload();
      });
    };

    AuthService.prototype.sendLoginLink = function(user) {
      return new RestfulClient('login_tokens').post('', {
        email: user.email
      }).then(function() {
        return user.sentLoginLink = true;
      });
    };

    AuthService.prototype.forgotPassword = function(user) {
      return Records.users.remote.post('set_password', {
        email: user.email
      }).then(function() {
        return user.sentPasswordLink = true;
      });
    };

    return AuthService;

  })());
});

angular.module('loomioApp').factory('CommunityService', function($location, $window, Records, Session, FormService, ModalService, PollCommonPublishModal, PollCommonShareModal) {
  var CommunityService;
  return new (CommunityService = (function() {
    var identityIdFor;

    function CommunityService() {}

    CommunityService.prototype.buildCommunity = function(poll, type) {
      var identityId;
      if (identityId = identityIdFor(type)) {
        return Records.communities.build({
          pollId: poll.id,
          communityType: type,
          identityId: identityId
        });
      } else {
        return this.fetchAccessToken(type);
      }
    };

    identityIdFor = function(type) {
      return (Session.user().identityFor(type) || {}).id;
    };

    CommunityService.prototype.fetchAccessToken = function(type) {
      delete $location.search().share;
      $location.search('add_community', type);
      $window.location = type + "/oauth?oauth_type=community";
      return false;
    };

    CommunityService.prototype.alreadyOnPoll = function(poll, obj, communityType) {
      return _.find(poll.communities(), function(community) {
        return !community.revoked && community.communityType === communityType && community.identifier === obj.id;
      });
    };

    CommunityService.prototype.submitCommunity = function(scope, model, options) {
      if (options == null) {
        options = {};
      }
      return FormService.submit(scope, model, _.merge({
        flashSuccess: "add_community_form.community_created",
        flashOptions: {
          type: model.communityType
        },
        successCallback: function(response) {
          delete $location.search().add_community;
          $location.search('share', true);
          return ModalService.open(PollCommonPublishModal, {
            poll: function() {
              return model.poll();
            },
            community: function() {
              return Records.communities.find(response.communities[0].id);
            },
            back: function() {
              return function() {
                return ModalService.open(PollCommonShareModal, {
                  poll: function() {
                    return model.poll();
                  }
                });
              };
            }
          });
        }
      }, options));
    };

    return CommunityService;

  })());
});

angular.module('loomioApp').factory('DraftService', function($timeout, AppConfig) {
  var DraftService;
  return new (DraftService = (function() {
    function DraftService() {}

    DraftService.prototype.applyDrafting = function(scope, model) {
      var draftMode, timeout;
      draftMode = function() {
        return model[model.constructor.draftParent]() && model.isNew();
      };
      timeout = $timeout(function() {});
      scope.$watch(function() {
        return _.pick(model, model.constructor.draftPayloadAttributes);
      }, function() {
        if (!draftMode()) {
          return;
        }
        $timeout.cancel(timeout);
        return timeout = $timeout((function() {
          return model.updateDraft();
        }), AppConfig.drafts.debounce);
      }, true);
      scope.restoreDraft = function() {
        if (draftMode()) {
          return model.restoreDraft();
        }
      };
      scope.restoreRemoteDraft = function() {
        if (draftMode()) {
          return model.fetchDraft().then(scope.restoreDraft);
        }
      };
      return scope.restoreRemoteDraft();
    };

    return DraftService;

  })());
});

angular.module('loomioApp').factory('EmojiService', function($timeout) {
  var EmojiService;
  return new (EmojiService = (function() {
    function EmojiService() {}

    EmojiService.prototype.source = window.Loomio.emojis.source;

    EmojiService.prototype.render = window.Loomio.emojis.render;

    EmojiService.prototype.defaults = window.Loomio.emojis.defaults;

    EmojiService.prototype.listen = function(scope, model, field, target) {
      return scope.$on('emojiSelected', function(event, emoji, selector) {
        var caretPosition, elem;
        if (selector !== target) {
          return;
        }
        elem = document.querySelector(selector);
        caretPosition = elem.selectionEnd;
        model[field] = (model[field].substring(0, elem.selectionEnd)) + " " + emoji + " " + (model[field].substring(elem.selectionEnd));
        return $timeout(function() {
          elem.selectionEnd = elem.selectionStart = caretPosition + emoji.length + 2;
          return elem.focus();
        });
      });
    };

    return EmojiService;

  })());
});



angular.module('loomioApp').directive('focusOn', function() {
  return function(scope, elem, attr) {
    console.log("elem");
    return scope.$on(attr.focusOn, function(e) {
      console.log("helllooo", e);
      return elem[0].focus();
    });
  };
});

angular.module('loomioApp').factory('FormService', function($rootScope, FlashService, DraftService, AbilityService, AttachmentService, $filter) {
  var FormService;
  return new (FormService = (function() {
    var calculateFlashOptions, cleanup, errorTypes, failure, prepare, success;

    function FormService() {}

    FormService.prototype.confirmDiscardChanges = function(event, record) {
      if (record.isModified() && !confirm($filter('translate')('common.confirm_discard_changes'))) {
        return event.preventDefault();
      }
    };

    errorTypes = {
      400: 'badRequest',
      401: 'unauthorizedRequest',
      403: 'forbiddenRequest',
      404: 'resourceNotFound',
      422: 'unprocessableEntity',
      500: 'internalServerError'
    };

    prepare = function(scope, model, options, prepareArgs) {
      FlashService.loading(options.loadingMessage);
      if (typeof options.prepareFn === 'function') {
        options.prepareFn(prepareArgs);
      }
      scope.isDisabled = true;
      return model.setErrors();
    };

    success = function(scope, model, options) {
      return function(response) {
        var flashKey;
        FlashService.dismiss();
        if (options.drafts && AbilityService.isLoggedIn()) {
          model.resetDraft();
        }
        if (options.flashSuccess != null) {
          flashKey = typeof options.flashSuccess === 'function' ? options.flashSuccess() : options.flashSuccess;
          FlashService.success(flashKey, calculateFlashOptions(options.flashOptions));
        }
        if ((options.skipClose == null) && typeof scope.$close === 'function') {
          scope.$close();
        }
        if (typeof options.successCallback === 'function') {
          options.successCallback(response);
        }
        if (options.successEvent) {
          return $rootScope.$broadcast(options.successEvent);
        }
      };
    };

    failure = function(scope, model, options) {
      return function(response) {
        FlashService.dismiss();
        if (typeof options.failureCallback === 'function') {
          options.failureCallback(response);
        }
        if (_.contains([401, 422], response.status)) {
          model.setErrors(response.data.errors);
        }
        return $rootScope.$broadcast(errorTypes[response.status] || 'unknownError', {
          model: model,
          response: response
        });
      };
    };

    cleanup = function(scope, model, options) {
      if (options == null) {
        options = {};
      }
      return function() {
        if (typeof options.cleanupFn === 'function') {
          options.cleanupFn(scope, model);
        }
        return scope.isDisabled = false;
      };
    };

    FormService.prototype.submit = function(scope, model, options) {
      var submitFn;
      if (options == null) {
        options = {};
      }
      if (options.drafts && AbilityService.isLoggedIn()) {
        DraftService.applyDrafting(scope, model);
      }
      submitFn = options.submitFn || model.save;
      return function(prepareArgs) {
        if (scope.isDisabled) {
          return;
        }
        prepare(scope, model, options, prepareArgs);
        return submitFn(model).then(success(scope, model, options), failure(scope, model, options))["finally"](cleanup(scope, model, options));
      };
    };

    FormService.prototype.upload = function(scope, model, options) {
      var submitFn;
      if (options == null) {
        options = {};
      }
      submitFn = options.submitFn;
      return function(files) {
        if (_.any(files)) {
          prepare(scope, model, options);
          return submitFn(files[0], options.uploadKind).then(success(scope, model, options), failure(scope, model, options))["finally"](cleanup(scope, model, options));
        }
      };
    };

    calculateFlashOptions = function(options) {
      _.each(_.keys(options), function(key) {
        if (typeof options[key] === 'function') {
          return options[key] = options[key]();
        }
      });
      return options;
    };

    return FormService;

  })());
});

angular.module('loomioApp').factory('HotkeyService', function(AppConfig, ModalService, KeyEventService, Records, Session, InvitationModal, GroupModal, DiscussionForm, PollCommonStartModal) {
  var HotkeyService;
  return new (HotkeyService = (function() {
    function HotkeyService() {}

    HotkeyService.prototype.keyboardShortcuts = {
      pressedI: {
        execute: function() {
          return ModalService.open(InvitationModal, {
            group: function() {
              return Session.currentGroup || Records.groups.build();
            }
          });
        }
      },
      pressedG: {
        execute: function() {
          return ModalService.open(GroupModal, {
            group: function() {
              return Records.groups.build();
            }
          });
        }
      },
      pressedT: {
        execute: function() {
          return ModalService.open(DiscussionForm, {
            discussion: function() {
              return Records.discussions.build({
                groupId: (Session.currentGroup || {}).id
              });
            }
          });
        }
      },
      pressedP: {
        execute: function() {
          return ModalService.open(PollCommonStartModal, {
            poll: function() {
              return Records.polls.build({
                authorId: Session.user().id
              });
            }
          });
        }
      }
    };

    HotkeyService.prototype.init = function(scope) {
      return _.each(this.keyboardShortcuts, (function(_this) {
        return function(args, key) {
          return KeyEventService.registerKeyEvent(scope, key, args.execute, function(event) {
            return KeyEventService.defaultShouldExecute(event) && !AppConfig.currentModal;
          });
        };
      })(this));
    };

    return HotkeyService;

  })());
});

angular.module('loomioApp').factory('IntercomService', function($rootScope, $window, AppConfig, Session, LmoUrlService) {
  var IntercomService, lastGroup, mapGroup, service;
  lastGroup = {};
  mapGroup = function(group) {
    if (group == null) {
      return null;
    }
    return {
      id: group.id,
      company_id: group.id,
      key: group.key,
      name: group.name,
      description: group.description.substring(0, 250),
      admin_link: LmoUrlService.group(group, {}, {
        noStub: true,
        absolute: true,
        namespace: 'admin/groups'
      }),
      plan: group.subscriptionKind,
      subscription_kind: group.subscriptionKind,
      subscription_plan: group.subscriptionPlan,
      subscription_expires_at: (group.subscriptionExpiresAt != null) && group.subscriptionExpiresAt.format(),
      creator_id: group.creatorId,
      group_privacy: group.groupPrivacy,
      cohort_id: group.cohortId,
      created_at: group.createdAt.format(),
      motions_count: group.motionsCount,
      discussions_count: group.discussionsCount,
      memberships_count: group.membershipsCount,
      proposal_outcomes_count: group.proposalOutcomesCount,
      closed_motions_count: group.closedMotionsCount,
      has_custom_cover: group.hasCustomCover,
      invitations_count: group.invitationsCount
    };
  };
  service = new (IntercomService = (function() {
    function IntercomService() {}

    IntercomService.prototype.available = function() {
      return ($window != null) && ($window.Intercom != null) && ($window.Intercom.booted != null);
    };

    IntercomService.prototype.boot = function() {
      var user;
      if (!(($window != null) && ($window.Intercom != null))) {
        return;
      }
      user = Session.user();
      lastGroup = mapGroup(user.parentGroups()[0]);
      return $window.Intercom('boot', {
        admin_link: LmoUrlService.user(user, {}, {
          noStub: true,
          absolute: true,
          namespace: 'admin/users',
          key: 'id'
        }),
        app_id: AppConfig.intercomAppId,
        user_id: user.id,
        user_hash: AppConfig.intercomUserHash,
        email: user.email,
        name: user.name,
        username: user.username,
        user_id: user.id,
        created_at: user.createdAt,
        is_coordinator: user.isCoordinator,
        locale: user.locale,
        company: lastGroup,
        has_profile_photo: user.hasProfilePhoto(),
        belongs_to_paying_group: user.belongsToPayingGroup()
      });
    };

    IntercomService.prototype.shutdown = function() {
      if (!this.available()) {
        return;
      }
      return $window.Intercom('shutdown');
    };

    IntercomService.prototype.updateWithGroup = function(group) {
      var user;
      if (!((group != null) && this.available())) {
        return;
      }
      if (_.isEqual(lastGroup, mapGroup(group))) {
        return;
      }
      if (group.isSubgroup()) {
        return;
      }
      user = Session.user();
      if (!user.isMemberOf(group)) {
        return;
      }
      lastGroup = mapGroup(group);
      return $window.Intercom('update', {
        email: user.email,
        user_id: user.id,
        company: lastGroup
      });
    };

    IntercomService.prototype.contactUs = function() {
      if (this.available()) {
        return $window.Intercom('showNewMessage');
      } else {
        return $window.open(LmoUrlService.contactForm(), '_blank');
      }
    };

    $rootScope.$on('logout', function(event, group) {
      return service.shutdown();
    });

    return IntercomService;

  })());
  return service;
});

angular.module('loomioApp').factory('KeyEventService', function($rootScope) {
  var KeyEventService;
  return new (KeyEventService = (function() {
    function KeyEventService() {}

    KeyEventService.prototype.keyboardShortcuts = {
      73: 'pressedI',
      71: 'pressedG',
      80: 'pressedP',
      84: 'pressedT',
      27: 'pressedEsc',
      13: 'pressedEnter',
      191: 'pressedSlash',
      38: 'pressedUpArrow',
      40: 'pressedDownArrow'
    };

    KeyEventService.prototype.broadcast = function(event) {
      var key;
      key = this.keyboardShortcuts[event.which];
      if (key === 'pressedEnter' || (key && !event.ctrlKey && !event.metaKey)) {
        return $rootScope.$broadcast(key, event, angular.element(document.activeElement)[0]);
      }
    };

    KeyEventService.prototype.registerKeyEvent = function(scope, eventCode, execute, shouldExecute) {
      shouldExecute = shouldExecute || this.defaultShouldExecute;
      scope.$$listeners[eventCode] = null;
      return scope.$on(eventCode, function(angularEvent, originalEvent, active) {
        if (shouldExecute(active, originalEvent)) {
          angularEvent.preventDefault() && originalEvent.preventDefault();
          return execute(active, originalEvent);
        }
      });
    };

    KeyEventService.prototype.defaultShouldExecute = function(active, event) {
      if (active == null) {
        active = {};
      }
      if (event == null) {
        event = {};
      }
      return !event.ctrlKey && !event.altKey && !_.contains(['INPUT', 'TEXTAREA', 'SELECT'], active.nodeName);
    };

    KeyEventService.prototype.submitOnEnter = function(scope, opts) {
      if (opts == null) {
        opts = {};
      }
      if (this.previousScope != null) {
        this.previousScope.$$listeners['pressedEnter'] = null;
      }
      this.previousScope = scope;
      return this.registerKeyEvent(scope, 'pressedEnter', scope[opts.submitFn || 'submit'], (function(_this) {
        return function(active, event) {
          return !scope.isDisabled && !scope.submitIsDisabled && (event.ctrlKey || event.metaKey || opts.anyEnter) && _.contains(active.classList, 'lmo-primary-form-input');
        };
      })(this));
    };

    return KeyEventService;

  })());
});

angular.module('loomioApp').factory('LmoUrlService', function(AppConfig) {
  var LmoUrlService;
  return new (LmoUrlService = (function() {
    function LmoUrlService() {}

    LmoUrlService.prototype.route = function(arg) {
      var action, model, params;
      model = arg.model, action = arg.action, params = arg.params;
      if ((model != null) && (action != null)) {
        return this[model.constructor.singular](model, {}, {
          noStub: true
        }) + this.routePath(action);
      } else if (model != null) {
        return this[model.constructor.singular](model);
      } else {
        return this.routePath(action);
      }
    };

    LmoUrlService.prototype.routePath = function(route) {
      return "/".concat(route).replace('//', '/');
    };

    LmoUrlService.prototype.group = function(g, params, options) {
      if (params == null) {
        params = {};
      }
      if (options == null) {
        options = {};
      }
      return this.buildModelRoute('g', g.key, g.fullName, params, options);
    };

    LmoUrlService.prototype.discussion = function(d, params, options) {
      if (params == null) {
        params = {};
      }
      if (options == null) {
        options = {};
      }
      return this.buildModelRoute('d', d.key, d.title, params, options);
    };

    LmoUrlService.prototype.poll = function(p, params, options) {
      if (params == null) {
        params = {};
      }
      if (options == null) {
        options = {};
      }
      return this.buildModelRoute('p', p.key, options.action || p.title, params, options);
    };

    LmoUrlService.prototype.pollSearch = function(params, options) {
      if (params == null) {
        params = {};
      }
      if (options == null) {
        options = {};
      }
      return this.buildModelRoute('polls', '', options.action, params, options);
    };

    LmoUrlService.prototype.searchResult = function(r, params, options) {
      if (params == null) {
        params = {};
      }
      if (options == null) {
        options = {};
      }
      return this.discussion(r, params, options);
    };

    LmoUrlService.prototype.user = function(u, params, options) {
      if (params == null) {
        params = {};
      }
      if (options == null) {
        options = {};
      }
      return this.buildModelRoute('u', u[options.key || 'username'], null, params, options);
    };

    LmoUrlService.prototype.proposal = function(p, params) {
      if (params == null) {
        params = {};
      }
      return this.route({
        model: p.discussion(),
        action: "proposal/" + p.key,
        params: params
      });
    };

    LmoUrlService.prototype.comment = function(c, params) {
      if (params == null) {
        params = {};
      }
      return this.route({
        model: c.discussion(),
        action: "comment/" + c.id,
        params: params
      });
    };

    LmoUrlService.prototype.membership = function(m, params, options) {
      if (params == null) {
        params = {};
      }
      if (options == null) {
        options = {};
      }
      return this.route({
        model: m.group(),
        action: 'memberships',
        params: params
      });
    };

    LmoUrlService.prototype.membershipRequest = function(mr, params, options) {
      if (params == null) {
        params = {};
      }
      if (options == null) {
        options = {};
      }
      return this.route({
        model: mr.group(),
        action: 'membership_requests',
        params: params
      });
    };

    LmoUrlService.prototype.visitor = function() {};

    LmoUrlService.prototype.oauthApplication = function(a, params, options) {
      if (params == null) {
        params = {};
      }
      if (options == null) {
        options = {};
      }
      return this.buildModelRoute('apps/registered', a.id, a.name, params, options);
    };

    LmoUrlService.prototype.contactForm = function() {
      return AppConfig.baseUrl + '/contact';
    };

    LmoUrlService.prototype.buildModelRoute = function(path, key, name, params, options) {
      var result;
      result = options.absolute ? AppConfig.baseUrl : "/";
      result += (options.namespace || path) + "/" + key;
      if (!((name == null) || (options.noStub != null))) {
        result += "/" + this.stub(name);
      }
      if (options.ext != null) {
        result += "." + options.ext;
      }
      if (_.keys(params).length) {
        result += "?" + this.queryStringFor(params);
      }
      return result;
    };

    LmoUrlService.prototype.stub = function(name) {
      return name.replace(/[^a-z0-9\-_]+/gi, '-').replace(/-+/g, '-').toLowerCase();
    };

    LmoUrlService.prototype.queryStringFor = function(params) {
      if (params == null) {
        params = {};
      }
      return _.map(params, function(value, key) {
        return key + "=" + value;
      }).join('&');
    };

    return LmoUrlService;

  })());
});

var slice = [].slice;

angular.module('loomioApp').factory('LoadingService', function(Records) {
  var LoadingService;
  return new (LoadingService = (function() {
    function LoadingService() {}

    LoadingService.prototype.applyLoadingFunction = function(controller, functionName) {
      var executing, loadingFunction;
      executing = functionName + "Executing";
      loadingFunction = controller[functionName];
      return controller[functionName] = function() {
        var args;
        args = 1 <= arguments.length ? slice.call(arguments, 0) : [];
        if (controller[executing]) {
          return;
        }
        controller[executing] = true;
        return loadingFunction.apply(null, args)["finally"](function() {
          return controller[executing] = false;
        });
      };
    };

    LoadingService.prototype.listenForLoading = function(scope) {
      scope.$on('processing', function() {
        return scope.isDisabled = true;
      });
      return scope.$on('doneProcessing', function() {
        return scope.isDisabled = false;
      });
    };

    return LoadingService;

  })());
});

angular.module('loomioApp').factory('MentionLinkService', function() {
  var MentionLinkService;
  return new (MentionLinkService = (function() {
    function MentionLinkService() {}

    MentionLinkService.prototype.cook = function(mentionedUsernames, text) {
      text;
      _.each(mentionedUsernames, function(username) {
        return text = text.replace(RegExp("@" + username, "g"), "[[@" + username + "]]");
      });
      return text;
    };

    return MentionLinkService;

  })());
});

var slice = [].slice;

angular.module('loomioApp').factory('MentionService', function(Records, Session) {
  var MentionService;
  return new (MentionService = (function() {
    function MentionService() {}

    MentionService.prototype.applyMentions = function(scope, model) {
      scope.unmentionableIds = [model.authorId, Session.user().id];
      return scope.fetchByNameFragment = function(fragment) {
        return Records.memberships.fetchByNameFragment(fragment, model.group().key).then(function(response) {
          var userIds;
          userIds = _.without.apply(_, [_.pluck(response.users, 'id')].concat(slice.call(scope.unmentionableIds)));
          return scope.mentionables = Records.users.find(userIds);
        });
      };
    };

    return MentionService;

  })());
});

angular.module('loomioApp').factory('MessageChannelService', function($http, $rootScope, $window, AppConfig, Records, AbilityService, ModalService, SignedOutModal, FlashService) {
  var MessageChannelService;
  return new (MessageChannelService = (function() {
    var handleSubscriptions;

    function MessageChannelService() {}

    MessageChannelService.prototype.subscribe = function(options) {
      if (options == null) {
        options = {};
      }
      if (!AbilityService.isLoggedIn()) {
        return;
      }
      return $http.post('/api/v1/message_channel/subscribe', options).then(handleSubscriptions);
    };

    MessageChannelService.prototype.subscribeToGroup = function(group) {
      return this.subscribe({
        group_key: group.key
      });
    };

    MessageChannelService.prototype.subscribeToPoll = function(poll) {
      return this.subscribe({
        poll_key: poll.key
      });
    };

    handleSubscriptions = function(subscriptions) {
      return _.each(subscriptions.data, function(subscription) {
        PrivatePub.sign(subscription);
        return PrivatePub.subscribe(subscription.channel, function(data) {
          var comment;
          if (data.action != null) {
            switch (data.action) {
              case 'logged_out':
                if (!AppConfig.loggingOut) {
                  ModalService.open(SignedOutModal, function() {
                    return {
                      preventClose: true
                    };
                  });
                }
            }
          }
          if (data.version != null) {
            FlashService.update('global.messages.app_update', {
              version: data.version
            }, 'global.messages.reload', function() {
              return $window.location.reload();
            });
          }
          if (data.memo != null) {
            switch (data.memo.kind) {
              case 'comment_destroyed':
                if (comment = Records.comments.find(memo.data.comment_id)) {
                  comment.destroy();
                }
                break;
              case 'comment_updated':
                Records.comments["import"](memo.data.comment);
                Records["import"](memo.data);
                break;
              case 'comment_unliked':
                if (comment = Records.comments.find(memo.data.comment_id)) {
                  comment.removeLikerId(memo.data.user_id);
                }
            }
          }
          if (data.event != null) {
            if (!_.isArray(data.events)) {
              data.events = [];
            }
            data.events.push(data.event);
          }
          if (data.notification != null) {
            if (!_.isArray(data.notifications)) {
              data.notifications = [];
            }
            data.notifications.push(data.notification);
          }
          Records["import"](data);
          return $rootScope.$digest();
        });
      });
    };

    return MessageChannelService;

  })());
});

angular.module('loomioApp').factory('ModalService', function($mdDialog, $rootScope, $timeout, $translate, AppConfig) {
  var ModalService;
  return new (ModalService = (function() {
    function ModalService() {}

    ModalService.prototype.open = function(modal, resolve, opts) {
      var $scope, modalType, snakeCaseName;
      if (resolve == null) {
        resolve = {};
      }
      if (opts == null) {
        opts = {};
      }
      $rootScope.$broadcast('modalOpened', modal);
      $scope = $rootScope.$new(true);
      $scope.$close = $mdDialog.cancel;
      $scope.focus = function() {
        return $timeout(function() {
          var elementToFocus;
          elementToFocus = document.querySelector('md-dialog [md-autofocus]') || document.querySelector('md-dialog h1');
          return elementToFocus.focus();
        }, 400);
      };
      $scope.$on('focus', $scope.focus);
      $scope.$on('$close', $scope.close);
      resolve.preventClose = resolve.preventClose || (function() {
        return false;
      });
      modalType = opts.type || 'alert';
      snakeCaseName = modal.templateUrl.split('/').pop().replace('.html', '');
      AppConfig.currentModal = $mdDialog[modalType]({
        scope: $scope,
        onComplete: $scope.focus,
        templateUrl: modal.templateUrl,
        role: 'dialog',
        ariaLabel: $translate.instant(snakeCaseName + ".aria_label"),
        controller: modal.controller,
        resolve: resolve,
        size: modal.size || '',
        backdrop: 'static',
        escapeToClose: !resolve.preventClose()
      });
      return $mdDialog.show(AppConfig.currentModal)["finally"](function() {
        return AppConfig.currentModal = void 0;
      });
    };

    return ModalService;

  })());
});

angular.module('loomioApp').factory('PaginationService', function(AppConfig) {
  var PaginationService;
  return new (PaginationService = (function() {
    function PaginationService() {}

    PaginationService.prototype.windowFor = function(arg) {
      var current, max, min, pageSize, pageType;
      current = arg.current, min = arg.min, max = arg.max, pageType = arg.pageType;
      pageSize = parseInt(AppConfig.pageSize[pageType]) || AppConfig.pageSize["default"];
      return {
        current: current,
        min: min,
        max: max,
        prev: current > min ? _.max([current - pageSize, min]) : void 0,
        next: current + pageSize < max ? current + pageSize : void 0,
        pageSize: pageSize
      };
    };

    return PaginationService;

  })());
});

angular.module('loomioApp').factory('PollService', function($window, $location, AppConfig, Records, FormService, LmoUrlService, ScrollService, AbilityService, AttachmentService) {
  var PollService;
  return new (PollService = (function() {
    function PollService() {}

    PollService.prototype.activePollTemplates = function() {
      return AppConfig.pollTemplates;
    };

    PollService.prototype.fieldFromTemplate = function(pollType, field) {
      var template;
      if (!(template = this.templateFor(pollType))) {
        return;
      }
      return template[field];
    };

    PollService.prototype.templateFor = function(pollType) {
      return this.activePollTemplates()[pollType];
    };

    PollService.prototype.lastStanceBy = function(participant, poll) {
      var criteria;
      criteria = {
        latest: true,
        pollId: poll.id
      };
      if (AppConfig.currentUserId) {
        criteria.userId = AppConfig.currentUserId;
      } else if (AppConfig.currentVisitorId) {
        criteria.visitorId = AppConfig.currentVisitorId;
      }
      return _.first(_.sortBy(Records.stances.find(criteria), 'createdAt'));
    };

    PollService.prototype.hasVoted = function(participant, poll) {
      return this.lastStanceBy(participant, poll) != null;
    };

    PollService.prototype.iconFor = function(poll) {
      return this.fieldFromTemplate(poll.pollType, 'material_icon');
    };

    PollService.prototype.usePollsFor = function(model) {
      return model.group().features.use_polls && !$location.search().proposalView;
    };

    PollService.prototype.optionByName = function(poll, name) {
      return _.find(poll.pollOptions(), function(option) {
        return option.name === name;
      });
    };

    PollService.prototype.submitOutcome = function(scope, model, options) {
      var actionName;
      if (options == null) {
        options = {};
      }
      actionName = scope.outcome.isNew() ? 'created' : 'updated';
      return FormService.submit(scope, model, _.merge({
        flashSuccess: "poll_common_outcome_form.outcome_" + actionName,
        drafts: true,
        failureCallback: function() {
          return ScrollService.scrollTo('.lmo-validation-error__message', {
            container: '.poll-common-modal'
          });
        },
        successCallback: function(data) {
          return scope.$emit('outcomeSaved', data.outcomes[0].id);
        }
      }, options));
    };

    PollService.prototype.submitPoll = function(scope, model, options) {
      var actionName;
      if (options == null) {
        options = {};
      }
      actionName = scope.poll.isNew() ? 'created' : 'updated';
      return FormService.submit(scope, model, _.merge({
        flashSuccess: "poll_" + model.pollType + "_form." + model.pollType + "_" + actionName,
        drafts: true,
        prepareFn: function() {
          return scope.$emit('processing');
        },
        failureCallback: function() {
          return ScrollService.scrollTo('.lmo-validation-error__message', {
            container: '.poll-common-modal'
          });
        },
        successCallback: function(data) {
          var poll;
          poll = Records.polls.find(data.polls[0].key);
          scope.$emit('saveComplete', poll);
          if (actionName === 'created') {
            return $location.path(LmoUrlService.poll(poll));
          } else {
            return AttachmentService.cleanupAfterUpdate(poll, 'poll');
          }
        },
        cleanupFn: function() {
          return scope.$emit('doneProcessing');
        }
      }, options));
    };

    PollService.prototype.submitStance = function(scope, model, options) {
      var actionName, pollType;
      if (options == null) {
        options = {};
      }
      actionName = scope.stance.isNew() ? 'created' : 'updated';
      pollType = model.poll().pollType;
      return FormService.submit(scope, model, _.merge({
        flashSuccess: "poll_" + pollType + "_vote_form.stance_" + actionName,
        drafts: true,
        prepareFn: function() {
          return scope.$emit('processing');
        },
        successCallback: function(data) {
          model.poll().clearStaleStances();
          AppConfig.currentVisitorId = data.stances[0].visitor_id;
          ScrollService.scrollTo('.poll-common-card__results-shown');
          return scope.$emit('stanceSaved', data.stances[0].key);
        },
        cleanupFn: function() {
          return scope.$emit('doneProcessing');
        }
      }, options));
    };

    return PollService;

  })());
});

angular.module('loomioApp').factory('PrivacyString', function($translate) {
  var PrivacyString;
  return new (PrivacyString = (function() {
    function PrivacyString() {}

    PrivacyString.prototype.groupPrivacyStatement = function(group) {
      var key;
      if (group.isSubgroup() && group.parent().privacyIsSecret()) {
        if (group.privacyIsClosed()) {
          return $translate.instant('group_form.privacy_statement.private_to_parent_members', {
            parent: group.parentName()
          });
        } else {
          return $translate.instant("group_form.privacy_statement.private_to_group");
        }
      } else {
        key = (function() {
          switch (group.groupPrivacy) {
            case 'open':
              return 'public_on_web';
            case 'closed':
              return 'public_on_web';
            case 'secret':
              return 'private_to_group';
          }
        })();
        return $translate.instant("group_form.privacy_statement." + key);
      }
    };

    PrivacyString.prototype.confirmGroupPrivacyChange = function(group) {
      var key;
      if (group.isNew()) {
        return;
      }
      key = group.attributeIsModified('groupPrivacy') ? group.privacyIsSecret() ? group.isParent() ? 'group_form.confirm_change_to_secret' : 'group_form.confirm_change_to_secret_subgroup' : group.privacyIsOpen() ? 'group_form.confirm_change_to_public' : void 0 : group.attributeIsModified('discussionPrivacyOptions') ? group.discussionPrivacyOptions === 'private_only' ? 'group_form.confirm_change_to_private_discussions_only' : void 0 : void 0;
      if (_.isString(key)) {
        return $translate.instant(key);
      } else {
        return false;
      }
    };

    PrivacyString.prototype.discussion = function(discussion, is_private) {
      var key;
      if (is_private == null) {
        is_private = null;
      }
      key = is_private === false ? 'privacy_public' : discussion.group().parentMembersCanSeeDiscussions ? 'privacy_organisation' : 'privacy_private';
      return $translate.instant("discussion_form." + key, {
        group: discussion.group().name,
        parent: discussion.group().parentName()
      });
    };

    PrivacyString.prototype.group = function(group, state) {
      var key;
      if (state == null) {
        state = null;
      }
      if (state === null) {
        state = group.groupPrivacy;
      }
      key = (function() {
        if (group.isParent()) {
          switch (state) {
            case 'open':
              return 'group_privacy_is_open_description';
            case 'secret':
              return 'group_privacy_is_secret_description';
            case 'closed':
              if (group.allowPublicDiscussions()) {
                return 'group_privacy_is_closed_public_threads_description';
              } else {
                return 'group_privacy_is_closed_description';
              }
          }
        } else {
          switch (state) {
            case 'open':
              return 'subgroup_privacy_is_open_description';
            case 'secret':
              return 'subgroup_privacy_is_secret_description';
            case 'closed':
              if (group.isSubgroupOfSecretParent()) {
                return 'subgroup_privacy_is_closed_secret_parent_description';
              } else if (group.allowPublicDiscussions()) {
                return 'subgroup_privacy_is_closed_public_threads_description';
              } else {
                return 'subgroup_privacy_is_closed_description';
              }
          }
        }
      })();
      return $translate.instant("group_form." + key, {
        parent: group.parentName()
      });
    };

    return PrivacyString;

  })());
});

angular.module('loomioApp').factory('RecordLoader', function(Records) {
  var RecordLoader;
  return RecordLoader = (function() {
    function RecordLoader(opts) {
      if (opts == null) {
        opts = {};
      }
      this.collection = opts.collection;
      this.params = opts.params || {};
      this.path = opts.path;
      this.from = opts.from || 0;
      this.per = opts.per || 25;
      this.numLoaded = opts.numLoaded || 0;
    }

    RecordLoader.prototype.reset = function() {
      this.from = 0;
      return this.numLoaded = 0;
    };

    RecordLoader.prototype.fetchRecords = function() {
      return Records[_.camelCase(this.collection)].fetch({
        path: this.path,
        params: _.merge(this.params, {
          from: this.from,
          per: this.per
        })
      }).then((function(_this) {
        return function(data) {
          _this.numLoaded += data[_this.collection].length;
          return data;
        };
      })(this));
    };

    RecordLoader.prototype.loadMore = function() {
      this.from += this.per;
      return this.fetchRecords();
    };

    return RecordLoader;

  })();
});

angular.module('loomioApp').value('RecordStoreDatabaseName', 'default.db');

angular.module('loomioApp').factory('ScrollService', function($timeout) {
  var ScrollService;
  new (ScrollService = (function() {
    function ScrollService() {}

    return ScrollService;

  })());
  return {
    scrollTo: function(target, options) {
      if (options == null) {
        options = {};
      }
      return $timeout(function() {
        var container, elem;
        elem = document.querySelector(target);
        container = document.querySelector(options.container || '.lmo-main-content');
        if (elem && container) {
          angular.element(container).scrollToElement(elem, options.offset || 50, options.speed || 100).then(function() {
            return angular.element(window).triggerHandler('checkInView');
          });
          return elem.focus();
        }
      });
    }
  };
});

angular.module('loomioApp').factory('Session', function($rootScope, $translate, $window, Records, AppConfig) {
  return {
    login: function(data) {
      var defaultParams, user;
      Records["import"](data);
      if (this.visitor()) {
        defaultParams = {
          participation_token: this.visitor().participationToken
        };
        Records.stances.remote.defaultParams = defaultParams;
        Records.polls.remote.defaultParams = defaultParams;
      }
      if (!(AppConfig.currentUserId = data.current_user_id)) {
        return;
      }
      user = this.user();
      $translate.use(user.locale);
      $rootScope.$broadcast('loggedIn', user);
      if (user.timeZone !== AppConfig.timeZone) {
        user.timeZone = AppConfig.timeZone;
        Records.users.updateProfile(user);
      }
      return user;
    },
    logout: function() {
      AppConfig.loggingOut = true;
      return Records.sessions.remote.destroy('').then(function() {
        return $window.location.href = '/dashboard';
      });
    },
    user: function() {
      return Records.users.find(AppConfig.currentUserId) || Records.users.build();
    },
    visitor: function() {
      return Records.visitors.find(AppConfig.currentVisitorId);
    },
    participant: function() {
      return this.visitor() || this.user();
    },
    currentGroupId: function() {
      return (this.currentGroup != null) && this.currentGroup.id;
    }
  };
});

angular.module('loomioApp').factory('ThreadQueryService', function(Records, AbilityService, Session) {
  var ThreadQueryService;
  return new (ThreadQueryService = (function() {
    var applyFilters, createBaseView, createGroupView, createTimeframeView, parseTimeOption, threadQueryFor;

    function ThreadQueryService() {}

    ThreadQueryService.prototype.filterQuery = function(filter, options) {
      if (options == null) {
        options = {};
      }
      return threadQueryFor(createBaseView(filter, options['queryType'] || 'all'));
    };

    ThreadQueryService.prototype.timeframeQuery = function(options) {
      if (options == null) {
        options = {};
      }
      return threadQueryFor(createTimeframeView(options['name'], options['filter'] || 'show_all', 'timeframe', options['timeframe']['from'], options['timeframe']['to']));
    };

    ThreadQueryService.prototype.groupQuery = function(group, options) {
      if (group == null) {
        group = {};
      }
      if (options == null) {
        options = {};
      }
      return threadQueryFor(createGroupView(group, options['filter'] || 'show_unread', options['queryType'] || 'inbox', options['applyWhere']));
    };

    threadQueryFor = function(view) {
      return {
        threads: function() {
          return view.data();
        },
        length: function() {
          return this.threads().length;
        },
        any: function() {
          return this.length() > 0;
        },
        constructor: {
          singular: 'query'
        }
      };
    };

    createBaseView = function(filters, queryType) {
      var view;
      view = Records.discussions.collection.addDynamicView('default');
      applyFilters(view, filters, queryType);
      return view;
    };

    createGroupView = function(group, filters, queryType, applyWhere) {
      var view;
      view = Records.discussions.collection.addDynamicView(group.name);
      view.applyFind({
        groupId: {
          $in: group.organisationIds()
        }
      });
      applyFilters(view, filters, queryType);
      if (applyWhere != null) {
        view.applyWhere(applyWhere);
      }
      return view;
    };

    createTimeframeView = function(name, filters, queryType, from, to) {
      var today, view;
      today = moment().startOf('day');
      view = Records.discussions.collection.addDynamicView(name);
      view.applyFind({
        lastActivityAt: {
          $gt: parseTimeOption(from)
        }
      });
      view.applyFind({
        lastActivityAt: {
          $lt: parseTimeOption(to)
        }
      });
      applyFilters(view, filters, queryType);
      return view;
    };

    parseTimeOption = function(options) {
      var parts;
      parts = options.split(' ');
      return moment().startOf('day').subtract(parseInt(parts[0]), parts[1]);
    };

    applyFilters = function(view, filters, queryType) {
      filters = [].concat(filters);
      if (AbilityService.isLoggedIn()) {
        view.applyFind({
          discussionReaderId: {
            $gt: 0
          }
        });
      }
      switch (queryType) {
        case 'important':
          view.applyWhere(function(thread) {
            return thread.isImportant();
          });
          break;
        case 'timeframe':
          view.applyWhere(function(thread) {
            return !thread.isImportant();
          });
          break;
        case 'inbox':
          view.applyFind({
            lastActivityAt: {
              $gt: moment().startOf('day').subtract(6, 'week').toDate()
            }
          });
          view.applyWhere(function(thread) {
            return thread.isUnread();
          });
          view.applyWhere(function(thread) {
            return !thread.isDismissed();
          });
          filters.push('show_not_muted');
      }
      _.each(filters, function(filter) {
        switch (filter) {
          case 'show_muted':
            return view.applyWhere(function(thread) {
              return thread.volume() === 'mute';
            });
          case 'show_not_muted':
            return view.applyWhere(function(thread) {
              return thread.volume() !== 'mute';
            });
          case 'only_threads_in_my_groups':
            return view.applyFind({
              groupId: {
                $in: Session.user().groupIds()
              }
            });
          case 'show_starred':
            return view.applyFind({
              starred: true
            });
          case 'show_proposals':
            return view.applyWhere(function(thread) {
              return thread.hasDecision();
            });
          case 'hide_proposals':
            return view.applyWhere(function(thread) {
              return !thread.hasDecision();
            });
        }
      });
      return view;
    };

    return ThreadQueryService;

  })());
});

angular.module('loomioApp').factory('ThreadService', function(Session, Records, ModalService, MuteExplanationModal, FlashService) {
  var ThreadService;
  return new (ThreadService = (function() {
    function ThreadService() {}

    ThreadService.prototype.mute = function(thread) {
      var previousVolume;
      if (!Session.user().hasExperienced("mutingThread")) {
        Records.users.saveExperience("mutingThread");
        return Records.users.updateProfile(Session.user()).then(function() {
          return ModalService.open(MuteExplanationModal, {
            thread: function() {
              return thread;
            }
          });
        });
      } else {
        previousVolume = thread.volume();
        return thread.saveVolume('mute').then((function(_this) {
          return function() {
            return FlashService.success("discussion.volume.mute_message", {
              name: thread.title
            }, 'undo', function() {
              return _this.unmute(thread, previousVolume);
            });
          };
        })(this));
      }
    };

    ThreadService.prototype.unmute = function(thread, previousVolume) {
      if (previousVolume == null) {
        previousVolume = 'normal';
      }
      return thread.saveVolume(previousVolume).then((function(_this) {
        return function() {
          return FlashService.success("discussion.volume.unmute_message", {
            name: thread.title
          }, 'undo', function() {
            return _this.mute(thread);
          });
        };
      })(this));
    };

    return ThreadService;

  })());
});

var bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

angular.module('loomioApp').factory('TimeService', function(AppConfig, $translate) {
  var TimeService;
  return new (TimeService = (function() {
    function TimeService() {
      this.inTimeZone = bind(this.inTimeZone, this);
      this.isoDate = bind(this.isoDate, this);
      this.displayDate = bind(this.displayDate, this);
    }

    TimeService.prototype.nameForZone = function(zone) {
      if (AppConfig.timeZone === zone) {
        return $translate.instant('common.local_time');
      } else {
        return _.invert(AppConfig.timeZones)[zone];
      }
    };

    TimeService.prototype.displayDate = function(m, zone) {
      if (m._f === 'YYYY-MM-DD') {
        return m.format("D MMMM" + (this.sameYear(m)));
      } else {
        return this.inTimeZone(m, zone).format("D MMMM" + (this.sameYear(m)) + " - h:mma");
      }
    };

    TimeService.prototype.isoDate = function(m, zone) {
      return this.inTimeZone(m, zone).toISOString();
    };

    TimeService.prototype.timesOfDay = function() {
      var times;
      times = [];
      _.times(24, function(hour) {
        hour = (hour + 8) % 24;
        if (hour < 10) {
          hour = "0" + hour;
        }
        times.push(moment("2015-01-01 " + hour + ":00").format('h:mm a'));
        return times.push(moment("2015-01-01 " + hour + ":30").format('h:mm a'));
      });
      return times;
    };

    TimeService.prototype.inTimeZone = function(m, zone) {
      return m.tz(zone || AppConfig.timeZone);
    };

    TimeService.prototype.sameYear = function(date) {
      if (date.year() === moment().year()) {
        return "";
      } else {
        return " YYYY";
      }
    };

    return TimeService;

  })());
});

angular.module('loomioApp').factory('TranslationService', function($translate) {
  var TranslationService;
  return new (TranslationService = (function() {
    function TranslationService() {}

    TranslationService.prototype.translationTable = function() {
      return this.translationTable = this.translationTable || $translate.getTranslationTable();
    };

    TranslationService.prototype.listenForTranslations = function(scope, context) {
      context = context || scope;
      return scope.$on('translationComplete', (function(_this) {
        return function(e, translatedFields) {
          if (e.defaultPrevented) {
            return;
          }
          e.preventDefault();
          return context.translation = translatedFields;
        };
      })(this));
    };

    return TranslationService;

  })());
});

angular.module('loomioApp').factory('UserHelpService', function($sce, Session) {
  var UserHelpService;
  return new (UserHelpService = (function() {
    function UserHelpService() {}

    UserHelpService.prototype.helpLocale = function() {
      switch (Session.user().locale) {
        case 'es':
        case 'an':
        case 'ca':
        case 'gl':
          return 'es';
        case 'zh-TW':
          return 'zh';
        case 'ar':
          return 'ar';
        default:
          return 'en';
      }
    };

    UserHelpService.prototype.helpLink = function() {
      return "https://loomio.gitbooks.io/manual/content/" + (this.helpLocale()) + "/index.html";
    };

    UserHelpService.prototype.helpVideo = function() {
      switch (Session.user().locale) {
        case 'es':
        case 'an':
        case 'ca':
        case 'gl':
          return "https://www.youtube.com/embed/BT9f0Nj0zB8";
        default:
          return "https://framatube.org/media/tutoriel-framavox/embed_player";
      }
    };

    UserHelpService.prototype.helpVideoUrl = function() {
      return $sce.trustAsResourceUrl(this.helpVideo());
    };

    UserHelpService.prototype.tenTipsArticleLink = function() {
      switch (Session.user().locale) {
        case 'es':
        case 'an':
        case 'ca':
        case 'gl':
          return "http://blog.loomio.org/2015/08/17/10-consejos-para-tomar-decisiones-con-loomio/";
        case 'fr':
          return "http://blog.loomio.org/2015/08/25/10-conseils-pour-prendre-de-grandes-decisions-grace-a-loomio/";
        default:
          return "https://blog.loomio.org/2015/09/10/10-tips-for-making-great-decisions-with-loomio/";
      }
    };

    UserHelpService.prototype.nineWaysArticleLink = function() {
      switch (Session.user().locale) {
        case 'es':
        case 'an':
        case 'ca':
        case 'gl':
          return "http://blog.loomio.org/2015/08/17/9-formas-de-utilizar-propuestas-en-loomio-para-convertir-conversaciones-en-accion/";
        case 'fr':
          return "https:////blog.loomio.org/2015/08/25/9-manieres-dutiliser-loomio-pour-transformer-une-conversation-en-actes/";
        default:
          return "https://blog.loomio.org/2015/09/18/9-ways-to-use-a-loomio-proposal-to-turn-a-conversation-into-action/";
      }
    };

    return UserHelpService;

  })());
});

angular.module('loomioApp').factory('ViewportService', function($window) {
  var ViewportService;
  return new (ViewportService = (function() {
    function ViewportService() {}

    ViewportService.prototype.viewportSize = function() {
      if ($window.innerWidth < 480) {
        return 'small';
      } else if ($window.innerWidth < 992) {
        return 'medium';
      } else {
        return 'large';
      }
    };

    return ViewportService;

  })());
});

angular.module('loomioApp').directive('addCommunityForm', function(Records, CommunityService, LoadingService, Session, ModalService, PollCommonShareModal) {
  return {
    scope: {
      community: '='
    },
    templateUrl: 'generated/components/add_community_form/add_community_form.html',
    controller: function($scope) {
      var actionNames, customFieldName, customFieldNames;
      $scope.vars = {};
      $scope.fetchAccessToken = function() {
        return CommunityService.fetchAccessToken($scope.community.communityType);
      };
      actionNames = {
        facebook: 'admin_groups',
        slack: 'channels'
      };
      customFieldNames = {
        facebook: 'facebook_group_name',
        slack: 'slack_channel_name'
      };
      customFieldName = function() {
        return customFieldNames[$scope.community.communityType];
      };
      $scope.fetch = function() {
        return Records.identities.performCommand($scope.community.identityId, actionNames[$scope.community.communityType]).then(function(response) {
          return $scope.allGroups = response;
        }, function(response) {
          return $scope.error = response.data.error;
        });
      };
      LoadingService.applyLoadingFunction($scope, 'fetch');
      $scope.fetch();
      $scope.reauth = function() {
        return CommunityService.fetchAccessToken($scope.community.communityType);
      };
      $scope.groups = function() {
        return _.filter($scope.allGroups, function(group) {
          return !CommunityService.alreadyOnPoll($scope.community.poll(), group, $scope.community.communityType) && (_.isEmpty($scope.vars.fragment) || group.name.match(RegExp("" + $scope.vars.fragment, "i")));
        });
      };
      $scope.submit = CommunityService.submitCommunity($scope, $scope.community, {
        prepareFn: function() {
          return $scope.community.customFields[customFieldName()] = _.find($scope.allGroups, function(group) {
            return $scope.community.identifier === group.id;
          }).name;
        }
      });
      $scope.back = function() {
        return ModalService.open(PollCommonShareModal, {
          poll: function() {
            return $scope.community.poll();
          }
        });
      };
      return $scope.placeholderKey = function() {
        return "add_community_form." + $scope.community.communityType + ".fragment_placeholder";
      };
    }
  };
});

angular.module('loomioApp').factory('AddCommunityModal', function() {
  return {
    templateUrl: 'generated/components/add_community_modal/add_community_modal.html',
    controller: function($scope, community) {
      return $scope.community = community;
    }
  };
});

angular.module('loomioApp').factory('ArchiveGroupForm', function() {
  return {
    templateUrl: 'generated/components/archive_group_form/archive_group_form.html',
    controller: function($scope, $rootScope, $location, group, FormService, Records) {
      $scope.group = group;
      return $scope.submit = FormService.submit($scope, $scope.group, {
        submitFn: $scope.group.archive,
        flashSuccess: 'group_page.messages.archive_group_success',
        successCallback: function() {
          return $location.path('/dashboard');
        }
      });
    }
  };
});

angular.module('loomioApp').directive('attachmentPreview', function() {
  return {
    scope: {
      attachment: '=',
      mode: '@'
    },
    restrict: 'E',
    templateUrl: 'generated/components/attachment_preview/attachment_preview.html',
    replace: true,
    controller: function($scope, $rootScope) {
      $scope.destroy = function(event) {
        $scope.$emit('attachmentRemoved', $scope.attachment);
        return event.preventDefault();
      };
      $scope.location = function() {
        return $scope.attachment[$scope.mode];
      };
      return $scope.displayMode = function() {
        return _.contains(['thread', 'context'], $scope.mode);
      };
    }
  };
});

angular.module('loomioApp').controller('AuthorizedAppsPageController', function($scope, $rootScope, Records, ModalService, RevokeAppForm) {
  $rootScope.$broadcast('currentComponent', {
    page: 'authorizedAppsPage'
  });
  $rootScope.$broadcast('setTitle', 'Apps');
  this.loading = true;
  this.applications = function() {
    return Records.oauthApplications.find({
      authorized: true
    });
  };
  Records.oauthApplications.fetchAuthorized().then((function(_this) {
    return function() {
      return _this.loading = false;
    };
  })(this));
  this.openRevokeForm = function(application) {
    return ModalService.open(RevokeAppForm, {
      application: function() {
        return application;
      }
    });
  };
});

angular.module('loomioApp').directive('barChart', function(AppConfig) {
  return {
    template: '<div class="bar-chart"></div>',
    replace: true,
    scope: {
      stanceCounts: '=',
      size: '@'
    },
    restrict: 'E',
    controller: function($scope, $element) {
      var draw, drawPlaceholder, scoreData, scoreMaxValue, shapes;
      draw = SVG($element[0]).size('100%', '100%');
      shapes = [];
      scoreData = function() {
        return _.take(_.map($scope.stanceCounts, function(score, index) {
          return {
            color: AppConfig.pollColors.poll[index],
            index: index,
            score: score
          };
        }), 5);
      };
      scoreMaxValue = function() {
        return _.max(_.map(scoreData(), function(data) {
          return data.score;
        }));
      };
      drawPlaceholder = function() {
        var barHeight, barWidths;
        barHeight = $scope.size / 3;
        barWidths = {
          0: $scope.size,
          1: 2 * $scope.size / 3,
          2: $scope.size / 3
        };
        return _.each(barWidths, function(width, index) {
          return draw.rect(width, barHeight - 2).fill("#ebebeb").x(0).y(index * barHeight);
        });
      };
      return $scope.$watchCollection('stanceCounts', function() {
        var barHeight;
        _.each(shapes, function(shape) {
          return shape.remove();
        });
        if (!(scoreData().length > 0 && scoreMaxValue() > 0)) {
          return drawPlaceholder();
        }
        barHeight = $scope.size / scoreData().length;
        return _.map(scoreData(), function(scoreData) {
          var barWidth;
          barWidth = _.max([($scope.size * scoreData.score) / scoreMaxValue(), 2]);
          return draw.rect(barWidth, barHeight - 2).fill(scoreData.color).x(0).y(scoreData.index * barHeight);
        });
      });
    }
  };
});

angular.module('loomioApp').factory('ChangePasswordForm', function() {
  return {
    templateUrl: 'generated/components/change_password_form/change_password_form.html',
    controller: function($scope, $rootScope, Session, Records, AuthService) {
      $scope.user = Session.user().clone();
      return $scope.submit = function() {
        $scope.processing = true;
        return AuthService.forgotPassword($scope.user).then(function() {
          return $scope.user.sentPasswordLink = true;
        })["finally"](function() {
          return $scope.processing = false;
        });
      };
    }
  };
});

angular.module('loomioApp').factory('ChangePictureForm', function() {
  return {
    templateUrl: 'generated/components/change_picture_form/change_picture_form.html',
    controller: function($scope, $timeout, Session, Records, FormService) {
      $scope.user = Session.user().clone();
      $scope.selectFile = function() {
        return $timeout(function() {
          return document.querySelector('.change-picture-form__file-input').click();
        });
      };
      $scope.submit = FormService.submit($scope, $scope.user, {
        flashSuccess: 'profile_page.messages.picture_changed',
        submitFn: Records.users.updateProfile,
        prepareFn: function(kind) {
          return $scope.user.avatarKind = kind;
        }
      });
      return $scope.upload = FormService.upload($scope, $scope.user, {
        flashSuccess: 'profile_page.messages.picture_changed',
        submitFn: Records.users.uploadAvatar,
        loadingMessage: 'common.action.uploading'
      });
    }
  };
});

angular.module('loomioApp').factory('ChangeVolumeForm', function() {
  return {
    templateUrl: 'generated/components/change_volume_form/change_volume_form.html',
    controller: function($scope, model, FormService, Session, FlashService) {
      $scope.model = model.clone();
      $scope.volumeLevels = ["loud", "normal", "quiet"];
      $scope.defaultVolume = function() {
        switch ($scope.model.constructor.singular) {
          case 'discussion':
            return $scope.model.volume();
          case 'membership':
            return $scope.model.volume;
          case 'user':
            return $scope.model.defaultMembershipVolume;
        }
      };
      $scope.buh = {
        volume: $scope.defaultVolume()
      };
      $scope.translateKey = function(key) {
        return "change_volume_form." + (key || $scope.model.constructor.singular);
      };
      $scope.flashTranslation = function() {
        var key;
        key = (function() {
          if ($scope.applyToAll) {
            switch ($scope.model.constructor.singular) {
              case 'discussion':
                return 'membership';
              case 'membership':
                return 'all_groups';
              case 'user':
                return 'all_groups';
            }
          } else {
            return $scope.model.constructor.singular;
          }
        })();
        return ($scope.translateKey(key)) + ".messages." + $scope.buh.volume;
      };
      $scope.submit = FormService.submit($scope, $scope.model, {
        submitFn: function(model) {
          return model.saveVolume($scope.buh.volume, $scope.applyToAll, $scope.setDefault);
        },
        flashSuccess: $scope.flashTranslation
      });
    }
  };
});

angular.module('loomioApp').controller('ContactPageController', function($scope, UserAuthService, ContactMessageModel, ContactMessageService, FormService, Session) {
  $scope.message = new ContactMessageModel({
    name: Session.user().name,
    email: Session.user().email
  });
  return FormService.applyForm($scope, ContactMessageService.save, $scope.message);
});

angular.module('loomioApp').directive('currentPollsCard', function(Records, LoadingService, ModalService, PollCommonStartModal) {
  return {
    scope: {
      model: '='
    },
    templateUrl: 'generated/components/current_polls_card/current_polls_card.html',
    controller: function($scope) {
      $scope.fetchRecords = function() {
        return Records.polls.fetchFor($scope.model, {
          status: 'active'
        });
      };
      LoadingService.applyLoadingFunction($scope, 'fetchRecords');
      $scope.fetchRecords();
      $scope.polls = function() {
        return _.take($scope.model.activePolls(), $scope.limit || 50);
      };
      return $scope.startPoll = function() {
        return ModalService.open(PollCommonStartModal, {
          poll: function() {
            return Records.polls.build({
              groupId: $scope.model.id
            });
          }
        });
      };
    }
  };
});

angular.module('loomioApp').controller('DashboardPageController', function($rootScope, $scope, Records, Session, LoadingService, ThreadQueryService, AbilityService, AppConfig, $routeParams, $mdMedia, ModalService, GroupModal) {
  $rootScope.$broadcast('currentComponent', {
    page: 'dashboardPage',
    filter: $routeParams.filter
  });
  $rootScope.$broadcast('setTitle', 'Recent');
  $rootScope.$broadcast('analyticsClearGroup');
  this.userHasMuted = function() {
    return Session.user().hasExperienced("mutingThread");
  };
  this.perPage = 50;
  this.loaded = {
    show_all: 0,
    show_muted: 0
  };
  this.views = {
    recent: {},
    groups: {}
  };
  this.loading = function() {
    return !AppConfig.dashboardLoaded;
  };
  this.timeframes = {
    today: {
      from: '1 second ago',
      to: '-10 year ago'
    },
    yesterday: {
      from: '1 day ago',
      to: '1 second ago'
    },
    thisweek: {
      from: '1 week ago',
      to: '1 day ago'
    },
    thismonth: {
      from: '1 month ago',
      to: '1 week ago'
    },
    older: {
      from: '3 month ago',
      to: '1 month ago'
    }
  };
  this.timeframeNames = _.map(this.timeframes, function(timeframe, name) {
    return name;
  });
  this.loadingViewNames = ['proposals', 'today', 'yesterday'];
  this.recentViewNames = ['proposals', 'starred', 'today', 'yesterday', 'thisweek', 'thismonth', 'older'];
  this.groupThreadLimit = 5;
  this.groups = function() {
    return Session.user().parentGroups();
  };
  this.moreForThisGroup = function(group) {
    return this.views.groups[group.key].length() > this.groupThreadLimit;
  };
  this.displayByGroup = function() {
    return _.contains(['show_muted'], this.filter);
  };
  this.updateQueries = (function(_this) {
    return function() {
      if (_this.loaded[_this.filter] > 0) {
        AppConfig.dashboardLoaded = true;
      }
      _this.currentBaseQuery = ThreadQueryService.filterQuery(['only_threads_in_my_groups', _this.filter]);
      if (_this.displayByGroup()) {
        return _.each(_this.groups(), function(group) {
          return _this.views.groups[group.key] = ThreadQueryService.groupQuery(group, {
            filter: _this.filter,
            queryType: 'all'
          });
        });
      } else {
        _this.views.recent.proposals = ThreadQueryService.filterQuery(['only_threads_in_my_groups', 'show_not_muted', 'show_proposals', _this.filter], {
          queryType: 'important'
        });
        _this.views.recent.starred = ThreadQueryService.filterQuery(['show_not_muted', 'show_starred', 'hide_proposals', _this.filter], {
          queryType: 'important'
        });
        return _.each(_this.timeframeNames, function(name) {
          return _this.views.recent[name] = ThreadQueryService.timeframeQuery({
            name: name,
            filter: ['only_threads_in_my_groups', 'show_not_muted', _this.filter],
            timeframe: _this.timeframes[name]
          });
        });
      }
    };
  })(this);
  this.loadMore = (function(_this) {
    return function() {
      var from;
      from = _this.loaded[_this.filter];
      _this.loaded[_this.filter] = _this.loaded[_this.filter] + _this.perPage;
      return Records.discussions.fetchDashboard({
        filter: _this.filter,
        from: from,
        per: _this.perPage
      }).then(_this.updateQueries);
    };
  })(this);
  LoadingService.applyLoadingFunction(this, 'loadMore');
  this.setFilter = (function(_this) {
    return function(filter) {
      if (filter == null) {
        filter = 'show_all';
      }
      _this.filter = filter;
      _this.updateQueries();
      if (_this.loaded[_this.filter] === 0) {
        return _this.loadMore();
      }
    };
  })(this);
  this.setFilter($routeParams.filter || 'show_all');
  this.noGroups = function() {
    return !Session.user().hasAnyGroups();
  };
  this.startGroup = function() {
    return ModalService.open(GroupModal, {
      group: function() {
        return Records.groups.build();
      }
    });
  };
  $scope.$on('currentUserMembershipsLoaded', (function(_this) {
    return function() {
      return _this.setFilter();
    };
  })(this));
  this.showLargeImage = function() {
    return $mdMedia("gt-sm");
  };
});

angular.module('loomioApp').factory('DeactivateUserForm', function() {
  return {
    templateUrl: 'generated/components/deactivate_user_form/deactivate_user_form.html',
    controller: function($scope, $rootScope, $window, Session, Records, FormService) {
      $scope.user = Session.user().clone();
      return $scope.submit = FormService.submit($scope, $scope.user, {
        submitFn: Records.users.deactivate,
        successCallback: function() {
          return $window.location.reload();
        }
      });
    }
  };
});

angular.module('loomioApp').factory('DeactivationModal', function() {
  return {
    templateUrl: 'generated/components/deactivation_modal/deactivation_modal.html',
    controller: function($scope, AbilityService, ModalService, DeactivateUserForm, OnlyCoordinatorModal) {
      return $scope.submit = function() {
        if (AbilityService.canDeactivateUser()) {
          return ModalService.open(DeactivateUserForm);
        } else {
          return ModalService.open(OnlyCoordinatorModal);
        }
      };
    }
  };
});

angular.module('loomioApp').factory('DeleteThreadForm', function() {
  return {
    templateUrl: 'generated/components/delete_thread_form/delete_thread_form.html',
    controller: function($scope, $location, discussion, FormService, LmoUrlService) {
      $scope.discussion = discussion;
      $scope.group = discussion.group();
      return $scope.submit = FormService.submit($scope, $scope.discussion, {
        submitFn: $scope.discussion.destroy,
        flashSuccess: 'delete_thread_form.messages.success',
        successCallback: function() {
          return $location.path(LmoUrlService.group($scope.group));
        }
      });
    }
  };
});

angular.module('loomioApp').factory('DiscussionForm', function() {
  return {
    templateUrl: 'generated/components/discussion_form/discussion_form.html',
    controller: function($scope, $controller, $location, discussion, Session, Records, AbilityService, FormService, MentionService, AttachmentService, KeyEventService, PrivacyString, EmojiService) {
      var actionName;
      $scope.discussion = discussion.clone();
      if ($scope.discussion.isNew()) {
        $scope.discussion.makeAnnouncement = true;
        $scope.showGroupSelect = true;
      }
      actionName = $scope.discussion.isNew() ? 'created' : 'updated';
      $scope.submit = FormService.submit($scope, $scope.discussion, {
        flashSuccess: "discussion_form.messages." + actionName,
        drafts: true,
        successCallback: (function(_this) {
          return function(response) {
            discussion = response.discussions[0];
            Records.attachments.find({
              attachableId: discussion.id,
              attachableType: 'Discussion'
            }).filter(function(attachment) {
              return !_.contains(discussion.attachment_ids, attachment.id);
            }).map(function(attachment) {
              return attachment.remove();
            });
            if (actionName === 'created') {
              return $location.path("/d/" + discussion.key);
            }
          };
        })(this)
      });
      $scope.availableGroups = function() {
        return _.filter(Session.user().groups(), function(group) {
          return AbilityService.canStartThread(group);
        });
      };
      $scope.restoreDraft = function() {
        if (!(($scope.discussion.group() != null) && $scope.discussion.isNew())) {
          return;
        }
        $scope.discussion.restoreDraft();
        return $scope.updatePrivacy();
      };
      $scope.privacyPrivateDescription = function() {
        return PrivacyString.discussion($scope.discussion, true);
      };
      $scope.updatePrivacy = function() {
        return $scope.discussion["private"] = $scope.discussion.privateDefaultValue();
      };
      $scope.showPrivacyForm = function() {
        if (!$scope.discussion.group()) {
          return;
        }
        return $scope.discussion.group().discussionPrivacyOptions === 'public_or_private';
      };
      $scope.descriptionSelector = '.discussion-form__description-input';
      EmojiService.listen($scope, $scope.discussion, 'description', $scope.descriptionSelector);
      AttachmentService.listenForPaste($scope);
      AttachmentService.listenForAttachments($scope, $scope.discussion);
      KeyEventService.submitOnEnter($scope);
      return MentionService.applyMentions($scope, $scope.discussion);
    }
  };
});

angular.module('loomioApp').factory('DismissExplanationModal', function() {
  return {
    templateUrl: 'generated/components/dismiss_explanation_modal/dismiss_explanation_modal.html',
    controller: function($scope, thread, Records, FlashService) {
      $scope.thread = thread;
      return $scope.dismiss = function() {
        $scope.thread.dismiss();
        FlashService.success('dashboard_page.thread_dismissed');
        return $scope.$close();
      };
    }
  };
});

angular.module('loomioApp').controller('EmailSettingsPageController', function($rootScope, $translate, Records, AbilityService, FormService, Session, $location, ModalService, ChangeVolumeForm) {
  $rootScope.$broadcast('currentComponent', {
    page: 'emailSettingsPage'
  });
  this.init = (function(_this) {
    return function() {
      if (!(AbilityService.isLoggedIn() || (Session.user().restricted != null))) {
        return;
      }
      _this.user = Session.user().clone();
      return $translate.use(_this.user.locale);
    };
  })(this);
  this.init();
  this.groupVolume = function(group) {
    return group.membershipFor(Session.user()).volume;
  };
  this.defaultSettingsDescription = function() {
    return "email_settings_page.default_settings." + (Session.user().defaultMembershipVolume) + "_description";
  };
  this.changeDefaultMembershipVolume = function() {
    return ModalService.open(ChangeVolumeForm, {
      model: (function(_this) {
        return function() {
          return Session.user();
        };
      })(this)
    });
  };
  this.editSpecificGroupVolume = function(group) {
    return ModalService.open(ChangeVolumeForm, {
      model: (function(_this) {
        return function() {
          return group.membershipFor(Session.user());
        };
      })(this)
    });
  };
  this.submit = FormService.submit(this, this.user, {
    submitFn: Records.users.updateProfile,
    flashSuccess: 'email_settings_page.messages.updated',
    successCallback: function() {
      if (AbilityService.isLoggedIn()) {
        return $location.path('/dashboard');
      }
    }
  });
});

angular.module('loomioApp').directive('emojiPicker', function() {
  return {
    scope: {
      targetSelector: '='
    },
    restrict: 'E',
    replace: true,
    templateUrl: 'generated/components/emoji_picker/emoji_picker.html',
    controller: function($scope, $timeout, EmojiService, KeyEventService) {
      $scope.render = EmojiService.render;
      $scope.search = function(term) {
        $scope.hovered = {};
        return $scope.source = term ? _.take(_.filter(EmojiService.source, function(emoji) {
          return emoji.match(RegExp("^:" + term, "i"));
        }), 15) : EmojiService.defaults;
      };
      $scope.search();
      $scope.toggleMenu = function() {
        $scope.showMenu = !$scope.showMenu;
        $scope.search();
        return $timeout(function() {
          if ($scope.showMenu) {
            return document.querySelector('.emoji-picker__search').focus();
          }
        });
      };
      $scope.hideMenu = function() {
        if (!$scope.showMenu) {
          return;
        }
        $scope.hovered = {};
        $scope.term = '';
        return $scope.toggleMenu();
      };
      KeyEventService.registerKeyEvent($scope, 'pressedEsc', $scope.toggleMenu, function() {
        return $scope.showMenu;
      });
      $scope.hover = function(emoji) {
        return $scope.hovered = {
          name: emoji,
          image: $scope.render(emoji)
        };
      };
      $scope.select = function(emoji) {
        $scope.$emit('emojiSelected', emoji, $scope.targetSelector);
        return $scope.hideMenu();
      };
      return $scope.noEmojisFound = function() {
        return $scope.source.length === 0;
      };
    }
  };
});

angular.module('loomioApp').directive('errorPage', function() {
  return {
    scope: {
      error: '='
    },
    restrict: 'E',
    templateUrl: 'generated/components/error_page/error_page.html',
    replace: true
  };
});

angular.module('loomioApp').controller('ExplorePageController', function(Records, $rootScope, $timeout, AppConfig, LoadingService) {
  $rootScope.$broadcast('currentComponent', {
    page: 'explorePage'
  });
  this.groupIds = [];
  this.resultsCount = 0;
  this.perPage = AppConfig.pageSize.exploreGroups;
  this.canLoadMoreGroups = true;
  this.query = "";
  $timeout(function() {
    return document.querySelector('#search-field').focus();
  });
  this.groups = (function(_this) {
    return function() {
      return Records.groups.find(_this.groupIds);
    };
  })(this);
  this.handleSearchResults = (function(_this) {
    return function(response) {
      Records.groups.getExploreResultsCount(_this.query).then(function(data) {
        return _this.resultsCount = data.count;
      });
      _this.groupIds = _this.groupIds.concat(_.pluck(response.groups, 'id'));
      return _this.canLoadMoreGroups = (response.groups || []).length === _this.perPage;
    };
  })(this);
  this.search = (function(_this) {
    return function() {
      _this.groupIds = [];
      return Records.groups.fetchExploreGroups(_this.query, {
        per: _this.perPage
      }).then(_this.handleSearchResults);
    };
  })(this);
  this.loadMore = (function(_this) {
    return function() {
      return Records.groups.fetchExploreGroups(_this.query, {
        from: _this.groupIds.length,
        per: _this.perPage
      }).then(_this.handleSearchResults);
    };
  })(this);
  LoadingService.applyLoadingFunction(this, 'search');
  this.search();
  this.groupCover = function(group) {
    return {
      'background-image': "url(" + (group.coverUrl('small')) + ")"
    };
  };
  this.groupDescription = function(group) {
    if (group.description) {
      return _.trunc(group.description, 100);
    }
  };
  this.showMessage = function() {
    return !this.searching && this.query && this.groups().length > 0;
  };
  this.searchResultsMessage = function() {
    if (this.groups().length === 1) {
      return 'explore_page.single_search_result';
    } else {
      return 'explore_page.multiple_search_results';
    }
  };
  this.noResultsFound = function() {
    return !this.searching && (this.groups().length < this.perPage || !this.canLoadMoreGroups);
  };
});

angular.module('loomioApp').directive('flash', function(AppConfig) {
  return {
    restrict: 'E',
    templateUrl: 'generated/components/flash/flash.html',
    replace: true,
    controllerAs: 'flash',
    controller: function($scope, $interval, FlashService) {
      $scope.pendingDismiss = null;
      $scope.$on('flashMessage', (function(_this) {
        return function(event, flash) {
          $scope.flash = _.merge(flash, {
            visible: true
          });
          if ($scope.loading()) {
            $scope.flash.message = $scope.flash.message || 'common.action.loading';
          }
          if ($scope.pendingDismiss != null) {
            $interval.cancel($scope.pendingDismiss);
          }
          return $scope.pendingDismiss = $interval($scope.dismiss, flash.duration, 1);
        };
      })(this));
      $scope.$on('dismissFlash', $scope.dismiss);
      $scope.loading = function() {
        return $scope.flash.level === 'loading';
      };
      $scope.display = function() {
        return $scope.flash.visible;
      };
      $scope.dismiss = function() {
        return $scope.flash.visible = false;
      };
      $scope.ariaLive = function() {
        if ($scope.loading()) {
          return 'polite';
        } else {
          return 'assertive';
        }
      };
      if (AppConfig.flash.success != null) {
        FlashService.success(AppConfig.flash.success);
      }
      if (AppConfig.flash.notice != null) {
        FlashService.info(AppConfig.flash.notice);
      }
      if (AppConfig.flash.warning != null) {
        FlashService.warning(AppConfig.flash.warning);
      }
      if (AppConfig.flash.error != null) {
        FlashService.error(AppConfig.flash.error);
      }
    }
  };
});

angular.module('loomioApp').factory('FlashService', function($rootScope, AppConfig) {
  var FlashService;
  return new (FlashService = (function() {
    var createFlashLevel;

    function FlashService() {}

    createFlashLevel = function(level, duration) {
      return function(translateKey, translateValues, actionKey, actionFn) {
        return $rootScope.$broadcast('flashMessage', {
          level: level,
          duration: duration || AppConfig.flashTimeout.short,
          message: translateKey,
          options: translateValues,
          action: actionKey,
          actionFn: actionFn
        });
      };
    };

    FlashService.prototype.success = createFlashLevel('success');

    FlashService.prototype.info = createFlashLevel('info');

    FlashService.prototype.warning = createFlashLevel('warning');

    FlashService.prototype.error = createFlashLevel('error');

    FlashService.prototype.loading = createFlashLevel('loading', AppConfig.flashTimeout.long);

    FlashService.prototype.update = createFlashLevel('update', AppConfig.flashTimeout.long);

    FlashService.prototype.dismiss = createFlashLevel('loading', 1);

    return FlashService;

  })());
});

angular.module('loomioApp').directive('formErrors', function() {
  return {
    scope: {
      model: '='
    },
    restrict: 'E',
    templateUrl: 'generated/components/form_errors/form_errors.html',
    replace: true,
    controller: 'FormErrorsController'
  };
});

angular.module('loomioApp').controller('FormErrorsController', function() {});

angular.module('loomioApp').directive('groupAvatar', function() {
  return {
    scope: {
      group: '=',
      size: '@?'
    },
    restrict: 'E',
    templateUrl: 'generated/components/group_avatar/group_avatar.html',
    replace: true,
    controller: function($scope) {
      var sizes;
      sizes = ['small', 'medium', 'large'];
      if (!_.contains(sizes, $scope.size)) {
        return $scope.size = 'small';
      }
    }
  };
});

angular.module('loomioApp').controller('GroupPageController', function($rootScope, $location, $routeParams, $scope, Records, Session, MessageChannelService, AbilityService, AppConfig, LmoUrlService, PaginationService, PollService, ModalService) {
  $rootScope.$broadcast('currentComponent', {
    page: 'groupPage',
    key: $routeParams.key,
    skipScroll: true
  });
  this.launchers = [];
  this.addLauncher = (function(_this) {
    return function(action, condition, opts) {
      if (condition == null) {
        condition = (function() {
          return true;
        });
      }
      if (opts == null) {
        opts = {};
      }
      return _this.launchers.push({
        priority: opts.priority || 9999,
        action: action,
        condition: condition,
        allowContinue: opts.allowContinue
      });
    };
  })(this);
  this.performLaunch = function() {
    return this.launchers.sort(function(a, b) {
      return a.priority - b.priority;
    }).map((function(_this) {
      return function(launcher) {
        if ((typeof launcher.action !== 'function') || _this.launched) {
          return;
        }
        if (launcher.condition()) {
          launcher.action();
          if (!launcher.allowContinue) {
            return _this.launched = true;
          }
        }
      };
    })(this));
  };
  $routeParams.key = $routeParams.key.split('-')[0];
  Records.groups.findOrFetchById($routeParams.key).then((function(_this) {
    return function(group) {
      return _this.init(group);
    };
  })(this), function(error) {
    return $rootScope.$broadcast('pageError', error);
  });
  this.init = (function(_this) {
    return function(group) {
      var maxDiscussions;
      _this.group = group;
      _this.performLaunch();
      MessageChannelService.subscribeToGroup(_this.group);
      _this.usePolls = PollService.usePollsFor(_this.group);
      if (AbilityService.canCreateContentFor(_this.group)) {
        Records.drafts.fetchFor(_this.group);
      }
      maxDiscussions = AbilityService.canViewPrivateContent(_this.group) ? _this.group.discussionsCount : _this.group.publicDiscussionsCount;
      _this.pageWindow = PaginationService.windowFor({
        current: parseInt($location.search().from || 0),
        min: 0,
        max: maxDiscussions,
        pageType: 'groupThreads'
      });
      $rootScope.$broadcast('viewingGroup', _this.group);
      $rootScope.$broadcast('setTitle', _this.group.fullName);
      $rootScope.$broadcast('analyticsSetGroup', _this.group);
      return $rootScope.$broadcast('currentComponent', {
        page: 'groupPage',
        group: _this.group,
        key: _this.group.key,
        links: {
          canonical: LmoUrlService.group(_this.group, {}, {
            absolute: true
          }),
          rss: !_this.group.privacyIsSecret() ? LmoUrlService.group(_this.group, {}, {
            absolute: true,
            ext: 'xml'
          }) : void 0,
          prev: _this.pageWindow.prev != null ? LmoUrlService.group(_this.group, {
            from: _this.pageWindow.prev
          }) : void 0,
          next: _this.pageWindow.next != null ? LmoUrlService.group(_this.group, {
            from: _this.pageWindow.next
          }) : void 0
        }
      });
    };
  })(this);
  this.canViewMemberships = function() {
    return AbilityService.canViewMemberships(this.group);
  };
  this.canManageMembershipRequests = function() {
    return AbilityService.canManageMembershipRequests(this.group);
  };
  this.canUploadPhotos = function() {
    return AbilityService.canAdministerGroup(this.group);
  };
  this.openUploadCoverForm = function() {
    return this.openModal.open(CoverPhotoForm, {
      group: (function(_this) {
        return function() {
          return _this.group;
        };
      })(this)
    });
  };
  this.openUploadLogoForm = function() {
    return this.openModal(LogoPhotoForm, {
      group: (function(_this) {
        return function() {
          return _this.group;
        };
      })(this)
    });
  };
  this.openModal = function(modal, resolve) {
    return ModalService.open(modal, resolve);
  };
  this.showPreviousPolls = function() {
    return this.usePolls && AbilityService.canViewPreviousPolls(this.group);
  };
});

angular.module('loomioApp').directive('h1', function() {
  return {
    restrict: 'E',
    link: function(scope, elem, attrs) {
      return elem.attr('tabindex', 0);
    }
  };
});

angular.module('loomioApp').directive('helpBubble', function() {
  return {
    scope: {
      helptext: '@'
    },
    restrict: 'E',
    templateUrl: 'generated/components/help_bubble/help_bubble.html',
    replace: true
  };
});

angular.module('loomioApp').directive('i', function() {
  return {
    restrict: 'E',
    link: function(scope, elem, attrs) {
      return elem.attr('aria-hidden', 'true');
    }
  };
});

angular.module('loomioApp').controller('InboxPageController', function($scope, $rootScope, Records, Session, AppConfig, LoadingService, ThreadQueryService, ModalService, GroupModal) {
  var filters;
  $rootScope.$broadcast('currentComponent', {
    page: 'inboxPage'
  });
  $rootScope.$broadcast('setTitle', 'Inbox');
  $rootScope.$broadcast('analyticsClearGroup');
  filters = ['only_threads_in_my_groups', 'show_unread', 'show_not_muted'];
  this.threadLimit = 50;
  this.views = {
    groups: {}
  };
  this.groups = function() {
    return _.flatten([Session.user().parentGroups(), Session.user().orphanSubgroups()]);
  };
  this.init = (function(_this) {
    return function() {
      return _.each(_this.groups(), function(group) {
        return _this.views.groups[group.key] = ThreadQueryService.groupQuery(group, {
          filter: filters
        });
      });
    };
  })(this);
  $scope.$on('currentUserMembershipsLoaded', this.init);
  this.init();
  this.hasThreads = function() {
    return ThreadQueryService.filterQuery(filters, {
      queryType: 'inbox'
    }).any();
  };
  this.moreForThisGroup = function(group) {
    return this.views.groups[group.key].length() > this.threadLimit;
  };
  this.noGroups = function() {
    return !Session.user().hasAnyGroups();
  };
  this.startGroup = function() {
    return ModalService.open(GroupModal, {
      group: function() {
        return Records.groups.build();
      }
    });
  };
});

angular.module('loomioApp').controller('InstallSlackPageController', function($rootScope, Session, ModalService, InstallSlackModal) {
  $rootScope.$broadcast('currentComponent', {
    page: 'installSlackPage'
  });
  if (Session.user().identityFor('slack')) {
    ModalService.open(InstallSlackModal, {
      preventClose: function() {
        return true;
      }
    });
  }
});

angular.module('loomioApp').factory('LeaveGroupForm', function() {
  return {
    templateUrl: 'generated/components/leave_group_form/leave_group_form.html',
    controller: function($scope, $location, $rootScope, group, FormService, Session, AbilityService) {
      $scope.group = group;
      $scope.membership = $scope.group.membershipFor(Session.user());
      $scope.submit = FormService.submit($scope, $scope.group, {
        submitFn: $scope.membership.destroy,
        flashSuccess: 'group_page.messages.leave_group_success',
        successCallback: function() {
          $rootScope.$broadcast('currentUserMembershipsLoaded');
          return $location.path('/dashboard');
        }
      });
      return $scope.canLeaveGroup = function() {
        return AbilityService.canRemoveMembership($scope.membership);
      };
    }
  };
});

angular.module('loomioApp').directive('lmoHref', function($window, $router) {
  return {
    restrict: 'A',
    scope: {
      route: '@lmoHref',
      target: '@target'
    },
    link: function(scope, elem, attrs) {
      return elem.bind('click', function($event) {
        if ($event.ctrlKey || $event.metaKey || scope.target === '_blank') {
          $event.stopImmediatePropagation();
          return $window.open(scope.route, '_blank');
        } else {
          return $router.navigate(scope.route);
        }
      });
    }
  };
});

angular.module('loomioApp').directive('lmoHrefFor', function(LmoUrlService) {
  return {
    restrict: 'A',
    scope: {
      model: '=lmoHrefFor',
      action: '@lmoHrefAction'
    },
    link: function(scope, elem, attrs) {
      elem.attr('href', LmoUrlService.route({
        model: scope.model,
        action: scope.action
      }));
      return elem.bind('click', function($event) {
        var attr_target;
        attr_target = $event.target.attributes.target;
        if ($event.ctrlKey || $event.metaKey || (attr_target && attr_target.value === '_blank')) {
          return $event.stopImmediatePropagation();
        }
      });
    }
  };
});

angular.module('loomioApp').directive('lmoStaticHref', function($window) {
  return {
    restrict: 'A',
    scope: {
      route: '@lmoStaticHref'
    },
    link: function(scope, elem, attrs) {
      scope.$watch('route', function() {
        return elem.attr('href', scope.route);
      });
      return elem.bind('click', function($event) {
        if ($event.trlKey || $event.metaKey) {
          return $window.open(scope.route);
        } else {
          return $window.location.href = scope.route;
        }
      });
    }
  };
});

angular.module('loomioApp').directive('loading', function() {
  return {
    scope: {},
    restrict: 'E',
    templateUrl: 'generated/components/loading/loading.html',
    replace: true
  };
});

angular.module('loomioApp').directive('loadingContent', function() {
  return {
    scope: {
      blockCount: '=?',
      lineCount: '=?'
    },
    restrict: 'E',
    templateUrl: 'generated/components/loading_content/loading_content.html',
    replace: true,
    controller: function($scope) {
      $scope.blocks = new Array($scope.blockCount || 1);
      return $scope.lines = new Array($scope.lineCount || 3);
    }
  };
});

angular.module('loomioApp').directive('materialModalHeaderCancelButton', function() {
  return {
    restrict: 'E',
    templateUrl: 'generated/components/material_modal_header_cancel_button/material_modal_header_cancel_button.html'
  };
});

angular.module('loomioApp').directive('matrixChart', function(AppConfig) {
  return {
    template: '<div class="matrix-chart"></div>',
    replace: true,
    scope: {
      matrixCounts: '=',
      size: '@'
    },
    restrict: 'E',
    controller: function($scope, $element) {
      var draw, drawPlaceholder, drawShape, shapes;
      draw = SVG($element[0]).size('100%', '100%');
      shapes = [];
      drawPlaceholder = function() {
        return _.each(_.times(5), function(row) {
          return _.each(_.times(5), function(col) {
            return drawShape(row, col, $scope.size / 5, false);
          });
        });
      };
      drawShape = function(row, col, width, value) {
        var color;
        color = value ? AppConfig.pollColors.meeting[0] : '#ebebeb';
        return shapes.push(draw.rect(width - 1, width - 1).fill(color).x(width * row).y(width * col));
      };
      return $scope.$watchCollection('matrixCounts', function() {
        var width;
        _.each(shapes, function(shape) {
          return shape.remove();
        });
        if (_.isEmpty($scope.matrixCounts)) {
          return drawPlaceholder();
        }
        width = $scope.size / _.max([$scope.matrixCounts.length, $scope.matrixCounts[0].length]);
        return _.each($scope.matrixCounts, function(values, row) {
          return _.each(values, function(value, col) {
            return drawShape(row, col, width, value);
          });
        });
      });
    }
  };
});

angular.module('loomioApp').directive('mdAttachmentForm', function(MdAttachmentFormController) {
  return {
    scope: {
      model: '=',
      showLabel: '=?'
    },
    restrict: 'E',
    templateUrl: 'generated/components/md_attachment_form/md_attachment_form.html',
    replace: true,
    controller: MdAttachmentFormController
  };
});

angular.module('loomioApp').factory('MdAttachmentFormController', function() {
  return function($scope, $element, Records) {
    $scope.upload = function() {
      var file, i, len, ref, results;
      $scope.model.setErrors({});
      ref = $scope.files;
      results = [];
      for (i = 0, len = ref.length; i < len; i++) {
        file = ref[i];
        $scope.$emit('disableAttachmentForm');
        $scope.currentUpload = Records.attachments.upload(file, $scope.progress);
        results.push($scope.currentUpload.then($scope.success, $scope.failure)["finally"]($scope.reset));
      }
      return results;
    };
    $scope.selectFile = function() {
      return $element.find('input')[0].click();
    };
    $scope.progress = function(progress) {
      return $scope.percentComplete = Math.floor(100 * progress.loaded / progress.total);
    };
    $scope.abort = function() {
      if ($scope.currentUpload) {
        return $scope.currentUpload.abort();
      }
    };
    $scope.success = function(response) {
      var data;
      data = response.data || response;
      return _.each(data.attachments, function(attachment) {
        return $scope.model.newAttachmentIds.push(attachment.id);
      });
    };
    $scope.failure = function(response) {
      return $scope.model.setErrors(response.data.errors);
    };
    $scope.reset = function() {
      $scope.files = $scope.currentUpload = null;
      $scope.percentComplete = 0;
      return $scope.$emit('enableAttachmentForm');
    };
    $scope.reset();
    return $scope.$on('attachmentPasted', function(event, file) {
      $scope.files = [file];
      return $scope.upload();
    });
  };
});

angular.module('loomioApp').controller('MembershipRequestsPageController', function($routeParams, $rootScope, Records, LoadingService, AbilityService, FlashService, AppConfig) {
  $rootScope.$broadcast('currentComponent', {
    page: 'membershipRequestsPage'
  });
  this.init = (function(_this) {
    return function() {
      return Records.groups.findOrFetchById($routeParams.key).then(function(group) {
        if (AbilityService.canManageMembershipRequests(group)) {
          _this.group = group;
          Records.membershipRequests.fetchPendingByGroup(group.key, {
            per: 100
          });
          return Records.membershipRequests.fetchPreviousByGroup(group.key, {
            per: 100
          });
        } else {
          return $rootScope.$broadcast('pageError', {
            status: 403
          });
        }
      }, function(error) {
        return $rootScope.$broadcast('pageError', {
          status: 403
        });
      });
    };
  })(this);
  this.init();
  this.pendingRequests = (function(_this) {
    return function() {
      return _this.group.pendingMembershipRequests();
    };
  })(this);
  this.previousRequests = (function(_this) {
    return function() {
      return _this.group.previousMembershipRequests();
    };
  })(this);
  this.approve = (function(_this) {
    return function(membershipRequest) {
      return Records.membershipRequests.approve(membershipRequest).then(function() {
        return FlashService.success("membership_requests_page.messages.request_approved_success");
      });
    };
  })(this);
  this.ignore = (function(_this) {
    return function(membershipRequest) {
      return Records.membershipRequests.ignore(membershipRequest).then(function() {
        return FlashService.success("membership_requests_page.messages.request_ignored_success");
      });
    };
  })(this);
  this.noPendingRequests = (function(_this) {
    return function() {
      return _this.pendingRequests.length === 0;
    };
  })(this);
});

angular.module('loomioApp').controller('MembershipsPageController', function($routeParams, $rootScope, Records, LoadingService, ModalService, InvitationModal, RemoveMembershipForm, AbilityService, FlashService, ScrollService) {
  var filteredMemberships;
  $rootScope.$broadcast('currentComponent', {
    page: 'membershipsPage'
  });
  this.init = (function(_this) {
    return function(group) {
      if ((_this.group != null) || (group == null)) {
        return;
      }
      if (AbilityService.canViewMemberships(group)) {
        _this.group = group;
        return Records.memberships.fetchByGroup(_this.group.key, {
          per: _this.group.membershipsCount
        }).then(function() {
          if ($routeParams.username != null) {
            return ScrollService.scrollTo("[data-username=" + $routeParams.username + "]");
          }
        });
      } else {
        return $rootScope.$broadcast('pageError', {
          status: 403
        }, group);
      }
    };
  })(this);
  this.fetchMemberships = (function(_this) {
    return function() {
      if (_this.fragment) {
        return Records.memberships.fetchByNameFragment(_this.fragment, _this.group.key);
      }
    };
  })(this);
  this.canAdministerGroup = function() {
    return AbilityService.canAdministerGroup(this.group);
  };
  this.canAddMembers = function() {
    return AbilityService.canAddMembers(this.group);
  };
  this.canRemoveMembership = function(membership) {
    return AbilityService.canRemoveMembership(membership);
  };
  this.canToggleAdmin = function(membership) {
    return this.canAdministerGroup(membership) && (!membership.admin || this.canRemoveMembership(membership));
  };
  this.toggleAdmin = function(membership) {
    var method;
    method = membership.admin ? 'makeAdmin' : 'removeAdmin';
    return Records.memberships[method](membership).then(function() {
      return FlashService.success("memberships_page.messages." + (_.snakeCase(method)) + "_success", {
        name: membership.userName()
      });
    });
  };
  this.openRemoveForm = function(membership) {
    return ModalService.open(RemoveMembershipForm, {
      membership: function() {
        return membership;
      }
    });
  };
  this.invitePeople = function() {
    return ModalService.open(InvitationModal, {
      group: (function(_this) {
        return function() {
          return _this.group;
        };
      })(this)
    });
  };
  filteredMemberships = (function(_this) {
    return function() {
      if (_this.fragment) {
        return _.filter(_this.group.memberships(), function(membership) {
          return _.contains(membership.userName().toLowerCase(), _this.fragment.toLowerCase());
        });
      } else {
        return _this.group.memberships();
      }
    };
  })(this);
  this.memberships = function() {
    return _.sortBy(filteredMemberships(), function(membership) {
      return membership.userName();
    });
  };
  this.name = function(membership) {
    return membership.userName();
  };
  Records.groups.findOrFetchById($routeParams.key).then(this.init, function(error) {
    return $rootScope.$broadcast('pageError', error);
  });
});

angular.module('loomioApp').directive('mentionField', function($compile, LmoUrlService) {
  return {
    restrict: 'A',
    priority: 1000,
    terminal: true,
    compile: function(elem) {
      elem.attr('mentio', true);
      elem.attr({
        'mentio-trigger-char': "'@'"
      });
      elem.attr({
        'mentio-items': 'mentionables'
      });
      elem.attr({
        'mentio-template-url': 'generated/components/thread_page/comment_form/mentio_menu.html'
      });
      elem.attr({
        'mentio-search': 'fetchByNameFragment(term)'
      });
      elem.attr({
        'ng-model-options': "{debounce: " + (elem.attr('mention-debounce') || 150) + "}"
      });
      elem.removeAttr('mention-field');
      elem.removeAttr('data-mention-field');
      elem.removeAttr('mention-debounce');
      return function(scope) {
        return $compile(elem)(scope);
      };
    }
  };
});

angular.module('loomioApp').directive('modalHeaderCancelButton', function() {
  return {
    restrict: 'E',
    templateUrl: 'generated/components/modal_header_cancel_button/modal_header_cancel_button.html',
    replace: true
  };
});

angular.module('loomioApp').factory('MoveThreadForm', function() {
  return {
    templateUrl: 'generated/components/move_thread_form/move_thread_form.html',
    controller: function($scope, $location, discussion, Session, FormService, Records, $translate) {
      $scope.discussion = discussion.clone();
      $scope.availableGroups = function() {
        return _.filter(Session.user().groups(), function(group) {
          return group.id !== discussion.groupId;
        });
      };
      $scope.submit = FormService.submit($scope, $scope.discussion, {
        submitFn: $scope.discussion.move,
        flashSuccess: 'move_thread_form.messages.success',
        flashOptions: {
          name: function() {
            return $scope.discussion.group().name;
          }
        }
      });
      $scope.updateTarget = function() {
        return $scope.targetGroup = Records.groups.find($scope.discussion.groupId);
      };
      $scope.updateTarget();
      return $scope.moveThread = function() {
        if ($scope.discussion["private"] && $scope.targetGroup.privacyIsOpen()) {
          if (confirm($translate.instant('move_thread_form.confirm_change_to_private_thread', {
            groupName: $scope.targetGroup.name
          }))) {
            return $scope.submit();
          }
        } else {
          return $scope.submit();
        }
      };
    }
  };
});

angular.module('loomioApp').factory('MuteExplanationModal', function() {
  return {
    templateUrl: 'generated/components/mute_explanation_modal/mute_explanation_modal.html',
    controller: function($scope, thread, Records, FlashService, ThreadService) {
      $scope.thread = thread;
      $scope.previousVolume = $scope.thread.volume();
      return $scope.muteThread = function() {
        return ThreadService.mute($scope.thread).then(function() {
          return $scope.$close();
        });
      };
    }
  };
});

angular.module('loomioApp').directive('navbar', function() {
  return {
    scope: {},
    restrict: 'E',
    templateUrl: 'generated/components/navbar/navbar.html',
    replace: true,
    controller: function($scope, $rootScope, $window, Records, ModalService, AuthModal, AppConfig, AbilityService) {
      var parser;
      parser = document.createElement('a');
      parser.href = AppConfig.baseUrl;
      $scope.showNavbar = true;
      $scope.$on('toggleNavbar', function(event, show) {
        return $scope.showNavbar = show;
      });
      $scope.hostName = parser.hostname;
      $scope.isLoggedIn = AbilityService.isLoggedIn;
      $scope.toggleSidebar = function() {
        return $rootScope.$broadcast('toggleSidebar');
      };
      return $scope.signIn = function() {
        return ModalService.open(AuthModal);
      };
    }
  };
});

angular.module('loomioApp').directive('navbarSearch', function() {
  return {
    scope: {},
    restrict: 'E',
    templateUrl: 'generated/components/navbar/navbar_search.html',
    replace: true,
    controller: function($scope, $element, $timeout, Session, Records, SearchResultModel, KeyEventService) {
      var highlightables;
      $scope.searchResults = [];
      $scope.query = '';
      $scope.focused = false;
      $scope.highlighted = null;
      $scope.closeSearchDropdown = function(e) {
        var target;
        if (e != null) {
          target = e.currentTarget;
          if (e.ctrlKey || e.metaKey) {
            target.target = '_blank';
          }
        }
        return $timeout(function() {
          if (target != null) {
            target.click();
          }
          $scope.focused = false;
          $scope.query = '';
          return $scope.updateHighlighted(null);
        });
      };
      $scope.handleSearchBlur = function() {
        if ($element[0].contains(document.activeElement)) {
          return;
        }
        return $scope.closeSearchDropdown();
      };
      $scope.showDropdown = function() {
        return $scope.focused && $scope.query;
      };
      highlightables = function() {
        return document.querySelectorAll('.navbar-search-list-option');
      };
      $scope.highlightedSelection = function() {
        return highlightables()[$scope.highlighted];
      };
      $scope.updateHighlighted = function(index) {
        $scope.highlighted = index;
        _.map(highlightables(), function(element) {
          return element.classList.remove("lmo-active");
        });
        if ($scope.highlightedSelection() != null) {
          $scope.highlightedSelection().firstChild.focus();
          return $scope.highlightedSelection().classList.add("lmo-active");
        }
      };
      $scope.searchField = function() {
        return angular.element(document.querySelector('#primary-search-input'))[0];
      };
      $scope.shouldExecuteWithSearchField = function(active, event) {
        return active === $scope.searchField() || KeyEventService.defaultShouldExecute(active, event);
      };
      KeyEventService.registerKeyEvent($scope, 'pressedEsc', function() {
        $scope.searchField().blur();
        return $scope.query = '';
      }, $scope.shouldExecuteWithSearchField);
      KeyEventService.registerKeyEvent($scope, 'pressedSlash', function(active) {
        $scope.searchField().focus();
        return $scope.query = '';
      });
      KeyEventService.registerKeyEvent($scope, 'pressedEnter', function() {
        if ($scope.highlightedSelection()) {
          return $scope.closeSearchDropdown(document.activeElement);
        }
      }, $scope.shouldExecuteWithSearchField);
      KeyEventService.registerKeyEvent($scope, 'pressedUpArrow', function(active) {
        if (isNaN(parseInt($scope.highlighted)) || $scope.highlighted === 0) {
          return $scope.updateHighlighted(highlightables().length - 1);
        } else {
          return $scope.updateHighlighted($scope.highlighted - 1);
        }
      }, $scope.shouldExecuteWithSearchField);
      KeyEventService.registerKeyEvent($scope, 'pressedDownArrow', function(active) {
        if (isNaN(parseInt($scope.highlighted)) || $scope.highlighted === highlightables().length - 1) {
          return $scope.updateHighlighted(0);
        } else {
          return $scope.updateHighlighted($scope.highlighted + 1);
        }
      }, $scope.shouldExecuteWithSearchField);
      $scope.clearAndFocusInput = function() {
        $scope.closeSearchDropdown();
        return $scope.searchField().focus();
      };
      $scope.queryPresent = function() {
        return $scope.query.length > 0;
      };
      $scope.queryEmpty = function() {
        return $scope.query.length === 0;
      };
      $scope.noResultsFound = function() {
        return !$scope.searching && $scope.searchResults.length === 0;
      };
      return $scope.getSearchResults = function(query) {
        if (query != null) {
          $scope.updateHighlighted(null);
          $scope.currentSearchQuery = query;
          $scope.searching = true;
          return Records.searchResults.fetchByFragment($scope.query).then(function() {
            $scope.searchResults = Records.searchResults.find({
              query: query
            });
            _.map($scope.searchResults, function(result) {
              return result.remove();
            });
            if ($scope.currentSearchQuery === query) {
              return $scope.searching = false;
            }
          });
        }
      };
    }
  };
});

angular.module('loomioApp').directive('searchResult', function() {
  return {
    scope: {
      result: '='
    },
    restrict: 'E',
    templateUrl: 'generated/components/navbar/search_result.html',
    replace: true,
    controller: function($scope, $rootScope, Records) {
      var escapeForRegex;
      escapeForRegex = function(str) {
        return str.replace(/\/|\?|\*|\.|\(|\)/g, '');
      };
      $scope.rawDiscussionBlurb = function() {
        return escapeForRegex($scope.result.blurb.replace(/\<\/?b\>/g, ''));
      };
      $scope.showBlurbLeader = function() {
        return !escapeForRegex($scope.result.description).match(RegExp("^" + ($scope.rawDiscussionBlurb())));
      };
      $scope.showBlurbTrailer = function() {
        return !escapeForRegex($scope.result.description).match(RegExp(($scope.rawDiscussionBlurb()) + "$"));
      };
    }
  };
});

angular.module('loomioApp').directive('notification', function() {
  return {
    scope: {
      notification: '='
    },
    restrict: 'E',
    templateUrl: 'generated/components/notification/notification.html',
    replace: true,
    controller: function($scope, Records) {
      $scope.actor = function() {
        return $scope.membershipRequestActor || $scope.notification.actor();
      };
      if ($scope.notification.kind === 'membership_requested') {
        $scope.membershipRequestActor = Records.users.build({
          name: $scope.notification.translationValues.name,
          avatarInitials: $scope.notification.translationValues.name.toString().split(' ').map(function(n) {
            return n[0];
          }).join(''),
          avatarKind: 'initials'
        });
      }
    }
  };
});

angular.module('loomioApp').directive('notifications', function() {
  return {
    scope: {},
    restrict: 'E',
    templateUrl: 'generated/components/notifications/notifications.html',
    replace: true,
    controller: function($scope, $rootScope, Records, AppConfig) {
      var notificationsView, unreadView;
      $scope.toggle = function(menu) {
        if (document.querySelector('.md-open-menu-container.md-active .notifications__menu-content')) {
          return $scope.close(menu);
        } else {
          return $scope.open(menu);
        }
      };
      $scope.open = function(menu) {
        menu.open();
        Records.notifications.viewed();
        return $rootScope.$broadcast('notificationsOpen');
      };
      $scope.close = function(menu) {
        menu.close();
        return $rootScope.$broadcast('notificationsClosed');
      };
      notificationsView = Records.notifications.collection.addDynamicView("notifications").applyFind({
        kind: {
          $in: AppConfig.notifications.kinds
        }
      });
      unreadView = Records.notifications.collection.addDynamicView("unread").applyFind({
        kind: {
          $in: AppConfig.notifications.kinds
        }
      }).applyFind({
        viewed: {
          $ne: true
        }
      });
      $scope.notifications = function() {
        return notificationsView.data();
      };
      $scope.unreadCount = (function(_this) {
        return function() {
          return unreadView.data().length;
        };
      })(this);
      $scope.hasUnread = (function(_this) {
        return function() {
          return $scope.unreadCount() > 0;
        };
      })(this);
    }
  };
});

angular.module('loomioApp').factory('OnlyCoordinatorModal', function() {
  return {
    templateUrl: 'generated/components/only_coordinator_modal/only_coordinator_modal.html',
    controller: function($scope, $location, Session, LmoUrlService) {
      $scope.groups = function() {
        return _.filter(Session.user().groups(), function(group) {
          return _.contains(group.adminIds(), Session.user().id) && !group.hasMultipleAdmins;
        });
      };
      return $scope.redirectToGroup = function(group) {
        $location.path(LmoUrlService.group(group));
        return $scope.$close();
      };
    }
  };
});

angular.module('loomioApp').directive('outlet', function($compile, AppConfig) {
  return {
    scope: {
      model: '=?'
    },
    restrict: 'E',
    replace: true,
    link: function(scope, elem, attrs) {
      var compileHtml, shouldCompile;
      shouldCompile = function(outlet) {
        var group;
        if ((scope.model == null) || (scope.model.group == null)) {
          return true;
        }
        if (!((outlet.experimental != null) || (outlet.plans != null))) {
          return true;
        }
        group = scope.model.group().parentOrSelf();
        if ((outlet.experimental != null) && group.enableExperiments) {
          return true;
        }
        if (_.include(outlet.plans, group.subscriptionPlan)) {
          return true;
        }
        return false;
      };
      compileHtml = function(model, component) {
        var modelDirective;
        if (model) {
          modelDirective = model.constructor.singular + "='model'";
        }
        return $compile("<" + (_.snakeCase(component)) + " " + modelDirective + " />");
      };
      return _.map(AppConfig.plugins.outlets[_.snakeCase(attrs.name)], function(outlet) {
        if (shouldCompile(outlet)) {
          return elem.append(compileHtml(scope.model, outlet.component)(scope));
        }
      });
    }
  };
});

angular.module('loomioApp').directive('pendingEmailForm', function($translate, KeyEventService) {
  return {
    scope: {
      emails: '='
    },
    restrict: 'E',
    templateUrl: 'generated/components/pending_email_form/pending_email_form.html',
    replace: true,
    controller: function($scope) {
      $scope.newEmail = '';
      $scope.addIfValid = function() {
        $scope.emailValidationError = null;
        $scope.checkEmailNotEmpty();
        $scope.checkEmailValid();
        $scope.checkEmailAvailable();
        if (!$scope.emailValidationError) {
          return $scope.add();
        }
      };
      $scope.add = function() {
        if (!($scope.newEmail.length > 0)) {
          return;
        }
        $scope.emails.push($scope.newEmail);
        $scope.newEmail = '';
        return $scope.emailValidationError = null;
      };
      $scope.submit = function() {
        $scope.emailValidationError = null;
        $scope.checkEmailValid();
        $scope.checkEmailAvailable();
        if (!$scope.emailValidationError) {
          $scope.add();
          return $scope.$emit('emailsSubmitted');
        }
      };
      $scope.checkEmailNotEmpty = function() {
        if ($scope.newEmail.length <= 0) {
          return $scope.emailValidationError = $translate.instant('pending_email_form.email_empty');
        }
      };
      $scope.checkEmailValid = function() {
        if ($scope.newEmail.length > 0 && !$scope.newEmail.match(/[^\s,;<>]+?@[^\s,;<>]+\.[^\s,;<>]+/g)) {
          return $scope.emailValidationError = $translate.instant('pending_email_form.email_invalid');
        }
      };
      $scope.checkEmailAvailable = function() {
        if (_.contains($scope.emails, $scope.newEmail)) {
          return $scope.emailValidationError = $translate.instant('pending_email_form.email_exists', {
            email: $scope.newEmail
          });
        }
      };
      $scope.remove = function(email) {
        return _.pull($scope.emails, email);
      };
      return KeyEventService.registerKeyEvent($scope, 'pressedEnter', $scope.add, function(active) {
        return active.classList.contains('poll-common-share-form__add-option-input');
      });
    }
  };
});

angular.module('loomioApp').directive('pieChart', function(AppConfig) {
  return {
    template: '<div class="pie-chart"></div>',
    replace: true,
    scope: {
      votes: '=',
      diameter: '@'
    },
    restrict: 'E',
    controller: function($scope, $element) {
      var arcPath, colors, draw, half, radius, shapes, sortedPositions, uniquePositionsCount;
      draw = SVG($element[0]).size('100%', '100%');
      half = $scope.diameter / 2.0;
      radius = half;
      shapes = [];
      arcPath = function(startAngle, endAngle) {
        var rad, x1, x2, y1, y2;
        rad = Math.PI / 180;
        x1 = half + radius * Math.cos(-startAngle * rad);
        x2 = half + radius * Math.cos(-endAngle * rad);
        y1 = half + radius * Math.sin(-startAngle * rad);
        y2 = half + radius * Math.sin(-endAngle * rad);
        return ["M", half, half, "L", x1, y1, "A", radius, radius, 0, +(endAngle - startAngle > 180), 0, x2, y2, "z"].join(' ');
      };
      uniquePositionsCount = function() {
        return _.sum($scope.votes, function(num) {
          return +(num > 0);
        });
      };
      sortedPositions = function() {
        return _.sortBy(_.pairs($scope.votes), function(arg) {
          var _, count;
          _ = arg[0], count = arg[1];
          return -count;
        });
      };
      colors = {
        agree: "#00D177",
        yes: "#00D177",
        abstain: "#F6A82B",
        disagree: "#F96268",
        no: "#F96168",
        block: "#CE261B"
      };
      return $scope.$watchCollection('votes', function() {
        var position, start;
        _.each(shapes, function(shape) {
          return shape.remove();
        });
        start = 90;
        switch (uniquePositionsCount()) {
          case 0:
            return shapes.push(draw.circle($scope.diameter).attr({
              'stroke-width': 0,
              fill: '#aaa'
            }));
          case 1:
            position = sortedPositions()[0][0];
            return shapes.push(draw.circle($scope.diameter).attr({
              'stroke-width': 0,
              fill: colors[position]
            }));
          default:
            return _.each(sortedPositions(), function(arg) {
              var angle, position, votes;
              position = arg[0], votes = arg[1];
              if (!(votes > 0)) {
                return;
              }
              angle = 360 / _.sum($scope.votes) * votes;
              shapes.push(draw.path(arcPath(start, start + angle)).attr({
                'stroke-width': 0,
                fill: colors[position]
              }));
              return start += angle;
            });
        }
      });
    }
  };
});

angular.module('loomioApp').directive('pieWithPosition', function() {
  return {
    scope: {
      proposal: '='
    },
    restrict: 'E',
    templateUrl: 'generated/components/pie_with_position/pie_with_position.html',
    replace: true,
    controller: function($scope, Session) {
      return $scope.lastVoteByCurrentUser = function(proposal) {
        return proposal.lastVoteByUser(Session.user());
      };
    }
  };
});

angular.module('loomioApp').controller('PollPageController', function($scope, $rootScope, $routeParams, CommunityService, MessageChannelService, Records, $location, ModalService, PollService, PollCommonOutcomeModal, PollCommonEditVoteModal, AddCommunityModal, PollCommonShareModal, Session) {
  $rootScope.$broadcast('currentComponent', {
    page: 'pollPage',
    skipScroll: true
  });
  this.init = (function(_this) {
    return function(poll) {
      if (poll && (_this.poll == null)) {
        _this.poll = poll;
        $rootScope.$broadcast('setTitle', _this.poll.title);
        MessageChannelService.subscribeToPoll(_this.poll);
        if ($location.search().add_community) {
          ModalService.open(AddCommunityModal, {
            community: function() {
              return CommunityService.buildCommunity(_this.poll, $location.search().add_community);
            }
          });
        }
        if ($location.search().share) {
          ModalService.open(PollCommonShareModal, {
            poll: function() {
              return _this.poll;
            }
          });
        }
        if ($location.search().set_outcome) {
          ModalService.open(PollCommonOutcomeModal, {
            outcome: function() {
              return Records.outcomes.build({
                pollId: _this.poll.id
              });
            }
          });
        }
        if ($location.search().change_vote) {
          return ModalService.open(PollCommonEditVoteModal, {
            stance: function() {
              return PollService.lastStanceBy(Session.participant(), _this.poll);
            }
          });
        }
      }
    };
  })(this);
  Records.polls.findOrFetchById($routeParams.key).then(this.init, function(error) {
    return $rootScope.$broadcast('pageError', error);
  });
});

angular.module('loomioApp').controller('PollsPageController', function($scope, $location, $q, $rootScope, AppConfig, Records, Session, AbilityService, LoadingService, ModalService, PollCommonStartModal, RecordLoader) {
  var now;
  $rootScope.$broadcast('currentComponent', {
    page: 'pollsPage'
  });
  this.statusFilters = _.map(AppConfig.searchFilters.status, function(filter) {
    return {
      name: _.capitalize(filter),
      value: filter
    };
  });
  this.groupFilters = _.map(Session.user().groups(), function(group) {
    return {
      name: group.fullName,
      value: group.key
    };
  });
  this.statusFilter = $location.search().status;
  this.groupFilter = $location.search().group_key;
  now = moment();
  this.pollImportance = (function(_this) {
    return function(poll) {
      return poll.importance(now);
    };
  })(this);
  this.loadMore = (function(_this) {
    return function() {
      return _this.loader.loadMore().then(function(response) {
        return _this.pollIds = _this.pollIds.concat(_.pluck(response.polls, 'id'));
      });
    };
  })(this);
  LoadingService.applyLoadingFunction(this, 'loadMore');
  this.fetchRecords = (function(_this) {
    return function() {
      $location.search('group_key', _this.groupFilter);
      $location.search('status', _this.statusFilter);
      _this.pollIds = [];
      _this.loader = new RecordLoader({
        collection: 'polls',
        path: 'search',
        params: $location.search(),
        per: 25
      });
      Records.polls.searchResultsCount($location.search()).then(function(response) {
        return _this.pollsCount = response;
      });
      return _this.loader.fetchRecords().then(function(response) {
        _this.group = Records.groups.find($location.search().group_key);
        return _this.pollIds = _.pluck(response.polls, 'id');
      }, function(error) {
        return $rootScope.$broadcast('pageError', error);
      });
    };
  })(this);
  LoadingService.applyLoadingFunction(this, 'fetchRecords');
  this.fetchRecords();
  this.loadedCount = function() {
    return this.pollCollection.polls().length;
  };
  this.canLoadMore = function() {
    return !this.fragment && this.loadedCount() < this.pollsCount;
  };
  this.startNewPoll = function() {
    return ModalService.open(PollCommonStartModal, {
      poll: function() {
        return Records.polls.build({
          authorId: Session.user().id
        });
      }
    });
  };
  this.searchPolls = (function(_this) {
    return function() {
      if (_this.fragment) {
        return Records.polls.search({
          query: _this.fragment,
          per: 10
        });
      } else {
        return $q.when();
      }
    };
  })(this);
  LoadingService.applyLoadingFunction(this, 'searchPolls');
  this.fetching = function() {
    return this.fetchRecordsExecuting || this.loadMoreExecuting;
  };
  this.pollCollection = {
    polls: (function(_this) {
      return function() {
        return _.sortBy(_.filter(Records.polls.find(_this.pollIds), function(poll) {
          return _.isEmpty(_this.fragment) || poll.title.match(RegExp("" + _this.fragment, "i"));
        }), '-createdAt');
      };
    })(this)
  };
});

angular.module('loomioApp').controller('PreviousProposalsPageController', function($scope, $rootScope, $routeParams, Records, AbilityService) {
  $rootScope.$broadcast('currentComponent', {
    page: 'previousProposalsPage'
  });
  Records.groups.findOrFetchById($routeParams.key).then((function(_this) {
    return function(group) {
      return _this.group = group;
    };
  })(this));
  Records.proposals.fetchClosedByGroup($routeParams.key);
});

angular.module('loomioApp').controller('ProfilePageController', function($rootScope, Records, FormService, $location, AbilityService, ModalService, ChangePictureForm, ChangePasswordForm, DeactivateUserForm, $translate, Session, AppConfig, DeactivationModal) {
  $rootScope.$broadcast('currentComponent', {
    page: 'profilePage'
  });
  this.init = (function(_this) {
    return function() {
      if (!AbilityService.isLoggedIn()) {
        return;
      }
      _this.user = Session.user().clone();
      $translate.use(_this.user.locale);
      return _this.submit = FormService.submit(_this, _this.user, {
        flashSuccess: 'profile_page.messages.updated',
        submitFn: Records.users.updateProfile,
        successCallback: _this.init
      });
    };
  })(this);
  this.init();
  this.availableLocales = function() {
    return AppConfig.locales;
  };
  this.changePicture = function() {
    return ModalService.open(ChangePictureForm);
  };
  this.changePassword = function() {
    return ModalService.open(ChangePasswordForm);
  };
  this.deactivateUser = function() {
    return ModalService.open(DeactivationModal);
  };
});

angular.module('loomioApp').directive('progressChart', function(AppConfig) {
  return {
    template: '<div class="progress-chart"></div>',
    replace: true,
    scope: {
      stanceCounts: '=',
      goal: '=',
      size: '@'
    },
    restrict: 'E',
    controller: function($scope, $element) {
      var draw;
      draw = SVG($element[0]).size('100%', '100%');
      return $scope.$watchCollection('stanceCounts', function() {
        var y;
        y = 0;
        _.each($scope.stanceCounts, function(count, index) {
          var height;
          height = ($scope.size * _.max([parseInt(count), 0])) / $scope.goal;
          draw.rect($scope.size, height).fill(AppConfig.pollColors.count[index]).x(0).y($scope.size - height - y);
          return y += height;
        });
        draw.circle($scope.size / 2).fill("#fff").x($scope.size / 4).y($scope.size / 4);
        return draw.text(($scope.stanceCounts[0] || 0).toString()).font({
          size: 16,
          anchor: 'middle'
        }).x($scope.size / 2).y(($scope.size / 4) + 3);
      });
    }
  };
});

angular.module('loomioApp').directive('proposalAccordian', function() {
  return {
    scope: {
      model: '=',
      selectedProposalId: '=?'
    },
    restrict: 'E',
    templateUrl: 'generated/components/proposal_accordian/proposal_accordian.html',
    replace: true,
    controller: function($scope) {
      $scope.$on('collapseProposal', function(event) {
        return $scope.selectedProposalId = null;
      });
      return $scope.selectProposal = (function(_this) {
        return function(proposal) {
          return $scope.selectedProposalId = proposal.id;
        };
      })(this);
    }
  };
});

angular.module('loomioApp').directive('proposalClosingTime', function() {
  return {
    scope: {
      proposal: '='
    },
    restrict: 'E',
    templateUrl: 'generated/components/proposal_closing_time/proposal_closing_time.html',
    replace: true
  };
});

angular.module('loomioApp').directive('closingAtField', function(AppConfig) {
  return {
    scope: {
      proposal: '='
    },
    restrict: 'E',
    templateUrl: 'generated/components/proposal_form/closing_at_field.html',
    replace: true,
    controller: function($scope) {
      var j, results, updateClosingAt;
      $scope.hours = (function() {
        results = [];
        for (j = 1; j <= 24; j++){ results.push(j); }
        return results;
      }).apply(this);
      $scope.closingHour = $scope.proposal.closingAt.format('H');
      $scope.closingDate = $scope.proposal.closingAt.toDate();
      updateClosingAt = function() {
        return $scope.proposal.closingAt = moment($scope.closingDate).startOf('day').add($scope.closingHour, 'hours');
      };
      $scope.$watch('closingDate', updateClosingAt);
      $scope.$watch('closingHour', updateClosingAt);
      $scope.hours = _.times(24, function(i) {
        return i;
      });
      $scope.times = _.times(24, function(i) {
        if (i < 10) {
          i = "0" + i;
        }
        return moment("2015-01-01 " + i + ":00").format('h a');
      });
      $scope.dateToday = moment().format('YYYY-MM-DD');
      return $scope.timeZone = AppConfig.timeZone;
    }
  };
});

angular.module('loomioApp').factory('ProposalForm', function() {
  return {
    templateUrl: 'generated/components/proposal_form/proposal_form.html',
    controller: function($scope, $rootScope, proposal, FormService, MentionService, KeyEventService, ScrollService, EmojiService, UserHelpService, Records, AttachmentService) {
      var actionName;
      $scope.nineWaysArticleLink = function() {
        return UserHelpService.nineWaysArticleLink();
      };
      $scope.proposal = proposal.clone();
      actionName = $scope.proposal.isNew() ? 'created' : 'updated';
      $scope.submit = FormService.submit($scope, $scope.proposal, {
        flashSuccess: "proposal_form.messages." + actionName,
        drafts: true,
        successEvent: 'proposalCreated',
        successCallback: function() {
          $rootScope.$broadcast('setSelectedProposal');
          Records.attachments.find({
            attachableId: proposal.id,
            attachableType: 'Motion'
          }).filter(function(attachment) {
            return !_.contains(proposal.attachment_ids, attachment.id);
          }).map(function(attachment) {
            return attachment.remove();
          });
          return ScrollService.scrollTo('#current-proposal-card-heading');
        }
      });
      $scope.descriptionSelector = '.proposal-form__details-field';
      EmojiService.listen($scope, $scope.proposal, 'description', $scope.descriptionSelector);
      KeyEventService.submitOnEnter($scope);
      MentionService.applyMentions($scope, $scope.proposal);
      return AttachmentService.listenForAttachments($scope, $scope.proposal);
    }
  };
});

angular.module('loomioApp').controller('ProposalRedirectController', function($router, $timeout, $rootScope, $routeParams, $location, Records, LmoUrlService) {
  $rootScope.$broadcast('currentComponent', 'proposalRedirect');
  Records.proposals.findOrFetchById($routeParams.key).then((function(_this) {
    return function(proposal) {
      return Records.discussions.findOrFetchById(proposal.discussionId).then(function(discussion) {
        var params;
        params = {};
        if ($location.search().position != null) {
          params.position = $location.search().position;
          $location.search('position', null);
        }
        return $timeout(function() {
          return $location.path(LmoUrlService.proposal(proposal, params));
        });
      });
    };
  })(this), function(error) {
    return $rootScope.$broadcast('pageError', error);
  });
});

angular.module('loomioApp').factory('RegisteredAppForm', function() {
  return {
    templateUrl: 'generated/components/registered_app_form/registered_app_form.html',
    controller: function($scope, $location, application, Records, FormService, LmoUrlService, KeyEventService) {
      var actionName;
      $scope.application = application.clone();
      actionName = $scope.application.isNew() ? 'created' : 'updated';
      $scope.submit = FormService.submit($scope, $scope.application, {
        flashSuccess: "registered_app_form.messages." + actionName,
        flashOptions: {
          name: function() {
            return $scope.application.name;
          }
        },
        successCallback: function(response) {
          if ($scope.application.isNew()) {
            return $location.path(LmoUrlService.oauthApplication(response.oauth_applications[0]));
          }
        }
      });
      $scope.upload = FormService.upload($scope, $scope.application, {
        flashSuccess: 'registered_app_form.messages.logo_changed',
        submitFn: $scope.application.uploadLogo,
        loadingMessage: 'common.action.uploading',
        skipClose: true,
        successCallback: function(response) {
          return $scope.application.logoUrl = response.data.oauth_applications[0].logo_url;
        }
      });
      $scope.clickFileUpload = function() {
        return document.querySelector('.registered-app-form__logo-input').click();
      };
      return KeyEventService.submitOnEnter($scope);
    }
  };
});

angular.module('loomioApp').controller('RegisteredAppPageController', function($scope, $rootScope, $routeParams, Records, FlashService, ModalService, RegisteredAppForm, RemoveAppForm) {
  this.init = (function(_this) {
    return function(application) {
      if (application && (_this.application == null)) {
        _this.application = application;
        $rootScope.$broadcast('currentComponent', {
          page: 'oauthApplicationPage'
        });
        return $rootScope.$broadcast('setTitle', _this.application.name);
      }
    };
  })(this);
  this.init(Records.oauthApplications.find(parseInt($routeParams.id)));
  Records.oauthApplications.findOrFetchById(parseInt($routeParams.id)).then(this.init, function(error) {
    return $rootScope.$broadcast('pageError', error);
  });
  this.copied = function() {
    return FlashService.success('common.copied');
  };
  this.openRemoveForm = function() {
    return ModalService.open(RemoveAppForm, {
      application: (function(_this) {
        return function() {
          return _this.application;
        };
      })(this)
    });
  };
  this.openEditForm = function() {
    return ModalService.open(RegisteredAppForm, {
      application: (function(_this) {
        return function() {
          return _this.application;
        };
      })(this)
    });
  };
});

angular.module('loomioApp').controller('RegisteredAppsPageController', function($scope, $rootScope, Records, ModalService, RegisteredAppForm, RemoveAppForm) {
  $rootScope.$broadcast('currentComponent', {
    page: 'registeredAppsPage'
  });
  $rootScope.$broadcast('setTitle', 'OAuth Application Dashboard');
  this.loading = true;
  this.applications = function() {
    return Records.oauthApplications.collection.data;
  };
  Records.oauthApplications.fetchOwned().then((function(_this) {
    return function() {
      return _this.loading = false;
    };
  })(this));
  this.openApplicationForm = function(application) {
    return ModalService.open(RegisteredAppForm, {
      application: function() {
        return Records.oauthApplications.build();
      }
    });
  };
  this.openDestroyForm = function(application) {
    return ModalService.open(RemoveAppForm, {
      application: function() {
        return application;
      }
    });
  };
});

angular.module('loomioApp').factory('RemoveAppForm', function() {
  return {
    templateUrl: 'generated/components/remove_app_form/remove_app_form.html',
    controller: function($scope, $rootScope, application, FlashService) {
      $scope.application = application;
      return $scope.submit = function() {
        return $scope.application.destroy().then(function() {
          FlashService.success('remove_app_form.messages.success', {
            name: $scope.application.name
          });
          return $scope.$close();
        }, function() {
          $rootScope.$broadcast('pageError', 'cantDestroyApplication', $scope.application);
          return $scope.$close();
        });
      };
    }
  };
});

angular.module('loomioApp').factory('RemoveMembershipForm', function() {
  return {
    templateUrl: 'generated/components/remove_membership_form/remove_membership_form.html',
    controller: function($scope, $location, $rootScope, membership, FlashService, Session) {
      $scope.membership = membership;
      return $scope.submit = function() {
        return $scope.membership.destroy().then(function() {
          FlashService.success('memberships_page.messages.remove_member_success', {
            name: $scope.membership.userName()
          });
          $scope.$close();
          if ($scope.membership.user() === Session.user()) {
            return $location.path("/dashboard");
          }
        }, function() {
          $rootScope.$broadcast('pageError', 'cantDestroyMembership', $scope.membership);
          return $scope.$close();
        });
      };
    }
  };
});

angular.module('loomioApp').factory('RevokeAppForm', function() {
  return {
    templateUrl: 'generated/components/revoke_app_form/revoke_app_form.html',
    controller: function($scope, $rootScope, application, FlashService) {
      $scope.application = application;
      return $scope.submit = function() {
        return $scope.application.revokeAccess().then(function() {
          FlashService.success('revoke_app_form.messages.success', {
            name: $scope.application.name
          });
          return $scope.$close();
        }, function() {
          $rootScope.$broadcast('pageError', 'cantRevokeApplication', $scope.application);
          return $scope.$close();
        });
      };
    }
  };
});

angular.module('loomioApp').directive('sidebar', function() {
  return {
    scope: false,
    restrict: 'E',
    templateUrl: 'generated/components/sidebar/sidebar.html',
    replace: true,
    controller: function($scope, Session, $rootScope, $window, RestfulClient, ThreadQueryService, UserHelpService, AppConfig, IntercomService, $mdMedia, $mdSidenav, LmoUrlService, Records, ModalService, GroupModal, DiscussionForm, AbilityService) {
      var availableGroups;
      $scope.currentState = "";
      $scope.showSidebar = true;
      $scope.hasAnyGroups = function() {
        return Session.user().hasAnyGroups();
      };
      availableGroups = function() {
        return _.filter(Session.user().groups(), function(group) {
          return AbilityService.canAddMembers(group);
        });
      };
      $scope.currentGroup = function() {
        if (availableGroups().length === 1) {
          return _.first(availableGroups());
        }
        return _.find(availableGroups(), function(g) {
          return g.id === Session.currentGroupId();
        }) || Records.groups.build();
      };
      $scope.$on('toggleSidebar', function(event, show) {
        if (!_.isUndefined(show)) {
          return $scope.showSidebar = show;
        } else {
          return $scope.showSidebar = !$scope.showSidebar;
        }
      });
      $scope.$on('currentComponent', function(el, component) {
        return $scope.currentState = component;
      });
      $scope.onPage = function(page, key, filter) {
        switch (page) {
          case 'groupPage':
            return $scope.currentState.key === key;
          case 'dashboardPage':
            return $scope.currentState.page === page && $scope.currentState.filter === filter;
          default:
            return $scope.currentState.page === page;
        }
      };
      $scope.groupUrl = function(group) {
        return LmoUrlService.group(group);
      };
      $scope.signOut = function() {
        return Session.logout();
      };
      $scope.helpLink = function() {
        return UserHelpService.helpLink();
      };
      $scope.unreadThreadCount = function() {
        return ThreadQueryService.filterQuery(['show_unread', 'only_threads_in_my_groups'], {
          queryType: 'inbox'
        }).length();
      };
      $scope.showContactUs = function() {
        return AppConfig.baseUrl === 'https://www.loomio.org/';
      };
      $scope.contactUs = function() {
        return IntercomService.contactUs();
      };
      $scope.sidebarItemSelected = function() {
        if (!$mdMedia("gt-md")) {
          return $mdSidenav('left').close();
        }
      };
      $scope.groups = function() {
        return Session.user().groups().concat(Session.user().orphanParents());
      };
      $scope.currentUser = function() {
        return Session.user();
      };
      $scope.startGroup = function() {
        return ModalService.open(GroupModal, {
          group: function() {
            return Records.groups.build();
          }
        });
      };
      return $scope.startThread = function() {
        return ModalService.open(DiscussionForm, {
          discussion: function() {
            return Records.discussions.build({
              groupId: $scope.currentGroup().id
            });
          }
        });
      };
    }
  };
});

angular.module('loomioApp').factory('SignedOutModal', function() {
  return {
    templateUrl: 'generated/components/signed_out_modal/signed_out_modal.html',
    controller: function($scope, preventClose, Session) {
      $scope.preventClose = preventClose;
      return $scope.submit = Session.logout;
    }
  };
});

angular.module('loomioApp').factory('SlackAddedModal', function(Records, ModalService, PollCommonStartModal) {
  return {
    templateUrl: 'generated/components/slack_added_modal/slack_added_modal.html',
    controller: function($scope, group) {
      $scope.group = group;
      return $scope.submit = function() {
        return ModalService.open(PollCommonStartModal, {
          poll: function() {
            return Records.polls.build();
          }
        });
      };
    }
  };
});

angular.module('loomioApp').directive('smartTime', function() {
  return {
    scope: {
      time: '='
    },
    restrict: 'E',
    templateUrl: 'generated/components/smart_time/smart_time.html',
    replace: true,
    controller: function($scope) {
      var format, now, sameDay, sameWeek, sameYear, time;
      time = moment($scope.time);
      now = moment();
      sameDay = function(time) {
        return time.year() === now.year() && time.month() === now.month() && time.date() === now.date();
      };
      sameWeek = function(time) {
        return time.year() === now.year() && time.month() === now.month() && time.week() === now.week();
      };
      sameYear = function(time) {
        return time.year() === now.year();
      };
      format = (function() {
        switch (false) {
          case !sameDay(time):
            return "h:mm a";
          case !sameWeek(time):
            return "ddd";
          case !sameYear(time):
            return "D MMM";
          default:
            return "MMM YYYY";
        }
      })();
      return $scope.value = time.format(format);
    }
  };
});

angular.module('loomioApp').directive('starToggle', function() {
  return {
    scope: {
      thread: '='
    },
    restrict: 'E',
    templateUrl: 'generated/components/star_toggle/star_toggle.html',
    replace: true,
    controller: function($scope, AbilityService) {
      $scope.isLoggedIn = AbilityService.isLoggedIn;
    }
  };
});

angular.module('loomioApp').controller('StartGroupPageController', function($scope, $location, $rootScope, Records, ModalService, InvitationModal) {
  $rootScope.$broadcast('currentComponent', {
    page: 'startGroupPage',
    skipScroll: true
  });
  this.init = function() {
    return this.group = Records.groups.build({
      name: $location.search().name,
      customFields: {
        pending_emails: _.compact(($location.search().pending_emails || "").split(','))
      }
    });
  };
  this.init();
  $scope.$on('createComplete', function(event, group) {
    return ModalService.open(InvitationModal, {
      group: function() {
        return group;
      }
    });
  });
});

angular.module('loomioApp').controller('StartPollPageController', function($scope, $location, $rootScope, $routeParams, Records, LoadingService, PollService, ModalService, PollCommonShareModal) {
  $rootScope.$broadcast('currentComponent', {
    page: 'startPollPage',
    skipScroll: true
  });
  this.poll = Records.polls.build({
    title: $location.search().title,
    pollType: $routeParams.poll_type,
    groupId: $location.search().group_id,
    communityId: $location.search().community_id,
    customFields: {
      pending_emails: _.compact(($location.search().pending_emails || "").split(','))
    }
  });
  this.icon = function() {
    return PollService.iconFor(this.poll);
  };
  $scope.$on('saveComplete', function(event, poll) {
    return ModalService.open(PollCommonShareModal, {
      poll: function() {
        return poll;
      }
    });
  });
  LoadingService.listenForLoading($scope);
});

angular.module('loomioApp').directive('threadLintel', function() {
  return {
    restrict: 'E',
    templateUrl: 'generated/components/thread_lintel/thread_lintel.html',
    replace: true,
    controller: function($scope, ScrollService) {
      $scope.show = function() {
        return $scope.showLintel && $scope.discussion;
      };
      $scope.scrollToThread = function() {
        return ScrollService.scrollTo('h1');
      };
      $scope.scrollToProposal = function() {
        return ScrollService.scrollTo('section.current-proposal-card');
      };
      $scope.$on('currentComponent', function(event, options) {
        return $scope.currentComponent = options['page'];
      });
      $scope.$on('viewingThread', function(event, discussion) {
        return $scope.discussion = discussion;
      });
      $scope.$on('showThreadLintel', function(event, bool) {
        return $scope.showLintel = bool;
      });
      $scope.$on('proposalInView', function(event, bool) {
        return $scope.proposalInView = bool;
      });
      $scope.$on('proposalButtonInView', function(event, bool) {
        return $scope.proposalButtonInView = bool;
      });
      return $scope.$on('threadPosition', function(event, discussion, position) {
        $scope.position = position;
        $scope.discussion = discussion;
        return $scope.positionPercent = (position / discussion.lastSequenceId) * 100;
      });
    }
  };
});

angular.module('loomioApp').controller('ThreadPageController', function($scope, $routeParams, $location, $rootScope, $window, $timeout, Records, KeyEventService, ModalService, ScrollService, AbilityService, Session, PaginationService, LmoUrlService, ProposalOutcomeForm, PollService) {
  var checkInView, handleCommentHash;
  $rootScope.$broadcast('currentComponent', {
    page: 'threadPage',
    skipScroll: true
  });
  this.requestedProposalKey = $routeParams.proposal || $location.search().proposal;
  this.requestedCommentId = parseInt($routeParams.comment || $location.search().comment);
  handleCommentHash = (function() {
    var match;
    if (match = $location.hash().match(/comment-(\d+)/)) {
      $location.search().comment = match[1];
      return $location.hash('');
    }
  })();
  this.performScroll = function() {
    ScrollService.scrollTo(this.elementToFocus(), {
      offset: 150
    });
    if (this.openVoteModal()) {
      $rootScope.$broadcast('triggerVoteForm', $location.search().position);
    }
    if (this.openOutcomeModal()) {
      ModalService.open(ProposalOutcomeForm, {
        proposal: (function(_this) {
          return function() {
            return _this.proposal;
          };
        })(this)
      });
    }
    return $location.url($location.path());
  };
  this.openVoteModal = function() {
    return $location.search().position && this.discussion.hasActiveProposal() && this.discussion.activeProposal().key === ($routeParams.proposal || $location.search().proposal || $routeParams.proposal) && AbilityService.canVoteOn(this.discussion.activeProposal());
  };
  this.openOutcomeModal = function() {
    return AbilityService.canCreateOutcomeFor(this.proposal) && ($routeParams.outcome != null) && (delete $routeParams.outcome);
  };
  this.elementToFocus = function() {
    if (this.proposal) {
      return "#proposal-" + this.proposal.key;
    } else if (this.comment) {
      return "#comment-" + this.comment.id;
    } else if (Records.events.findByDiscussionAndSequenceId(this.discussion, this.sequenceIdToFocus)) {
      return '.activity-card__last-read-activity';
    } else {
      return '.context-panel';
    }
  };
  this.threadElementsLoaded = function() {
    return this.eventsLoaded && this.proposalsLoaded;
  };
  this.init = (function(_this) {
    return function(discussion) {
      if (discussion && (_this.discussion == null)) {
        _this.discussion = discussion;
        _this.usePolls = PollService.usePollsFor(_this.discussion);
        _this.sequenceIdToFocus = parseInt($location.search().from || _this.discussion.lastReadSequenceId);
        _this.pageWindow = PaginationService.windowFor({
          current: _this.sequenceIdToFocus,
          min: _this.discussion.firstSequenceId,
          max: _this.discussion.lastSequenceId,
          pageType: 'activityItems'
        });
        $rootScope.$broadcast('viewingThread', _this.discussion);
        $rootScope.$broadcast('setTitle', _this.discussion.title);
        $rootScope.$broadcast('analyticsSetGroup', _this.discussion.group());
        return $rootScope.$broadcast('currentComponent', {
          page: 'threadPage',
          group: _this.discussion.group(),
          links: {
            canonical: LmoUrlService.discussion(_this.discussion, {}, {
              absolute: true
            }),
            rss: !_this.discussion["private"] ? LmoUrlService.discussion(_this.discussion) + '.xml' : void 0,
            prev: _this.pageWindow.prev != null ? LmoUrlService.discussion(_this.discussion, {
              from: _this.pageWindow.prev
            }) : void 0,
            next: _this.pageWindow.next != null ? LmoUrlService.discussion(_this.discussion, {
              from: _this.pageWindow.next
            }) : void 0
          },
          skipScroll: true
        });
      }
    };
  })(this);
  this.init(Records.discussions.find($routeParams.key));
  Records.discussions.findOrFetchById($routeParams.key).then(this.init, function(error) {
    return $rootScope.$broadcast('pageError', error);
  });
  $scope.$on('threadPageEventsLoaded', (function(_this) {
    return function(e, event) {
      if (_this.discussion.requireReloadFor(event)) {
        $window.location.reload();
      }
      _this.eventsLoaded = true;
      if (!isNaN(_this.requestedCommentId)) {
        _this.comment = Records.comments.find(_this.requestedCommentId);
      }
      if (_this.proposalsLoaded || !_this.discussion.anyClosedProposals()) {
        return _this.performScroll();
      }
    };
  })(this));
  $scope.$on('threadPageProposalsLoaded', (function(_this) {
    return function(event) {
      _this.proposalsLoaded = true;
      _this.proposal = Records.proposals.find(_this.requestedProposalKey);
      $rootScope.$broadcast('setSelectedProposal', _this.proposal);
      if (_this.eventsLoaded) {
        return _this.performScroll();
      }
    };
  })(this));
  this.hasClosedPolls = function() {
    return _.any(this.discussion.closedPolls());
  };
  this.canStartProposal = function() {
    return this.eventsLoaded && AbilityService.canStartProposal(this.discussion);
  };
  this.canViewMemberships = function() {
    return this.eventsLoaded && AbilityService.canViewMemberships(this.discussion.group());
  };
  this.proposalInView = function($inview) {
    return $rootScope.$broadcast('proposalInView', $inview);
  };
  this.proposalButtonInView = function($inview) {
    return $rootScope.$broadcast('proposalButtonInView', $inview);
  };
  checkInView = function() {
    return angular.element(window).triggerHandler('checkInView');
  };
  this.canStartPoll = function() {
    return this.usePolls && AbilityService.canStartPoll(this.discussion.group());
  };
  KeyEventService.registerKeyEvent($scope, 'pressedUpArrow', checkInView);
  KeyEventService.registerKeyEvent($scope, 'pressedDownArrow', checkInView);
});

angular.module('loomioApp').directive('threadPreview', function() {
  return {
    scope: {
      thread: '='
    },
    restrict: 'E',
    templateUrl: 'generated/components/thread_preview/thread_preview.html',
    replace: true,
    controller: function($scope, Records, Session, LmoUrlService, FlashService, ModalService, MuteExplanationModal, DismissExplanationModal, ThreadService, PollService) {
      $scope.lastVoteByCurrentUser = function(thread) {
        return thread.activeProposal().lastVoteByUser(Session.user());
      };
      $scope.dismiss = function() {
        if (!Session.user().hasExperienced("dismissThread")) {
          Records.users.saveExperience("dismissThread");
          return ModalService.open(DismissExplanationModal, {
            thread: function() {
              return $scope.thread;
            }
          });
        } else {
          $scope.thread.dismiss();
          return FlashService.success("dashboard_page.thread_dismissed");
        }
      };
      $scope.muteThread = function() {
        return ThreadService.mute($scope.thread);
      };
      $scope.unmuteThread = function() {
        return ThreadService.unmute($scope.thread);
      };
      $scope.translationData = function(thread) {
        return {
          position: $scope.lastVoteByCurrentUser(thread).position
        };
      };
      $scope.usePolls = function() {
        return PollService.usePollsFor($scope.thread);
      };
    }
  };
});

angular.module('loomioApp').directive('threadPreviewCollection', function() {
  return {
    scope: {
      query: '=',
      limit: '=?'
    },
    restrict: 'E',
    templateUrl: 'generated/components/thread_preview_collection/thread_preview_collection.html',
    replace: true,
    controller: function($scope) {
      return $scope.importance = function(thread) {
        if (thread.starred && thread.hasDecision()) {
          return -3;
        } else if (thread.hasDecision()) {
          return -2;
        } else if (thread.starred) {
          return -1;
        } else {
          return 0;
        }
      };
    }
  };
});

angular.module('loomioApp').directive('timeZoneSelect', function($translate, AppConfig, TimeService) {
  return {
    scope: {
      zone: '='
    },
    restrict: 'E',
    templateUrl: 'generated/components/time_zone_select/time_zone_select.html',
    replace: true,
    controller: function($scope) {
      $scope.q = "";
      $scope.isOpen = false;
      $scope.zone = $scope.zone || AppConfig.timeZone;
      $scope.currentZone = function() {
        return TimeService.nameForZone($scope.zone);
      };
      $scope.zoneFromName = function(name) {
        return AppConfig.timeZones[name];
      };
      $scope.offsetFromName = function(name) {
        return moment().tz(zoneFromName(name)).format('Z');
      };
      $scope.open = function() {
        $scope.q = "";
        return $scope.isOpen = true;
      };
      $scope.close = function() {
        return $scope.isOpen = false;
      };
      $scope.names = function() {
        return _.filter(_.keys(AppConfig.timeZones), function(name) {
          return name.toLowerCase().includes($scope.q.toLowerCase());
        });
      };
      return $scope.change = function() {
        if (AppConfig.timeZones[$scope.q]) {
          $scope.zone = AppConfig.timeZones[$scope.q];
          $scope.$emit('timeZoneSelected', AppConfig.timeZones[$scope.q]);
          return $scope.close();
        }
      };
    }
  };
});

angular.module('loomioApp').directive('timeago', function() {
  return {
    scope: {
      timestamp: '='
    },
    restrict: 'E',
    templateUrl: 'generated/components/timeago/timeago.html',
    replace: true
  };
});

angular.module('loomioApp').directive('translateButton', function() {
  return {
    scope: {
      model: '=',
      showdot: '=?'
    },
    restrict: 'E',
    templateUrl: 'generated/components/translate_button/translate_button.html',
    replace: true,
    controller: function($scope, Records, AbilityService, Session, LoadingService) {
      $scope.canTranslate = function() {
        return AbilityService.canTranslate($scope.model) && !$scope.translateExecuting && !$scope.translated;
      };
      $scope.translate = function() {
        return Records.translations.fetchTranslation($scope.model, Session.user().locale).then(function(data) {
          $scope.translated = true;
          return $scope.$emit('translationComplete', data.translations[0].fields);
        });
      };
      return LoadingService.applyLoadingFunction($scope, 'translate');
    }
  };
});

angular.module('loomioApp').directive('translation', function() {
  return {
    scope: {
      translation: '=',
      field: '@'
    },
    restrict: 'E',
    templateUrl: 'generated/components/translation/translation.html',
    replace: true,
    controller: function($scope) {
      return $scope.translated = $scope.translation[$scope.field];
    }
  };
});

angular.module('loomioApp').directive('userAvatar', function() {
  return {
    scope: {
      user: '=',
      coordinator: '=?',
      size: '@?'
    },
    restrict: 'E',
    templateUrl: 'generated/components/user_avatar/user_avatar.html',
    replace: true,
    controller: function($scope) {
      if (!_.contains(['small', 'medium', 'medium-circular', 'large', 'large-circular', 'featured'], $scope.size)) {
        $scope.size = 'small';
      }
      $scope.gravatarSize = function() {
        switch ($scope.size) {
          case 'small':
            return 30;
          case 'medium':
          case 'medium-circular':
            return 50;
          case 'large':
          case 'large-circular':
            return 80;
          case 'featured':
            return 175;
        }
      };
    }
  };
});

angular.module('loomioApp').controller('UserPageController', function($rootScope, $routeParams, Records, LoadingService) {
  $rootScope.$broadcast('currentComponent', {
    page: 'userPage'
  });
  this.init = (function(_this) {
    return function() {
      if (_this.user) {
        return;
      }
      if (_this.user = (Records.users.find($routeParams.key) || Records.users.find({
        username: $routeParams.key
      }))[0]) {
        return _this.loadGroupsFor(_this.user.key);
      }
    };
  })(this);
  this.loadGroupsFor = function(userKey) {
    return Records.memberships.fetchByUser(userKey);
  };
  LoadingService.applyLoadingFunction(this, 'loadGroupsFor');
  this.init();
  Records.users.findOrFetchById($routeParams.key).then(this.init, function(error) {
    return $rootScope.$broadcast('pageError', error);
  });
});

angular.module('loomioApp').directive('validationErrors', function() {
  return {
    scope: {
      subject: '=',
      field: '@'
    },
    restrict: 'E',
    templateUrl: 'generated/components/validation_errors/validation_errors.html',
    replace: true
  };
});

angular.module('loomioApp').directive('voteIcon', function() {
  return {
    scope: {
      position: '='
    },
    restrict: 'E',
    templateUrl: 'generated/components/vote_icon/vote_icon.html',
    replace: true,
    controller: function($scope) {
      return $scope.positionImg = function() {
        switch ($scope.position) {
          case 'yes':
            return '/img/agree.svg';
          case 'abstain':
            return '/img/abstain.svg';
          case 'no':
            return '/img/disagree.svg';
          case 'block':
            return '/img/block.svg';
        }
      };
    }
  };
});

angular.module('loomioApp').directive('authAvatar', function() {
  return {
    scope: {
      user: '=',
      helperBot: '=?'
    },
    templateUrl: 'generated/components/auth/avatar/auth_avatar.html',
    controller: function($scope) {
      if (!$scope.user.id && $scope.user.avatarKind === 'initials') {
        return $scope.avatarUser = {
          constructor: {
            singular: 'user'
          },
          avatarKind: 'uploaded',
          avatarUrl: '/img/mascot.png'
        };
      } else {
        return $scope.avatarUser = $scope.user;
      }
    }
  };
});

angular.module('loomioApp').directive('authComplete', function() {
  return {
    scope: {
      user: '='
    },
    templateUrl: 'generated/components/auth/complete/auth_complete.html'
  };
});

angular.module('loomioApp').directive('authEmailForm', function($translate, AppConfig, KeyEventService, AuthService) {
  return {
    scope: {
      user: '='
    },
    templateUrl: 'generated/components/auth/email_form/auth_email_form.html',
    controller: function($scope) {
      $scope.submit = function() {
        if (!$scope.validateEmail()) {
          return;
        }
        $scope.$emit('processing');
        $scope.user.email = $scope.email;
        return AuthService.emailStatus($scope.user)["finally"](function() {
          return $scope.$emit('doneProcessing');
        });
      };
      $scope.validateEmail = function() {
        $scope.user.errors = {};
        if (!$scope.email) {
          $scope.user.errors.email = [$translate.instant('auth_form.email_not_present')];
        } else if (!$scope.email.match(/[^\s,;<>]+?@[^\s,;<>]+\.[^\s,;<>]+/g)) {
          $scope.user.errors.email = [$translate.instant('auth_form.invalid_email')];
        }
        return $scope.user.errors.email == null;
      };
      KeyEventService.submitOnEnter($scope, {
        anyEnter: true
      });
      return $scope.$emit('focus');
    }
  };
});

angular.module('loomioApp').directive('authForm', function(AppConfig, LoadingService) {
  return {
    scope: {
      preventClose: '=',
      user: '='
    },
    templateUrl: 'generated/components/auth/form/auth_form.html',
    controller: function($scope) {
      $scope.loginComplete = function() {
        return $scope.user.sentLoginLink || $scope.user.sentPasswordLink;
      };
      if (_.contains(_.pluck(AppConfig.identityProviders, 'name'), (AppConfig.pendingIdentity || {}).identity_type)) {
        $scope.pendingProviderIdentity = AppConfig.pendingIdentity;
      }
      return LoadingService.listenForLoading($scope);
    }
  };
});

angular.module('loomioApp').directive('authIdentityForm', function($translate) {
  return {
    scope: {
      user: '=',
      identity: '='
    },
    templateUrl: 'generated/components/auth/identity_form/auth_identity_form.html',
    controller: function($scope, AuthService, KeyEventService) {
      $scope.createAccount = function() {
        $scope.$emit('processing');
        return AuthService.confirmOauth().then((function() {}), function() {
          return $scope.$emit('doneProcessing');
        });
      };
      $scope.submit = function() {
        $scope.$emit('processing');
        $scope.user.email = $scope.email;
        return AuthService.sendLoginLink($scope.user).then((function() {}), function() {
          return $scope.user.errors = {
            email: [$translate.instant('auth_form.email_not_found')]
          };
        })["finally"](function() {
          return $scope.$emit('doneProcessing');
        });
      };
      return KeyEventService.submitOnEnter($scope, {
        anyEnter: true
      });
    }
  };
});

angular.module('loomioApp').factory('AuthModal', function(AuthService, Records, AppConfig) {
  return {
    templateUrl: 'generated/components/auth/modal/auth_modal.html',
    controller: function($scope, preventClose) {
      $scope.user = AuthService.applyEmailStatus(Records.users.build(), AppConfig.pendingIdentity);
      $scope.preventClose = preventClose;
      $scope.$on('signedIn', $scope.$close);
      $scope.back = function() {
        return $scope.user.emailStatus = null;
      };
      return $scope.showBackButton = function() {
        return $scope.user.emailStatus && !$scope.user.sentLoginLink && !$scope.user.sentPasswordLink;
      };
    }
  };
});

angular.module('loomioApp').directive('authProviderForm', function() {
  return {
    scope: {
      user: '='
    },
    templateUrl: 'generated/components/auth/provider_form/auth_provider_form.html',
    controller: function($scope, $window, AppConfig) {
      $scope.providers = AppConfig.identityProviders;
      return $scope.select = function(provider) {
        $scope.$emit('processing');
        return $window.location = provider.href + "?back_to=" + $window.location.href;
      };
    }
  };
});

angular.module('loomioApp').directive('authSigninForm', function($translate, $window, Session, AuthService, FlashService, KeyEventService) {
  return {
    scope: {
      user: '='
    },
    templateUrl: 'generated/components/auth/signin_form/auth_signin_form.html',
    controller: function($scope) {
      $scope.signIn = function() {
        $scope.$emit('processing');
        return AuthService.signIn($scope.user).then((function() {}), function() {
          $scope.user.errors = {
            password: [$translate.instant('auth_form.invalid_password')]
          };
          return $scope.$emit('doneProcessing');
        });
      };
      $scope.sendLoginLink = function() {
        $scope.$emit('processing');
        return AuthService.sendLoginLink($scope.user)["finally"](function() {
          return $scope.$emit('doneProcessing');
        });
      };
      $scope.submit = function() {
        if ($scope.user.hasPassword) {
          return $scope.signIn();
        } else {
          return $scope.sendLoginLink();
        }
      };
      $scope.setPassword = function() {
        $scope.$emit('processing');
        return AuthService.forgotPassword($scope.user)["finally"](function() {
          return $scope.$emit('doneProcessing');
        });
      };
      KeyEventService.submitOnEnter($scope, {
        anyEnter: true
      });
      return $scope.$emit('focus');
    }
  };
});

angular.module('loomioApp').directive('authSignupForm', function($translate, AppConfig, AuthService, KeyEventService) {
  return {
    scope: {
      user: '='
    },
    templateUrl: 'generated/components/auth/signup_form/auth_signup_form.html',
    controller: function($scope) {
      $scope.recaptchaKey = AppConfig.recaptchaKey;
      $scope.name = $scope.user.name;
      $scope.submit = function() {
        if ($scope.name) {
          $scope.user.errors = {};
          $scope.$emit('processing');
          $scope.user.name = $scope.name;
          return AuthService.signUp($scope.user)["finally"](function() {
            return $scope.$emit('doneProcessing');
          });
        } else {
          return $scope.user.errors = {
            name: [$translate.instant('auth_form.name_required')]
          };
        }
      };
      KeyEventService.submitOnEnter($scope, {
        anyEnter: true
      });
      return $scope.$emit('focus');
    }
  };
});

angular.module('loomioApp').directive('groupForm', function() {
  return {
    scope: {
      group: '='
    },
    templateUrl: 'generated/components/group/form/group_form.html',
    controller: function($scope, $location, KeyEventService, LmoUrlService, FormService, Records, PrivacyString) {
      var submitForm;
      $scope.i18n = (function() {
        var groupMessaging;
        groupMessaging = {};
        if ($scope.group.isParent()) {
          groupMessaging.group_name = 'group_form.group_name';
          if ($scope.group.isNew()) {
            groupMessaging.submit = 'group_form.submit_start_group';
          } else {
            groupMessaging.submit = 'common.action.update_settings';
          }
        } else {
          groupMessaging.group_name = 'group_form.subgroup_name';
          if ($scope.group.isNew()) {
            groupMessaging.heading = 'group_form.start_subgroup_heading';
            groupMessaging.submit = 'group_form.submit_start_subgroup';
          } else {
            groupMessaging.heading = 'group_form.edit_subgroup_heading';
            groupMessaging.submit = 'common.action.update_settings';
          }
        }
        return groupMessaging;
      })();
      submitForm = FormService.submit($scope, $scope.group, {
        drafts: true,
        skipClose: true,
        flashSuccess: function() {
          if ($scope.group.isNew()) {
            return 'group_form.messages.group_created';
          } else {
            return 'group_form.messages.group_updated';
          }
        },
        successCallback: function(response) {
          var group;
          group = Records.groups.find(response.groups[0].key);
          $scope.$emit('createComplete', group);
          return $location.path(LmoUrlService.group(group));
        }
      });
      $scope.submit = function() {
        var message;
        if (message = PrivacyString.confirmGroupPrivacyChange($scope.group)) {
          if (window.confirm(message)) {
            return submitForm();
          }
        } else {
          return submitForm();
        }
      };
      $scope.expandForm = function() {
        return $scope.expanded = true;
      };
      $scope.privacyStatement = function() {
        return PrivacyString.groupPrivacyStatement($scope.group);
      };
      $scope.privacyStringFor = function(state) {
        return PrivacyString.group($scope.group, state);
      };
      $scope.buh = {};
      $scope.buh.allowPublicThreads = $scope.group.allowPublicDiscussions();
      $scope.allowPublicThreadsClicked = function() {
        if ($scope.buh.allowPublicThreads) {
          return $scope.group.discussionPrivacyOptions = 'public_or_private';
        } else {
          return $scope.group.discussionPrivacyOptions = 'private_only';
        }
      };
      $scope.groupPrivacyChanged = function() {
        $scope.group.parentMembersCanSeeDiscussions = !$scope.group.privacyIsSecret();
        switch ($scope.group.groupPrivacy) {
          case 'open':
            return $scope.group.discussionPrivacyOptions = 'public_only';
          case 'closed':
            return $scope.allowPublicThreadsClicked();
          case 'secret':
            return $scope.group.discussionPrivacyOptions = 'private_only';
        }
      };
      return KeyEventService.submitOnEnter($scope);
    }
  };
});

angular.module('loomioApp').factory('GroupModal', function() {
  return {
    templateUrl: 'generated/components/group/modal/group_modal.html',
    controller: function($scope, group) {
      $scope.group = group.clone();
      $scope.currentStep = 'create';
      $scope.$on('createComplete', function(event, group) {
        if (!$scope.group.isNew() || $scope.group.parentId) {
          return $scope.$close();
        } else {
          $scope.group = group;
          return $scope.currentStep = 'invite';
        }
      });
      return $scope.$on('inviteComplete', $scope.$close);
    }
  };
});

angular.module('loomioApp').factory('CoverPhotoForm', function() {
  return {
    templateUrl: 'generated/components/group_page/cover_photo_form/cover_photo_form.html',
    controller: function($scope, $timeout, $rootScope, group, Records, FormService) {
      $scope.selectFile = function() {
        return $timeout(function() {
          return document.querySelector('.cover-photo-form__file-input').click();
        });
      };
      return $scope.upload = FormService.upload($scope, group, {
        uploadKind: 'cover_photo',
        submitFn: group.uploadPhoto,
        loadingMessage: 'common.action.uploading',
        successCallback: function(data) {
          return $rootScope.$broadcast('setBackgroundImageUrl', group);
        },
        flashSuccess: 'cover_photo_form.upload_success'
      });
    }
  };
});

angular.module('loomioApp').directive('descriptionCard', function() {
  return {
    scope: {
      group: '='
    },
    restrict: 'E',
    templateUrl: 'generated/components/group_page/description_card/description_card.html',
    replace: true,
    controller: function($scope, FormService, AbilityService) {
      $scope.editorEnabled = false;
      $scope.canEditGroup = function() {
        return AbilityService.canEditGroup($scope.group);
      };
      $scope.enableEditor = function() {
        $scope.editorEnabled = true;
        return $scope.editableDescription = $scope.group.description;
      };
      $scope.disableEditor = function() {
        return $scope.editorEnabled = false;
      };
      return $scope.save = FormService.submit($scope, $scope.group, {
        drafts: true,
        prepareFn: function() {
          return $scope.group.description = $scope.editableDescription;
        },
        flashSuccess: 'description_card.messages.description_updated',
        successCallback: function() {
          return $scope.disableEditor();
        }
      });
    }
  };
});

angular.module('loomioApp').directive('discussionsCard', function() {
  return {
    scope: {
      group: '=',
      pageWindow: '='
    },
    restrict: 'E',
    templateUrl: 'generated/components/group_page/discussions_card/discussions_card.html',
    replace: true,
    controller: function($scope, $location, Records, ModalService, DiscussionForm, ThreadQueryService, KeyEventService, LoadingService, AbilityService) {
      $scope.threadLimit = $scope.pageWindow.current;
      $scope.init = function() {
        return $scope.discussions = ThreadQueryService.groupQuery($scope.group, {
          filter: 'all',
          queryType: 'all'
        });
      };
      $scope.init();
      $scope.$on('subgroupsLoaded', $scope.init);
      $scope.loadMore = function() {
        var current;
        current = $scope.pageWindow.current;
        $scope.pageWindow.current += $scope.pageWindow.pageSize;
        $scope.threadLimit += $scope.pageWindow.pageSize;
        return Records.discussions.fetchByGroup($scope.group.key, {
          from: current,
          per: $scope.pageWindow.pageSize
        });
      };
      LoadingService.applyLoadingFunction($scope, 'loadMore');
      $scope.loadMore();
      $scope.canLoadMoreDiscussions = function() {
        return $scope.pageWindow.current < $scope.pageWindow.max;
      };
      $scope.openDiscussionForm = function() {
        return ModalService.open(DiscussionForm, {
          discussion: function() {
            return Records.discussions.build({
              groupId: $scope.group.id
            });
          }
        });
      };
      $scope.showThreadsPlaceholder = function() {
        return AbilityService.canStartThread($scope.group) && $scope.group.discussions().length < 4;
      };
      $scope.whyImEmpty = function() {
        if (!AbilityService.canViewGroup($scope.group)) {
          return 'discussions_are_private';
        } else if (!$scope.group.hasDiscussions) {
          return 'no_discussions_in_group';
        } else if ($scope.group.discussionPrivacyOptions === 'private_only') {
          return 'discussions_are_private';
        } else {
          return 'no_public_discussions';
        }
      };
      $scope.howToGainAccess = function() {
        if (!$scope.group.hasDiscussions) {
          return null;
        } else if ($scope.group.membershipGrantedUpon === 'request') {
          return 'join_group';
        } else if ($scope.group.membershipGrantedUpon === 'approval') {
          return 'request_membership';
        } else if ($scope.group.membersCanAddMembers) {
          return 'membership_is_invitation_only';
        } else {
          return 'membership_is_invitation_by_admin_only';
        }
      };
      return $scope.canStartThread = function() {
        return AbilityService.canStartThread($scope.group);
      };
    }
  };
});

angular.module('loomioApp').directive('groupActionsDropdown', function() {
  return {
    scope: {
      group: '='
    },
    restrict: 'E',
    templateUrl: 'generated/components/group_page/group_actions_dropdown/group_actions_dropdown.html',
    replace: true,
    controller: function($scope, $window, AppConfig, AbilityService, Session, ChangeVolumeForm, ModalService, GroupModal, LeaveGroupForm, ArchiveGroupForm, Records) {
      $scope.canAdministerGroup = function() {
        return AbilityService.canAdministerGroup($scope.group);
      };
      $scope.canEditGroup = (function(_this) {
        return function() {
          return AbilityService.canEditGroup($scope.group);
        };
      })(this);
      $scope.canAddSubgroup = function() {
        return AbilityService.canCreateSubgroups($scope.group);
      };
      $scope.canArchiveGroup = (function(_this) {
        return function() {
          return AbilityService.canArchiveGroup($scope.group);
        };
      })(this);
      $scope.canChangeVolume = function() {
        return AbilityService.canChangeGroupVolume($scope.group);
      };
      $scope.openChangeVolumeForm = function() {
        return ModalService.open(ChangeVolumeForm, {
          model: function() {
            return $scope.group.membershipFor(Session.user());
          }
        });
      };
      $scope.editGroup = function() {
        return ModalService.open(GroupModal, {
          group: function() {
            return $scope.group;
          }
        });
      };
      $scope.addSubgroup = function() {
        return ModalService.open(GroupModal, {
          group: function() {
            return Records.groups.build({
              parentId: $scope.group.id
            });
          }
        });
      };
      $scope.leaveGroup = function() {
        return ModalService.open(LeaveGroupForm, {
          group: function() {
            return $scope.group;
          }
        });
      };
      $scope.archiveGroup = function() {
        return ModalService.open(ArchiveGroupForm, {
          group: function() {
            return $scope.group;
          }
        });
      };
    }
  };
});

angular.module('loomioApp').directive('groupHelpCard', function() {
  return {
    scope: {
      group: '='
    },
    restrict: 'E',
    templateUrl: 'generated/components/group_page/group_help_card/group_help_card.html',
    replace: true,
    controller: function($scope, Session, AppConfig, UserHelpService) {
      $scope.showVideo = AppConfig.loadVideos;
      $scope.helpLink = function() {
        return UserHelpService.helpLink();
      };
      $scope.helpVideo = function() {
        return UserHelpService.helpVideoUrl();
      };
      $scope.showHelpCard = function() {
        return Session.user().isMemberOf($scope.group);
      };
      $scope.tenTipsArticleLink = function() {
        return UserHelpService.tenTipsArticleLink();
      };
      return $scope.nineWaysArticleLink = function() {
        return UserHelpService.nineWaysArticleLink();
      };
    }
  };
});

angular.module('loomioApp').directive('groupPreviousProposalsCard', function() {
  return {
    scope: {
      group: '='
    },
    restrict: 'E',
    templateUrl: 'generated/components/group_page/group_previous_proposals_card/group_previous_proposals_card.html',
    replace: true,
    controller: function($scope, Session, Records, AbilityService) {
      if (AbilityService.canViewPreviousProposals($scope.group)) {
        Records.proposals.fetchClosedByGroup($scope.group.key, {
          per: 3
        }).then(function() {
          if (AbilityService.isLoggedIn()) {
            return Records.votes.fetchMyVotes($scope.group);
          }
        });
      }
      $scope.showPreviousProposals = function() {
        return AbilityService.canViewPreviousProposals($scope.group) && $scope.group.hasPreviousProposals();
      };
      $scope.lastVoteByCurrentUser = function(proposal) {
        return proposal.lastVoteByUser(Session.user());
      };
      return $scope.canShowMore = function() {
        return $scope.group.closedMotionsCount > 3;
      };
    }
  };
});

angular.module('loomioApp').directive('groupPrivacyButton', function() {
  return {
    restrict: 'E',
    templateUrl: 'generated/components/group_page/group_privacy_button/group_privacy_button.html',
    replace: true,
    scope: {
      group: '='
    },
    controller: function($scope, PrivacyString) {
      $scope.iconClass = function() {
        switch ($scope.group.groupPrivacy) {
          case 'open':
            return 'fa-globe';
          case 'closed':
            return 'fa-lock';
          case 'secret':
            return 'fa-lock';
        }
      };
      return $scope.privacyDescription = function() {
        return PrivacyString.group($scope.group);
      };
    }
  };
});

angular.module('loomioApp').directive('groupTheme', function() {
  return {
    scope: {
      group: '=',
      homePage: '=',
      compact: '=',
      discussion: '=?'
    },
    restrict: 'E',
    templateUrl: 'generated/components/group_page/group_theme/group_theme.html',
    replace: true,
    controller: function($scope, $rootScope, Session, AbilityService, ModalService, CoverPhotoForm, LogoPhotoForm) {
      $rootScope.$broadcast('setBackgroundImageUrl', $scope.group);
      $scope.logoStyle = function() {
        return {
          'background-image': "url(" + ($scope.group.logoUrl()) + ")"
        };
      };
      $scope.isMember = function() {
        return Session.user().membershipFor($scope.group) != null;
      };
      $scope.canUploadPhotos = function() {
        return $scope.homePage && AbilityService.canAdministerGroup($scope.group);
      };
      $scope.openUploadCoverForm = function() {
        return ModalService.open(CoverPhotoForm, {
          group: (function(_this) {
            return function() {
              return $scope.group;
            };
          })(this)
        });
      };
      $scope.openUploadLogoForm = function() {
        return ModalService.open(LogoPhotoForm, {
          group: (function(_this) {
            return function() {
              return $scope.group;
            };
          })(this)
        });
      };
      $scope.themeHoverIn = function() {
        return $scope.themeHover = true;
      };
      $scope.themeHoverOut = function() {
        return $scope.themeHover = false;
      };
      $scope.logoHoverIn = function() {
        return $scope.logoHover = true;
      };
      return $scope.logoHoverOut = function() {
        return $scope.logoHover = false;
      };
    }
  };
});

angular.module('loomioApp').directive('joinGroupButton', function() {
  return {
    scope: {
      group: '=',
      block: '=?'
    },
    restrict: 'E',
    templateUrl: 'generated/components/group_page/join_group_button/join_group_button.html',
    replace: true,
    controller: function($rootScope, $scope, AbilityService, ModalService, AuthModal, Session, Records, FlashService, MembershipRequestForm) {
      Records.membershipRequests.fetchMyPendingByGroup($scope.group.key);
      $scope.isMember = function() {
        return Session.user().membershipFor($scope.group) != null;
      };
      $scope.canJoinGroup = function() {
        return AbilityService.canJoinGroup($scope.group);
      };
      $scope.canRequestMembership = function() {
        return AbilityService.canRequestMembership($scope.group);
      };
      $scope.hasRequestedMembership = function() {
        return $scope.group.hasPendingMembershipRequestFrom(Session.user());
      };
      $scope.askToJoinText = function() {
        if ($scope.hasRequestedMembership()) {
          return 'join_group_button.membership_requested';
        } else {
          return 'join_group_button.ask_to_join_group';
        }
      };
      $scope.joinGroup = function() {
        if (AbilityService.isLoggedIn()) {
          return Records.memberships.joinGroup($scope.group).then(function() {
            $rootScope.$broadcast('joinedGroup');
            return FlashService.success('join_group_button.messages.joined_group', {
              group: $scope.group.fullName
            });
          });
        } else {
          return ModalService.open(AuthModal);
        }
      };
      $scope.requestToJoinGroup = function() {
        return ModalService.open(MembershipRequestForm, {
          group: function() {
            return $scope.group;
          }
        });
      };
      return $scope.isLoggedIn = function() {
        return AbilityService.isLoggedIn();
      };
    }
  };
});

angular.module('loomioApp').factory('LogoPhotoForm', function() {
  return {
    scope: {
      group: '='
    },
    templateUrl: 'generated/components/group_page/logo_photo_form/logo_photo_form.html',
    controller: function($scope, $timeout, group, Records, FormService) {
      $scope.selectFile = function() {
        return $timeout(function() {
          return document.querySelector('.logo-photo-form__file-input').click();
        });
      };
      return $scope.upload = FormService.upload($scope, group, {
        uploadKind: 'logo',
        submitFn: group.uploadPhoto,
        loadingMessage: 'common.action.uploading',
        flashSuccess: 'logo_photo_form.upload_success'
      });
    }
  };
});

angular.module('loomioApp').directive('membersCard', function() {
  return {
    scope: {
      group: '='
    },
    restrict: 'E',
    templateUrl: 'generated/components/group_page/members_card/members_card.html',
    replace: true,
    controller: function($scope, Records, AbilityService, ModalService, InvitationModal) {
      Records.memberships.fetchByGroup($scope.group.key, {
        per: 10
      });
      $scope.canAddMembers = function() {
        return AbilityService.canAddMembers($scope.group);
      };
      $scope.isAdmin = function() {
        return AbilityService.canAdministerGroup($scope.group);
      };
      $scope.memberIsAdmin = function(member) {
        return $scope.group.membershipFor(member).admin;
      };
      $scope.showMembersPlaceholder = function() {
        return AbilityService.canAdministerGroup($scope.group) && $scope.group.memberships().length <= 1;
      };
      return $scope.invitePeople = function() {
        return ModalService.open(InvitationModal, {
          group: function() {
            return $scope.group;
          }
        });
      };
    }
  };
});

angular.module('loomioApp').factory('MembershipRequestForm', function() {
  return {
    templateUrl: 'generated/components/group_page/membership_request_form/membership_request_form.html',
    controller: function($scope, FormService, Records, group, AbilityService, Session) {
      $scope.membershipRequest = Records.membershipRequests.build({
        groupId: group.id,
        name: Session.user().name,
        email: Session.user().email
      });
      $scope.submit = FormService.submit($scope, $scope.membershipRequest, {
        flashSuccess: 'membership_request_form.messages.membership_requested',
        flashOptions: {
          group: group.fullName
        }
      });
      $scope.isLoggedIn = AbilityService.isLoggedIn;
    }
  };
});

angular.module('loomioApp').directive('membershipRequestsCard', function() {
  return {
    scope: {
      group: '='
    },
    restrict: 'E',
    templateUrl: 'generated/components/group_page/membership_requests_card/membership_requests_card.html',
    replace: true,
    controller: function($scope, Records, AbilityService) {
      $scope.canManageMembershipRequests = function() {
        return AbilityService.canManageMembershipRequests($scope.group);
      };
      if ($scope.canManageMembershipRequests()) {
        return Records.membershipRequests.fetchPendingByGroup($scope.group.key);
      }
    }
  };
});

angular.module('loomioApp').directive('subgroupsCard', function() {
  return {
    scope: {
      group: '='
    },
    restrict: 'E',
    templateUrl: 'generated/components/group_page/subgroups_card/subgroups_card.html',
    replace: true,
    controller: function($scope, $rootScope, Records, AbilityService, ModalService, GroupModal) {
      Records.groups.fetchByParent($scope.group).then(function() {
        return $rootScope.$broadcast('subgroupsLoaded', $scope.group);
      });
      $scope.canCreateSubgroups = function() {
        return AbilityService.canCreateSubgroups($scope.group);
      };
      $scope.startSubgroup = function() {
        return ModalService.open(GroupModal, {
          group: function() {
            return Records.groups.build({
              parentId: $scope.group.id
            });
          }
        });
      };
      return $scope.showSubgroupsCard = function() {
        return $scope.group.subgroups().length;
      };
    }
  };
});

angular.module('loomioApp').directive('installSlackCard', function() {
  return {
    scope: {
      group: '='
    },
    templateUrl: 'generated/components/install_slack/card/install_slack_card.html'
  };
});

angular.module('loomioApp').directive('installSlackDecideForm', function(Session, Records, ModalService, PollCommonStartModal) {
  return {
    templateUrl: 'generated/components/install_slack/decide_form/install_slack_decide_form.html',
    controller: function($scope) {
      $scope.poll = Records.polls.build({
        groupId: Session.currentGroupId()
      });
      return $scope.$on('saveComplete', function() {
        return $scope.$emit('decideComplete');
      });
    }
  };
});

angular.module('loomioApp').directive('installSlackForm', function(FormService, Session, Records, LoadingService, LmoUrlService) {
  return {
    templateUrl: 'generated/components/install_slack/form/install_slack_form.html',
    controller: function($scope) {
      $scope.currentStep = 'install';
      $scope.$on('installComplete', function(event, group) {
        $scope.group = group;
        $scope.currentStep = 'invite';
        return $scope.isDisabled = false;
      });
      $scope.$on('inviteComplete', function() {
        $scope.currentStep = 'decide';
        return $scope.isDisabled = false;
      });
      $scope.$on('decideComplete', $scope.$close);
      $scope.progress = function() {
        switch ($scope.currentStep) {
          case 'install':
            return 0;
          case 'invite':
            return 50;
          case 'decide':
            return 100;
        }
      };
      return LoadingService.listenForLoading($scope);
    }
  };
});

angular.module('loomioApp').directive('installSlackInstallForm', function($location, KeyEventService, FormService, Session, Records, LmoUrlService) {
  return {
    templateUrl: 'generated/components/install_slack/install_form/install_slack_install_form.html',
    controller: function($scope) {
      var newGroup;
      $scope.groups = function() {
        return _.filter(_.sortBy(Session.user().adminGroups(), 'fullName'), function(group) {
          return !group.identityId;
        });
      };
      newGroup = Records.groups.build({
        name: Session.user().identityFor('slack').customFields.slack_team_name
      });
      $scope.toggleExistingGroup = function() {
        return $scope.setSubmit($scope.group.id ? newGroup : _.first($scope.groups()));
      };
      $scope.setSubmit = function(group) {
        $scope.group = group;
        return $scope.submit = FormService.submit($scope, $scope.group, {
          prepareFn: function() {
            $scope.$emit('processing');
            return $scope.group.identityId = Session.user().identityFor('slack').id;
          },
          flashSuccess: 'install_slack.install.slack_installed',
          skipClose: true,
          successCallback: function(response) {
            var g;
            g = Records.groups.find(response.groups[0].key);
            $location.path(LmoUrlService.group(g));
            return $scope.$emit('installComplete', g);
          }
        });
      };
      $scope.setSubmit(_.first($scope.groups()) || newGroup);
      KeyEventService.submitOnEnter($scope, {
        anyEnter: true
      });
      $scope.$on('focus', $scope.focus);
    }
  };
});

angular.module('loomioApp').directive('installSlackInviteForm', function($timeout, Session, KeyEventService, Records, CommunityService) {
  return {
    scope: {
      group: '='
    },
    templateUrl: 'generated/components/install_slack/invite_form/install_slack_invite_form.html',
    controller: function($scope) {
      $scope.group.makeAnnouncement = true;
      $scope.fetchChannels = function() {
        if ($scope.channels) {
          return;
        }
        return Records.identities.performCommand(Session.user().identityFor('slack').id, 'channels').then(function(response) {
          return $scope.channels = response;
        }, function(response) {
          return $scope.error = response.data.error;
        });
      };
      $scope.submit = function() {
        var channel;
        $scope.$emit('processing');
        channel = '#' + _.find($scope.channels, function(c) {
          return c.id === $scope.identifier;
        }).name;
        return $scope.group.publish($scope.identifier, channel).then(function() {
          return $scope.$emit('inviteComplete');
        })["finally"](function() {
          return $scope.$emit('doneProcessing');
        });
      };
      KeyEventService.submitOnEnter($scope, {
        anyEnter: true
      });
      return $scope.$emit('focus');
    }
  };
});

angular.module('loomioApp').directive('installSlackInvitePreview', function(Session, $timeout) {
  return {
    templateUrl: 'generated/components/install_slack/invite_preview/install_slack_invite_preview.html',
    controller: function($scope) {
      return $timeout(function() {
        $scope.group = Session.currentGroup;
        $scope.userName = Session.user().name;
        return $scope.timestamp = function() {
          return moment().format('h:ma');
        };
      });
    }
  };
});

angular.module('loomioApp').factory('InstallSlackModal', function() {
  return {
    templateUrl: 'generated/components/install_slack/modal/install_slack_modal.html',
    controller: function($scope, preventClose) {
      $scope.$on('$close', $scope.$close);
      return $scope.preventClose = preventClose;
    }
  };
});

angular.module('loomioApp').directive('installSlackProgress', function(Session) {
  return {
    scope: {
      slackProgress: '='
    },
    templateUrl: 'generated/components/install_slack/progress/install_slack_progress.html',
    controller: function($scope) {
      return $scope.progressPercent = function() {
        return $scope.slackProgress + "%";
      };
    }
  };
});

angular.module('loomioApp').directive('installSlackTableau', function(Session) {
  return {
    templateUrl: 'generated/components/install_slack/tableau/install_slack_tableau.html',
    controller: function($scope) {
      return $scope.group = function() {
        return Session.currentGroup;
      };
    }
  };
});

angular.module('loomioApp').factory('AddMembersModal', function() {
  return {
    templateUrl: 'generated/components/invitation/add_members_modal/add_members_modal.html',
    controller: function($scope, Records, LoadingService, group, AppConfig, FlashService, ModalService, InvitationModal) {
      $scope.isDisabled = false;
      $scope.group = group;
      $scope.loading = true;
      $scope.selectedIds = [];
      $scope.load = function() {
        return Records.memberships.fetchByGroup(group.parent().key, {
          per: group.parent().membershipsCount
        });
      };
      $scope.members = function() {
        return _.filter(group.parent().members(), function(user) {
          return !user.isMemberOf(group);
        });
      };
      $scope.canAddMembers = function() {
        return $scope.members().length > 0;
      };
      LoadingService.applyLoadingFunction($scope, 'load');
      $scope.load();
      $scope.reopenInvitationsForm = function() {
        return ModalService.open(InvitationModal, {
          group: function() {
            return $scope.group;
          }
        });
      };
      return $scope.submit = function() {
        $scope.isDisabled = true;
        return Records.memberships.addUsersToSubgroup({
          groupId: $scope.group.id,
          userIds: $scope.selectedIds
        }).then(function(data) {
          if (data.memberships.length === 1) {
            FlashService.success('add_members_modal.user_added_to_subgroup', {
              name: data.users[0].name
            });
          } else {
            FlashService.success('add_members_modal.users_added_to_subgroup', {
              count: data.memberships.length
            });
          }
          return $scope.$close();
        })["finally"](function() {
          return $scope.isDisabled = false;
        });
      };
    }
  };
});

angular.module('loomioApp').directive('invitationForm', function() {
  return {
    scope: {
      group: '=',
      selectGroup: '='
    },
    templateUrl: 'generated/components/invitation/form/invitation_form.html',
    controller: function($scope, Records, Session, AbilityService, FormService, FlashService, RestfulClient, ModalService, AddMembersModal) {
      var submitForm;
      $scope.availableGroups = function() {
        return _.filter(Session.user().groups(), function(g) {
          return AbilityService.canAddMembers(g);
        });
      };
      $scope.form = Records.invitationForms.build({
        groupId: $scope.group.id
      });
      $scope.fetchShareableInvitation = function() {
        if ($scope.form.group()) {
          return Records.invitations.fetchShareableInvitationByGroupId($scope.form.group().id);
        }
      };
      $scope.fetchShareableInvitation();
      $scope.showCustomMessageField = false;
      $scope.isDisabled = false;
      $scope.noInvitations = false;
      $scope.addMembers = function() {
        return ModalService.open(AddMembersModal, {
          group: function() {
            return $scope.form.group();
          }
        });
      };
      $scope.showCustomMessageField = function() {
        return $scope.addCustomMessageClicked || $scope.form.message;
      };
      $scope.addCustomMessage = function() {
        return $scope.addCustomMessageClicked = true;
      };
      $scope.invitees = function() {
        return $scope.form.emails.match(/[^\s,;<>]+?@[^\s,;<>]+\.[^\s,;<>]+/g) || [];
      };
      $scope.maxInvitations = function() {
        return $scope.invitees().length > 100;
      };
      $scope.submitText = function() {
        if ($scope.form.emails.length > 0) {
          return 'invitation_form.send';
        } else {
          return 'invitation_form.done';
        }
      };
      $scope.invalidEmail = function() {
        return $scope.invitees().length === 0 && $scope.form.emails.length > 0;
      };
      $scope.canSubmit = function() {
        return $scope.invitees().length > 0 && $scope.form.group();
      };
      $scope.copied = function() {
        return FlashService.success('common.copied');
      };
      $scope.invitationLink = function() {
        if (!($scope.form.group() && $scope.form.group().shareableInvitation())) {
          return;
        }
        return $scope.form.group().shareableInvitation().url;
      };
      $scope.submit = function() {
        if ($scope.invitees().length === 0) {
          return $scope.$emit('inviteComplete');
        } else {
          return submitForm();
        }
      };
      submitForm = FormService.submit($scope, $scope.form, {
        drafts: true,
        submitFn: Records.invitations.sendByEmail,
        successCallback: (function(_this) {
          return function(response) {
            var invitationCount;
            $scope.$emit('inviteComplete');
            invitationCount = response.invitations.length;
            switch (invitationCount) {
              case 0:
                return $scope.noInvitations = true;
              case 1:
                return FlashService.success('invitation_form.messages.invitation_sent');
              default:
                return FlashService.success('invitation_form.messages.invitations_sent', {
                  count: invitationCount
                });
            }
          };
        })(this)
      });
    }
  };
});

angular.module('loomioApp').directive('invitable', function() {
  return {
    scope: {
      invitable: '='
    },
    restrict: 'E',
    templateUrl: 'generated/components/invitation/invitable/invitable.html',
    replace: true,
    controller: function($scope) {
      $scope.match = {
        label: $scope.invitable
      };
    }
  };
});

angular.module('loomioApp').factory('InvitationModal', function() {
  return {
    templateUrl: 'generated/components/invitation/modal/invitation_modal.html',
    controller: function($scope, group) {
      $scope.group = group.clone();
      return $scope.$on('inviteComplete', $scope.$close);
    }
  };
});

angular.module('loomioApp').directive('adminMembershipsPanel', function() {
  return {
    scope: {
      memberships: '=',
      group: '='
    },
    restrict: 'E',
    templateUrl: 'generated/components/memberships_page/admin_memberships_panel/admin_memberships_panel.html',
    replace: true,
    controller: 'AdminMembershipsPanelController'
  };
});

angular.module('loomioApp').controller('AdminMembershipsPanelController', function($scope, Session, AbilityService, ModalService, Records, FlashService, RemoveMembershipForm, InvitationModal, $filter) {
  $scope.canRemoveMembership = function(membership) {
    return AbilityService.canRemoveMembership(membership);
  };
  $scope.canToggleAdmin = function(membership) {
    return AbilityService.canAdministerGroup($scope.group) && (!membership.admin || $scope.canRemoveMembership(membership));
  };
  $scope.toggleAdmin = function(membership) {
    var method;
    method = membership.admin ? 'makeAdmin' : 'removeAdmin';
    if (!membership.admin && membership.user() === Session.user() && !confirm($filter('translate')('memberships_page.remove_admin_from_self.question'))) {
      membership.admin = true;
      return;
    }
    return Records.memberships[method](membership).then(function() {
      return FlashService.success("memberships_page.messages." + (_.snakeCase(method)) + "_success", {
        name: membership.userName()
      });
    });
  };
  $scope.openRemoveForm = function(membership) {
    return ModalService.open(RemoveMembershipForm, {
      membership: function() {
        return membership;
      }
    });
  };
  $scope.canAddMembers = function() {
    return AbilityService.canAddMembers($scope.group);
  };
  return $scope.invitePeople = function() {
    return ModalService.open(InvitationModal, {
      group: (function(_this) {
        return function() {
          return $scope.group;
        };
      })(this)
    });
  };
});

angular.module('loomioApp').factory('CancelInvitationForm', function() {
  return {
    templateUrl: 'generated/components/memberships_page/cancel_invitation_form/cancel_invitation_form.html',
    controller: function($scope, invitation, FlashService, Records, FormService) {
      $scope.invitation = invitation;
      return $scope.submit = FormService.submit($scope, $scope.invitation, {
        submitFn: $scope.invitation.destroy,
        flashSuccess: 'cancel_invitation_form.messages.success'
      });
    }
  };
});

angular.module('loomioApp').directive('membershipsPanel', function() {
  return {
    scope: {
      memberships: '=',
      group: '='
    },
    restrict: 'E',
    templateUrl: 'generated/components/memberships_page/memberships_panel/memberships_panel.html',
    replace: true,
    controller: 'AdminMembershipsPanelController'
  };
});

angular.module('loomioApp').directive('pendingInvitationsCard', function() {
  return {
    scope: {
      group: '='
    },
    restrict: 'E',
    templateUrl: 'generated/components/memberships_page/pending_invitations_card/pending_invitations_card.html',
    replace: true,
    controller: function($scope, Session, Records, ModalService, CancelInvitationForm, AppConfig) {
      $scope.canSeeInvitations = function() {
        return Session.user().isAdminOf($scope.group);
      };
      if ($scope.canSeeInvitations()) {
        Records.invitations.fetchPendingByGroup($scope.group.key, {
          per: 1000
        });
      }
      $scope.baseUrl = AppConfig.baseUrl;
      $scope.openCancelForm = function(invitation) {
        return ModalService.open(CancelInvitationForm, {
          invitation: function() {
            return invitation;
          }
        });
      };
      return $scope.invitationCreatedAt = function(invitation) {
        return moment(invitation.createdAt).format('DD MMM YY');
      };
    }
  };
});

angular.module('loomioApp').directive('activityCard', function() {
  return {
    scope: {
      discussion: '=',
      loading: '=',
      activeCommentId: '=?'
    },
    restrict: 'E',
    templateUrl: 'generated/components/thread_page/activity_card/activity_card.html',
    replace: true,
    controller: function($scope, $location, $mdDialog, $rootScope, $window, $timeout, Records, AppConfig, AbilityService, PaginationService, LoadingService, ModalService) {
      var addSequenceId, removeSequenceId, updateLastSequenceId, visibleSequenceIds;
      $scope.$on('fetchRecordsForPrint', function(event, options) {
        if (options == null) {
          options = {};
        }
        return $scope.loadEvents({
          per: Number.MAX_SAFE_INTEGER
        }).then(function() {
          $mdDialog.cancel();
          return $timeout(function() {
            return $window.print();
          });
        });
      });
      $scope.firstLoadedSequenceId = 0;
      $scope.lastLoadedSequenceId = 0;
      $scope.lastReadSequenceId = $scope.discussion.lastReadSequenceId;
      $scope.hasNewActivity = $scope.discussion.isUnread();
      $scope.pagination = function(current) {
        return PaginationService.windowFor({
          current: current,
          min: $scope.discussion.firstSequenceId,
          max: $scope.discussion.lastSequenceId,
          pageType: 'activityItems'
        });
      };
      visibleSequenceIds = [];
      $scope.init = function() {
        $scope.discussion.markAsRead(0);
        return $scope.loadEventsForwards({
          commentId: $scope.activeCommentId,
          sequenceId: $scope.initialLoadSequenceId()
        }).then(function() {
          return $rootScope.$broadcast('threadPageEventsLoaded');
        });
      };
      $scope.initialLoadSequenceId = function() {
        if ($location.search().from) {
          return $location.search().from;
        }
        if (!AbilityService.isLoggedIn()) {
          return 0;
        }
        if ($scope.discussion.isUnread()) {
          return $scope.discussion.lastReadSequenceId - 5;
        }
        return $scope.pagination($scope.discussion.lastSequenceId).prev;
      };
      $scope.beforeCount = function() {
        return $scope.firstLoadedSequenceId - $scope.discussion.firstSequenceId;
      };
      updateLastSequenceId = function() {
        visibleSequenceIds = _.uniq(visibleSequenceIds);
        return $rootScope.$broadcast('threadPosition', $scope.discussion, _.max(visibleSequenceIds));
      };
      addSequenceId = function(id) {
        visibleSequenceIds.push(id);
        return updateLastSequenceId();
      };
      removeSequenceId = function(id) {
        visibleSequenceIds = _.without(visibleSequenceIds, id);
        return updateLastSequenceId();
      };
      $scope.threadItemHidden = function(item) {
        return removeSequenceId(item.sequenceId);
      };
      $scope.threadItemVisible = function(item) {
        addSequenceId(item.sequenceId);
        $scope.discussion.markAsRead(item.sequenceId);
        if ($scope.loadMoreAfterReading(item)) {
          return $scope.loadEventsForwards({
            sequenceId: $scope.lastLoadedSequenceId
          });
        }
      };
      $scope.loadEvents = function(arg) {
        var commentId, from, per;
        from = arg.from, per = arg.per, commentId = arg.commentId;
        if (from == null) {
          from = 0;
        }
        per = per || $scope.pagination().pageSize;
        return Records.events.fetchByDiscussion($scope.discussion.key, {
          from: from,
          per: per,
          comment_id: commentId
        }).then(function() {
          $scope.firstLoadedSequenceId = $scope.discussion.minLoadedSequenceId();
          return $scope.lastLoadedSequenceId = $scope.discussion.maxLoadedSequenceId();
        });
      };
      $scope.loadEventsForwards = function(arg) {
        var commentId, sequenceId;
        commentId = arg.commentId, sequenceId = arg.sequenceId;
        return $scope.loadEvents({
          commentId: commentId,
          from: sequenceId
        });
      };
      LoadingService.applyLoadingFunction($scope, 'loadEventsForwards');
      $scope.loadEventsBackwards = function() {
        return $scope.loadEvents({
          from: $scope.pagination($scope.firstLoadedSequenceId).prev
        });
      };
      LoadingService.applyLoadingFunction($scope, 'loadEventsBackwards');
      $scope.canLoadBackwards = function() {
        return $scope.firstLoadedSequenceId > $scope.discussion.firstSequenceId && !($scope.loadEventsForwardsExecuting || $scope.loadEventsBackwardsExecuting);
      };
      $scope.loadMoreAfterReading = function(item) {
        return item.sequenceId === $scope.lastLoadedSequenceId && item.sequenceId < $scope.discussion.lastSequenceId;
      };
      $scope.safeEvent = function(kind) {
        return _.contains(AppConfig.safeThreadItemKinds, kind);
      };
      $scope.events = function() {
        return _.filter($scope.discussion.events(), function(event) {
          return $scope.safeEvent(event.kind);
        });
      };
      $scope.noEvents = function() {
        return !$scope.loadEventsForwardsExecuting && !_.any($scope.events());
      };
      $scope.init();
    }
  };
});

angular.module('loomioApp').controller('DiscussionEditedItemController', function($scope, Records) {
  var discussion, version;
  version = Records.versions.find($scope.event.eventable.id);
  discussion = Records.discussions.find(version.discussionId);
  $scope.title = version.attributeEdited('title') ? version.changes.title[1] : '';
  $scope.onlyPrivacyEdited = function() {
    return version.attributeEdited('private') && !version.attributeEdited('title') && !version.attributeEdited('description');
  };
  $scope.privacyKey = function() {
    if (!$scope.onlyPrivacyEdited()) {
      return;
    }
    if (version.changes["private"][1]) {
      return 'discussion_edited_item.public_to_private';
    } else {
      return 'discussion_edited_item.private_to_public';
    }
  };
  return $scope.translationKey = _.isEqual(version.editedAttributeNames(), ['attachment_ids']) ? 'attachment_ids' : _.without(version.editedAttributeNames(), 'attachment_ids').join('_');
});

angular.module('loomioApp').controller('DiscussionMovedItemController', function($scope, Records) {
  return $scope.group = Records.groups.find($scope.event.eventable.id);
});

angular.module('loomioApp').controller('MotionClosedItemController', function($scope, Records) {
  return $scope.proposal = Records.proposals.find($scope.event.eventable.id);
});

angular.module('loomioApp').controller('MotionClosedItemController', function($scope, Records) {
  return $scope.proposal = Records.proposals.find($scope.event.eventable.id);
});

angular.module('loomioApp').controller('MotionEditedItemController', function($scope, Records) {
  var version;
  version = Records.versions.find($scope.event.eventable.id);
  $scope.closingAt = version.attributeEdited('closing_at') ? moment(version.changes.closing_at[1]).format("ha dddd, Do MMMM YYYY") : void 0;
  $scope.title = version.attributeEdited('name') ? version.changes.name[1] : version.proposal().name;
  return $scope.translationKey = version.editedAttributeNames().join('_');
});

angular.module('loomioApp').controller('MotionOutcomeCreatedItemController', function($scope, Records) {
  return $scope.proposal = Records.proposals.find($scope.event.eventable.id);
});

angular.module('loomioApp').controller('MotionOutcomeUpdatedItemController', function($scope, Records) {
  return $scope.proposal = Records.proposals.find($scope.event.eventable.id);
});

angular.module('loomioApp').controller('NewCommentItemController', function($scope, $rootScope, $translate, Records, Session, ModalService, EditCommentForm, DeleteCommentForm, AbilityService, TranslationService, RevisionHistoryModal) {
  var renderLikedBySentence, updateLikedBySentence;
  $scope.comment = Records.comments.find($scope.event.eventable.id);
  renderLikedBySentence = function() {
    var joinedNames, name, otherIds, otherNames, otherUsers;
    otherIds = _.without($scope.comment.likerIds, Session.user().id);
    otherUsers = _.filter($scope.comment.likers(), function(user) {
      return _.contains(otherIds, user.id);
    });
    otherNames = _.map(otherUsers, function(user) {
      return user.name;
    });
    if ($scope.currentUserLikesIt()) {
      switch (otherNames.length) {
        case 0:
          return $translate('discussion.you_like_this').then(updateLikedBySentence);
        case 1:
          return $translate('discussion.liked_by_you_and_someone', {
            name: otherNames[0]
          }).then(updateLikedBySentence);
        default:
          joinedNames = otherNames.slice(0, -1).join(', ');
          name = otherNames.slice(-1)[0];
          return $translate('discussion.liked_by_you_and_others', {
            joinedNames: joinedNames,
            name: name
          }).then(updateLikedBySentence);
      }
    } else {
      switch (otherNames.length) {
        case 0:
          return '';
        case 1:
          return $translate('discussion.liked_by_someone', {
            name: otherNames[0]
          }).then(updateLikedBySentence);
        case 2:
          return $translate('discussion.liked_by_two_others', {
            name_1: otherNames[0],
            name_2: otherNames[1]
          }).then(updateLikedBySentence);
        default:
          joinedNames = otherNames.slice(0, -1).join(', ');
          name = otherNames.slice(-1)[0];
          return $translate('discussion.liked_by_many_others', {
            joinedNames: joinedNames,
            name: name
          }).then(updateLikedBySentence);
      }
    }
  };
  $scope.editComment = function() {
    return ModalService.open(EditCommentForm, {
      comment: function() {
        return $scope.comment;
      }
    });
  };
  $scope.deleteComment = function() {
    return ModalService.open(DeleteCommentForm, {
      comment: function() {
        return $scope.comment;
      }
    });
  };
  $scope.showContextMenu = function() {
    return $scope.canEditComment($scope.comment) || $scope.canDeleteComment($scope.comment);
  };
  $scope.canEditComment = function() {
    return AbilityService.canEditComment($scope.comment);
  };
  $scope.canDeleteComment = function() {
    return AbilityService.canDeleteComment($scope.comment);
  };
  $scope.showCommentActions = function() {
    return AbilityService.canRespondToComment($scope.comment);
  };
  $scope.like = function() {
    $scope.addLiker();
    return Records.comments.like(Session.user(), $scope.comment).then((function() {}), $scope.removeLiker);
  };
  $scope.unlike = function() {
    $scope.removeLiker();
    return Records.comments.unlike(Session.user(), $scope.comment).then((function() {}), $scope.addLiker);
  };
  $scope.currentUserLikesIt = function() {
    return _.contains($scope.comment.likerIds, Session.user().id);
  };
  $scope.anybodyLikesIt = function() {
    return $scope.comment.likerIds.length > 0;
  };
  $scope.likedBySentence = '';
  updateLikedBySentence = function(sentence) {
    return $scope.likedBySentence = sentence;
  };
  $scope.addLiker = function() {
    $scope.comment.addLiker(Session.user());
    return renderLikedBySentence();
  };
  $scope.removeLiker = function() {
    $scope.comment.removeLiker(Session.user());
    return renderLikedBySentence();
  };
  $scope.$watch('comment.likerIds', function() {
    return renderLikedBySentence();
  });
  $scope.reply = function() {
    return $rootScope.$broadcast('replyToCommentClicked', $scope.comment);
  };
  $scope.showRevisionHistory = function() {
    return ModalService.open(RevisionHistoryModal, {
      model: (function(_this) {
        return function() {
          return $scope.comment;
        };
      })(this)
    });
  };
  return TranslationService.listenForTranslations($scope);
});

angular.module('loomioApp').controller('NewMotionItemController', function($scope, Records) {
  return $scope.proposal = Records.proposals.find($scope.event.eventable.id);
});

angular.module('loomioApp').controller('NewVoteItemController', function($scope, Records, TranslationService) {
  $scope.vote = Records.votes.find($scope.event.eventable.id);
  return TranslationService.listenForTranslations($scope);
});

angular.module('loomioApp').controller('OutcomeCreatedItemController', function($scope, Records) {
  return $scope.outcome = Records.outcomes.find($scope.event.eventable.id);
});

angular.module('loomioApp').controller('PollClosedByUserItemController', function($scope, Records) {
  return $scope.poll = Records.polls.find($scope.event.eventable.id);
});

angular.module('loomioApp').controller('PollCreatedItemController', function($scope, Records) {
  return $scope.poll = Records.polls.find($scope.event.eventable.id);
});

angular.module('loomioApp').controller('PollEditedItemController', function($scope, Records) {
  $scope.version = Records.versions.find($scope.event.eventable.id);
  return $scope.poll = $scope.version.poll();
});

angular.module('loomioApp').controller('PollExpiredItemController', function($scope, Records) {
  return $scope.poll = Records.polls.find($scope.event.eventable.id);
});

angular.module('loomioApp').controller('StanceCreatedItemController', function($scope, Records, TranslationService) {
  $scope.stance = Records.stances.find($scope.event.eventable.id);
  return TranslationService.listenForTranslations($scope);
});

angular.module('loomioApp').factory('CloseProposalForm', function() {
  return {
    templateUrl: 'generated/components/thread_page/close_proposal_form/close_proposal_form.html',
    controller: function($scope, $rootScope, FormService, proposal) {
      $scope.proposal = proposal;
      return $scope.submit = FormService.submit($scope, $scope.proposal, {
        submitFn: $scope.proposal.close,
        flashSuccess: 'close_proposal_form.messages.success',
        successCallback: function() {
          return $rootScope.$broadcast('setSelectedProposal');
        }
      });
    }
  };
});

angular.module('loomioApp').directive('commentForm', function() {
  return {
    scope: {
      discussion: '='
    },
    restrict: 'E',
    templateUrl: 'generated/components/thread_page/comment_form/comment_form.html',
    replace: true,
    controller: function($scope, $rootScope, FormService, Records, Session, KeyEventService, AbilityService, MentionService, AttachmentService, ScrollService, EmojiService, ModalService, AuthModal) {
      var successMessage, successMessageName;
      $scope.$on('remindUndecided', function(event) {
        var undecided;
        if (!$scope.discussion.activeProposal()) {
          return;
        }
        ScrollService.scrollTo('.comment-form__comment-field');
        undecided = _.map($scope.discussion.activeProposal().undecidedMembers(), function(member) {
          return "@" + member.username;
        });
        return $scope.comment.body = undecided.join(', ');
      });
      $scope.showCommentForm = function() {
        return AbilityService.canAddComment($scope.discussion);
      };
      $scope.isLoggedIn = AbilityService.isLoggedIn;
      $scope.signIn = function() {
        return ModalService.open(AuthModal);
      };
      $scope.threadIsPublic = function() {
        return $scope.discussion["private"] === false;
      };
      $scope.threadIsPrivate = function() {
        return $scope.discussion["private"] === true;
      };
      successMessage = function() {
        if ($scope.comment.isReply()) {
          return 'comment_form.messages.replied';
        } else {
          return 'comment_form.messages.created';
        }
      };
      successMessageName = function() {
        if ($scope.comment.isReply()) {
          return $scope.comment.parent().authorName();
        }
      };
      $scope.listenForSubmitOnEnter = function() {
        return KeyEventService.submitOnEnter($scope);
      };
      $scope.$on('voteCreated', $scope.listenForSubmitOnEnter);
      $scope.$on('proposalCreated', $scope.listenForSubmitOnEnter);
      $scope.init = function() {
        $scope.comment = Records.comments.build({
          discussionId: $scope.discussion.id,
          authorId: Session.user().id
        });
        $scope.submit = FormService.submit($scope, $scope.comment, {
          drafts: true,
          submitFn: $scope.comment.save,
          flashSuccess: successMessage,
          flashOptions: {
            name: successMessageName
          },
          successCallback: $scope.init
        });
        $scope.listenForSubmitOnEnter();
        return $scope.$broadcast('commentFormInit', $scope.comment);
      };
      $scope.init();
      $scope.$on('replyToCommentClicked', function(event, parentComment) {
        $scope.comment.parentId = parentComment.id;
        $scope.comment.parentAuthorName = parentComment.authorName();
        return ScrollService.scrollTo('.comment-form__comment-field');
      });
      $scope.bodySelector = '.comment-form__comment-field';
      EmojiService.listen($scope, $scope.comment, 'body', $scope.bodySelector);
      AttachmentService.listenForPaste($scope);
      MentionService.applyMentions($scope, $scope.comment);
      return AttachmentService.listenForAttachments($scope, $scope.comment);
    }
  };
});

angular.module('loomioApp').factory('DeleteCommentForm', function() {
  return {
    templateUrl: 'generated/components/thread_page/comment_form/delete_comment_form.html',
    controller: function($scope, $rootScope, Records, comment) {
      $scope.comment = comment;
      return $scope.submit = function() {
        return $scope.comment.destroy().then(function() {
          $scope.$close();
          return _.find($scope.comment.discussion().events(), function(event) {
            return event.kind === 'new_comment' && event.eventable.id === $scope.comment.id;
          })["delete"]();
        }, function() {
          return $rootScope.$broadcast('pageError', 'cantDeleteComment');
        });
      };
    }
  };
});

angular.module('loomioApp').factory('EditCommentForm', function() {
  return {
    templateUrl: 'generated/components/thread_page/comment_form/edit_comment_form.html',
    controller: function($scope, comment, FormService, EmojiService, MentionService) {
      $scope.comment = comment.clone();
      $scope.submit = FormService.submit($scope, $scope.comment, {
        flashSuccess: 'comment_form.messages.updated'
      });
      $scope.bodySelector = '.edit-comment-form__comment-field';
      EmojiService.listen($scope, $scope.comment, 'body', $scope.bodySelector);
      return MentionService.applyMentions($scope, $scope.comment);
    }
  };
});

angular.module('loomioApp').directive('contextPanel', function() {
  return {
    scope: {
      discussion: '='
    },
    restrict: 'E',
    replace: true,
    templateUrl: 'generated/components/thread_page/context_panel/context_panel.html',
    controller: function($scope, $rootScope, $window, $timeout, AbilityService, Session, ModalService, ChangeVolumeForm, DiscussionForm, ThreadService, MoveThreadForm, PrintModal, DeleteThreadForm, RevisionHistoryModal, TranslationService, ScrollService) {
      $scope.showContextMenu = function() {
        return AbilityService.canChangeThreadVolume($scope.discussion);
      };
      $scope.canChangeVolume = function() {
        return Session.user().isMemberOf($scope.discussion.group());
      };
      $scope.openChangeVolumeForm = function() {
        return ModalService.open(ChangeVolumeForm, {
          model: (function(_this) {
            return function() {
              return $scope.discussion;
            };
          })(this)
        });
      };
      $scope.canEditThread = function() {
        return AbilityService.canEditThread($scope.discussion);
      };
      $scope.editThread = function() {
        return ModalService.open(DiscussionForm, {
          discussion: (function(_this) {
            return function() {
              return $scope.discussion;
            };
          })(this)
        });
      };
      $scope.scrollToCommentForm = function() {
        return ScrollService.scrollTo('.comment-form__comment-field');
      };
      $scope.muteThread = function() {
        return ThreadService.mute($scope.discussion);
      };
      $scope.unmuteThread = function() {
        return ThreadService.unmute($scope.discussion);
      };
      $scope.canMoveThread = function() {
        return AbilityService.canMoveThread($scope.discussion);
      };
      $scope.moveThread = function() {
        return ModalService.open(MoveThreadForm, {
          discussion: (function(_this) {
            return function() {
              return $scope.discussion;
            };
          })(this)
        });
      };
      $scope.requestPagePrinted = function() {
        $rootScope.$broadcast('toggleSidebar', false);
        if ($scope.discussion.allEventsLoaded()) {
          return $timeout(function() {
            return $window.print();
          });
        } else {
          ModalService.open(PrintModal, {
            preventClose: function() {
              return true;
            }
          });
          return $rootScope.$broadcast('fetchRecordsForPrint');
        }
      };
      $scope.canDeleteThread = function() {
        return AbilityService.canDeleteThread($scope.discussion);
      };
      $scope.deleteThread = function() {
        return ModalService.open(DeleteThreadForm, {
          discussion: (function(_this) {
            return function() {
              return $scope.discussion;
            };
          })(this)
        });
      };
      $scope.showLintel = function(bool) {
        return $rootScope.$broadcast('showThreadLintel', bool);
      };
      $scope.showRevisionHistory = function() {
        return ModalService.open(RevisionHistoryModal, {
          model: (function(_this) {
            return function() {
              return $scope.discussion;
            };
          })(this)
        });
      };
      $scope.canAddComment = function() {
        return AbilityService.canAddComment($scope.discussion);
      };
      return TranslationService.listenForTranslations($scope);
    }
  };
});

angular.module('loomioApp').directive('currentProposalCard', function() {
  return {
    scope: {
      proposal: '=',
      loading: '='
    },
    restrict: 'E',
    templateUrl: 'generated/components/thread_page/current_proposal_card/current_proposal_card.html',
    replace: true
  };
});

angular.module('loomioApp').directive('decisionToolsCard', function() {
  return {
    scope: {
      discussion: '='
    },
    restrict: 'E',
    templateUrl: 'generated/components/thread_page/decision_tools_card/decision_tools_card.html',
    replace: true
  };
});

angular.module('loomioApp').factory('ExtendProposalForm', function() {
  return {
    templateUrl: 'generated/components/thread_page/extend_proposal_form/extend_proposal_form.html',
    controller: function($scope, proposal, FlashService) {
      $scope.proposal = proposal.clone();
      $scope.submit = function() {
        return $scope.proposal.save().then(function() {
          $scope.$close();
          return FlashService.success('extend_proposal_form.success');
        });
      };
      return $scope.cancel = function($event) {
        return $scope.$close();
      };
    }
  };
});

angular.module('loomioApp').directive('positionButtonsPanel', function() {
  return {
    scope: {
      proposal: '='
    },
    restrict: 'E',
    templateUrl: 'generated/components/thread_page/position_buttons_panel/position_buttons_panel.html',
    replace: true,
    controller: function($scope, ModalService, VoteForm, Session, Records, AbilityService) {
      $scope.showPositionButtons = function() {
        return AbilityService.canVoteOn($scope.proposal) && $scope.undecided();
      };
      $scope.undecided = function() {
        return !($scope.proposal.lastVoteByUser(Session.user()) != null);
      };
      $scope.$on('triggerVoteForm', function(event, position) {
        var myVote;
        myVote = $scope.proposal.lastVoteByUser(Session.user()) || {};
        return $scope.select(position, myVote.statement);
      });
      return $scope.select = function(position) {
        return ModalService.open(VoteForm, {
          vote: function() {
            return Records.votes.build({
              proposalId: $scope.proposal.id,
              position: position
            });
          }
        });
      };
    }
  };
});

angular.module('loomioApp').directive('previousProposalsCard', function() {
  return {
    scope: {
      discussion: '='
    },
    restrict: 'E',
    templateUrl: 'generated/components/thread_page/previous_proposals_card/previous_proposals_card.html',
    replace: true,
    controller: function($scope, $rootScope, Records) {
      var lastRecentlyClosedProposal;
      Records.votes.fetchMyVotes($scope.discussion);
      Records.proposals.fetchByDiscussion($scope.discussion).then(function() {
        return $rootScope.$broadcast('threadPageProposalsLoaded');
      });
      lastRecentlyClosedProposal = function() {
        var proposal;
        if (!($scope.anyProposals() && !$scope.discussion.hasActiveProposal())) {
          return;
        }
        proposal = $scope.discussion.closedProposals()[0];
        if (moment().add(-1, 'month') < proposal.closedOrClosingAt) {
          return proposal;
        }
      };
      $scope.$on('setSelectedProposal', function(event, proposal) {
        return $scope.selectedProposalId = (proposal || lastRecentlyClosedProposal() || {}).id;
      });
      $scope.anyProposals = function() {
        return $scope.discussion.closedProposals().length > 0;
      };
    }
  };
});

angular.module('loomioApp').factory('PrintModal', function() {
  return {
    templateUrl: 'generated/components/thread_page/print_modal/print_modal.html',
    controller: function($scope) {}
  };
});

angular.module('loomioApp').directive('proposalCollapsed', function() {
  return {
    scope: {
      proposal: '='
    },
    restrict: 'E',
    templateUrl: 'generated/components/thread_page/proposal_collapsed/proposal_collapsed.html',
    replace: true,
    controller: function($scope, Session) {
      return $scope.lastVoteByCurrentUser = function(proposal) {
        return proposal.lastVoteByUser(Session.user());
      };
    }
  };
});

angular.module('loomioApp').directive('proposalExpanded', function() {
  return {
    scope: {
      proposal: '=',
      canCollapse: '='
    },
    restrict: 'E',
    templateUrl: 'generated/components/thread_page/proposal_expanded/proposal_expanded.html',
    replace: true,
    controller: function($scope, Records, Session, AbilityService, TranslationService) {
      Records.proposals.findOrFetchById($scope.proposal.key);
      Records.votes.fetchByProposal($scope.proposal);
      $scope.collapse = function() {
        return $scope.$emit('collapseProposal');
      };
      $scope.showActionsDropdown = function() {
        return AbilityService.canCloseOrExtendProposal($scope.proposal);
      };
      $scope.onlyVoterIsYou = function() {
        var uniqueVotes;
        uniqueVotes = $scope.proposal.uniqueVotes();
        return (uniqueVotes.length === 1) && (uniqueVotes[0].authorId === Session.user().id);
      };
      $scope.currentUserHasVoted = function() {
        return $scope.proposal.userHasVoted(Session.user());
      };
      $scope.currentUserVote = function() {
        return $scope.proposal.lastVoteByUser(Session.user());
      };
      $scope.showOutcomePanel = function() {
        return $scope.proposal.hasOutcome() || AbilityService.canCreateOutcomeFor($scope.proposal);
      };
      return TranslationService.listenForTranslations($scope);
    }
  };
});

angular.module('loomioApp').factory('ProposalOutcomeForm', function() {
  return {
    templateUrl: 'generated/components/thread_page/proposal_outcome_form/proposal_outcome_form.html',
    controller: function($scope, proposal, FormService, EmojiService) {
      $scope.proposal = proposal.clone();
      $scope.hasOutcome = proposal.hasOutcome();
      return $scope.submit = FormService.submit($scope, $scope.proposal, !$scope.hasOutcome ? {
        submitFn: $scope.proposal.createOutcome,
        flashSuccess: 'proposal_outcome_form.messages.created'
      } : {
        submitFn: $scope.proposal.updateOutcome,
        flashSuccess: 'proposal_outcome_form.messages.updated'
      }, $scope.outcomeSelector = '.proposal-form__outcome-field', EmojiService.listen($scope, $scope.proposal, 'outcome', $scope.outcomeSelector));
    }
  };
});

angular.module('loomioApp').directive('proposalOutcomePanel', function() {
  return {
    scope: {
      proposal: '='
    },
    restrict: 'E',
    templateUrl: 'generated/components/thread_page/proposal_outcome_panel/proposal_outcome_panel.html',
    replace: true,
    controller: function($scope, AbilityService, ModalService, ProposalOutcomeForm) {
      $scope.canCreateOutcome = function() {
        return AbilityService.canCreateOutcomeFor($scope.proposal);
      };
      $scope.openProposalOutcomeForm = function() {
        return ModalService.open(ProposalOutcomeForm, {
          proposal: (function(_this) {
            return function() {
              return $scope.proposal;
            };
          })(this)
        });
      };
      $scope.canUpdateOutcome = function() {
        return AbilityService.canUpdateOutcomeFor($scope.proposal);
      };
    }
  };
});

angular.module('loomioApp').directive('proposalPositionsPanel', function() {
  return {
    scope: {
      proposal: '='
    },
    restrict: 'E',
    templateUrl: 'generated/components/thread_page/proposal_positions_panel/proposal_positions_panel.html',
    replace: true,
    controller: function($scope, Records, Session, ModalService, VoteForm, AbilityService) {
      var sortValueForVote;
      $scope.undecidedPanelOpen = false;
      $scope.changeVote = function() {
        return ModalService.open(VoteForm, {
          vote: function() {
            return $scope.proposal.lastVoteByUser(Session.user()).clone();
          }
        });
      };
      sortValueForVote = function(vote) {
        var positionValues;
        positionValues = {
          yes: 1,
          abstain: 2,
          no: 3,
          block: 4
        };
        if ($scope.voteIsMine(vote)) {
          return 0;
        } else {
          return positionValues[vote.position];
        }
      };
      $scope.curatedVotes = function() {
        return _.sortBy($scope.proposal.uniqueVotes(), function(vote) {
          return sortValueForVote(vote);
        });
      };
      $scope.voteIsMine = function(vote) {
        return vote.authorId === Session.user().id;
      };
      $scope.showChangeVoteOption = function(vote) {
        return AbilityService.canVoteOn($scope.proposal) && $scope.voteIsMine(vote);
      };
      return $scope.showUndecided = function() {
        return $scope.undecidedPanelOpen = true;
      };
    }
  };
});

angular.module('loomioApp').directive('proposalsCard', function() {
  return {
    scope: {},
    bindToController: {
      discussion: '='
    },
    restrict: 'E',
    templateUrl: 'generated/components/thread_page/proposals_card/proposals_card.html',
    replace: true,
    controllerAs: 'proposalsCard',
    controller: function(Records) {
      Records.proposals.fetchByDiscussion(this.discussion);
      Records.votes.fetchMyVotes(this.discussion);
      this.isExpanded = function(proposal) {
        if (this.selectedProposal != null) {
          return proposal.id === this.selectedProposal.id;
        } else {
          return proposal.isActive();
        }
      };
      this.selectProposal = (function(_this) {
        return function(proposal) {
          return _this.selectedProposal = proposal;
        };
      })(this);
      this.canStartProposal = (function(_this) {
        return function() {
          return AbilityService.canStartProposal(_this.discussion);
        };
      })(this);
    }
  };
});

angular.module('loomioApp').factory('RevisionHistoryModal', function() {
  return {
    templateUrl: 'generated/components/thread_page/revision_history_modal/revision_history_modal.html',
    controller: function($scope, model, Records, LoadingService) {
      $scope.model = model;
      $scope.loading = true;
      $scope.load = function() {
        switch ($scope.model.constructor.singular) {
          case 'discussion':
            return Records.versions.fetchByDiscussion($scope.model.key);
          case 'comment':
            return Records.versions.fetchByComment($scope.model.id);
        }
      };
      $scope.header = (function() {
        switch ($scope.model.constructor.singular) {
          case 'discussion':
            return 'revision_history_modal.thread_header';
          case 'comment':
            return 'revision_history_modal.comment_header';
        }
      })();
      $scope.discussionRevision = function() {
        return $scope.model.constructor.singular === 'discussion';
      };
      $scope.commentRevision = function() {
        return $scope.model.constructor.singular === 'comment';
      };
      $scope.threadTitle = function(version) {
        return $scope.model.attributeForVersion('title', version);
      };
      $scope.revisionBody = function(version) {
        switch ($scope.model.constructor.singular) {
          case 'discussion':
            return $scope.model.attributeForVersion('description', version);
          case 'comment':
            return $scope.model.attributeForVersion('body', version);
        }
      };
      $scope.threadDetails = function(version) {
        if (version.isOriginal()) {
          return 'revision_history_modal.started_by';
        } else {
          return 'revision_history_modal.edited_by';
        }
      };
      $scope.versionCreatedAt = function(version) {
        return moment(version).format('Do MMMM YYYY, h:mma');
      };
      LoadingService.applyLoadingFunction($scope, 'load');
      $scope.load();
    }
  };
});

angular.module('loomioApp').directive('startProposalButton', function() {
  return {
    scope: {
      discussion: '='
    },
    restrict: 'E',
    templateUrl: 'generated/components/thread_page/start_proposal_button/start_proposal_button.html',
    replace: true,
    controller: function($scope, Records, ModalService, ProposalForm, AbilityService) {
      $scope.canStartProposal = function() {
        return AbilityService.canStartProposal($scope.discussion);
      };
      return $scope.startProposal = function() {
        return ModalService.open(ProposalForm, {
          proposal: function() {
            return Records.proposals.build({
              discussionId: $scope.discussion.id
            });
          }
        });
      };
    }
  };
});

angular.module('loomioApp').directive('startProposalCard', function() {
  return {
    scope: {
      discussion: '='
    },
    restrict: 'E',
    templateUrl: 'generated/components/thread_page/start_proposal_card/start_proposal_card.html',
    replace: true
  };
});

angular.module('loomioApp').directive('undecidedPanel', function() {
  return {
    scope: {
      proposal: '='
    },
    restrict: 'E',
    templateUrl: 'generated/components/thread_page/undecided_panel/undecided_panel.html',
    replace: true,
    controller: function($scope, $timeout, $rootScope, Records, ScrollService) {
      $scope.undecidedPanelOpen = false;
      $scope.showUndecided = function() {
        $scope.proposal.fetchUndecidedMembers();
        $scope.undecidedPanelOpen = true;
        return $timeout(function() {
          return document.querySelector('.undecided-panel__heading').focus();
        });
      };
      $scope.hideUndecided = function() {
        return $scope.undecidedPanelOpen = false;
      };
      return $scope.remindUndecided = function() {
        return $rootScope.$broadcast('remindUndecided');
      };
    }
  };
});

angular.module('loomioApp').factory('VoteForm', function() {
  return {
    templateUrl: 'generated/components/thread_page/vote_form/vote_form.html',
    controller: function($scope, vote, Session, FlashService, FormService, KeyEventService, EmojiService) {
      $scope.vote = vote.clone();
      $scope.editing = false;
      $scope.$on('modal.closing', function(event) {
        return FormService.confirmDiscardChanges(event, $scope.vote);
      });
      $scope.submit = FormService.submit($scope, $scope.vote, {
        flashSuccess: 'vote_form.messages.created',
        successEvent: 'voteCreated'
      });
      $scope.yourLastVote = function() {
        return $scope.vote.proposal().lastVoteByUser(Session.user());
      };
      $scope.statementSelector = '.vote-form__statement-field';
      EmojiService.listen($scope, $scope.vote, 'statement', $scope.statementSelector);
      return KeyEventService.submitOnEnter($scope);
    }
  };
});

angular.module('loomioApp').directive('pollCommonActionPanel', function($location, AppConfig, ModalService, AbilityService, PollService, Session, Records, PollCommonEditVoteModal) {
  return {
    scope: {
      poll: '='
    },
    templateUrl: 'generated/components/poll/common/action_panel/poll_common_action_panel.html',
    controller: function($scope, Records, Session) {
      $scope.init = function() {
        return $scope.stance = PollService.lastStanceBy(Session.participant(), $scope.poll) || Records.stances.build({
          pollId: $scope.poll.id,
          visitorId: AppConfig.currentVisitorId,
          userId: AppConfig.currentUserId
        }).choose($location.search().poll_option_id);
      };
      $scope.$on('refreshStance', $scope.init);
      $scope.init();
      $scope.userHasVoted = function() {
        return PollService.hasVoted(Session.participant(), $scope.poll);
      };
      return $scope.openStanceForm = function() {
        return ModalService.open(PollCommonEditVoteModal, {
          stance: function() {
            return $scope.init();
          }
        });
      };
    }
  };
});

angular.module('loomioApp').directive('pollCommonActionsDropdown', function() {
  return {
    scope: {
      poll: '='
    },
    templateUrl: 'generated/components/poll/common/actions_dropdown/poll_common_actions_dropdown.html',
    controller: function($scope, AbilityService, ModalService, PollCommonShareModal, PollCommonFormModal, PollCommonCloseForm, PollCommonDeleteModal, PollCommonUnsubscribeModal) {
      $scope.canSharePoll = function() {
        return AbilityService.canSharePoll($scope.poll);
      };
      $scope.canEditPoll = function() {
        return AbilityService.canEditPoll($scope.poll);
      };
      $scope.canClosePoll = function() {
        return AbilityService.canClosePoll($scope.poll);
      };
      $scope.canDeletePoll = function() {
        return AbilityService.canDeletePoll($scope.poll);
      };
      $scope.sharePoll = function() {
        return ModalService.open(PollCommonShareModal, {
          poll: function() {
            return $scope.poll;
          }
        });
      };
      $scope.editPoll = function() {
        return ModalService.open(PollCommonFormModal, {
          poll: function() {
            return $scope.poll;
          }
        });
      };
      $scope.closePoll = function() {
        return ModalService.open(PollCommonCloseForm, {
          poll: function() {
            return $scope.poll;
          }
        });
      };
      $scope.deletePoll = function() {
        return ModalService.open(PollCommonDeleteModal, {
          poll: function() {
            return $scope.poll;
          }
        });
      };
      return $scope.toggleSubscription = function() {
        return ModalService.open(PollCommonUnsubscribeModal, {
          poll: function() {
            return $scope.poll;
          }
        });
      };
    }
  };
});

angular.module('loomioApp').directive('pollCommonAttachmentForm', function(AttachmentService) {
  return {
    scope: {
      poll: '='
    },
    restrict: 'E',
    templateUrl: 'generated/components/poll/common/attachment_form/poll_common_attachment_form.html',
    replace: true,
    controller: function($scope) {
      return AttachmentService.listenForAttachments($scope, $scope.poll);
    }
  };
});

angular.module('loomioApp').directive('pollCommonBarChart', function(AppConfig, Records) {
  return {
    scope: {
      poll: '='
    },
    templateUrl: 'generated/components/poll/common/bar_chart/poll_common_bar_chart.html',
    controller: function($scope) {
      var backgroundImageFor, percentageFor;
      $scope.countFor = function(option) {
        return $scope.poll.stanceData[option.name] || 0;
      };
      $scope.barTextFor = function(option) {
        return (($scope.countFor(option)) + " - " + option.name).replace(/\s/g, '\u00a0');
      };
      percentageFor = function(option) {
        var max;
        max = _.max(_.values($scope.poll.stanceData));
        if (!(max > 0)) {
          return;
        }
        return (100 * $scope.countFor(option) / max) + "%";
      };
      backgroundImageFor = function(option) {
        return "url(/img/poll_backgrounds/" + (option.color.replace('#', '')) + ".png)";
      };
      return $scope.styleData = function(option) {
        return {
          'background-image': backgroundImageFor(option),
          'background-size': percentageFor(option)
        };
      };
    }
  };
});

angular.module('loomioApp').directive('pollCommonBarChartPanel', function(AppConfig, Records) {
  return {
    scope: {
      poll: '='
    },
    templateUrl: 'generated/components/poll/common/bar_chart_panel/poll_common_bar_chart_panel.html'
  };
});

angular.module('loomioApp').directive('pollCommonCalendarInvite', function(Records, PollService, TimeService) {
  return {
    scope: {
      outcome: '='
    },
    templateUrl: 'generated/components/poll/common/calendar_invite/poll_common_calendar_invite.html',
    controller: function($scope) {
      var bestOption;
      $scope.options = _.map($scope.outcome.poll().pollOptions(), function(option) {
        return {
          id: option.id,
          value: TimeService.displayDate(moment(option.name)),
          attendees: option.stances().length
        };
      });
      bestOption = _.first(_.sortBy($scope.options, function(option) {
        return -1 * option.attendees;
      }));
      $scope.outcome.calendarInvite = true;
      $scope.outcome.pollOptionId = $scope.outcome.pollOptionId || bestOption.id;
      return $scope.outcome.customFields.event_summary = $scope.outcome.customFields.event_summary || $scope.outcome.poll().title;
    }
  };
});

angular.module('loomioApp').directive('pollCommonCard', function(Session, Records, LoadingService, PollService) {
  return {
    scope: {
      poll: '='
    },
    templateUrl: 'generated/components/poll/common/card/poll_common_card.html',
    replace: true,
    controller: function($scope) {
      $scope.init = function() {
        return Records.polls.findOrFetchById($scope.poll.key);
      };
      LoadingService.applyLoadingFunction($scope, 'init');
      if (!$scope.poll.complete) {
        $scope.init();
      }
      $scope.buttonPressed = false;
      $scope.$on('showResults', function() {
        return $scope.buttonPressed = true;
      });
      LoadingService.listenForLoading($scope);
      $scope.showResults = function() {
        return $scope.buttonPressed || PollService.hasVoted(Session.participant(), $scope.poll) || $scope.poll.isClosed();
      };
      return $scope.$on('stanceSaved', function() {
        return $scope.$broadcast('refreshStance');
      });
    }
  };
});

angular.module('loomioApp').directive('pollCommonCardHeader', function() {
  return {
    scope: {
      poll: '='
    },
    templateUrl: 'generated/components/poll/common/card_header/poll_common_card_header.html',
    controller: function($scope, AbilityService, PollService) {
      $scope.pollHasActions = function() {
        return AbilityService.canSharePoll($scope.poll) || AbilityService.canEditPoll($scope.poll) || AbilityService.canClosePoll($scope.poll) || AbilityService.canDeletePoll($scope.poll);
      };
      return $scope.icon = function() {
        return PollService.iconFor($scope.poll);
      };
    }
  };
});

angular.module('loomioApp').directive('pollCommonChartPreview', function(PollService, Session) {
  return {
    scope: {
      poll: '='
    },
    templateUrl: 'generated/components/poll/common/chart_preview/poll_common_chart_preview.html',
    controller: function($scope) {
      $scope.chartType = function() {
        return PollService.fieldFromTemplate($scope.poll.pollType, 'chart_type');
      };
      return $scope.myStance = function() {
        return PollService.lastStanceBy(Session.participant(), $scope.poll);
      };
    }
  };
});

angular.module('loomioApp').directive('pollCommonChooseType', function(PollService) {
  return {
    scope: {
      poll: '='
    },
    templateUrl: 'generated/components/poll/common/choose_type/poll_common_choose_type.html',
    controller: function($scope) {
      $scope.choose = function(type) {
        $scope.poll.pollType = type;
        return $scope.$emit('chooseComplete');
      };
      $scope.pollTypes = function() {
        return _.keys(PollService.activePollTemplates());
      };
      return $scope.iconFor = function(pollType) {
        return PollService.fieldFromTemplate(pollType, 'material_icon');
      };
    }
  };
});

angular.module('loomioApp').factory('PollCommonCloseForm', function() {
  return {
    templateUrl: 'generated/components/poll/common/close_form/poll_common_close_form.html',
    controller: function($scope, poll, FormService) {
      $scope.poll = poll;
      return $scope.submit = FormService.submit($scope, poll, {
        submitFn: $scope.poll.close,
        flashSuccess: "poll_common_close_form." + $scope.poll.pollType + "_closed"
      });
    }
  };
});

angular.module('loomioApp').directive('pollCommonClosingAt', function() {
  return {
    scope: {
      poll: '='
    },
    restrict: 'E',
    templateUrl: 'generated/components/poll/common/closing_at/poll_common_closing_at.html',
    replace: true,
    controller: function($scope, $filter) {
      $scope.time = function() {
        var key;
        key = $scope.poll.isActive() ? 'closingAt' : 'closedAt';
        return $filter('timeFromNowInWords')($scope.poll[key]);
      };
      return $scope.translationKey = function() {
        if ($scope.poll.isActive()) {
          return 'common.closing_in';
        } else {
          return 'common.closed_ago';
        }
      };
    }
  };
});

angular.module('loomioApp').directive('pollCommonClosingAtField', function(AppConfig) {
  return {
    scope: {
      poll: '='
    },
    restrict: 'E',
    templateUrl: 'generated/components/poll/common/closing_at_field/poll_common_closing_at_field.html',
    replace: true,
    controller: function($scope) {
      var j, results, updateClosingAt;
      $scope.hours = (function() {
        results = [];
        for (j = 1; j <= 24; j++){ results.push(j); }
        return results;
      }).apply(this);
      $scope.closingHour = $scope.poll.closingAt.format('H');
      $scope.closingDate = $scope.poll.closingAt.toDate();
      updateClosingAt = function() {
        return $scope.poll.closingAt = moment($scope.closingDate).startOf('day').add($scope.closingHour, 'hours');
      };
      $scope.$watch('closingDate', updateClosingAt);
      $scope.$watch('closingHour', updateClosingAt);
      $scope.hours = _.times(24, function(i) {
        return i;
      });
      $scope.times = _.times(24, function(i) {
        if (i < 10) {
          i = "0" + i;
        }
        return moment("2015-01-01 " + i + ":00").format('h a');
      });
      $scope.dateToday = moment().format('YYYY-MM-DD');
      return $scope.timeZone = AppConfig.timeZone;
    }
  };
});

angular.module('loomioApp').directive('pollCommonCollapsed', function() {
  return {
    scope: {
      poll: '='
    },
    templateUrl: 'generated/components/poll/common/collapsed/poll_common_collapsed.html',
    controller: function($scope) {
      return $scope.formattedPollType = function(type) {
        return _.capitalize(type.replace('_', '-'));
      };
    }
  };
});

angular.module('loomioApp').factory('PollCommonDeleteModal', function($location, LmoUrlService, FormService) {
  return {
    templateUrl: 'generated/components/poll/common/delete_modal/poll_common_delete_modal.html',
    controller: function($scope, poll) {
      $scope.poll = poll;
      return $scope.submit = FormService.submit($scope, $scope.poll, {
        submitFn: $scope.poll.destroy,
        flashSuccess: 'poll_common_delete_modal.success',
        successCallback: function() {
          var path;
          path = $scope.poll.discussion() ? LmoUrlService.discussion($scope.poll.discussion()) : $scope.poll.group() ? LmoUrlService.group($scope.poll.group()) : '/dashboard';
          return $location.path(path);
        }
      });
    }
  };
});

angular.module('loomioApp').directive('pollCommonDirective', function($compile, $injector) {
  return {
    scope: {
      poll: '=?',
      stance: '=?',
      outcome: '=?',
      back: '=?',
      name: '@'
    },
    link: function($scope, element) {
      var directiveName, model;
      model = $scope.stance || $scope.outcome || {
        poll: function() {}
      };
      $scope.poll = $scope.poll || model.poll();
      directiveName = $injector.has(_.camelCase("poll_" + $scope.poll.pollType + "_" + $scope.name + "_directive")) ? "poll_" + $scope.poll.pollType + "_" + $scope.name : "poll_common_" + $scope.name;
      return element.append($compile("<" + directiveName + " poll='poll' stance='stance' outcome='outcome' back='back' />")($scope));
    }
  };
});

angular.module('loomioApp').factory('PollCommonEditVoteModal', function($rootScope, PollService, LoadingService) {
  return {
    templateUrl: 'generated/components/poll/common/edit_vote_modal/poll_common_edit_vote_modal.html',
    controller: function($scope, stance) {
      $scope.stance = stance.clone();
      $scope.stance.visitorAttributes = _.pick($scope.stance.participant().serialize().visitor, 'name', 'email', 'participation_token');
      $scope.$on('stanceSaved', function() {
        $scope.$close();
        return $rootScope.$broadcast('refreshStance');
      });
      LoadingService.listenForLoading($scope);
      return $scope.icon = function() {
        return PollService.iconFor($scope.stance.poll());
      };
    }
  };
});

angular.module('loomioApp').directive('pollCommonExampleCard', function($translate) {
  return {
    scope: {
      poll: '='
    },
    templateUrl: 'generated/components/poll/common/example_card/poll_common_example_card.html',
    replace: true,
    controller: function($scope) {
      return $scope.type = function() {
        return $translate.instant("poll_types." + $scope.poll.pollType).toLowerCase();
      };
    }
  };
});

angular.module('loomioApp').directive('pollCommonFormActions', function() {
  return {
    scope: {
      submit: '=',
      poll: '='
    },
    templateUrl: 'generated/components/poll/common/form_actions/poll_common_form_actions.html',
    controller: function($scope) {
      return $scope.back = function() {
        $scope.poll.pollType = null;
        return $scope.$emit('saveBack');
      };
    }
  };
});

angular.module('loomioApp').factory('PollCommonFormModal', function(PollService, LoadingService) {
  return {
    templateUrl: 'generated/components/poll/common/form_modal/poll_common_form_modal.html',
    controller: function($scope, poll) {
      $scope.poll = poll.clone();
      $scope.poll.makeAnnouncement = $scope.poll.isNew();
      $scope.icon = function() {
        return PollService.iconFor($scope.poll);
      };
      LoadingService.listenForLoading($scope);
      return $scope.$on('saveComplete', $scope.$close);
    }
  };
});

angular.module('loomioApp').directive('pollCommonFormOptions', function(PollService) {
  return {
    scope: {
      poll: '='
    },
    templateUrl: 'generated/components/poll/common/form_options/poll_common_form_options.html',
    controller: function($scope, KeyEventService) {
      $scope.existingOptions = _.clone($scope.poll.pollOptionNames);
      $scope.addOption = function() {
        if (!($scope.poll.newOptionName && !_.contains($scope.poll.pollOptionNames, $scope.poll.newOptionName))) {
          return;
        }
        $scope.poll.pollOptionNames.push($scope.poll.newOptionName);
        if (!$scope.poll.isNew()) {
          $scope.poll.makeAnnouncement = true;
        }
        return $scope.poll.newOptionName = '';
      };
      $scope.datesAsOptions = PollService.fieldFromTemplate($scope.poll.pollType, 'dates_as_options');
      $scope.$on('addPollOption', function() {
        return $scope.addOption();
      });
      $scope.removeOption = function(name) {
        if (!$scope.isExisting(name)) {
          return _.pull($scope.poll.pollOptionNames, name);
        }
      };
      $scope.isExisting = function(name) {
        return _.contains($scope.existingOptions, name);
      };
      return KeyEventService.registerKeyEvent($scope, 'pressedEnter', $scope.addOption, function(active) {
        return active.classList.contains('poll-poll-form__add-option-input');
      });
    }
  };
});

angular.module('loomioApp').directive('pollCommonIndexCard', function($location, Records, LoadingService, LmoUrlService) {
  return {
    scope: {
      model: '=',
      limit: '@?',
      viewMoreLink: '=?'
    },
    templateUrl: 'generated/components/poll/common/index_card/poll_common_index_card.html',
    replace: true,
    controller: function($scope) {
      $scope.fetchRecords = function() {
        return Records.polls.fetchFor($scope.model, {
          limit: $scope.limit,
          status: 'closed'
        });
      };
      LoadingService.applyLoadingFunction($scope, 'fetchRecords');
      $scope.fetchRecords();
      $scope.displayViewMore = function() {
        return $scope.viewMoreLink && $scope.model.closedPollsCount > $scope.polls().length;
      };
      $scope.viewMore = function() {
        var opts;
        opts = {};
        opts[$scope.model.constructor.singular + "_key"] = $scope.model.key;
        opts["status"] = "closed";
        return $location.path('polls').search(opts);
      };
      return $scope.polls = function() {
        return _.take($scope.model.closedPolls(), $scope.limit || 50);
      };
    }
  };
});

angular.module('loomioApp').directive('pollCommonNotifyGroup', function() {
  return {
    scope: {
      model: '=',
      notifyAction: '@'
    },
    templateUrl: 'generated/components/poll/common/notify_group/poll_common_notify_group.html'
  };
});

angular.module('loomioApp').directive('pollCommonNotifyOnParticipate', function() {
  return {
    scope: {
      poll: '='
    },
    templateUrl: 'generated/components/poll/common/notify_on_participate/poll_common_notify_on_participate.html'
  };
});

angular.module('loomioApp').directive('pollCommonOutcomeForm', function() {
  return {
    scope: {
      outcome: '='
    },
    templateUrl: 'generated/components/poll/common/outcome_form/poll_common_outcome_form.html',
    controller: function($scope, $translate, PollService, LoadingService, TranslationService, MentionService, KeyEventService) {
      $scope.outcome.makeAnnouncement = $scope.outcome.isNew();
      $scope.submit = PollService.submitOutcome($scope, $scope.outcome);
      $scope.datesAsOptions = function() {
        return PollService.fieldFromTemplate($scope.outcome.poll().pollType, 'dates_as_options');
      };
      MentionService.applyMentions($scope, $scope.outcome);
      KeyEventService.submitOnEnter($scope);
      return LoadingService.listenForLoading($scope);
    }
  };
});

angular.module('loomioApp').factory('PollCommonOutcomeModal', function() {
  return {
    templateUrl: 'generated/components/poll/common/outcome_modal/poll_common_outcome_modal.html',
    controller: function($scope, outcome) {
      $scope.outcome = outcome.clone();
      return $scope.$on('outcomeSaved', $scope.$close);
    }
  };
});

angular.module('loomioApp').directive('pollCommonOutcomePanel', function() {
  return {
    scope: {
      poll: '='
    },
    templateUrl: 'generated/components/poll/common/outcome_panel/poll_common_outcome_panel.html',
    controller: function($scope, ModalService, AbilityService, PollCommonOutcomeModal) {
      $scope.showUpdateButton = function() {
        return AbilityService.canSetPollOutcome($scope.poll);
      };
      return $scope.openOutcomeForm = function() {
        return ModalService.open(PollCommonOutcomeModal, {
          outcome: function() {
            return $scope.poll.outcome();
          }
        });
      };
    }
  };
});

angular.module('loomioApp').directive('pollCommonParticipantForm', function() {
  return {
    scope: {
      stance: '='
    },
    templateUrl: 'generated/components/poll/common/participant_form/poll_common_participant_form.html',
    controller: function($scope, $location, AbilityService) {
      return $scope.showParticipantForm = function() {
        return !AbilityService.isLoggedIn() && $scope.stance.isNew();
      };
    }
  };
});

angular.module('loomioApp').directive('pollCommonPercentVoted', function() {
  return {
    scope: {
      poll: '='
    },
    templateUrl: 'generated/components/poll/common/percent_voted/poll_common_percent_voted.html'
  };
});

angular.module('loomioApp').directive('pollCommonPreview', function(PollService, Session) {
  return {
    scope: {
      poll: '=',
      displayGroupName: '=?'
    },
    templateUrl: 'generated/components/poll/common/preview/poll_common_preview.html',
    controller: function($scope) {
      return $scope.showGroupName = function() {
        return $scope.displayGroupName && $scope.poll.group();
      };
    }
  };
});

angular.module('loomioApp').directive('pollCommonSchoolLink', function() {
  return {
    scope: {
      translation: '@',
      href: '@'
    },
    templateUrl: 'generated/components/poll/common/school_link/poll_common_school_link.html'
  };
});

angular.module('loomioApp').directive('pollCommonSetOutcomePanel', function(Records, ModalService, PollCommonOutcomeModal, AbilityService) {
  return {
    scope: {
      poll: '='
    },
    templateUrl: 'generated/components/poll/common/set_outcome_panel/poll_common_set_outcome_panel.html',
    controller: function($scope) {
      $scope.showPanel = function() {
        return !$scope.poll.outcome() && AbilityService.canSetPollOutcome($scope.poll);
      };
      return $scope.openOutcomeForm = function() {
        return ModalService.open(PollCommonOutcomeModal, {
          outcome: function() {
            return $scope.poll.outcome() || Records.outcomes.build({
              pollId: $scope.poll.id
            });
          }
        });
      };
    }
  };
});

angular.module('loomioApp').factory('PollCommonShareModal', function(PollService) {
  return {
    templateUrl: 'generated/components/poll/common/share_modal/poll_common_share_modal.html',
    controller: function($scope, poll) {
      $scope.poll = poll.clone();
      $scope.icon = function() {
        return PollService.iconFor($scope.poll);
      };
      return $scope.$on('$close', $scope.$close);
    }
  };
});

angular.module('loomioApp').directive('showResultsButton', function() {
  return {
    templateUrl: 'generated/components/poll/common/show_results_button/show_results_button.html',
    controller: function($scope) {
      $scope.clicked = false;
      return $scope.press = function() {
        $scope.$emit('showResults');
        return $scope.clicked = true;
      };
    }
  };
});

angular.module('loomioApp').directive('pollCommonStanceChoice', function(PollService) {
  return {
    scope: {
      stanceChoice: '='
    },
    templateUrl: 'generated/components/poll/common/stance_choice/poll_common_stance_choice.html',
    controller: function($scope) {
      $scope.translateOptionName = function() {
        if (!$scope.stanceChoice.poll()) {
          return;
        }
        return PollService.fieldFromTemplate($scope.stanceChoice.poll().pollType, 'translate_option_name');
      };
      $scope.hasVariableScore = function() {
        if (!$scope.stanceChoice.poll()) {
          return;
        }
        return PollService.fieldFromTemplate($scope.stanceChoice.poll().pollType, 'has_variable_score');
      };
      return $scope.datesAsOptions = function() {
        if (!$scope.stanceChoice.poll()) {
          return;
        }
        return PollService.fieldFromTemplate($scope.stanceChoice.poll().pollType, 'dates_as_options');
      };
    }
  };
});

angular.module('loomioApp').directive('pollCommonStanceIcon', function(PollService) {
  return {
    scope: {
      stanceChoice: '='
    },
    templateUrl: 'generated/components/poll/common/stance_icon/poll_common_stance_icon.html',
    controller: function($scope) {
      return $scope.useOptionIcon = function() {
        return PollService.fieldFromTemplate($scope.stanceChoice.poll().pollType, 'has_option_icons');
      };
    }
  };
});

angular.module('loomioApp').directive('pollCommonStartForm', function() {
  return {
    scope: {
      discussion: '=?',
      group: '=?'
    },
    restrict: 'E',
    templateUrl: 'generated/components/poll/common/start_form/poll_common_start_form.html',
    replace: true,
    controller: function($scope, AppConfig, Records, ModalService, PollCommonStartModal, PollService) {
      $scope.discussion = $scope.discussion || {};
      $scope.group = $scope.group || {};
      $scope.pollTypes = function() {
        return _.keys(PollService.activePollTemplates());
      };
      $scope.startPoll = function(pollType) {
        return ModalService.open(PollCommonStartModal, {
          poll: function() {
            return Records.polls.build({
              pollType: pollType,
              discussionId: $scope.discussion.id,
              groupId: $scope.discussion.groupId || $scope.group.id,
              pollOptionNames: _.pluck($scope.fieldFromTemplate(pollType, 'poll_options_attributes'), 'name')
            });
          }
        });
      };
      return $scope.fieldFromTemplate = function(pollType, field) {
        return PollService.fieldFromTemplate(pollType, field);
      };
    }
  };
});

angular.module('loomioApp').factory('PollCommonStartModal', function($location, PollService, LmoUrlService, LoadingService, Records) {
  return {
    templateUrl: 'generated/components/poll/common/start_modal/poll_common_start_modal.html',
    controller: function($scope, poll) {
      $scope.poll = poll.clone();
      $scope.icon = function() {
        return PollService.iconFor($scope.poll);
      };
      LoadingService.listenForLoading($scope);
      return $scope.$on('$close', $scope.$close);
    }
  };
});

angular.module('loomioApp').directive('pollCommonStartPoll', function($window, Records, PollService, LmoUrlService) {
  return {
    scope: {
      poll: '='
    },
    templateUrl: 'generated/components/poll/common/start_poll/poll_common_start_poll.html',
    controller: function($scope) {
      $scope.currentStep = $scope.poll.pollType ? 'save' : 'choose';
      $scope.$on('chooseComplete', function() {
        $scope.currentStep = 'save';
        return $scope.isDisabled = false;
      });
      $scope.$on('saveBack', function() {
        $scope.currentStep = 'choose';
        return $scope.isDisabled = false;
      });
      $scope.poll.makeAnnouncement = $scope.poll.isNew();
      return $scope.$on('saveComplete', function(event, poll) {
        if (poll.group()) {
          $scope.$emit('$close');
        } else {
          $scope.poll = poll;
          $scope.currentStep = 'share';
        }
        return $scope.isDisabled = false;
      });
    }
  };
});

angular.module('loomioApp').directive('pollCommonSummaryPanel', function(TranslationService) {
  return {
    scope: {
      poll: '='
    },
    templateUrl: 'generated/components/poll/common/summary_panel/poll_common_summary_panel.html',
    controller: function($scope) {
      return TranslationService.listenForTranslations($scope);
    }
  };
});

angular.module('loomioApp').directive('pollCommonToolTip', function(Session) {
  return {
    scope: {
      poll: '='
    },
    templateUrl: 'generated/components/poll/common/tool_tip/poll_common_tool_tip.html',
    controller: function($scope, Records) {
      var experienceKey;
      experienceKey = $scope.poll.pollType + "_tool_tip";
      $scope.collapsed = Session.user().hasExperienced(experienceKey);
      if (!Session.user().hasExperienced(experienceKey)) {
        Records.users.saveExperience(experienceKey);
      }
      $scope.hide = function() {
        return $scope.collapsed = true;
      };
      return $scope.show = function() {
        return $scope.collapsed = false;
      };
    }
  };
});

angular.module('loomioApp').directive('pollCommonVoterAddOptions', function(PollService) {
  return {
    scope: {
      poll: '='
    },
    templateUrl: 'generated/components/poll/common/voter_add_options/poll_common_voter_add_options.html',
    controller: function($scope) {
      return $scope.validPollType = function() {
        return PollService.fieldFromTemplate($scope.poll.pollType, 'can_add_options');
      };
    }
  };
});

angular.module('loomioApp').directive('pollCommonVotesPanel', function(PollService, RecordLoader) {
  return {
    scope: {
      poll: '='
    },
    templateUrl: 'generated/components/poll/common/votes_panel/poll_common_votes_panel.html',
    controller: function($scope) {
      var sortFn;
      $scope.fix = {};
      $scope.sortOptions = PollService.fieldFromTemplate($scope.poll.pollType, 'sort_options');
      $scope.fix.votesOrder = $scope.sortOptions[0];
      $scope.loader = new RecordLoader({
        collection: 'stances',
        params: {
          poll_id: $scope.poll.key,
          order: $scope.fix.votesOrder
        }
      });
      $scope.hasSomeVotes = function() {
        return $scope.poll.stancesCount > 0;
      };
      $scope.moreToLoad = function() {
        return $scope.loader.numLoaded < $scope.poll.stancesCount;
      };
      sortFn = {
        newest_first: function(stance) {
          return -stance.createdAt;
        },
        oldest_first: function(stance) {
          return stance.createdAt;
        },
        priority_first: function(stance) {
          return stance.pollOption().priority;
        },
        priority_last: function(stance) {
          return -(stance.pollOption().priority);
        }
      };
      $scope.stances = function() {
        return $scope.poll.uniqueStances(sortFn[$scope.fix.votesOrder], $scope.loader.numLoaded);
      };
      $scope.changeOrder = function() {
        $scope.loader.reset();
        $scope.loader.params.order = $scope.fix.votesOrder;
        return $scope.loader.fetchRecords();
      };
      $scope.loader.fetchRecords();
      return $scope.$on('refreshStance', function() {
        return $scope.loader.fetchRecords();
      });
    }
  };
});

angular.module('loomioApp').directive('pollCommonVotesPanelStance', function(TranslationService) {
  return {
    scope: {
      stance: '='
    },
    templateUrl: 'generated/components/poll/common/votes_panel_stance/poll_common_votes_panel_stance.html',
    controller: function($scope) {
      return TranslationService.listenForTranslations($scope);
    }
  };
});

angular.module('loomioApp').directive('pollCountChartPanel', function(AppConfig, Records) {
  return {
    scope: {
      poll: '='
    },
    templateUrl: 'generated/components/poll/count/chart_panel/poll_count_chart_panel.html',
    controller: function($scope) {
      $scope.percentComplete = function(index) {
        return (100 * $scope.poll.stanceCounts[index] / $scope.poll.goal()) + "%";
      };
      return $scope.colors = AppConfig.pollColors.count;
    }
  };
});

angular.module('loomioApp').directive('pollCountForm', function() {
  return {
    scope: {
      poll: '=',
      back: '=?'
    },
    templateUrl: 'generated/components/poll/count/form/poll_count_form.html',
    controller: function($scope, FormService, AttachmentService, PollService, KeyEventService) {
      $scope.submit = PollService.submitPoll($scope, $scope.poll, {
        prepareFn: function() {
          $scope.$emit('processing');
          return $scope.poll.pollOptionNames = _.pluck(PollService.fieldFromTemplate('count', 'poll_options_attributes'), 'name');
        }
      });
      return KeyEventService.submitOnEnter($scope);
    }
  };
});

angular.module('loomioApp').directive('pollCountVoteForm', function(AppConfig, Records, PollService, MentionService, KeyEventService) {
  return {
    scope: {
      stance: '='
    },
    templateUrl: 'generated/components/poll/count/vote_form/poll_count_vote_form.html',
    controller: function($scope) {
      $scope.submit = PollService.submitStance($scope, $scope.stance, {
        prepareFn: function(option) {
          var pollOptionId;
          $scope.$emit('processing');
          pollOptionId = option.name ? option.id : $scope.yes.id;
          return $scope.stance.stanceChoicesAttributes = [
            {
              poll_option_id: pollOptionId
            }
          ];
        }
      });
      $scope.yes = PollService.optionByName($scope.stance.poll(), 'yes');
      $scope.no = PollService.optionByName($scope.stance.poll(), 'no');
      MentionService.applyMentions($scope, $scope.stance);
      return KeyEventService.submitOnEnter($scope);
    }
  };
});

angular.module('loomioApp').directive('pollDotVoteChartPanel', function() {
  return {
    scope: {
      poll: '='
    },
    templateUrl: 'generated/components/poll/dot_vote/chart_panel/poll_dot_vote_chart_panel.html'
  };
});

angular.module('loomioApp').directive('pollDotVoteForm', function() {
  return {
    scope: {
      poll: '=',
      back: '=?'
    },
    templateUrl: 'generated/components/poll/dot_vote/form/poll_dot_vote_form.html',
    controller: function($scope, PollService, AttachmentService, KeyEventService) {
      $scope.poll.customFields.dots_per_person = $scope.poll.customFields.dots_per_person || 8;
      $scope.submit = PollService.submitPoll($scope, $scope.poll, {
        prepareFn: function() {
          $scope.$emit('processing');
          return $scope.$broadcast('addPollOption');
        }
      });
      return KeyEventService.submitOnEnter($scope);
    }
  };
});

angular.module('loomioApp').directive('pollDotVoteVoteForm', function() {
  return {
    scope: {
      stance: '='
    },
    templateUrl: 'generated/components/poll/dot_vote/vote_form/poll_dot_vote_vote_form.html',
    controller: function($scope, Records, PollService, MentionService, KeyEventService) {
      var backgroundImageFor, percentageFor;
      $scope.vars = {};
      percentageFor = function(choice) {
        var max;
        max = $scope.stance.poll().customFields.dots_per_person;
        if (!(max > 0)) {
          return;
        }
        return (100 * choice.score / max) + "%";
      };
      backgroundImageFor = function(option) {
        return "url(/img/poll_backgrounds/" + (option.color.replace('#', '')) + ".png)";
      };
      $scope.styleData = function(choice) {
        var option;
        option = $scope.optionFor(choice);
        return {
          'border-color': option.color,
          'background-image': backgroundImageFor(option),
          'background-size': percentageFor(choice)
        };
      };
      $scope.stanceChoiceFor = function(option) {
        return _.first(_.filter($scope.stance.stanceChoices(), function(choice) {
          return choice.pollOptionId === option.id;
        }).concat({
          score: 0
        }));
      };
      $scope.adjust = function(choice, amount) {
        return choice.score += amount;
      };
      $scope.optionFor = function(choice) {
        return Records.pollOptions.find(choice.poll_option_id);
      };
      $scope.dotsRemaining = function() {
        return $scope.stance.poll().customFields.dots_per_person - _.sum(_.pluck($scope.stanceChoices, 'score'));
      };
      $scope.tooManyDots = function() {
        return $scope.dotsRemaining() < 0;
      };
      $scope.setStanceChoices = function() {
        return $scope.stanceChoices = _.map($scope.stance.poll().pollOptions(), function(option) {
          return {
            poll_option_id: option.id,
            score: $scope.stanceChoiceFor(option).score
          };
        });
      };
      $scope.setStanceChoices();
      $scope.$on('pollOptionsAdded', $scope.setStanceChoices);
      $scope.submit = PollService.submitStance($scope, $scope.stance, {
        prepareFn: function() {
          $scope.$emit('processing');
          if (!(_.sum(_.pluck($scope.stanceChoices, 'score')) > 0)) {
            return;
          }
          return $scope.stance.stanceChoicesAttributes = $scope.stanceChoices;
        }
      });
      MentionService.applyMentions($scope, $scope.stance);
      return KeyEventService.submitOnEnter($scope);
    }
  };
});

angular.module('loomioApp').directive('pollDotVoteVotesPanelStance', function(PollService, RecordLoader) {
  return {
    scope: {
      stance: '='
    },
    templateUrl: 'generated/components/poll/dot_vote/votes_panel_stance/poll_dot_vote_votes_panel_stance.html',
    controller: function($scope) {
      var backgroundImageFor, percentageFor;
      $scope.barTextFor = function(choice) {
        return (choice.score + " - " + (choice.pollOption().name)).replace(/\s/g, '\u00a0');
      };
      percentageFor = function(choice) {
        var max;
        max = $scope.stance.poll().customFields.dots_per_person;
        if (!(max > 0)) {
          return;
        }
        return (100 * choice.score / max) + "%";
      };
      backgroundImageFor = function(choice) {
        return "url(/img/poll_backgrounds/" + (choice.pollOption().color.replace('#', '')) + ".png)";
      };
      $scope.styleData = function(choice) {
        return {
          'background-image': backgroundImageFor(choice),
          'background-size': percentageFor(choice)
        };
      };
      return $scope.stanceChoices = function() {
        return _.sortBy($scope.stance.stanceChoices(), 'score').reverse();
      };
    }
  };
});

angular.module('loomioApp').directive('pollMeetingChartPanel', function() {
  return {
    scope: {
      poll: '='
    },
    templateUrl: 'generated/components/poll/meeting/chart_panel/poll_meeting_chart_panel.html',
    controller: function($scope) {
      return $scope.$on('timeZoneSelected', function(e, zone) {
        return $scope.zone = zone;
      });
    }
  };
});

angular.module('loomioApp').directive('pollMeetingForm', function() {
  return {
    scope: {
      poll: '=',
      back: '=?'
    },
    templateUrl: 'generated/components/poll/meeting/form/poll_meeting_form.html',
    controller: function($scope, AppConfig, PollService, AttachmentService, KeyEventService, TimeService) {
      $scope.removeOption = function(name) {
        return _.pull($scope.poll.pollOptionNames, name);
      };
      $scope.durations = AppConfig.durations;
      $scope.poll.customFields.meeting_duration = $scope.poll.customFields.meeting_duration || 60;
      if ($scope.poll.isNew()) {
        $scope.poll.closingAt = moment().add(2, 'day');
        $scope.poll.notifyOnParticipate = true;
        if ($scope.poll.group()) {
          $scope.poll.makeAnnouncement = true;
        }
      }
      $scope.submit = PollService.submitPoll($scope, $scope.poll, {
        prepareFn: function() {
          $scope.$emit('processing');
          return $scope.$broadcast('addPollOption');
        }
      });
      $scope.$on('timeZoneSelected', function(e, zone) {
        return $scope.poll.customFields.time_zone = zone;
      });
      return KeyEventService.submitOnEnter($scope);
    }
  };
});

angular.module('loomioApp').directive('pollMeetingTime', function(TimeService) {
  return {
    scope: {
      name: '=',
      zone: '='
    },
    template: "<span>{{displayDate(date, zone)}}</span>",
    controller: function($scope) {
      $scope.date = moment($scope.name);
      return $scope.displayDate = TimeService.displayDate;
    }
  };
});

angular.module('loomioApp').directive('pollMeetingTimeField', function(TimeService) {
  return {
    scope: {
      poll: '='
    },
    templateUrl: 'generated/components/poll/meeting/time_field/poll_meeting_time_field.html',
    controller: function($scope) {
      var determineOptionName;
      $scope.dateToday = moment().format('YYYY-MM-DD');
      $scope.option = {};
      $scope.times = TimeService.timesOfDay();
      $scope.addOption = function() {
        var optionName;
        optionName = determineOptionName();
        if (!($scope.option.date && !_.contains($scope.poll.pollOptionNames, optionName))) {
          return;
        }
        return $scope.poll.pollOptionNames.push(optionName);
      };
      $scope.$on('addPollOption', $scope.addOption);
      $scope.hasTime = function() {
        return ($scope.option.time || "").length > 0;
      };
      return determineOptionName = function() {
        var optionName;
        optionName = moment($scope.option.date).format('YYYY-MM-DD');
        if ($scope.hasTime()) {
          optionName = moment(optionName + " " + $scope.option.time, 'YYYY-MM-DD h:mma').toISOString();
        }
        return optionName;
      };
    }
  };
});

angular.module('loomioApp').directive('pollMeetingVoteForm', function() {
  return {
    scope: {
      stance: '='
    },
    templateUrl: 'generated/components/poll/meeting/vote_form/poll_meeting_vote_form.html',
    controller: function($scope, PollService, MentionService, KeyEventService) {
      var initForm;
      $scope.vars = {};
      initForm = (function() {
        return $scope.pollOptionIdsChecked = _.fromPairs(_.map($scope.stance.stanceChoices(), function(choice) {
          return [choice.pollOptionId, true];
        }));
      })();
      $scope.submit = PollService.submitStance($scope, $scope.stance, {
        prepareFn: function() {
          var attrs;
          $scope.$emit('processing');
          attrs = _.map(_.compact(_.map($scope.pollOptionIdsChecked, function(v, k) {
            if (v) {
              return parseInt(k);
            }
          })), function(id) {
            return {
              poll_option_id: id
            };
          });
          if (_.any(attrs)) {
            return $scope.stance.stanceChoicesAttributes = attrs;
          }
        }
      });
      $scope.$on('timeZoneSelected', function(e, zone) {
        return $scope.zone = zone;
      });
      MentionService.applyMentions($scope, $scope.stance);
      return KeyEventService.submitOnEnter($scope);
    }
  };
});

angular.module('loomioApp').directive('pollMeetingVotesPanelStance', function(TranslationService) {
  return {
    scope: {
      stance: '='
    },
    templateUrl: 'generated/components/poll/meeting/votes_panel_stance/poll_meeting_votes_panel_stance.html',
    controller: function($scope) {
      return TranslationService.listenForTranslations($scope);
    }
  };
});

angular.module('loomioApp').directive('pollPollChartPanel', function() {
  return {
    scope: {
      poll: '='
    },
    templateUrl: 'generated/components/poll/poll/chart_panel/poll_poll_chart_panel.html'
  };
});

angular.module('loomioApp').directive('pollPollForm', function() {
  return {
    scope: {
      poll: '=',
      back: '=?'
    },
    templateUrl: 'generated/components/poll/poll/form/poll_poll_form.html',
    controller: function($scope, PollService, KeyEventService) {
      $scope.submit = PollService.submitPoll($scope, $scope.poll, {
        prepareFn: function() {
          $scope.$emit('processing');
          return $scope.$broadcast('addPollOption');
        }
      });
      return KeyEventService.submitOnEnter($scope);
    }
  };
});

angular.module('loomioApp').directive('pollPollVoteForm', function() {
  return {
    scope: {
      stance: '='
    },
    templateUrl: 'generated/components/poll/poll/vote_form/poll_poll_vote_form.html',
    controller: function($scope, PollService, MentionService, KeyEventService) {
      var initForm, multipleChoice;
      $scope.vars = {};
      multipleChoice = $scope.stance.poll().multipleChoice;
      initForm = (function() {
        if (multipleChoice) {
          return $scope.pollOptionIdsChecked = _.fromPairs(_.map($scope.stance.stanceChoices(), function(choice) {
            return [choice.pollOptionId, true];
          }));
        } else {
          return $scope.vars.pollOptionId = $scope.stance.pollOptionId();
        }
      })();
      $scope.submit = PollService.submitStance($scope, $scope.stance, {
        prepareFn: function() {
          var selectedOptionIds;
          $scope.$emit('processing');
          selectedOptionIds = multipleChoice ? _.compact(_.map($scope.pollOptionIdsChecked, function(v, k) {
            if (v) {
              return parseInt(k);
            }
          })) : [$scope.vars.pollOptionId];
          if (_.any(selectedOptionIds)) {
            return $scope.stance.stanceChoicesAttributes = _.map(selectedOptionIds, function(id) {
              return {
                poll_option_id: id
              };
            });
          }
        }
      });
      MentionService.applyMentions($scope, $scope.stance);
      return KeyEventService.submitOnEnter($scope);
    }
  };
});

angular.module('loomioApp').directive('pollProposalForm', function() {
  return {
    scope: {
      poll: '=',
      back: '=?'
    },
    templateUrl: 'generated/components/poll/proposal/form/poll_proposal_form.html',
    controller: function($scope, PollService, AttachmentService, KeyEventService, MentionService) {
      $scope.submit = PollService.submitPoll($scope, $scope.poll, {
        prepareFn: function() {
          $scope.$emit('processing');
          return $scope.poll.pollOptionNames = _.pluck(PollService.fieldFromTemplate('proposal', 'poll_options_attributes'), 'name');
        }
      });
      MentionService.applyMentions($scope, $scope.poll);
      return KeyEventService.submitOnEnter($scope);
    }
  };
});

angular.module('loomioApp').directive('pollProposalVoteForm', function() {
  return {
    scope: {
      stance: '='
    },
    templateUrl: 'generated/components/poll/proposal/vote_form/poll_proposal_vote_form.html',
    controller: function($scope, PollService, MentionService, KeyEventService) {
      $scope.stance.selectedOption = $scope.stance.pollOption();
      $scope.submit = PollService.submitStance($scope, $scope.stance, {
        prepareFn: function() {
          $scope.$emit('processing');
          if (!$scope.stance.selectedOption) {
            return;
          }
          return $scope.stance.stanceChoicesAttributes = [
            {
              poll_option_id: $scope.stance.selectedOption.id
            }
          ];
        }
      });
      $scope.cancelOption = function() {
        return $scope.stance.selected = null;
      };
      $scope.reasonPlaceholder = function() {
        var pollOptionName;
        pollOptionName = ($scope.stance.selectedOption || {
          name: 'default'
        }).name;
        return "poll_proposal_vote_form." + pollOptionName + "_reason_placeholder";
      };
      MentionService.applyMentions($scope, $scope.stance);
      return KeyEventService.submitOnEnter($scope);
    }
  };
});

angular.module('loomioApp').directive('pollProposalChart', function(AppConfig) {
  return {
    template: '<div class="poll-proposal-chart"></div>',
    replace: true,
    scope: {
      stanceCounts: '=',
      diameter: '@'
    },
    restrict: 'E',
    controller: function($scope, $element) {
      var arcPath, draw, half, radius, shapes, uniquePositionsCount;
      draw = SVG($element[0]).size('100%', '100%');
      half = $scope.diameter / 2.0;
      radius = half;
      shapes = [];
      arcPath = function(startAngle, endAngle) {
        var rad, x1, x2, y1, y2;
        rad = Math.PI / 180;
        x1 = half + radius * Math.cos(-startAngle * rad);
        x2 = half + radius * Math.cos(-endAngle * rad);
        y1 = half + radius * Math.sin(-startAngle * rad);
        y2 = half + radius * Math.sin(-endAngle * rad);
        return ["M", half, half, "L", x1, y1, "A", radius, radius, 0, +(endAngle - startAngle > 180), 0, x2, y2, "z"].join(' ');
      };
      uniquePositionsCount = function() {
        return _.compact($scope.stanceCounts).length;
      };
      return $scope.$watchCollection('stanceCounts', function() {
        var start;
        _.each(shapes, function(shape) {
          return shape.remove();
        });
        start = 90;
        switch (uniquePositionsCount()) {
          case 0:
            return shapes.push(draw.circle($scope.diameter).attr({
              'stroke-width': 0,
              fill: '#aaa'
            }));
          case 1:
            return shapes.push(draw.circle($scope.diameter).attr({
              'stroke-width': 0,
              fill: AppConfig.pollColors.proposal[_.findIndex($scope.stanceCounts, function(count) {
                return count > 0;
              })]
            }));
          default:
            return _.each($scope.stanceCounts, function(count, index) {
              var angle;
              if (!(count > 0)) {
                return;
              }
              angle = 360 / _.sum($scope.stanceCounts) * count;
              shapes.push(draw.path(arcPath(start, start + angle)).attr({
                'stroke-width': 0,
                fill: AppConfig.pollColors.proposal[index]
              }));
              return start += angle;
            });
        }
      });
    }
  };
});

angular.module('loomioApp').directive('pollProposalChartPanel', function(Records) {
  return {
    scope: {
      poll: '='
    },
    templateUrl: 'generated/components/poll/proposal/chart_panel/poll_proposal_chart_panel.html',
    controller: function($scope, $translate) {
      $scope.pollOptionNames = function() {
        return ['agree', 'abstain', 'disagree', 'block'];
      };
      $scope.countFor = function(name) {
        return $scope.poll.stanceData[name] || 0;
      };
      return $scope.translationFor = function(name) {
        return $translate.instant("poll_proposal_options." + name);
      };
    }
  };
});

angular.module('loomioApp').directive('pollProposalChartPreview', function() {
  return {
    scope: {
      myStance: '=',
      stanceCounts: '='
    },
    templateUrl: 'generated/components/poll/proposal/chart_preview/poll_proposal_chart_preview.html'
  };
});

angular.module('loomioApp').directive('proposalActionsDropdown', function() {
  return {
    scope: {
      proposal: '='
    },
    restrict: 'E',
    templateUrl: 'generated/components/thread_page/current_proposal_card/proposal_actions_dropdown/proposal_actions_dropdown.html',
    replace: true,
    controller: function($scope, ModalService, ProposalForm, AbilityService, CloseProposalForm, ExtendProposalForm) {
      $scope.canCloseOrExtendProposal = function() {
        return AbilityService.canCloseOrExtendProposal($scope.proposal);
      };
      $scope.canEditProposal = function() {
        return AbilityService.canEditProposal($scope.proposal);
      };
      $scope.editProposal = function() {
        return ModalService.open(ProposalForm, {
          proposal: function() {
            return $scope.proposal.clone();
          }
        });
      };
      $scope.closeProposal = function() {
        return ModalService.open(CloseProposalForm, {
          proposal: function() {
            return $scope.proposal;
          }
        });
      };
      $scope.extendProposal = function() {
        return ModalService.open(ExtendProposalForm, {
          proposal: function() {
            return $scope.proposal;
          }
        });
      };
    }
  };
});

angular.module('loomioApp').directive('pollCommonAddOptionButton', function(ModalService, PollCommonAddOptionModal) {
  return {
    scope: {
      poll: '='
    },
    templateUrl: 'generated/components/poll/common/add_option/button/poll_common_add_option_button.html',
    replace: true,
    controller: function($scope) {
      return $scope.open = function() {
        return ModalService.open(PollCommonAddOptionModal, {
          poll: function() {
            return $scope.poll;
          }
        });
      };
    }
  };
});

angular.module('loomioApp').directive('pollCommonAddOptionForm', function(PollService) {
  return {
    scope: {
      poll: '='
    },
    templateUrl: 'generated/components/poll/common/add_option/form/poll_common_add_option_form.html',
    replace: true,
    controller: function($scope, $rootScope) {
      return $scope.submit = PollService.submitPoll($scope, $scope.poll, {
        submitFn: $scope.poll.addOptions,
        prepareFn: function() {
          $scope.$broadcast('addPollOption');
          return $scope.$emit('processing');
        },
        successCallback: function() {
          $scope.$emit('$close');
          return $rootScope.$broadcast('pollOptionsAdded', $scope.poll);
        },
        flashSuccess: "poll_common_add_option.form.options_added"
      });
    }
  };
});

angular.module('loomioApp').factory('PollCommonAddOptionModal', function(LoadingService) {
  return {
    templateUrl: 'generated/components/poll/common/add_option/modal/poll_common_add_option_modal.html',
    controller: function($scope, poll) {
      $scope.poll = poll.clone();
      $scope.$on('$close', $scope.$close);
      return LoadingService.listenForLoading($scope);
    }
  };
});

angular.module('loomioApp').directive('pollCommonPublishFacebookPreview', function() {
  return {
    scope: {
      community: '=',
      poll: '=',
      message: '='
    },
    templateUrl: 'generated/components/poll/common/publish/facebook_preview/poll_common_publish_facebook_preview.html',
    controller: function($scope, $location) {
      return $scope.host = function() {
        return $location.host();
      };
    }
  };
});

angular.module('loomioApp').directive('pollCommonPublishForm', function(PollService, FlashService, LoadingService) {
  return {
    scope: {
      poll: '=',
      community: '=',
      back: '=?'
    },
    templateUrl: 'generated/components/poll/common/publish/form/poll_common_publish_form.html',
    controller: function($scope) {
      $scope.submit = function() {
        return $scope.poll.publish($scope.community, $scope.message).then(function() {
          FlashService.success('poll_common_publish_form.published');
          if ($scope.back != null) {
            return $scope.back();
          }
        });
      };
      return LoadingService.applyLoadingFunction($scope, 'submit');
    }
  };
});

angular.module('loomioApp').factory('PollCommonPublishModal', function(PollService) {
  return {
    templateUrl: 'generated/components/poll/common/publish/modal/poll_common_publish_modal.html',
    controller: function($scope, poll, community, back) {
      $scope.poll = poll;
      $scope.community = community;
      $scope.back = back;
      $scope.icon = function() {
        return PollService.iconFor($scope.poll);
      };
      return $scope.$on('createComplete', $scope.$close);
    }
  };
});

angular.module('loomioApp').directive('pollCommonPublishSlackPreview', function() {
  return {
    scope: {
      community: '=',
      poll: '=',
      message: '='
    },
    templateUrl: 'generated/components/poll/common/publish/slack_preview/poll_common_publish_slack_preview.html',
    controller: function($scope, $translate, Session) {
      $scope.userName = Session.user().name;
      return $scope.timestamp = function() {
        return moment().format('h:ma');
      };
    }
  };
});

angular.module('loomioApp').directive('pollCommonShareEmailForm', function($translate, Records, FlashService, KeyEventService) {
  return {
    scope: {
      poll: '='
    },
    restrict: 'E',
    templateUrl: 'generated/components/poll/common/share/email_form/poll_common_share_email_form.html',
    controller: function($scope) {
      $scope.newEmail = '';
      $scope.addIfValid = function() {
        $scope.emailValidationError = null;
        $scope.checkEmailNotEmpty();
        $scope.checkEmailValid();
        $scope.checkEmailAvailable();
        if (!$scope.emailValidationError) {
          return $scope.add();
        }
      };
      $scope.add = function() {
        if (!($scope.newEmail.length > 0)) {
          return;
        }
        $scope.poll.customFields.pending_emails.push($scope.newEmail);
        $scope.newEmail = '';
        return $scope.emailValidationError = null;
      };
      $scope.submit = function() {
        $scope.emailValidationError = null;
        $scope.checkEmailValid();
        $scope.checkEmailAvailable();
        if (!$scope.emailValidationError) {
          $scope.add();
          return $scope.poll.createVisitors().then(function() {
            FlashService.success('poll_common_share_form.guests_invited', {
              count: $scope.poll.customFields.pending_emails.length
            });
            return $scope.$emit('$close');
          });
        }
      };
      $scope.checkEmailNotEmpty = function() {
        if ($scope.newEmail.length <= 0) {
          return $scope.emailValidationError = $translate.instant('poll_common_share_form.email_empty');
        }
      };
      $scope.checkEmailValid = function() {
        if ($scope.newEmail.length > 0 && !$scope.newEmail.match(/[^\s,;<>]+?@[^\s,;<>]+\.[^\s,;<>]+/g)) {
          return $scope.emailValidationError = $translate.instant('poll_common_share_form.email_invalid');
        }
      };
      $scope.checkEmailAvailable = function() {
        if (_.contains($scope.poll.customFields.pending_emails, $scope.newEmail)) {
          return $scope.emailValidationError = $translate.instant('poll_common_share_form.email_exists', {
            email: $scope.newEmail
          });
        }
      };
      $scope.remove = function(email) {
        return _.pull($scope.poll.customFields.pending_emails, email);
      };
      return KeyEventService.registerKeyEvent($scope, 'pressedEnter', $scope.add, function(active) {
        return active.classList.contains('poll-common-share-form__add-option-input');
      });
    }
  };
});

angular.module('loomioApp').directive('pollCommonShareForm', function(Session, Records, AbilityService) {
  return {
    scope: {
      poll: '='
    },
    restrict: 'E',
    templateUrl: 'generated/components/poll/common/share/form/poll_common_share_form.html',
    controller: function($scope) {
      $scope.hasGroups = function() {
        return _.any(_.filter(Session.user().groups(), function(group) {
          return AbilityService.canStartPoll(group);
        }));
      };
      return $scope.hasPendingEmails = function() {
        return _.has($scope.poll, 'customFields.pending_emails.length') && $scope.poll.customFields.pending_emails.length > 0;
      };
    }
  };
});

angular.module('loomioApp').directive('pollCommonShareGroupForm', function(Session, AbilityService, PollService) {
  return {
    scope: {
      poll: '='
    },
    restrict: 'E',
    templateUrl: 'generated/components/poll/common/share/group_form/poll_common_share_group_form.html',
    controller: function($scope) {
      $scope.groupId = $scope.poll.groupId;
      $scope.submit = PollService.submitPoll($scope, $scope.poll, {
        flashSuccess: 'poll_common_share_form.group_set',
        successCallback: function() {
          return $scope.groupId = $scope.poll.groupId;
        }
      });
      return $scope.groups = function() {
        return _.filter(Session.user().groups(), function(group) {
          return AbilityService.canStartPoll(group);
        });
      };
    }
  };
});

angular.module('loomioApp').directive('pollCommonShareLinkForm', function(LmoUrlService, FlashService) {
  return {
    scope: {
      poll: '='
    },
    restrict: 'E',
    templateUrl: 'generated/components/poll/common/share/link_form/poll_common_share_link_form.html',
    controller: function($scope) {
      $scope.shareableLink = LmoUrlService.poll($scope.poll, {}, {
        absolute: true
      });
      $scope.setAnyoneCanParticipate = function() {
        $scope.settingAnyoneCanParticipate = true;
        return $scope.poll.save().then(function() {
          return FlashService.success("poll_common_share_form.anyone_can_participate_" + $scope.poll.anyoneCanParticipate);
        })["finally"](function() {
          return $scope.settingAnyoneCanParticipate = false;
        });
      };
      return $scope.copied = function() {
        return FlashService.success('common.copied');
      };
    }
  };
});

angular.module('loomioApp').directive('pollCommonShareVisitorForm', function($translate, Records, KeyEventService, FlashService) {
  return {
    scope: {
      poll: '='
    },
    restrict: 'E',
    templateUrl: 'generated/components/poll/common/share/visitor_form/poll_common_share_visitor_form.html',
    controller: function($scope) {
      $scope.visitors = function() {
        return Records.visitors.find({
          pollId: $scope.poll.id
        });
      };
      $scope.init = function() {
        Records.visitors.fetch({
          params: {
            poll_id: $scope.poll.id
          }
        });
        return $scope.newVisitor = Records.visitors.build({
          email: '',
          pollId: $scope.poll.id
        });
      };
      $scope.init();
      $scope.invite = function() {
        if ($scope.newVisitor.email.length <= 0) {
          return $scope.emailValidationError = $translate.instant('poll_common_share_form.email_empty');
        } else if (_.contains(_.pluck($scope.visitors(), 'email'), $scope.newVisitor.email)) {
          return $scope.emailValidationError = $translate.instant('poll_common_share_form.email_exists', {
            email: $scope.newVisitor.email
          });
        } else if (!$scope.newVisitor.email.match(/[^\s,;<>]+?@[^\s,;<>]+\.[^\s,;<>]+/g)) {
          return $scope.emailValidationError = $translate.instant('poll_common_share_form.email_invalid');
        } else {
          $scope.emailValidationError = null;
          return $scope.newVisitor.invite($scope.poll).then(function() {
            FlashService.success('poll_common_share_form.email_invited', {
              email: $scope.newVisitor.email
            });
            $scope.init();
            return document.querySelector('.poll-common-share-form__add-option-input').focus();
          });
        }
      };
      $scope.revoke = function(visitor) {
        return visitor.destroy().then(function() {
          visitor.revoked = true;
          return FlashService.success("poll_common_share_form.guest_revoked", {
            email: visitor.email
          });
        });
      };
      $scope.remind = function(visitor) {
        return visitor.remind($scope.poll).then(function() {
          visitor.reminded = true;
          return FlashService.success('poll_common_share_form.email_invited', {
            email: visitor.email
          });
        });
      };
      return KeyEventService.registerKeyEvent($scope, 'pressedEnter', $scope.invite, function(active) {
        return active.classList.contains('poll-common-share-form__add-option-input');
      });
    }
  };
});

angular.module('loomioApp').directive('pollCommonUndecidedPanel', function($location, Records, RecordLoader, AbilityService, PollService) {
  return {
    scope: {
      poll: '='
    },
    templateUrl: 'generated/components/poll/common/undecided/panel/poll_common_undecided_panel.html',
    controller: function($scope) {
      var params;
      $scope.canShowUndecided = function() {
        return $scope.poll.undecidedCount() > 0 && !$scope.showingUndecided;
      };
      params = {
        poll_id: $scope.poll.key,
        participation_token: $location.search().participation_token
      };
      $scope.usersLoader = $scope.poll.isActive() ? new RecordLoader({
        collection: 'memberships',
        path: 'undecided',
        params: params
      }) : new RecordLoader({
        collection: 'poll_did_not_votes',
        params: params
      });
      $scope.visitorsLoader = new RecordLoader({
        collection: 'visitors',
        params: params
      });
      $scope.canViewVisitors = function() {
        return AbilityService.canAdministerPoll($scope.poll);
      };
      $scope.moreUsersToLoad = function() {
        return $scope.usersLoader.numLoaded < $scope.poll.undecidedUserCount;
      };
      $scope.moreVisitorsToLoad = function() {
        return $scope.visitorsLoader.numLoaded < $scope.poll.undecidedVisitorCount;
      };
      return $scope.showUndecided = function() {
        $scope.showingUndecided = true;
        $scope.usersLoader.fetchRecords();
        return $scope.visitorsLoader.fetchRecords();
      };
    }
  };
});

angular.module('loomioApp').directive('pollCommonUndecidedUser', function() {
  return {
    scope: {
      user: '='
    },
    templateUrl: 'generated/components/poll/common/undecided/user/poll_common_undecided_user.html'
  };
});

angular.module('loomioApp').directive('pollCommonUnsubscribeForm', function(FormService) {
  return {
    scope: {
      poll: '='
    },
    templateUrl: 'generated/components/poll/common/unsubscribe/form/poll_common_unsubscribe_form.html',
    controller: function($scope) {
      return $scope.toggle = FormService.submit($scope, $scope.poll, {
        submitFn: $scope.poll.toggleSubscription,
        flashSuccess: function() {
          if ($scope.poll.subscribed) {
            return 'poll_common_unsubscribe_form.subscribed';
          } else {
            return 'poll_common_unsubscribe_form.unsubscribed';
          }
        }
      });
    }
  };
});

angular.module('loomioApp').factory('PollCommonUnsubscribeModal', function(PollService) {
  return {
    templateUrl: 'generated/components/poll/common/unsubscribe/modal/poll_common_unsubscribe_modal.html',
    controller: function($scope, poll) {
      return $scope.poll = poll;
    }
  };
});

angular.module('loomioApp').controller('RootController', function($scope, $timeout, $location, $router, $mdMedia, AuthModal, KeyEventService, MessageChannelService, IntercomService, ScrollService, Session, AppConfig, Records, ModalService, GroupModal, AbilityService, AhoyService, ViewportService, HotkeyService) {
  $scope.isLoggedIn = AbilityService.isLoggedIn;
  $scope.currentComponent = 'nothing yet';
  $scope.refresh = function() {
    $scope.pageError = null;
    $scope.refreshing = true;
    return $timeout(function() {
      return $scope.refreshing = false;
    });
  };
  $scope.renderSidebar = $mdMedia('gt-md');
  $scope.$on('toggleSidebar', function(event, show) {
    if (show === false) {
      return;
    }
    return $scope.renderSidebar = true;
  });
  $scope.$on('loggedIn', function(event, user) {
    $scope.refresh();
    if ($location.search().start_group != null) {
      ModalService.open(GroupModal, {
        group: function() {
          return Records.groups.build({
            customFields: {
              pending_emails: $location.search().pending_emails
            }
          });
        }
      });
    }
    IntercomService.boot();
    return MessageChannelService.subscribe();
  });
  $scope.$on('currentComponent', function(event, options) {
    if (options == null) {
      options = {};
    }
    Session.currentGroup = options.group;
    IntercomService.updateWithGroup(Session.currentGroup);
    $scope.pageError = null;
    $scope.$broadcast('clearBackgroundImageUrl');
    if (!options.skipScroll) {
      ScrollService.scrollTo(options.scrollTo || 'h1');
    }
    $scope.links = options.links || {};
    if (AbilityService.requireLoginFor(options.page) || (AppConfig.pendingIdentity != null)) {
      return $scope.forceSignIn();
    }
  });
  $scope.$on('setTitle', function(event, title) {
    return document.querySelector('title').text = _.trunc(title, 300) + ' | Loomio';
  });
  $scope.$on('pageError', function(event, error) {
    $scope.pageError = error;
    if (!AbilityService.isLoggedIn() && error.status === 403) {
      return $scope.forceSignIn();
    }
  });
  $scope.$on('setBackgroundImageUrl', function(event, group) {
    var url;
    url = group.coverUrl(ViewportService.viewportSize());
    return angular.element(document.querySelector('.lmo-main-background')).attr('style', "background-image: url(" + url + ")");
  });
  $scope.$on('clearBackgroundImageUrl', function(event) {
    return angular.element(document.querySelector('.lmo-main-background')).removeAttr('style');
  });
  $scope.forceSignIn = function() {
    if ($scope.forcedSignIn) {
      return;
    }
    $scope.forcedSignIn = true;
    return ModalService.open(AuthModal, {
      preventClose: function() {
        return true;
      }
    });
  };
  $scope.keyDown = function(event) {
    return KeyEventService.broadcast(event);
  };
  $router.config(AppConfig.routes.concat(AppConfig.plugins.routes));
  AppConfig.records = Records;
  AhoyService.init();
  Session.login(AppConfig.bootData);
  if (AbilityService.isLoggedIn()) {
    HotkeyService.init($scope);
  }
});

angular.module("loomioApp").run(["$templateCache", function($templateCache) {$templateCache.put("generated/components/add_community_form/add_community_form.html","<div class=\"add-facebook-community-form\"><h3 translate=\"add_community_form.currently_signed_in_as\"></h3><div class=\"add-community-form__header lmo-flex\"> <img ng-src=\"{{community.identity().logo}}\" class=\"add-community-form__logo\">  <div class=\"add-community-form__name\">{{ community.identity().name }}</div> <a href=\"#\" ng-click=\"fetchAccessToken()\"><span translate=\"add_community_form.{{community.communityType}}.another_account\"></span></a></div><md-input-container md-no-float=\"true\" class=\"lmo-search-container\"><i class=\"material-icons\">search</i><input ng-model=\"vars.fragment\" placeholder=\"{{placeholderKey() | translate }}\" ng-model-options=\"{debounce: 250}\"></md-input-container><div class=\"add-community-form__options\"><loading ng-if=\"fetchExecuting\"></loading><md-radio-group ng-if=\"!fetchExecuting &amp;&amp; !error\" ng-model=\"community.identifier\"><md-radio-button ng-repeat=\"group in groups() track by group.id\" ng-value=\"group.id\" aria-label=\"{{group.name}}\" class=\"add-community-form__radio-button\">{{ group.name }}</md-radio-button></md-radio-group><div ng-if=\"!fetchExecuting &amp;&amp; error\" class=\"add-community-form__oops\"><div class=\"add-community-form__error-icon material-icons\">warning</div><div translate=\"add_community_form.error_explanation\" translate-value-type=\"{{community.communityType}}\" class=\"add-community-form__error-explanation poll-common-helptext\"></div><md-button ng-click=\"reauth()\" class=\"md-primary md-raised add-community-form__retry-button\"><span translate=\"common.action.retry\"></span></md-button></div></div><div class=\"lmo-flex lmo-flex__space-between\"><md-button ng-click=\"back()\"><span translate=\"common.action.back\"></span></md-button><md-button ng-click=\"submit()\" ng-disabled=\"!community.identifier\" class=\"md-raised md-primary\"><span translate=\"common.action.ok\"></span></md-button></div></div>");
$templateCache.put("generated/components/add_community_modal/add_community_modal.html","<md-dialog class=\"poll-common-modal\"><div ng-show=\"isDisabled\" class=\"lmo-disabled-form\"></div><md-toolbar><div class=\"md-toolbar-tools lmo-flex__space-between\"><i class=\"material-icons\">public</i><h1 translate=\"add_community_form.title\" class=\"lmo-h1\"></h1><modal_header_cancel_button></modal_header_cancel_button></div></md-toolbar><md-dialog-content><div class=\"md-dialog-content\"><add_community_form community=\"community\"></add_community_form></div></md-dialog-content></md-dialog>");
$templateCache.put("generated/components/archive_group_form/archive_group_form.html","<md-dialog class=\"deactivation-modal\"><div ng-show=\"isDisabled\" class=\"lmo-disabled-form\"></div><md-toolbar><div class=\"md-toolbar-tools lmo-flex__space-between\"><h1 translate=\"archive_group_form.title\" class=\"lmo-h1\"></h1><material_modal_header_cancel_button></material_modal_header_cancel_button></div></md-toolbar><div class=\"md-dialog-content\"><p translate=\"archive_group_form.question\"></p><div class=\"lmo-md-actions\"><md-button ng-click=\"$close()\" type=\"button\" translate=\"common.action.cancel\"></md-button><md-button ng-click=\"submit()\" translate=\"archive_group_form.submit\" class=\"md-primary md-raised archive-group-form__submit\"></md-button></div></div></md-dialog>");
$templateCache.put("generated/components/attachment_preview/attachment_preview.html","<div class=\"blank\"> <a lmo-href=\"{{attachment.original}}\" target=\"_blank\" class=\"attachment-preview__link\"><div class=\"attachment-preview attachment-preview--{{mode}}\" ng-if=\"displayMode()\"><div ng-if=\"attachment.isAnImage\" class=\"attachment-preview__image\"><img ng-src=\"{{location()}}\" alt=\"{{ \'activity_card.attachment_display.image_alt\' | translate }}\"></div><div ng-class=\"{\'attachment-preview__file--image\': attachment.isAnImage}\" class=\"attachment-preview__file attachment-preview__file--thread\"> <i class=\"fa fa-lg fa-paperclip\"></i>  <span>{{attachment.filename}}</span> <span>{{attachment.formattedFilesize()}}</span></div></div><div ng-if=\"mode == \'thumb\'\" title=\"{{attachment.filename}}\" class=\"attachment-preview attachment-preview--thumb\"><img ng-if=\"attachment.isAnImage\" ng-src=\"{{location()}}\" alt=\"{{ \'activity_card.attachment_display.image_alt\' | translate }}\" class=\"attachment-preview__image\"><div ng-if=\"!attachment.isAnImage\" class=\"attachment-preview__file attachment-preview__file--thumb\"><i class=\"fa fa-lg fa-paperclip\"></i></div><button ng-if=\"!attachment.commentId\" ng-click=\"destroy($event)\" title=\"{{ \'comment_form.attachments.remove_attachment\' | translate }}\" class=\"close attachment-preview__destroy md-attachment-form__cancel\"><span aria-hidden=\"true\">&times;</span></button></div></a> </div>");
$templateCache.put("generated/components/authorized_apps_page/authorized_apps_page.html","<div class=\"lmo-one-column-layout\"><loading ng-show=\"authorizedAppsPage.loading\"></loading><main ng-if=\"!authorizedAppsPage.loading\" class=\"authorized-apps-page\"><h1 translate=\"authorized_apps_page.title\" class=\"lmo-h1\"></h1><hr><div ng-if=\"authorizedAppsPage.applications().length == 0\" translate=\"authorized_apps_page.no_applications\" class=\"lmo-placeholder\"></div><div ng-if=\"authorizedAppsPage.applications().length &gt; 0\" class=\"authorized-apps-page__table\"><div translate=\"authorized_apps_page.notice\" class=\"lmo-placeholder\"></div><div ng-repeat=\"application in authorizedAppsPage.applications() | orderBy: \'name\' track by application.id\" class=\"row authorized-apps-page__table-row\"><div class=\"col-xs-1 align-right\"> <img ng-src=\"{{application.logoUrl}}\" class=\"authorized-apps-page__avatar\"> </div><div class=\"col-xs-7\"><div class=\"authorized-apps-page__name\">{{ application.name }}</div></div><div class=\"col-xs-4 align-center\"><button ng-click=\"authorizedAppsPage.openRevokeForm(application)\" class=\"lmo-btn--danger\"><span translate=\"authorized_apps_page.revoke\"></span></button></div><div class=\"clearfix\"></div></div></div></main></div>");
$templateCache.put("generated/components/change_password_form/change_password_form.html","<md-dialog class=\"change-password-form\"><div ng-show=\"processing\" class=\"lmo-disabled-form\"></div><md-toolbar><div class=\"md-toolbar-tools lmo-flex__space-between\"><h1 translate=\"change_password_form.title\" class=\"lmo-h1\"></h1><material_modal_header_cancel_button></material_modal_header_cancel_button></div></md-toolbar><div class=\"md-dialog-content\"><div ng-if=\"!user.sentPasswordLink\" class=\"lmo-blank\"><p translate=\"change_password_form.helptext\"></p><div class=\"lmo-md-actions\"><md-button ng-click=\"submit()\" translate=\"change_password_form.reset_my_password\" class=\"md-primary md-raised change-password-form__submit\"></md-button></div></div><auth_complete user=\"user\" ng-if=\"user.sentPasswordLink\"></auth_complete></div></md-dialog>");
$templateCache.put("generated/components/change_picture_form/change_picture_form.html","<div class=\"change-picture-form\"><div ng-show=\"isDisabled\" class=\"lmo-disabled-form\"></div><div class=\"modal-header\"><modal_header_cancel_button aria-hidden=\"true\"></modal_header_cancel_button><h1 translate=\"change_picture_form.title\" class=\"lmo-h1\"></h1></div><div class=\"modal-body\"><div translate=\"change_picture_form.helptext\" class=\"change-picture-form__helptext\"></div><ul class=\"change-picture-form__options-list\"><li class=\"change-picture-form__option\"><button ng-click=\"submit(\'initials\')\" class=\"lmo-btn-link\"><div class=\"user-avatar lmo-box--small option-icon lmo-float--left\"><div class=\"user-avatar__initials--small\">{{user.avatarInitials}}</div></div><span translate=\"change_picture_form.use_initials\" class=\"lmo-option-text\"></span></button></li><li class=\"change-picture-form__option\"><button ng-click=\"submit(\'gravatar\')\" class=\"lmo-btn-link\"><div class=\"user-avatar lmo-box--small option-icon lmo-float--left\"><img gravatar-src-once=\"user.gravatarMd5\" alt=\"{{::user.name}}\" class=\"user-avatar__image--small\"></div><span translate=\"change_picture_form.use_gravatar\" class=\"lmo-option-text\"></span></button></li><li class=\"change-picture-form__option\"><button ng-click=\"selectFile()\" class=\"lmo-btn-link change-picture-form__upload-button\"><div class=\"user-avatar lmo-box--small option-icon lmo-float--left\"><div class=\"user-avatar__initials--small\"><i class=\"fa fa-lg fa-camera\"></i></div></div><span translate=\"change_picture_form.use_uploaded\" class=\"lmo-option-text\"></span></button><input type=\"file\" ng-model=\"files\" ng-file-select=\"upload(files)\" class=\"hidden change-picture-form__file-input\"></li></ul></div><div class=\"modal-footer\"><button ng-click=\"$close()\" type=\"button\" translate=\"common.action.cancel\" class=\"lmo-btn--cancel\"></button></div></div>");
$templateCache.put("generated/components/change_volume_form/change_volume_form.html","<md-dialog class=\"change-volume-form\"><form ng-submit=\"submit()\"><div ng-show=\"isDisabled\" class=\"lmo-disabled-form\"></div><md-toolbar><div class=\"md-toolbar-tools lmo-flex__space-between\"><h1 translate=\"{{translateKey()}}.title\" translate-value-title=\"{{model.title || model.groupName()}}\" class=\"lmo-h1 change-volume-form__title\"></h1><material_modal_header_cancel_button></material_modal_header_cancel_button></div></md-toolbar><div class=\"md-dialog-content\"><p><ul class=\"change-volume-form__list md-body-1\"><li ng-repeat=\"level in volumeLevels\" class=\"change-volume-form__option\"><input id=\"volume-{{level}}\" type=\"radio\" ng-model=\"buh.volume\" name=\"volume\" value=\"{{level}}\" class=\"change-volume-form__input\"><label for=\"volume-{{level}}\" class=\"change-volume-form__label\"><strong translate=\"change_volume_form.{{level}}_label\"></strong><p translate=\"{{translateKey()}}.{{level}}_description\" class=\"change-volume-form__description\"></p></label></li></ul><input type=\"checkbox\" ng-model=\"applyToAll\" class=\"change-volume-form__apply-to-all\" id=\"apply-to-all\"><label for=\"apply-to-all\" translate=\"{{translateKey()}}.apply_to_all\" class=\"change-volume-form__apply-to-all-label md-body-1\"></label></p><div class=\"lmo-md-actions\"><md-button type=\"button\" translate=\"common.action.cancel\" ng-click=\"$close()\" class=\"change-volume-form__cancel\"></md-button><md-button type=\"submit\" ng-disabled=\"isDisabled\" translate=\"common.action.update\" class=\"md-raised md-primary change-volume-form__submit\"></md-button></div></div></form></md-dialog>");
$templateCache.put("generated/components/contact_page/contact_page.html","<form name=\"groupForm\" ng_submit=\"submit()\" class=\"form-card\"><h1 translate=\"contact_message_form.contact_us\" class=\"lmo-h1\"></h1><div class=\"lmo-form-group\"><label for=\"contact-name-field\" translate=\"contact_message_form.name_label\"></label><input ng-model=\"message.name\" ng_required=\"true\" placeholder=\"{{contact_message_form.name_placeholder | translate}}\" class=\"form-control\" id=\"contact-name-field\"><label for=\"contact-email-field\" translate=\"contact_message_form.email_label\"></label><input ng-model=\"message.email\" ng_required=\"true\" placeholder=\"{{contact_message_form.email_placeholder | translate}}\" class=\"form-control\" id=\"contact-email-field\"><label for=\"contact-message-field\" translate=\"contact_message_form.message_label\"></label><textarea ng-model=\"message.message\" placeholder=\"{{contact_message_form.message_placeholder | translate}}\" id=\"contact-message-field\"></textarea></div><button type=\"submit\" translate=\"contact_message_form.send_message\" class=\"lmo-btn--submit\"></button></form>");
$templateCache.put("generated/components/current_polls_card/current_polls_card.html","<div class=\"current-polls-card lmo-card\"><div class=\"lmo-flex lmo-flex__space-between\"><h2 translate=\"current_polls_card.title\" class=\"lmo-card-heading lmo-truncate-text\"></h2><md-button ng-click=\"startPoll()\" translate=\"current_polls_card.start_poll\" class=\"md-primary md-raised current-polls-card__new-poll-button\"></md-button></div><loading ng-if=\"fetchRecordsExecuting\"></loading><div ng-if=\"!fetchRecordsExecuting\" class=\"current-polls-card__polls\"><div ng-if=\"!polls().length\" translate=\"current_polls_card.no_polls\" class=\"current-polls-card__no-polls poll-common-helptext\"></div><poll_common_preview poll=\"poll\" ng-repeat=\"poll in polls() track by poll.id\"></poll_common_preview></div></div>");
$templateCache.put("generated/components/dashboard_page/dashboard_page.html","<div class=\"lmo-one-column-layout\"><main class=\"dashboard-page lmo-row\"><h1 translate=\"dashboard_page.filtering.all\" ng-show=\"dashboardPage.filter == \'show_all\'\" class=\"lmo-h1-medium dashboard-page__heading\"></h1><h1 translate=\"dashboard_page.filtering.muted\" ng-show=\"dashboardPage.filter == \'show_muted\'\" class=\"lmo-h1-medium dashboard-page__heading\"></h1><section ng-if=\"dashboardPage.loading()\" ng-repeat=\"viewName in dashboardPage.loadingViewNames track by $index\" class=\"dashboard-page__collections dashboard-page__{{viewName}}\" aria-hidden=\"true\"><h2 translate=\"dashboard_page.threads_from.{{viewName}}\" class=\"dashboard-page__date-range\"></h2><div class=\"thread-previews-container\"><loading_content line-count=\"2\" ng-repeat=\"i in [1,2] track by $index\" class=\"thread-preview\"></loading_content></div></section><div ng-if=\"!dashboardPage.loading()\" class=\"lmo-blank\"><div ng-hide=\"dashboardPage.loadMoreExecuting || dashboardPage.currentBaseQuery.any() || dashboardPage.noGroups()\" class=\"dashboard-page__no-threads\"> <span ng-show=\"dashboardPage.filter == \'show_all\'\" translate=\"dashboard_page.no_threads.show_all\"></span>  <span ng-show=\"dashboardPage.filter == \'show_muted\' &amp;&amp; dashboardPage.userHasMuted()\" translate=\"dashboard_page.no_threads.show_muted\"></span>  <a ng-show=\"dashboardPage.filter != \'show_all\' &amp;&amp; dashboardPage.userHasMuted()\" translate=\"dashboard_page.view_recent\" ng-click=\"dashboardPage.setFilter(\'show_all\')\"></a> </div><div ng-show=\"dashboardPage.noGroups() &amp;&amp; dashboardPage.filter == \'show_all\'\" class=\"dashboard-page__no-groups\"> <p translate=\"dashboard_page.no_groups.show_all\"></p>  <button translate=\"dashboard_page.no_groups.start\" ng-click=\"dashboardPage.startGroup()\" class=\"lmo-btn-link--blue\"></button>  <span translate=\"dashboard_page.no_groups.or\"></span>  <span translate=\"dashboard_page.no_groups.join_group\"></span> </div><div ng-show=\"dashboardPage.filter == \'show_muted\' &amp;&amp; !dashboardPage.userHasMuted()\" class=\"dashboard-page__explain-mute\"><p><strong translate=\"dashboard_page.explain_mute.title\"></strong></p><p translate=\"dashboard_page.explain_mute.explanation_html\"></p><p translate=\"dashboard_page.explain_mute.instructions\"></p><div ng-show=\"dashboardPage.showLargeImage()\" class=\"dashboard-page__mute-image--large\"><img src=\"/img/mute.png\"></div><div ng-show=\"!dashboardPage.showLargeImage()\" class=\"dashboard-page__mute-image--small\"><img src=\"/img/mute-small.png\"></div><p translate=\"dashboard_page.explain_mute.see_muted_html\"></p></div><div ng-if=\"!dashboardPage.displayByGroup()\" class=\"dashboard-page__collections\"><section ng-if=\"dashboardPage.views.recent[viewName].any()\" class=\"thread-preview-collection__container dashboard-page__{{viewName}}\" ng-repeat=\"viewName in dashboardPage.recentViewNames\"><h2 translate=\"dashboard_page.threads_from.{{viewName}}\" class=\"dashboard-page__date-range\"></h2><thread_preview_collection query=\"dashboardPage.views.recent[viewName]\" class=\"thread-previews-container\"></thread_preview_collection></section><div in-view=\"$inview &amp;&amp; dashboardPage.loadMore()\" in-view-options=\"{debounce: 200}\" class=\"dashboard-page__footer\">.</div><loading ng-show=\"dashboardPage.loadMoreExecuting\"></loading></div><div ng-if=\"dashboardPage.displayByGroup()\" class=\"dashboard-page__collections\"><div ng-repeat=\"group in dashboardPage.groups() | orderBy:\'name\' track by group.id\" class=\"dashboard-page__group\"><section ng-if=\"dashboardPage.views.groups[group.key].any()\" role=\"region\" aria-label=\"{{ \'dashboard_page.threads_from.group\' | translate }} {{group.name}}\"> <img ng-src=\"{{group.logoUrl()}}\" aria-hidden=\"true\" class=\"selector-list-item-group-logo pull-left\"> <h2 class=\"dashboard-page__group-name\"><a lmo-href-for=\"group\">{{group.name}}</a></h2><div class=\"dashboard-groups thread-previews-container\"><thread_preview_collection query=\"dashboardPage.views.groups[group.key]\" limit=\"dashboardPage.groupThreadLimit\"></thread_preview_collection></div></section></div><loading ng-show=\"dashboardPage.loadMoreExecuting\"></loading></div></div></main></div>");
$templateCache.put("generated/components/deactivate_user_form/deactivate_user_form.html","<form ng-submit=\"submit()\" class=\"deactivate-user-form\"><div ng-show=\"isDisabled\" class=\"lmo-disabled-form\"></div><div class=\"modal-header\"><modal_header_cancel_button aria-hidden=\"true\"></modal_header_cancel_button><h1 translate=\"deactivate_user_form.title\" class=\"lmo-h1\"></h1></div><div class=\"modal-body\"><h4 translate=\"deactivate_user_form.question\" class=\"lmo-h4\"></h4><textarea ng-model=\"user.deactivationResponse\" placeholder=\"{{ \'deactivate_user_form.placeholder\' | translate }}\" class=\"lmo-textarea form-control\"></textarea></div><div class=\"modal-footer\"><button type=\"submit\" translate=\"deactivate_user_form.submit\" class=\"lmo-btn--danger\"></button><button ng-click=\"$close()\" type=\"button\" translate=\"common.action.cancel\" class=\"lmo-btn--cancel\"></button></div></form>");
$templateCache.put("generated/components/deactivation_modal/deactivation_modal.html","<md-dialog class=\"deactivation-modal\"><md-toolbar><div class=\"md-toolbar-tools lmo-flex__space-between\"><h1 translate=\"deactivate_user_form.title\" class=\"lmo-h1\"></h1><material_modal_header_cancel_button></material_modal_header_cancel_button></div></md-toolbar><div class=\"md-dialog-content\"><p translate=\"deactivation_modal.introduction\"></p><ul><li translate=\"deactivation_modal.no_longer_group_member\"></li><li translate=\"deactivation_modal.name_removed\"></li><li translate=\"deactivation_modal.no_emails\"></li><li translate=\"deactivation_modal.contact_for_reactivation\"></li></ul><div class=\"lmo-md-actions\"><md-button ng-click=\"$close()\" type=\"button\" translate=\"common.action.cancel\"></md-button><md-button ng-click=\"submit()\" translate=\"deactivation_modal.submit\" class=\"md-primary md-raised deactivation-modal__confirm\"></md-button></div></div></md-dialog>");
$templateCache.put("generated/components/delete_thread_form/delete_thread_form.html","<form ng-submit=\"submit()\" class=\"delete-thread-form\"><div ng-show=\"isDisabled\" class=\"lmo-disabled-form\"></div><md-toolbar><div class=\"md-toolbar-tools lmo-flex__space-between\"><h1 translate=\"delete_thread_form.title\" class=\"lmo-h1\"></h1><material_modal_header_cancel_button></material_modal_header_cancel_button></div></md-toolbar><div class=\"md-dialog-content md-body-1\"><p translate=\"delete_thread_form.body\"></p><div class=\"lmo-md-actions\"><md-button ng-click=\"$close()\" type=\"button\" translate=\"common.action.cancel\"></md-button><md-button type=\"submit\" translate=\"delete_thread_form.confirm\" class=\"md-primary md-raised delete_thread_form__submit\"></md-button></div></div></form>");
$templateCache.put("generated/components/discussion_form/discussion_form.html","<form name=\"discussionForm\" ng-paste=\"handlePaste($event)\" ng_submit=\"submit()\" ng-disabled=\"isDisabled\" class=\"discussion-forms\"><div ng-show=\"isDisabled\" class=\"lmo-disabled-form\"></div><md-toolbar><div class=\"md-toolbar-tools lmo-flex__space-between\"><h1 translate=\"discussion_form.new_discussion_title\" ng-if=\"discussion.isNew()\" class=\"lmo-h1 modal-title lmo-flex__grow\"></h1><h1 translate=\"discussion_form.edit_discussion_title\" ng-if=\"!discussion.isNew()\" class=\"lmo-h1 modal-title lmo-flex__grow\"></h1><material_modal_header_cancel_button aria-hidden=\"true\"></material_modal_header_cancel_button></div></md-toolbar><div class=\"md-dialog-content\"><form_errors record=\"discussion\"></form_errors><div translate=\"group_page.discussions_placeholder\" ng-show=\"discussion.isNew()\" class=\"thread-helptext\"></div><div ng-show=\"showGroupSelect\" class=\"lmo-form-group\"><label for=\"discussion-group-field\" translate=\"discussion_form.group_label\"></label><select ng-model=\"discussion.groupId\" ng-required=\"true\" ng-options=\"group.id as group.fullName for group in availableGroups() | orderBy: \'fullName\'\" ng-change=\"restoreRemoteDraft(); updatePrivacy()\" class=\"discussion-form__group-select form-control\" id=\"discussion-group-field\"><option translate=\"discussion_form.group_placeholder\"></option></select></div><div ng_if=\"discussion.groupId\" class=\"discussion-group-selected\"><div class=\"lmo-form-group\"><label for=\"discussion-title\" translate=\"discussion_form.title_label\"></label><div class=\"lmo-relative\"><input placeholder=\"{{ \'discussion_form.title_placeholder\' | translate }}\" ng-model=\"discussion.title\" ng-required=\"true\" maxlength=\"255\" class=\"discussion-form__title-input form-control lmo-primary-form-input\" id=\"discussion-title\"></div><validation_errors subject=\"discussion\" field=\"title\"></validation_errors></div><div class=\"lmo-form-group\"><label for=\"discussion-context\" translate=\"discussion_form.context_label\"></label><outlet name=\"before-discussion-form-textarea\" model=\"discussion\"></outlet><div class=\"lmo-relative lmo-textarea-wrapper\"><textarea msd-elastic=\"true\" ng-model=\"discussion.description\" placeholder=\"{{ \'discussion_form.context_placeholder\' | translate }}\" mentio=\"true\" mentio-trigger-char=\"\'@\'\" mentio-items=\"mentionables\" mentio-template-url=\"generated/components/thread_page/comment_form/mentio_menu.html\" mentio-search=\"fetchByNameFragment(term)\" mentio-id=\"discussion-field\" ng-model-options=\"{debounce: 150}\" class=\"lmo-textarea discussion-form__description-input form-control lmo-primary-form-input\" id=\"discussion-context\"></textarea><emoji_picker target-selector=\"descriptionSelector\" class=\"lmo-action-dock\"></emoji_picker></div><outlet name=\"after-discussion-form-textarea\" model=\"discussion\"></outlet></div><div class=\"discussion-form__attachments\"><div class=\"lmo-md-actions\"><a lmo-href=\"/markdown\" tabindex=\"0\" target=\"_blank\" title=\"{{ \'common.formatting_help.title\' | translate }}\" class=\"discussion-form__formatting-help md-button md-accent\"><span translate=\"common.formatting_help.label\"></span></a><md_attachment_form model=\"discussion\" ng-if=\"discussion.groupId\" show-label=\"true\" class=\"discussion-form__attachment-form\"></md_attachment_form></div><validation_errors subject=\"discussion\" field=\"file\" class=\"discussion-form__attachment-errors\"></validation_errors><label class=\"lmo-form-labelled-input\"><attachment_preview attachment=\"attachment\" mode=\"thumb\" ng-repeat=\"attachment in discussion.newAttachments() | orderBy: \'id\' track by attachment.id\"></attachment_preview></label><div class=\"clearfix\"></div></div><div ng-show=\"showPrivacyForm()\" class=\"lmo-form-group\"><label class=\"lmo-form-labelled-input discussion-form__public\"><input type=\"radio\" ng-model=\"discussion.private\" ng-value=\"false\"><span> <i class=\"fa fa-globe\"></i> <strong translate=\"common.privacy.public\"></strong><span class=\"seperator\"></span><span translate=\"discussion_form.privacy_public\"></span></span></label><label class=\"lmo-form-labelled-input discussion-form__private\"><input type=\"radio\" ng-model=\"discussion.private\" ng-value=\"true\"><span> <i class=\"fa fa-lock\"></i> <strong translate=\"common.privacy.private\"></strong><span class=\"seperator\"></span><span ng-bind-html=\"privacyPrivateDescription()\"></span></span></label></div><div ng-show=\"!showPrivacyForm()\" class=\"privacy-notice\"><label ng_hide=\"discussion.private\" class=\"lmo-form-labelled-input\"><span> <i class=\"fa fa-globe\"></i> <strong translate=\"common.privacy.public\"></strong><span class=\"seperator\"></span><span translate=\"discussion_form.privacy_public\"></span></span></label><label ng-show=\"discussion.private\" class=\"lmo-form-labelled-input\"><span> <i class=\"fa fa-lock\"></i> <strong translate=\"common.privacy.private\"></strong><span class=\"seperator\"></span><span ng-bind-html=\"privacyPrivateDescription()\"></span></span></label></div><label ng-if=\"discussion.isNew()\" class=\"lmo-form-labelled-input\"><input type=\"checkbox\" ng-model=\"discussion.makeAnnouncement\" class=\"discussion-form__make-announcement\"><span> <i class=\"fa fa-bell\"></i>  <strong translate=\"discussion_form.make_announcement\"></strong> <span ng-if=\"discussion.makeAnnouncement\" class=\"seperator\"></span><span translate=\"discussion_form.notified_count\" ng-if=\"discussion.makeAnnouncement\" translate-values=\"{count: discussion.group().announcementRecipientsCount}\"></span></span></label></div><div class=\"md-dialog-actions lmo-md-action\"><md-button type=\"submit\" ng-disabled=\"submitIsDisabled || !discussion.groupId\" translate=\"common.action.start\" ng-if=\"discussion.isNew()\" class=\"md-primary md-raised discussion-form__submit\"></md-button><md-button type=\"submit\" ng-disabled=\"submitIsDisabled\" translate=\"common.action.save\" ng-if=\"!discussion.isNew()\" class=\"md-primary md-raised discussion-form__update\"></md-button></div></div></form>");
$templateCache.put("generated/components/dismiss_explanation_modal/dismiss_explanation_modal.html","<div class=\"dismiss-explanation-modal\"><div class=\"modal-header\"><modal_header_cancel_button aria-hidden=\"true\"></modal_header_cancel_button><h1 translate=\"dismiss_explanation_modal.dismiss_thread\" class=\"lmo-h1 dismiss-explanation-modal__title\"></h1></div><div class=\"modal-body dismiss-explanation-modal__body\"><div translate=\"dismiss_explanation_modal.body_html\" class=\"dismiss-explanation-modal__dismiss-explanation\"></div></div><div class=\"modal-footer lmo-clearfix\"><button type=\"button\" ng-click=\"dismiss()\" translate=\"dismiss_explanation_modal.dismiss_thread\" class=\"dismiss-explanation-modal__dismiss-thread lmo-btn--submit\"></button><button type=\"button\" ng-click=\"$close()\" translate=\"common.action.cancel\" class=\"dismiss-explanation-modal__cancel lmo-btn--cancel\"></button></div></div>");
$templateCache.put("generated/components/email_settings_page/email_settings_page.html","<div class=\"lmo-one-column-layout\"><main ng-if=\"emailSettingsPage.user\" class=\"email-settings-page\"><div class=\"lmo-page-heading\"><h1 translate=\"email_settings_page.header\" class=\"lmo-h1-medium\"></h1></div><div class=\"email-settings-page__email-settings\"><div class=\"email-settings-page__global-settings\"><h3 translate=\"email_settings_page.all_groups\" class=\"lmo-h3\"></h3><form ng-submit=\"emailSettingsPage.submit()\"><div class=\"email-settings-page__global-email-settings-options\"><div class=\"email-settings-page__field\"><input type=\"checkbox\" ng-model=\"emailSettingsPage.user.emailMissedYesterday\" class=\"email-settings-page__daily-summary\" id=\"daily-summary-email\"><label for=\"daily-summary-email\" translate=\"email_settings_page.daily_summary_label\"></label><div translate=\"email_settings_page.daily_summary_description\" class=\"email-settings-page__input-description\"></div></div><div class=\"email-settings-page__field\"><input type=\"checkbox\" ng-model=\"emailSettingsPage.user.emailOnParticipation\" class=\"email-settings-page__on-participation\" id=\"on-participation-email\"><label for=\"on-participation-email\" translate=\"email_settings_page.on_participation_label\"></label><div translate=\"email_settings_page.on_participation_description\" class=\"email-settings-page__input-description\"></div></div><div class=\"email-settings-page__field\"><input type=\"checkbox\" ng-model=\"emailSettingsPage.user.emailWhenMentioned\" class=\"email-settings-page__mentioned\" id=\"mentioned-email\"><label for=\"mentioned-email\" translate=\"email_settings_page.mentioned_label\"></label><div translate=\"email_settings_page.mentioned_description\" class=\"email-settings-page__input-description\"></div></div></div><button type=\"submit\" ng-disabled=\"isDisabled\" translate=\"email_settings_page.update_settings\" class=\"email-settings-page__update-button\"></button></form></div><div class=\"email-settings-page__specific-group-settings\"><h3 translate=\"email_settings_page.specific_groups\" class=\"lmo-h3\"></h3><div translate=\"{{emailSettingsPage.defaultSettingsDescription()}}\" class=\"email-settings-page__default-description\"></div><button ng-click=\"emailSettingsPage.changeDefaultMembershipVolume()\" translate=\"email_settings_page.change_default\" class=\"email-settings-page__change-default-link lmo-btn-link\"></button><ul class=\"email-settings-page__groups\"><li ng-repeat=\"group in emailSettingsPage.user.groups() track by group.id\" class=\"email-settings-page__group\"><group_avatar group=\"group\" size=\"medium\"></group_avatar><div class=\"email-settings-page__group-details\"><strong class=\"email-settings-page__group-name\"> <span ng-if=\"group.isSubgroup()\">{{group.parentName()}} -</span> <span>{{group.name}}</span></strong><p translate=\"change_volume_form.{{emailSettingsPage.groupVolume(group)}}_label\" class=\"email-settings-page__membership-volume\"></p></div><div class=\"email-settings-page__edit\"><button ng-click=\"emailSettingsPage.editSpecificGroupVolume(group)\" translate=\"email_settings_page.edit\" class=\"email-settings-page__edit-membership-volume-link lmo-btn-link\"></button></div></li></ul><a href=\"https://loomio.gitbooks.io/manual/content/en/keeping_up_to_date.html\" target=\"_blank\" translate=\"Learn more about email settings...\" class=\"email-settings-page__learn-more-link\"></a></div></div></main></div>");
$templateCache.put("generated/components/emoji_picker/emoji_picker.html","<div aria-hidden=\"true\" off-click=\"hideMenu()\" class=\"emoji-picker\"><button type=\"button\" tabindex=\"-1\" ng-click=\"toggleMenu()\" class=\"btn btn-nude emoji-picker__toggle\"><i class=\"fa fa-smile-o\"></i></button><div ng-if=\"showMenu\" class=\"emoji-picker__menu\"><div ng-show=\"hovered.name\" class=\"emoji-picker__preview\"> <span ng-bind-html=\"hovered.image\"></span> <span class=\"emoji-picker__hovered-name\">{{ hovered.name }}</span><hr class=\"emoji-picker__hr\"></div><div ng-if=\"noEmojisFound()\" translate=\"emoji_picker.no_emojis_found\" translate-value-term=\"{{term}}\" class=\"emoji-picker__none-found\"></div><div ng-repeat=\"emoji in source\" class=\"emoji-picker__selector\"><a ng-click=\"select(emoji)\" ng-mouseover=\"hover(emoji)\" class=\"emoji-picker__link\"><div ng-bind-html=\"render(emoji)\" class=\"emoji-picker__icon\"></div></a></div><input ng-model=\"term\" ng-change=\"search(term)\" placeholder=\"{{\'emoji_picker.search\' | translate}}\" class=\"emoji-picker__search form-control\"></div></div>");
$templateCache.put("generated/components/error_page/error_page.html","<div class=\"error-page\"><div translate=\"error_page.forbidden\" ng-if=\"error.status == 403\" class=\"error-page__forbidden\"></div><div translate=\"error_page.page_not_found\" ng-if=\"error.status == 404\" class=\"error-page__page-not-found\"></div><div translate=\"error_page.internal_server_error\" ng-if=\"error.status == 500\" class=\"error-page__internal-server-error\"></div></div>");
$templateCache.put("generated/components/explore_page/explore_page.html","<div class=\"lmo-one-column-layout\"><main class=\"explore-page\"><h1 translate=\"explore_page.header\" class=\"lmo-h1-medium\"></h1><div class=\"explore-page__search-field\"><input ng-model=\"explorePage.query\" ng-model-options=\"{debounce: 600}\" ng-change=\"explorePage.search()\" placeholder=\"{{ \'explore_page.search_placeholder\' | translate }}\" class=\"form-control\" id=\"search-field\"><i ng-hide=\"explorePage.query\" aria-hidden=\"true\" class=\"fa fa-lg fa-fw fa-search explore-page__search-input-icon\"></i><loading ng-show=\"searchExecuting\"></loading></div><div ng-show=\"explorePage.showMessage()\" translate=\"{{explorePage.searchResultsMessage()}}\" translate-values=\"{resultCount: explorePage.resultsCount, searchTerm: explorePage.query}\" class=\"explore-page__search-results\"></div><div class=\"explore-page__groups\"><a ng-repeat=\"group in explorePage.groups() | orderBy: \'-recentActivityCount\' track by group.id\" lmo-href-for=\"group\" class=\"explore-page__group\"><div ng-style=\"explorePage.groupCover(group)\" class=\"explore-page__group-cover\"></div><div class=\"explore-page__group-details\"><h2 class=\"lmo-h2\">{{group.name}}</h2><div class=\"explore-page__group-description\">{{explorePage.groupDescription(group)}}</div><div class=\"explore-page__group-memberships-count\"> <i class=\"fa fa-lg fa-group\"></i> <span>{{group.membershipsCount}}</span></div><div class=\"explore-page__group-discussions-count\"> <i class=\"fa fa-lg fa-comment\"></i> <span>{{group.discussionsCount}}</span></div><div class=\"explore-page__group-motions-count\"> <i class=\"fa fa-lg fa-pie-chart\"></i> <span>{{group.motionsCount}}</span></div></div></a></div><div ng-show=\"explorePage.canLoadMoreGroups\" class=\"lmo-show-more\"><button ng-hide=\"searchExecuting\" ng-click=\"explorePage.loadMore()\" translate=\"common.action.show_more\" class=\"explore-page__show-more\"></button></div><loading ng-show=\"searchExecuting\"></loading><div ng-show=\"explorePage.noResultsFound()\" translate=\"explore_page.no_results_found\" class=\"explore-page__no-results-found\"></div></main></div>");
$templateCache.put("generated/components/flash/flash.html","<div aria-live=\"{{ariaLive()}}\" role=\"alert\" class=\"flash-root\"><div ng-if=\"display()\" ng-animate=\"\'enter\'\" class=\"flash-root__container animated alert-{{flash.level}}\"><div class=\"flash-root__message\"><loading ng-if=\"loading()\" class=\"flash-root__loading\"></loading>{{ flash.message | translate:flash.options }}</div><div ng-if=\"flash.actionFn\" class=\"flash-root__action\"><a ng-click=\"flash.actionFn()\" translate=\"{{flash.action}}\"></a></div></div></div>");
$templateCache.put("generated/components/form_errors/form_errors.html","<div ng-hide=\"record.isValid()\" ng-animate=\"\'enter\'\" class=\"form-errors\"><ul><li ng-repeat=\"error in record.errors\"># translation soon{{error}}</li></ul></div>");
$templateCache.put("generated/components/group_avatar/group_avatar.html","<div class=\"group-avatar lmo-box--{{size}}\" aria-hidden=\"true\"><img class=\"lmo-box--{{size}}\" alt=\"{{group.name}}\" ng-src=\"{{::group.logoUrl()}}\"></div>");
$templateCache.put("generated/components/group_page/group_page.html","<div class=\"loading-wrapper lmo-two-column-layout\"><loading ng-if=\"!groupPage.group\"></loading><main ng-if=\"groupPage.group\" class=\"group-page lmo-row\"><outlet name=\"before-group-page\" model=\"groupPage.group\"></outlet><group_theme group=\"groupPage.group\" home-page=\"true\"></group_theme><div class=\"lmo-row\"><div class=\"lmo-group-column-left\"><description_card group=\"groupPage.group\"></description_card><discussions_card group=\"groupPage.group\" page_window=\"groupPage.pageWindow\"></discussions_card></div><div class=\"lmo-group-column-right\"><outlet name=\"before-group-page-column-right\" model=\"groupPage.group\"></outlet><current_polls_card model=\"groupPage.group\"></current_polls_card><poll_common_index_card model=\"groupPage.group\" limit=\"5\" view-more-link=\"true\"></poll_common_index_card><group_previous_proposals_card group=\"groupPage.group\" ng-if=\"!groupPage.usePolls\"></group_previous_proposals_card><membership_requests_card group=\"groupPage.group\"></membership_requests_card><members_card group=\"groupPage.group\" ng-if=\"groupPage.canViewMemberships()\"></members_card><install_slack_card group=\"groupPage.group\"></install_slack_card><outlet name=\"after-slack-card\" group=\"groupPage.group\"></outlet><subgroups_card group=\"groupPage.group\"></subgroups_card><group_help_card group=\"groupPage.group\"></group_help_card></div></div></main></div>");
$templateCache.put("generated/components/help_bubble/help_bubble.html","<div class=\"help-bubble\"><i class=\"material-icons\">help_outline</i><md-tooltip class=\"help-bubble__tooltip\"><span translate=\"{{helptext | translate}}\"></span></md-tooltip></div>");
$templateCache.put("generated/components/help_page/help_page.html","<div class=\"form-card\"><div class=\"col-xs-12\" id=\"help-content\"><section id=\"how-it-works\"><div class=\"inner-container\"><div class=\"row\"><div class=\"subhead\"><h1 translate=\"help_page.how_it_works\" class=\"lmo-h1\"></h1></div></div><div class=\"row\" id=\"video-tutorial\"><div class=\"col-sm-12\"><h2 translate=\"help_page.video_tutorial\" class=\"lmo-h2\"></h2></div><div class=\"col-md-12\"><p translate=\"help_page.video_tutorial_description\"></p></div><div class=\"col-md-12\"><iframe width=\"356px\" height=\"267px\" src=\"//www.youtube.com/embed/eu6A1IQar0g\" frameborder=\"0\" allowfullscreen></iframe></div></div><div class=\"row\" id=\"getting-started\"><div class=\"col-sm-12\"><h2 translate=\"help_page.getting_started\" class=\"lmo-h2\"></h2></div><div class=\"col-sm-8\"><p translate=\"help_page.getting_started_description\"></p></div></div><div class=\"row\" id=\"have-a-discussion\"><div class=\"col-sm-12\"><h2 translate=\"help_page.have_a_discussion\" class=\"lmo-h2\"></h2></div><div class=\"col-sm-8\"><p translate=\"help_page.have_a_discussion_description\"></p></div></div><div class=\"row\" id=\"make-a-decision\"><div class=\"col-sm-12\"><h2 translate=\"help_page.make_a_decision\" class=\"lmo-h2\"></h2></div><div class=\"col-sm-8\"><p translate=\"help_page.make_a_decision_description\"></p></div></div><div class=\"row\" id=\"have-your-say\"><div class=\"col-sm-12\"><h2 translate=\"help_page.have_your_say\" class=\"lmo-h2\"></h2></div><div translate=\"help_page.have_your_say_details_html\" class=\"col-sm-6\"></div><div class=\"col-sm-6\"><div class=\"positions\"><p translate=\"help_page.positions_agree_description\"></p><p translate=\"help_page.positions_abstain_description\"></p><p translate=\"help_page.positions_disagree_description\"></p><p translate=\"help_page.positions_block_description\"></p></div></div></div><div class=\"row\" id=\"prompt-people\"><div class=\"col-md-10\"><h2 translate=\"help_page.prompt_people_html\" class=\"lmo-h2\"></h2></div></div><div class=\"row\"><div class=\"col-md-8\"><p translate=\"help_page.prompt_people_details\"></p></div></div><div class=\"row\" id=\"keep-up-to-date\"><div class=\"col-md-8\"><h2 translate=\"help_page.keep_up_to_date\" class=\"lmo-h2\"><a lmo-href=\"/email\"><span translate=\"help_page.keep_up_to_date_html\"></span></a></h2></div></div><div class=\"row\" id=\"images-and-text-formatting\"><div class=\"col-md-8\"><h2 translate=\"help_page.images_and_text_formatting\" class=\"lmo-h2\"></h2><span translate=\"help_page.images_and_text_formatting_html\"></span></div></div></div></section><section id=\"getting-the-most-out-of-Loomio\"><div class=\"inner-container\"><div class=\"row\"><h1 translate=\"help_page.getting_the_most\" class=\"lmo-h1\"></h1></div><div class=\"row\" id=\"starting-an-engaging-discussion\"><div class=\"col-md-10\"><h2 translate=\"help_page.start_discussion\" class=\"lmo-h2\"></h2></div></div><div class=\"row\"><div translate=\"help_page.start_discussion_html\" class=\"col-md-8\"></div></div><div class=\"row\" id=\"writing-a-clear-proposal\"><div class=\"col-md-8\"><h2 translate=\"help_page.start_proposal\" class=\"lmo-h2\"></h2><span translate=\"help_page.start_proposal_html\"></span></div></div><div class=\"row\" id=\"different-ways-to-use-Loomio\"><div class=\"col-md-9\"><h2 translate=\"help_page.different_ways\" class=\"lmo-h2\"></h2></div></div><div class=\"row\"><div class=\"col-md-8\"><div translate=\"help_page.example1_html\" class=\"example\"></div><div translate=\"help_page.example2_html\" class=\"example\"></div></div></div></div></section><section id=\"coordinating-your-Loomio-group\"><div class=\"inner-container\"><div class=\"row\"><h1 translate=\"help_page.coordinating\" class=\"lmo-h1\"></h1></div><div class=\"row\"><div class=\"col-md-8 extra-margin\"><p translate=\"help_page.coordinating_details\"></p></div></div><div class=\"row\" id=\"writing-a-great-invitation\"><div translate=\"help_page.invitation_html\" class=\"col-md-8\"></div></div><div class=\"row\" id=\"tips-for-engaging-your-group\"><div translate=\"help_page.engaging_html\" class=\"col-md-8\"></div></div></div></section><section id=\"get-in-touch\"><div class=\"inner-container\"><div class=\"row\"><h1 translate=\"help_page.get_in_touch\" class=\"lmo-h1\"></h1></div><div class=\"row\"><div class=\"col-md-8\"><a lmo-href=\"/contact\"><p translate=\"help_page.get_in_touch_description_html\" class=\"extra-margin\"></p></a></div></div><div class=\"row\" id=\"join-the-community\"><h1 translate=\"help_page.join_the_community\" class=\"lmo-h1\"></h1></div><div class=\"row\"><div class=\"col-md-8\"><p translate=\"help_page.join_the_community_description_html\" class=\"extra-margin\"></p></div></div></div></section></div><div class=\"clearfix\"></div></div>");
$templateCache.put("generated/components/inbox_page/inbox_page.html","<div class=\"lmo-one-column-layout\"><main class=\"inbox-page\"><div class=\"thread-preview-collection__container\"><h1 translate=\"inbox_page.unread_threads\" class=\"lmo-h1-medium inbox-page__heading\"></h1><div class=\"inbox-page__threads\"><div ng-show=\"!inboxPage.hasThreads() &amp;&amp; !inboxPage.noGroups()\" class=\"inbox-page__no-threads\"> <span translate=\"inbox_page.no_threads\"></span> <span></span> 🙌</div><div ng-show=\"inboxPage.noGroups()\" class=\"inbox-page__no-groups\"> <p translate=\"inbox_page.no_groups.explanation\"></p>  <button translate=\"inbox_page.no_groups.start\" ng-click=\"inboxPage.startGroup()\" class=\"lmo-btn-link--blue\"></button>  <span translate=\"inbox_page.no_groups.or\"></span>  <span translate=\"inbox_page.no_groups.join_group\"></span> </div><div ng-repeat=\"group in inboxPage.groups() | orderBy: \'name\' track by group.id\" class=\"inbox-page__group\"><section ng-if=\"inboxPage.views.groups[group.key].any()\" role=\"region\" aria-label=\"{{ \'inbox_page.threads_from.group\' | translate }} {{group.name}}\"><div class=\"inbox-page__group-name-container\"> <img ng-src=\"{{group.logoUrl()}}\" aria-hidden=\"true\" class=\"selector-list-item-group-logo pull-left\"> <h2 class=\"inbox-page__group-name\"><a href=\"/g/{{group.key}}\">{{group.name}}</a></h2></div><div class=\"inbox-page__groups thread-previews-container\"><thread_preview_collection query=\"inboxPage.views.groups[group.key]\" limit=\"inboxPage.threadLimit\"></thread_preview_collection></div></section></div></div></div></main></div>");
$templateCache.put("generated/components/install_slack_page/install_slack_page.html","<div class=\"install-slack-page\"></div>");
$templateCache.put("generated/components/leave_group_form/leave_group_form.html","<md-dialog class=\"leave-group-form\"><form ng-submit=\"submit()\"><div ng-show=\"isDisabled\" class=\"lmo-disabled-form\"></div><md-toolbar><div class=\"md-toolbar-tools lmo-flex__space-between\"><h1 translate=\"leave_group_form.title\" class=\"lmo-h1\"></h1><material_modal_header_cancel_button></material_modal_header_cancel_button></div></md-toolbar><div class=\"md-dialog-content\"><p ng-if=\"canLeaveGroup()\" translate=\"leave_group_form.question\"></p><p ng-if=\"!canLeaveGroup()\" translate=\"leave_group_form.cannot_leave_group\"></p><div class=\"lmo-md-actions\"><md-button ng-click=\"$close()\" type=\"button\" translate=\"common.action.cancel\"></md-button><md-button ng-if=\"canLeaveGroup()\" type=\"submit\" class=\"md-primary md-raised leave-group-form__submit\"><span translate=\"leave_group_form.submit\"></span></md-button></div></div></form></md-dialog>");
$templateCache.put("generated/components/loading/loading.html","<div class=\"page-loading\"><i class=\"fa fa-lg fa-spin fa-circle-o-notch\"></i></div>");
$templateCache.put("generated/components/loading_content/loading_content.html","<div class=\"loading-content\"><div ng-repeat=\"block in blocks track by $index\" class=\"loading-content__background-wrapper\"><div ng-repeat=\"line in lines track by $index\" class=\"loading-content__background\"></div></div></div>");
$templateCache.put("generated/components/material_modal_header_cancel_button/material_modal_header_cancel_button.html","<md-button aria-label=\"close\" ng-click=\"$close()\" class=\"md-icon-button material-modal-header-cancel-button modal-cancel\"><span translate=\"common.action.cancel\" class=\"sr-only\"></span><i class=\"material-icons\">close</i></md-button>");
$templateCache.put("generated/components/md_attachment_form/md_attachment_form.html","<div class=\"md-attachment-form\"><md-button type=\"button\" ng-click=\"selectFile()\" ng-hide=\"files\" aria-label=\"{{ \'comment_form.attachments.add_attachment\' | translate }}\" class=\"md-accent md-attachment-form__button\"><i class=\"material-icons md-attachment-form__icon\">attach_file</i><span translate=\"comment_form.attachments.add_attachment\" ng-if=\"showLabel\"></span></md-button><input type=\"file\" ng-model=\"files\" ng-file-select=\"upload(files)\" class=\"md-attachment-form__file-input hidden\"><div ng-repeat=\"file in files\" class=\"md-attachment-form-in-progress\"><div class=\"progress active md-attachment-form-progress-field\"><strong class=\"md-attachment-form-progress-text\">{{ file.name }}: {{ percentComplete }} %</strong><md-progress-linear md-mode=\"determinate\" value=\"{{percentComplete}}\"></md-progress-linear></div><button type=\"button\" ng-click=\"abort()\" class=\"close md-attachment-form-cancel cancel-upload md-attachment-form__cancel\">&times;</button></div></div>");
$templateCache.put("generated/components/membership_requests_page/membership_requests_page.html","<div class=\"loading-wrapper lmo-one-column-layout\"><loading ng-if=\"!membershipRequestsPage.group\"></loading><main ng-if=\"membershipRequestsPage.group\" class=\"membership-requests-page\"><div class=\"lmo-group-theme-padding\"></div><group_theme group=\"membershipRequestsPage.group\"></group_theme><div class=\"membership-requests-page__pending-requests\"><h2 translate=\"membership_requests_page.heading\" class=\"lmo-h2\"></h2><ul ng-if=\"membershipRequestsPage.group.hasPendingMembershipRequests()\"><li ng-repeat=\"request in membershipRequestsPage.pendingRequests() track by request.id\" class=\"media membership-requests-page__pending-request\"><div class=\"media-left\"><user_avatar user=\"request.actor()\" size=\"medium\"></user_avatar></div><div class=\"media-body\"><span class=\"membership-requests-page__pending-request-name\"><strong>{{request.actor().name}}</strong></span><div class=\"membership-requests-page__pending-request-email\">{{request.email}}</div><div class=\"membership-requests-page__pending-request-date\"><timeago timestamp=\"request.createdAt\"></timeago></div><div class=\"membership-requests-page__pending-request-introduction\">{{request.introduction}}</div><div class=\"membership-requests-page__actions\"><button ng-click=\"membershipRequestsPage.approve(request)\" translate=\"membership_requests_page.approve\" class=\"lmo-btn--primary membership-requests-page__approve\"></button><button ng-click=\"membershipRequestsPage.ignore(request)\" translate=\"membership_requests_page.ignore\" class=\"lmo-btn--default membership-requests-page__ignore\"></button></div></div></li></ul><div ng-if=\"!membershipRequestsPage.group.hasPendingMembershipRequests()\" translate=\"membership_requests_page.no_pending_requests\" class=\"membership-requests-page__no-pending-requests\"></div></div><div class=\"membership-requests-page__previous-requests\"><h3 translate=\"membership_requests_page.previous_requests\" class=\"lmo-card-heading\"></h3><ul ng-if=\"membershipRequestsPage.group.hasPreviousMembershipRequests()\"><li ng-repeat=\"request in membershipRequestsPage.previousRequests() track by request.id\" class=\"media membership-requests-page__previous-request\"><div class=\"media-left\"><user_avatar user=\"request.actor()\" size=\"medium\"></user_avatar></div><div class=\"media-body\"><span class=\"membership-requests-page__previous-request-name\"><strong>{{request.actor().name}}</strong></span><div class=\"membership-requests-page__previous-request-email\">{{request.email}}</div><div class=\"membership-requests-page__previous-request-response\"><span translate=\"membership_requests_page.previous_request_response\" translate-value-response=\"{{request.formattedResponse()}}\" translate-value-responder=\"{{request.responder().name}}\"></span><timeago timestamp=\"request.respondedAt\"></timeago></div><div class=\"membership-requests-page__previous-request-introduction\">{{request.introduction}}</div></div></li></ul><div ng-if=\"!membershipRequestsPage.group.hasPreviousMembershipRequests()\" translate=\"membership_requests_page.no_previous_requests\" class=\"membership-requests-page__no-previous-requests\"></div></div></main></div>");
$templateCache.put("generated/components/memberships_page/memberships_page.html","<div class=\"loading-wrapper lmo-one-column-layout\"><loading ng-if=\"!membershipsPage.group\"></loading><main ng-if=\"membershipsPage.group\" class=\"memberships-page\"><div class=\"lmo-group-theme-padding\"></div><group_theme group=\"membershipsPage.group\"></group_theme><div class=\"lmo-card\"><div class=\"memberships-page__top-bar lmo-flex lmo-flex__space-between lmo-flex__baseline\"><h2 translate=\"memberships_page.members\" class=\"md-title\"></h2><md-button ng-click=\"membershipsPage.invitePeople()\" class=\"md-primary md-raised\"><span translate=\"group_page.invite_people\"></span></md-button></div><div class=\"memberships-page__search-filter\"><input ng-model=\"membershipsPage.fragment\" ng-model-options=\"{debounce: 300}\" ng-change=\"membershipsPage.fetchMemberships()\" placeholder=\"{{\'memberships_page.fragment_placeholder\' | translate}}\" class=\"membership-page__search-filter form-control\"><i class=\"fa fa-lg fa-search\"></i></div><admin_memberships_panel ng-if=\"membershipsPage.canAdministerGroup()\" memberships=\"membershipsPage.memberships\" group=\"membershipsPage.group\"></admin_memberships_panel><div class=\"memberships-page__memberships\"><memberships_panel ng-if=\"!membershipsPage.canAdministerGroup()\" memberships=\"membershipsPage.memberships\" group=\"membershipsPage.group\"></memberships_panel></div></div><pending_invitations_card group=\"membershipsPage.group\"></pending_invitations_card></main></div>");
$templateCache.put("generated/components/modal_header_cancel_button/modal_header_cancel_button.html","<button type=\"button\" class=\"modal-header-cancel-button close\" ng-click=\"$close()\"><span>&times;</span><span translate=\"common.action.cancel\" class=\"sr-only\"></span></button>");
$templateCache.put("generated/components/move_thread_form/move_thread_form.html","<form ng-submit=\"submit()\"><div ng-show=\"isDisabled\" class=\"lmo-disabled-form\"></div><div class=\"move-thread-form\"><div class=\"modal-header\"><modal_header_cancel_button aria-hidden=\"true\"></modal_header_cancel_button><h1 translate=\"move_thread_form.title\" class=\"lmo-h1\"></h1></div><div class=\"modal-body\"><label for=\"group-dropdown\" translate=\"move_thread_form.body\"></label><select ng-model=\"discussion.groupId\" ng-required=\"ng-required\" ng-options=\"group.id as group.fullName for group in availableGroups() | orderBy: \'fullName\'\" ng-change=\"updateTarget()\" class=\"move-thread-form__group-dropdown form-control\" id=\"group-dropdown\"></select></div><div class=\"modal-footer\"><button type=\"button\" translate=\"move_thread_form.confirm\" ng-click=\"moveThread()\" class=\"lmo-btn--submit move-thread-form__submit\"></button><button ng-click=\"$close()\" type=\"button\" translate=\"common.action.cancel\" class=\"lmo-btn--cancel\"></button></div></div></form>");
$templateCache.put("generated/components/mute_explanation_modal/mute_explanation_modal.html","<div class=\"mute-explanation-modal\"><md-toolbar><div class=\"md-toolbar-tools lmo-flex__space-between\"><h1 translate=\"mute_explanation_modal.mute_thread\" class=\"lmo-h1 mute-explanation-modal__title\"></h1><material_modal_header_cancel_button></material_modal_header_cancel_button></div></md-toolbar><div class=\"md-dialog-content md-body-1\"><div translate=\"mute_explanation_modal.body_html\" class=\"mute-explanation-modal__mute-explanation\"></div><div class=\"mute-explanation-modal__muted-threads-image\"><img src=\"/img/muted-threads-sidebar.png\" alt=\"{{ \'mute_explanation_modal.image_alt\' | translate }}\"></div><div class=\"lmo-md-actions\"><md-button type=\"button\" ng-click=\"$close()\" translate=\"common.action.cancel\" class=\"mute-explanation-modal__cancel\"></md-button><md-button type=\"button\" ng-click=\"muteThread()\" translate=\"mute_explanation_modal.mute_thread\" class=\"md-raised md-primary mute-explanation-modal__mute-thread\"></md-button></div></div></div>");
$templateCache.put("generated/components/navbar/navbar.html","<blank class=\"lmo-navbar\"><header ng-if=\"showNavbar\"><md_toolbar><div class=\"md-toolbar-tools lmo-relative\"><div class=\"navbar__left\"><md_icon_button aria-hidden=\"true\" ng-show=\"isLoggedIn()\" ng-click=\"toggleSidebar()\" aria-label=\"{{ \'navbar.toggle_sidebar\' | translate }}\" class=\"navbar__sidenav-toggle\"><i class=\"fa fa-lg fa-bars\"></i></md_icon_button></div><div class=\"navbar__middle\"><a lmo-static-href=\"/\" aria-label=\"{{ \'navbar.logo_title\' | translate }}\"><div class=\"navbar__logo-container\"><img src=\"/img/logo.png\" alt=\"Loomio logo\" class=\"navbar__logo\"></div></a></div><div class=\"navbar__right\"><navbar_search ng-show=\"isLoggedIn()\"></navbar_search><notifications ng-show=\"isLoggedIn()\"></notifications><div ng-if=\"!isLoggedIn()\" class=\"buh\"> <button ng-click=\"signIn()\" class=\"navbar__sign-in\"><span translate=\"navbar.sign_in\"></span></button> </div></div></div></md_toolbar></header></blank>");
$templateCache.put("generated/components/navbar/navbar_search.html","<div class=\"navbar-search\"><div class=\"navbar-search__input-wrapper\"><label translate=\"navbar.search.label\" class=\"sr-only\"></label><input ng-model=\"query\" ng-model-options=\"{debounce: {default: 400, blur: 200}}\" ng-change=\"getSearchResults(query)\" placeholder=\"{{ \'navbar.search.placeholder\' | translate }}\" ng-focus=\"focused = true\" ng-blur=\"handleSearchBlur()\" tabindex=\"2\" aria-haspopup=\"true\" class=\"navbar-search__input form-control\" id=\"primary-search-input\"><i ng-hide=\"query\" aria-hidden=\"true\" class=\"fa fa-fw fa-search navbar-search__input-icon\"></i><i ng-show=\"query\" ng-click=\"clearAndFocusInput()\" title=\"{{ \'navbar.search.close\' | translate }}\" aria-label=\"{{ \'navbar.search.close\' | translate }}\" class=\"fa fa-fw fa-times navbar-search__input-icon\"></i></div><div ng-show=\"showDropdown()\" aria-label=\"{{ \'navbar.search.results_aria_label\' | translate }}\" class=\"navbar-search__results\"><ul ng-show=\"query\" class=\"thread-list selector-list\"><li ng-show=\"searching\" class=\"navbar-search__list-item selector-list-item search-loading\"><loading></loading></li><li ng-show=\"noResultsFound()\" translate=\"navbar.search.no_results\" class=\"navbar-search__list-item selector-list-item no-results-found\"></li><li ng-show=\"searchResults &amp;&amp; !(searching || noResultsFound())\" class=\"navbar-search__list-item selector-list-header\"><h3 translate=\"navbar.search.discussions\" class=\"lmo-dropdown-heading\"></h3></li><li ng-show=\"!(searching || noResultsFound())\" ng-repeat=\"searchResult in searchResults | orderBy: [\'-rank\', \'-lastActivityAt\']\" class=\"navbar-search__list-item navbar-search__list-option selector-list-item media\"><a lmo-href-for=\"searchResult\" ng-mousedown=\"closeSearchDropdown($event)\" ng-blur=\"handleSearchBlur()\" class=\"search-result selector-list-item-link\" id=\"navbar-search__selector-link\"><search_result result=\"searchResult\"></search_result></a></li></ul></div></div>");
$templateCache.put("generated/components/navbar/search_result.html","<div class=\"blank\"><div class=\"pull-right\"> <smart_time time=\"result.lastActivityAt\"></smart_time> </div><div class=\"search-result-item\"><div class=\"search-result-title\">{{ result.title }}</div><div class=\"search-result-group-name\">{{ result.resultGroupName }}</div><div ng-if=\"result.blurb\" class=\"search-result-blurb\"><span ng-if=\"showBlurbLeader()\" class=\"search-result__leader\">…</span><span ng-bind-html=\"result.blurb\" class=\"search-result__blurb\"></span><span ng-if=\"showBlurbTrailer()\" class=\"search-result__trailer\">…</span></div></div></div>");
$templateCache.put("generated/components/notification/notification.html","<li ng-class=\"{\'lmo-active\': !notification.viewed}\" class=\"selector-list-item\"><a class=\"notification navbar-notifications__{{notification.kind}}\" href=\"{{notification.url}}\" ng-click=\"broadcastThreadEvent(notification)\"><div class=\"notification__avatar\"><user_avatar ng-if=\"actor()\" user=\"actor()\" size=\"small\"></user_avatar><div ng-if=\"!actor()\" class=\"thread-item__proposal-icon\"></div></div><div class=\"notification__content\"> <span ng-bind-html=\"notification.content()\"></span>  <timeago timestamp=\"notification.createdAt\"></timeago> <div ng-if=\"notification.translationValues.publish_outcome\" translate=\"notifications.publish_outcome\" class=\"notifications__publish-outcome\"></div></div></a></li>");
$templateCache.put("generated/components/notifications/comment_liked.html","<div class=\"media-left\"><user_avatar user=\"notification.actor()\" size=\"small\"></user_avatar></div><div class=\"media-body\"><span translate=\"notifications.comment_liked\" translate-value-name=\"{{notification.actor().name}}\" translate-value-discussion=\"{{notification.discussion().title}}\"></span> <timeago timestamp=\"notification.createdAt\"></timeago> </div>");
$templateCache.put("generated/components/notifications/comment_replied_to.html","<div class=\"media-left\"><user_avatar user=\"notification.actor()\" size=\"small\"></user_avatar></div><div class=\"media-body\"><span translate=\"notifications.comment_replied_to\" translate-value-name=\"{{notification.actor().name}}\" translate-value-discussion=\"{{notification.discussion().title}}\"></span> <timeago timestamp=\"notification.createdAt\"></timeago> </div>");
$templateCache.put("generated/components/notifications/invitation_accepted.html","<div class=\"media-left\"><user_avatar user=\"notification.actor()\" size=\"small\"></user_avatar></div><div class=\"media-body\"><span translate=\"notifications.invitation_accepted\" translate-value-name=\"{{notification.actor().name}}\" translate-value-group=\"{{notification.group().name}}\"></span> <timeago timestamp=\"notification.createdAt\"></timeago> </div>");
$templateCache.put("generated/components/notifications/membership_request_approved.html","<div class=\"media-left\"><user_avatar user=\"notification.actor()\" size=\"small\"></user_avatar></div><div class=\"media-body\"><span translate=\"notifications.membership_request_approved\" translate-value-name=\"{{notification.actor().name}}\" translate-value-group=\"{{notification.group().fullName}}\"></span> <timeago timestamp=\"notification.createdAt\"></timeago> </div>");
$templateCache.put("generated/components/notifications/membership_requested.html","<div ng-if=\"notification.model().byExistingUser()\" class=\"media\"><div class=\"media-left\"><user_avatar user=\"notification.actor()\" size=\"small\"></user_avatar></div><div class=\"media-body\"><span translate=\"notifications.membership_requested\" translate-value-name=\"{{notification.actor().name}}\" translate-value-group=\"{{notification.group().fullName}}\"></span></div></div><div ng-if=\"!notification.model().byExistingUser()\" class=\"media\"><div class=\"media-left\"><img ng-src=\"{{notification.group().logoUrl()}}\" aria-hidden=\"true\" class=\"lmo-box--small lmo-rounded-corners\"></div><div class=\"media-body\"><span translate=\"notifications.membership_requested\" translate-value-name=\"{{notification.model().name}}\" translate-value-group=\"{{notification.group().fullName}}\"></span> <timeago timestamp=\"notification.createdAt\"></timeago> </div></div>");
$templateCache.put("generated/components/notifications/motion_closed.html","<div class=\"media-left\"><user_avatar user=\"notification.actor()\" size=\"small\"></user_avatar></div><div class=\"media-body\"><span translate=\"notifications.motion_closed\" translate-value-proposal=\"{{notification.model().name}}\"></span> <timeago timestamp=\"notification.createdAt\"></timeago> <div translate=\"notifications.publish_outcome\" class=\"notifications__publish-outcome\"></div></div>");
$templateCache.put("generated/components/notifications/motion_closing_soon.html","<div class=\"media-left\"><div class=\"thread-item__proposal-icon\"></div></div><div class=\"media-body\"><span translate=\"notifications.motion_closing_soon\" translate-value-proposal=\"{{notification.model().name}}\"></span> <timeago timestamp=\"notification.createdAt\"></timeago> </div>");
$templateCache.put("generated/components/notifications/motion_outcome_created.html","<div class=\"media-left\"><user_avatar user=\"notification.actor()\" size=\"small\"></user_avatar></div><div class=\"media-body\"><span translate=\"notifications.motion_outcome_created\" translate-value-name=\"{{notification.actor().name}}\" translate-value-proposal=\"{{notification.model().name}}\"></span> <timeago timestamp=\"notification.createdAt\"></timeago> </div>");
$templateCache.put("generated/components/notifications/new_coordinator.html","<div class=\"media-left\"><user_avatar user=\"notification.actor()\" size=\"small\"></user_avatar></div><div class=\"media-body\"><span><strong><a lmo-href-for=\"notification.actor()\" class=\"notifications__link\">{{notification.actor().name}}</a></strong></span><span translate=\"notifications.new_coordinator\"></span><span><strong><a lmo-href-for=\"notification.group()\" class=\"notifications__link\">{{notification.group().name}}</a></strong></span> <timeago timestamp=\"notification.createdAt\"></timeago> </div>");
$templateCache.put("generated/components/notifications/notifications.html","<div class=\"blank\"><md-menu id=\"notifications\" md-position-mode=\"target target\" md-offset=\"0 48\"><md-button ng-click=\"toggle($mdMenu)\" tabindex=\"4\" class=\"notifications__button\"><div translate=\"navbar.notifications\" class=\"sr-only\"></div><i ng-if=\"hasUnread()\" class=\"material-icons\">notifications</i><i ng-if=\"!hasUnread()\" class=\"material-icons\">notifications_none</i><span ng-if=\"hasUnread()\" class=\"badge notifications__activity\">{{unreadCount()}}</span></md-button><md-menu-content class=\"notifications__menu-content notifications__dropdown\"><notification notification=\"notification\" ng-repeat=\"notification in notifications() | orderBy: \'-createdAt\' track by notification.id\"></notification><li ng-if=\"notifications().length == 0\" class=\"selector-list-item\"><span translate=\"notifications.no_notifications\" class=\"notification\"></span></li></md-menu-content></md-menu></div>");
$templateCache.put("generated/components/notifications/user_added_to_group.html","<span ng-if=\"!notification.model().inviter()\"><span translate=\"notifications.user_added_to_group_no_inviter\" translate-value-group=\"{{notification.group().fullName}}\"></span></span><div ng-if=\"notification.model().inviter()\" class=\"lmo-wrap\"><div class=\"media-left\"><user_avatar user=\"notification.model().inviter()\" size=\"small\"></user_avatar></div><div class=\"media-body\"><span translate=\"notifications.user_added_to_group\" translate-value-name=\"{{notification.model().inviter().name}}\" translate-value-group=\"{{notification.group().fullName}}\"></span> <timeago timestamp=\"notification.createdAt\"></timeago> </div></div>");
$templateCache.put("generated/components/notifications/user_mentioned.html","<div class=\"media-left\"><user_avatar user=\"notification.actor()\" size=\"small\"></user_avatar></div><div class=\"media-body\"><span translate=\"notifications.user_mentioned\" translate-value-name=\"{{notification.actor().name}}\" translate-value-title=\"{{notification.discussion().title}}\"></span> <timeago timestamp=\"notification.createdAt\"></timeago> </div>");
$templateCache.put("generated/components/only_coordinator_modal/only_coordinator_modal.html","<div class=\"only-coordinator-modal\"><div class=\"modal-header\"><modal_header_cancel_button aria-hidden=\"true\"></modal_header_cancel_button><h1 translate=\"deactivate_user_form.title\" class=\"lmo-h1\"></h1></div><div class=\"modal-body\"><p translate=\"only_coordinator_modal.explanation\"></p><ul class=\"only-coordinator-modal__list\"><li ng-repeat=\"group in groups()\" class=\"only-coordinator-modal__list-item\"><a ng-click=\"redirectToGroup(group)\" class=\"lmo-link\">{{group.fullName}}</a></li></ul><p translate=\"only_coordinator_modal.instructions\"></p></div><div class=\"modal-footer\"><button ng-click=\"$close()\" type=\"button\" translate=\"common.action.cancel\" class=\"only-coordinator-modal__list__button lmo-btn--cancel\"></button></div></div>");
$templateCache.put("generated/components/pending_email_form/pending_email_form.html","<md-list class=\"md-block pending-email-form\"><md-list-item ng-if=\"emails.length\" layout=\"column\" class=\"pending-email-form__emails\"><div ng-repeat=\"email in emails track by $index\" class=\"pending-email-form__email lmo-flex lmo-flex__space-between lmo-flex__center\"><div class=\"pending-email-form__email-row lmo-flex__grow\">{{email}}</div><md-button ng-click=\"remove(email)\" class=\"lmo-inline-action\"><i class=\"material-icons\">clear</i></md-button></div></md-list-item><md-list-item flex=\"true\" layout=\"row\" class=\"pending-email-form__add-option lmo-flex__align-top\"><md-input-container md-no-float=\"true\" class=\"lmo-flex__grow\"><input type=\"text\" placeholder=\"{{ \'pending_email_form.enter_email\' | translate }}\" ng-model=\"newEmail\" class=\"pending-email-form__add-option-input\"></md-input-container><md-button ng-click=\"addIfValid()\" ng-disabled=\"emails.length == 0\" aria-label=\"{{ \'pending_email_form.enter_email\' | translate }}\" class=\"pending-email-form__option-button pending-email-form__add-button\"><i class=\"material-icons\">add</i></md-button></md-list-item><div class=\"lmo-validation-error\">{{ emailValidationError }}</div><div class=\"lmo-flex lmo-flex__space-between\"><div></div><md-button ng-disabled=\"emails.length == 0\" ng-click=\"submit()\" aria-label=\"{{ \'pending_email_form.send_email\' | translate }}\" class=\"md-primary md-raised poll-common-share-form__button poll-common-share-form__option-button\"><span translate=\"pending_email_form.send_email\"></span></md-button></div></md-list>");
$templateCache.put("generated/components/pie_with_position/pie_with_position.html","<div class=\"pie-with-position\"><pie_chart votes=\"proposal.voteCounts\" diameter=\"50\" class=\"pie-with-position__pie-canvas\"></pie_chart><div class=\"pie-with-position__position-icon-container\"><div ng-if=\"lastVoteByCurrentUser(proposal)\" class=\"pie-with-position__position-icon thread-preview__position-icon--{{lastVoteByCurrentUser(proposal).position}}\"></div><div ng-if=\"!lastVoteByCurrentUser(proposal)\" class=\"pie-with-position__undecided-icon\"><i class=\"fa fa-question\"></i></div></div></div>");
$templateCache.put("generated/components/poll_page/poll_page.html","<div class=\"lmo-one-column-layout\"><loading ng-if=\"!pollPage.poll\"></loading><main ng-if=\"pollPage.poll\" class=\"poll-page lmo-row\"><group_theme ng-if=\"pollPage.poll.group()\" group=\"pollPage.poll.group()\" discussion=\"pollPage.poll.discussion()\" compact=\"true\"></group_theme><div layout=\"column\" class=\"poll-page__main-content lmo-flex\"><poll_common_example_card ng-if=\"pollPage.poll.example\" poll=\"pollPage.poll\"></poll_common_example_card><poll_common_card poll=\"pollPage.poll\" class=\"lmo-card--no-padding\"></poll_common_card></div></main></div>");
$templateCache.put("generated/components/polls_page/polls_page.html","<div class=\"loading-wrapper lmo-one-column-layout\"><main class=\"polls-page\"><div class=\"lmo-flex lmo-flex__space-between lmo-flex__baseline\"><h1 ng-if=\"pollsPage.group\" class=\"lmo-h1 dashboard-page__heading polls-page__heading\"><a lmo-href-for=\"pollsPage.group\"><span translate=\"polls_page.heading_with_group\" translate-value-name=\"{{pollsPage.group.fullName}}\"></span></a></h1><h1 ng-if=\"!pollsPage.group\" translate=\"polls_page.heading\" class=\"lmo-h1 dashboard-page__heading polls-page__heading\"></h1><div class=\"buh\"><md-button ng-click=\"pollsPage.startNewPoll()\" class=\"md-primary md-raised\"><span translate=\"polls_page.start_new_poll\"></span></md-button></div></div><div class=\"lmo-card\"><div class=\"polls-page__filters lmo-flex\"><md-input-container md-no-float=\"true\" class=\"polls-page__search md-block lmo-search-container md-whiteframe-z1\"><i class=\"material-icons\">search</i><input ng-model=\"pollsPage.fragment\" placeholder=\"{{\'polls_page.search_placeholder\' | translate}}\" ng-change=\"pollsPage.searchPolls()\" ng-model-options=\"{debounce: 250}\"></md-input-container><md-select ng-model=\"pollsPage.statusFilter\" placeholder=\"{{ \'polls_page.filter_placeholder\' | translate }}\" ng-change=\"pollsPage.fetchRecords()\" class=\"polls-page__status-filter\"><md-option ng-value=\"null\">{{ \'polls_page.filter_placeholder\' | translate }}</md-option><md-option ng-repeat=\"filter in pollsPage.statusFilters track by filter.value\" ng-value=\"filter.value\">{{filter.name}}</md-option></md-select><md-select ng-model=\"pollsPage.groupFilter\" placeholder=\"{{ \'polls_page.groups_placeholder\' | translate }}\" ng-change=\"pollsPage.fetchRecords()\" class=\"polls-page__group-filter\"><md-option ng-value=\"null\">{{ \'polls_page.groups_placeholder\' | translate }}</md-option><md-option ng-repeat=\"filter in pollsPage.groupFilters track by filter.value\" ng-value=\"filter.value\">{{filter.name}}</md-option></md-select></div><loading ng-if=\"pollsPage.fetchRecordsExecuting\"></loading><div ng-if=\"!pollsPage.fetchRecordsExecuting\" class=\"polls-page__polls\"><poll_common_preview ng-repeat=\"poll in pollsPage.pollCollection.polls() | orderBy:pollsPage.pollImportance track by poll.id\" poll=\"poll\" display-group-name=\"!pollsPage.group\"></poll_common_preview><loading ng-if=\"pollsPage.loadMoreExecuting\"></loading><div translate=\"polls_page.polls_count\" translate-value-count=\"{{pollsPage.loadedCount()}}\" translate-value-total=\"{{pollsPage.pollsCount}}\" class=\"polls-page__count\"></div><div ng-if=\"pollsPage.canLoadMore()\" class=\"polls-page__load-more\"><md-button translate=\"poll_common.load_more\" ng-click=\"pollsPage.loadMore()\" class=\"md-primary\"></md-button></div></div></div></main></div>");
$templateCache.put("generated/components/previous_proposals_page/previous_proposals_page.html","<div class=\"loading-wrapper lmo-one-column-layout\"><loading ng-if=\"!previousProposalsPage.group\"></loading><main ng-if=\"previousProposalsPage.group\" class=\"previous-proposals-page\"><div class=\"lmo-group-theme-padding\"></div><group_theme group=\"previousProposalsPage.group\"></group_theme><div class=\"previous-proposals-page__previous-proposals\"><div class=\"lmo-card-left-right-padding\"><h2 translate=\"previous_proposals_page.heading\" class=\"lmo-h2\"></h2></div><proposal_accordian ng-if=\"previousProposalsPage.group\" model=\"previousProposalsPage.group\"></proposal_accordian></div></main></div>");
$templateCache.put("generated/components/profile_page/profile_page.html","<div class=\"loading-wrapper lmo-one-column-layout\"><loading ng-if=\"!profilePage.user\"></loading><main ng-if=\"profilePage.user\" class=\"profile-page\"><div class=\"lmo-page-heading\"><h1 translate=\"profile_page.profile\" class=\"lmo-h1-medium\"></h1></div><div class=\"profile-page-card\"><div ng-show=\"profilePage.isDisabled\" class=\"lmo-disabled-form\"></div><h3 translate=\"profile_page.edit_profile\" class=\"lmo-h3\"></h3><label for=\"user-picture-field\" translate=\"profile_page.picture_label\"></label><user_avatar user=\"profilePage.user\" size=\"featured\"></user_avatar><div class=\"lmo-vertical-spacer\"><button ng-click=\"profilePage.changePicture()\" translate=\"profile_page.change_picture_link\" class=\"profile-page__change-picture lmo-link\"></button></div><div class=\"profile-page__profile-fieldset lmo-vertical-spacer\"><label for=\"user-name-field\" translate=\"profile_page.name_label\"></label><input ng-required=\"ng-required\" ng-model=\"profilePage.user.name\" class=\"profile-page__name-input form-control\" id=\"user-name-field\"><validation_errors subject=\"profilePage.user\" field=\"name\"></validation_errors><label for=\"user-username-field\" translate=\"profile_page.username_label\"></label><input ng-required=\"ng-required\" ng-model=\"profilePage.user.username\" class=\"profile-page__username-input form-control\" id=\"user-username-field\"><validation_errors subject=\"profilePage.user\" field=\"username\"></validation_errors><label for=\"user-email-field\" translate=\"profile_page.email_label\"></label><input ng-required=\"ng-required\" ng-model=\"profilePage.user.email\" class=\"profile-page__email-input form-control\" id=\"user-email-field\"><validation_errors subject=\"profilePage.user\" field=\"email\"></validation_errors><label for=\"user-email-field\" translate=\"profile_page.short_bio_label\"></label><textarea ng-model=\"profilePage.user.shortBio\" rows=\"3\" placeholder=\"{{\'profile_page.short_bio_placeholder\' | translate}}\" class=\"profile-page__short-bio-input form-control\" id=\"user-short-bio-field\"></textarea><validation_errors subject=\"profilePage.user\" field=\"short_bio\"></validation_errors><label for=\"user-locale-field\" translate=\"profile_page.locale_label\"></label><select ng-model=\"profilePage.user.selectedLocale\" ng-required=\"true\" ng-options=\"locale.key as locale.name for locale in profilePage.availableLocales()\" class=\"profile-page__language-input form-control\" id=\"user-locale-field\"></select><validation_errors subject=\"profilePage.user\" field=\"selectedLocale\"></validation_errors></div><div class=\"profile-page__update-account lmo-vertical-spacer\"><button ng-click=\"profilePage.submit()\" ng-disabled=\"isDisabled\" translate=\"profile_page.update_profile\" class=\"lmo-btn--submit profile-page__update-button\"></button></div><div class=\"lmo-vertical-spacer\"><button ng-click=\"profilePage.changePassword()\" translate=\"profile_page.change_password_link\" class=\"profile-page__change-password lmo-link\"></button></div></div><div class=\"profile-page-card\"><h3 translate=\"profile_page.deactivate_account\" class=\"lmo-h3\"></h3><button ng-click=\"profilePage.deactivateUser()\" translate=\"profile_page.deactivate_user_link\" class=\"profile-page__deactivate lmo-link lmo-vertical-spacer\"></button></div></main></div>");
$templateCache.put("generated/components/proposal_accordian/proposal_accordian.html","<div class=\"proposal-accordian\"><div ng-repeat=\"proposal in model.closedProposals() | orderBy:\'-closedOrClosingAt\' track by proposal.id\" id=\"proposal-{{proposal.id}}\" class=\"lmo-wrap proposal-accordian__proposal\"><div ng-if=\"selectedProposalId == proposal.id\" class=\"proposal-accordian__active\"><proposal_expanded proposal=\"proposal\" can_collapse=\"true\"></proposal_expanded></div><div ng-if=\"selectedProposalId != proposal.id\" ng-click=\"selectProposal(proposal)\" class=\"proposal-accordian__inactive\"><proposal_collapsed proposal=\"proposal\"></proposal_collapsed></div></div></div>");
$templateCache.put("generated/components/proposal_closing_time/proposal_closing_time.html"," <abbr class=\"closing-in timeago timeago--inline\"><span ng-if=\"proposal.isActive()\" translate=\"common.closing_in\" translate-value-time=\"{{proposal.closingAt | timeFromNowInWords}}\" ng-attr-title=\"{{proposal.closingAt | exactDateWithTime}}\"></span><span ng-if=\"!proposal.isActive()\" translate=\"common.closed_ago\" translate-value-time=\"{{proposal.closedOrClosingAt | timeFromNowInWords}}\" ng-attr-title=\"{{proposal.closedOrClosingAt | exactDateWithTime}}\"></span></abbr> ");
$templateCache.put("generated/components/proposal_form/closing_at_field.html","<div class=\"closing-at-field\"><div class=\"not-a-form-group\"><label for=\"proposal-close-date-field\" translate=\"proposal_form.closes\"></label> <timeago timestamp=\"proposal.closingAt\"></timeago> <div class=\"closing-at-group\"><input type=\"date\" min=\"{{dateToday}}\" ng-model=\"closingDate\" class=\"closing-at-field__date\" id=\"proposal-close-date-field\"><select ng-model=\"closingHour\" class=\"closing-at-field__time\"><option ng-repeat=\"hour in hours\" value=\"{{hour}}\" ng-selected=\"hour == closingHour\">{{ times[hour] }}</option></select><span class=\"closing-at-field__zone\">{{ timeZone }}</span></div></div></div>");
$templateCache.put("generated/components/proposal_form/proposal_form.html","<form ng-submit=\"submit()\" ng-disabled=\"proposal.processing\" class=\"proposal-form\"><div ng-show=\"isDisabled\" class=\"lmo-disabled-form\"></div><md-toolbar><div class=\"md-toolbar-tools lmo-flex__space-between\"><h1 ng-if=\"proposal.isNew()\" translate=\"proposal_form.start_a_proposal\" class=\"lmo-h1\"></h1><h1 ng-if=\"!proposal.isNew()\" translate=\"proposal_form.edit_proposal\" class=\"lmo-h1\"></h1><material_modal_header_cancel_button aria-hidden=\"true\"></material_modal_header_cancel_button></div></md-toolbar><div class=\"md-dialog-content md-body-1\"><p ng-if=\"proposal.isNew()\" translate=\"proposal_form.what_does_a_proposal_do\"></p><p ng-if=\"!proposal.isNew()\" translate=\"proposal_form.editing_title_and_description_forbidden\"></p><form_errors record=\"proposal\"></form_errors><fieldset ng-disabled=\"!proposal.canBeEdited()\"><div class=\"lmo-form-group\"><label for=\"proposal-title-field\" translate=\"proposal_form.title_label\"></label><div class=\"lmo-relative\"><input placeholder=\"{{ \'proposal_form.title_placeholder\' | translate }}\" ng-model=\"proposal.name\" ng-required=\"false\" maxlength=\"255\" class=\"proposal-form__title-field form-control lmo-primary-form-input\" id=\"proposal-title-field\"></div><validation_errors subject=\"proposal\" field=\"name\"></validation_errors><div ng-if=\"proposal.isNew()\" class=\"proposal-form__proposal_ideas\"><a href=\"{{nineWaysArticleLink()}}\" title=\"{{ \'proposal_form.proposal_ideas_title\' | translate }}\" target=\"_blank\" class=\"proposal-form__proposal_ideas_link\"><span translate=\"proposal_form.proposal_ideas\"></span></a></div></div><div class=\"lmo-form-group\"><label for=\"proposal-details-field\" translate=\"proposal_form.details_label\"></label><outlet name=\"before-proposal-form-textarea\" model=\"proposal\"></outlet><div class=\"lmo-relative lmo-textarea-wrapper\"><textarea msd-elastic=\"true\" ng-model=\"proposal.description\" placeholder=\"{{ \'proposal_form.details_placeholder\' | translate }}\" mentio=\"true\" mentio-trigger-char=\"\'@\'\" mentio-items=\"mentionables\" mentio-template-url=\"generated/components/thread_page/comment_form/mentio_menu.html\" mentio-search=\"fetchByNameFragment(term)\" ng-model-options=\"{debounce: 150}\" class=\"lmo-textarea proposal-form__details-field form-control lmo-primary-form-input\" id=\"proposal-details-field\"></textarea><emoji_picker target-selector=\"descriptionSelector\" class=\"lmo-action-dock\"></emoji_picker></div><outlet name=\"after-proposal-form-textarea\" model=\"proposal\"></outlet></div><div class=\"proposal-form__attachments\"><md_attachment_form model=\"proposal\" ng-if=\"proposal.discussion().groupId\" show-label=\"true\" class=\"proposal-form__attachment-form\"></md_attachment_form><validation_errors subject=\"proposal\" field=\"file\" class=\"proposal-form__attachment-errors\"></validation_errors><label class=\"lmo-form-labelled-input\"><attachment_preview attachment=\"attachment\" mode=\"thumb\" ng-repeat=\"attachment in proposal.newAttachments() | orderBy: \'id\' track by attachment.id\"></attachment_preview></label><div class=\"clearfix\"></div></div><closing_at_field proposal=\"proposal\"></closing_at_field><validation_errors subject=\"proposal\" field=\"closingAt\"></validation_errors></fieldset><div class=\"lmo-md-action\"><md-button ng-if=\"proposal.isNew()\" type=\"submit\" translate=\"proposal_form.start_proposal\" class=\"md-primary md-raised proposal-form__start-btn\"></md-button><md-button ng-if=\"!proposal.isNew()\" type=\"submit\" translate=\"common.action.save_changes\" class=\"md-primary md-raised proposal-form__save-changes-btn\"></md-button></div></div></form>");
$templateCache.put("generated/components/proposal_redirect/proposal_redirect.html","<div class=\"lmo-one-column-layout\">redirecting to thread</div>");
$templateCache.put("generated/components/registered_app_form/registered_app_form.html","<md-dialog class=\"poll-common-modal\"><div ng-show=\"isDisabled\" class=\"lmo-disabled-form\"></div><md-toolbar><div class=\"md-toolbar-tools lmo-flex__space-between\"><h1 translate=\"registered_app_form.new_application_title\" ng-show=\"application.isNew()\" class=\"lmo-h1\"></h1><h1 translate=\"registered_app_form.edit_application_title\" ng-hide=\"application.isNew()\" class=\"lmo-h1\"></h1><modal_header_cancel_button aria-hidden=\"true\"></modal_header_cancel_button></div></md-toolbar><md-dialog-content><form class=\"md-dialog-content registered-app-form\"><div layout=\"row\" class=\"lmo-flex\"><div class=\"lmo-form-group registered-app-form__avatar\"><img ng-src=\"{{application.logoUrl}}\"> <button ng-if=\"!application.isNew()\" type=\"button\" ng-click=\"clickFileUpload()\" class=\"lmo-btn--cancel registered-app-form__logo-upload-button\"> <i class=\"fa fa-lg fa-camera\"></i>  <span translate=\"common.action.upload\"></span> </button> <input type=\"file\" ng-model=\"file\" ng-file-select=\"upload(file)\" class=\"registered-app-form__logo-input hidden\"></div><div class=\"lmo-form-group registered-app-form__text-fields\"><label for=\"application-name\" translate=\"registered_app_form.name_label\"></label><input placeholder=\"{{ \'registered_app_form.name_placeholder\' | translate }}\" ng-model=\"application.name\" ng-required=\"true\" maxlength=\"255\" class=\"registered-app-form__name-input form-control lmo-primary-form-input\"><validation_errors subject=\"application\" field=\"name\"></validation_errors><label for=\"application-redirect-uri\" translate=\"registered_app_form.redirect_uri_label\"></label><textarea msd-elastic=\"true\" ng-model=\"application.redirectUri\" ng-required=\"true\" placeholder=\"{{ \'registered_app_form.redirect_uri_placeholder\' | translate }}\" class=\"lmo-textarea registered-app-form__redirect-uris-input form-control\"></textarea><abbr translate=\"registered_app_form.redirect_uri_note\" class=\"registered-app-form__helptext\"></abbr><validation_errors subject=\"application\" field=\"redirectUri\"></validation_errors></div></div><div class=\"lmo-flex lmo-flex__space-between registered-app-form__actions\"><button type=\"button\" ng-click=\"$close()\" translate=\"common.action.cancel\" class=\"lmo-btn--cancel registered-app-form__cancel\"></button><button ng-click=\"submit()\" translate=\"registered_app_form.new_application_submit\" ng-show=\"application.isNew()\" class=\"lmo-btn--submit registered-app-form__submit\"></button><button ng-click=\"submit()\" translate=\"registered_app_form.edit_application_submit\" ng-hide=\"application.isNew()\" class=\"lmo-btn--primary registered-app-form__update\"></button></div></form></md-dialog-content></md-dialog>");
$templateCache.put("generated/components/registered_app_page/registered_app_page.html","<div class=\"lmo-one-column-layout\"><loading ng-if=\"!registeredAppPage.application\"></loading><main ng-if=\"registeredAppPage.application\" class=\"registered-app-page\"><div layout=\"column\" class=\"registered-app-page__app lmo-flex\"><div layout=\"row\" class=\"lmo-flex\"><img ng-src=\"{{registeredAppPage.application.logoUrl}}\" class=\"registered-app-page__avatar\"><h1 class=\"lmo-h1 registered-app-page__title\">{{ registeredAppPage.application.name }}</h1></div><h3 translate=\"registered_app_page.uid\" class=\"lmo-h3 registered-app-page__header\"></h3><div layout=\"row\" class=\"registered-app-page__field lmo-flex\"><code class=\"registered-app-page__code\">{{ registeredAppPage.application.uid }}</code><button type=\"button\" title=\"{{ \'common.copy\' | translate }}\" clipboard=\"true\" text=\"registeredAppPage.application.uid\" on-copied=\"registeredAppPage.copied()\" class=\"lmo-btn--nude registered-app-page__btn--copy\"> <i class=\"fa fa-copy\"></i> </button></div><h3 translate=\"registered_app_page.secret\" class=\"lmo-h3 registered-app-page__header\"></h3><div layout=\"row\" class=\"registered-app-page__field lmo-flex\"><code class=\"registered-app-page__code\">{{ registeredAppPage.application.secret }}</code><button type=\"button\" title=\"{{ \'common.copy\' | translate }}\" clipboard=\"true\" text=\"registeredAppPage.application.secret\" on-copied=\"registeredAppPage.copied()\" class=\"lmo-btn--nude registered-app-page__btn--copy\"> <i class=\"fa fa-copy\"></i> </button></div><h3 translate=\"registered_apps_page.redirect_uris\" class=\"lmo-h3 registered-app-page__header\"></h3><div layout=\"row\" ng-repeat=\"uri in registeredAppPage.application.redirectUriArray()\" class=\"registered-app-page__field lmo-flex\"><code class=\"registered-app-page__code\">{{ uri }}</code><button type=\"button\" title=\"{{ \'common.copy\' | translate }}\" clipboard=\"true\" text=\"uri\" on-copied=\"registeredAppPage.copied()\" class=\"lmo-btn--nude registered-app-page__btn--copy\"> <i class=\"fa fa-copy\"></i> </button></div></div><div class=\"registered-app-page__actions\"><a lmo-href=\"/apps/registered\" class=\"lmo-btn--cancel pull-left\"><span translate=\"common.action.back\"></span></a> <button type=\"button\" translate=\"common.action.remove\" ng-click=\"registeredAppPage.openRemoveForm()\" class=\"lmo-btn--danger pull-right\"></button>  <button type=\"button\" translate=\"common.action.edit\" ng-click=\"registeredAppPage.openEditForm()\" class=\"lmo-btn--primary pull-right registered-app-page__edit\"></button> </div><div class=\"clearfix\"></div></main></div>");
$templateCache.put("generated/components/registered_apps_page/registered_apps_page.html","<div class=\"lmo-one-column-layout\"><loading ng-show=\"registeredAppsPage.loading\"></loading><main ng-if=\"!registeredAppsPage.loading\" class=\"registered-apps-page\"><div class=\"lmo-flex lmo-flex__space-between\"><h1 translate=\"registered_apps_page.title\" class=\"lmo-h1\"></h1><button ng-click=\"registeredAppsPage.openApplicationForm()\" class=\"lmo-btn lmo-btn--submit\"><span translate=\"registered_apps_page.create_new_application\"></span></button></div><div ng-if=\"registeredAppsPage.applications().length == 0\" translate=\"registered_apps_page.no_applications\" class=\"lmo-placeholder\"></div><table ng-if=\"registeredAppsPage.applications().length &gt; 0\" class=\"registered-apps-page__table\"><thead><td></td><td translate=\"registered_apps_page.name\"></td><td translate=\"registered_apps_page.redirect_uris\"></td><td translate=\"common.action.remove\"></td></thead><tbody><tr ng-repeat=\"application in registeredAppsPage.applications() | orderBy: \'name\' track by application.id\"><td> <img ng-src=\"{{application.logoUrl}}\" class=\"registered-apps-page__avatar\"> </td><td><a lmo-href-for=\"application\" class=\"registered-apps-page__name\">{{ application.name }}</a></td><td><code ng-repeat=\"uri in application.redirectUriArray()\" class=\"registered-apps-page__code\">{{uri}}</code></td><td><button ng-click=\"registeredAppsPage.openDestroyForm(application)\" class=\"registered-apps-page__remove-link\"><i class=\"fa fa-lg fa-times-circle\"></i></button></td></tr></tbody></table></main></div>");
$templateCache.put("generated/components/remove_app_form/remove_app_form.html","<form ng-submit=\"submit()\"><md-toolbar><div class=\"md-toolbar-tools lmo-flex__space-between\"><h1 translate=\"remove_app_form.title\" translate-value-name=\"{{application.name}}\" class=\"lmo-h1\"></h1><modal_header_cancel_button aria-hidden=\"true\"></modal_header_cancel_button></div></md-toolbar><md-dialog-content class=\"md-dialog-content\"><span translate=\"remove_app_form.question\" translate-value-name=\"{{application.name}}\"></span><div class=\"lmo-flex lmo-flex__space-between remove-app-form__actions\"><button ng-click=\"$close()\" type=\"button\" translate=\"common.action.cancel\" class=\"lmo-btn--cancel\"></button><button type=\"submit\" translate=\"remove_app_form.submit\" class=\"lmo-btn--danger\"></button></div></md-dialog-content></form>");
$templateCache.put("generated/components/remove_membership_form/remove_membership_form.html","<md-dialog class=\"remove-membership-form\"><form><md-toolbar><div class=\"md-toolbar-tools lmo-flex__space-between\"><h1 translate=\"remove_membership_form.title\" class=\"lmo-h1\"></h1><material_modal_header_cancel_button></material_modal_header_cancel_button></div></md-toolbar><div class=\"md-dialog-content\"><p translate=\"remove_membership_form.question\" translate-value-name=\"{{membership.userName()}}\" translate-value-group=\"{{membership.group().name}}\"></p><div class=\"lmo-md-actions\"><md-button ng-click=\"$close()\" translate=\"common.action.cancel\"></md-button><md-button ng-click=\"submit()\" translate=\"remove_membership_form.submit\" class=\"md-primary md-raised memberships-page__remove-membership-confirm\"></md-button></div></div></form></md-dialog>");
$templateCache.put("generated/components/revoke_app_form/revoke_app_form.html","<form ng-submit=\"submit()\"><div class=\"modal-header\"><modal_header_cancel_button aria-hidden=\"true\"></modal_header_cancel_button><h1 translate=\"revoke_app_form.title\" translate-value-name=\"{{application.name}}\" class=\"lmo-h1\"></h1></div><div class=\"modal-body\"><span translate=\"revoke_app_form.question\" translate-value-name=\"{{application.name}}\"></span></div><div class=\"modal-footer\"><button type=\"submit\" translate=\"revoke_app_form.submit\" class=\"lmo-btn--danger\"></button><button ng-click=\"$close()\" type=\"button\" translate=\"common.action.cancel\" class=\"lmo-btn--cancel\"></button></div></form>");
$templateCache.put("generated/components/sidebar/sidebar.html","<md-sidenav role=\"navigation\" md-component-id=\"left\" md-is-open=\"showSidebar\" md-is-locked-open=\"$mdMedia(\'gt-md\') &amp;&amp; showSidebar\" md-whiteframe=\"4\" aria-label=\"{{ \'sidebar.aria_labels.heading\' | translate }}\" class=\"md-sidenav-left lmo-no-print\"><div class=\"sidebar__user-panel lmo-no-print\"><a lmo-href=\"/profile\" class=\"sidebar__user-avatar-container\"><user_avatar user=\"currentUser()\" size=\"medium-circular\" ng-if=\"currentUser().avatarKind != \'initials\'\"></user_avatar><div ng-if=\"currentUser().avatarKind == \'initials\'\" class=\"sidebar__user-icon-container\"><i class=\"sidebar__user-icon fa fa-lg fa-user\"></i></div></a><div class=\"sidebar__user-details\"><div class=\"sidebar__user-name\">{{currentUser().name}}</div><div class=\"sidebar__user-username\">@{{currentUser().username}}</div></div></div><outlet name=\"after-user-username\" model=\"currentUser()\"></outlet><md_content layout=\"column\" ng-click=\"sidebarItemSelected()\" role=\"navigation\" class=\"sidebar__content lmo-no-print\"><md_list layout=\"column\" aria-label=\"{{ \'sidebar.aria_labels.threads_list\' | translate }}\" class=\"sidebar__list sidebar__threads\"><md_list_item><md_button lmo-href=\"/polls\" ng-click=\"isActive()\" aria-label=\"{{ \'sidebar.my_decisions\' | translate }}\" ng-class=\"{\'sidebar__list-item--selected\': onPage(\'pollsPage\')}\" class=\"sidebar__list-item-button sidebar__list-item-button--recent\"><md_avatar_icon class=\"sidebar__list-item-icon material-icons\">thumbs_up_down</md_avatar_icon><span translate=\"common.decisions\"></span></md_button></md_list_item><md_list_item><md_button lmo-href=\"/dashboard\" ng-click=\"isActive()\" aria-label=\"{{ \'sidebar.recent\' | translate }}\" ng-class=\"{\'sidebar__list-item--selected\': onPage(\'dashboardPage\')}\" class=\"sidebar__list-item-button sidebar__list-item-button--recent\"><md_avatar_icon class=\"sidebar__list-item-icon fa fa-lg fa-clock-o\"></md_avatar_icon><span translate=\"sidebar.recent_threads\"></span></md_button></md_list_item><md_list_item><md_button lmo-href=\"/inbox\" ng-click=\"isActive()\" aria-label=\"{{ \'sidebar.unread\' | translate }}\" ng-class=\"{\'sidebar__list-item--selected\': onPage(\'inboxPage\')}\" class=\"sidebar__list-item-button sidebar__list-item-button--unread\"><md_avatar_icon class=\"sidebar__list-item-icon i fa fa-lg fa-inbox\"></md_avatar_icon><span translate=\"sidebar.unread_threads\" translate-value-count=\"{{unreadThreadCount()}}\"></span></md_button></md_list_item><md_list_item><md_button lmo-href=\"/dashboard/show_muted\" ng-click=\"isActive()\" aria-label=\"{{ \'sidebar.muted\' | translate }}\" ng-class=\"{\'sidebar__list-item--selected\': onPage(\'dashboardPage\', nil, \'show_muted\')}\" class=\"sidebar__list-item-button sidebar__list-item-button--muted\"><md_avatar_icon class=\"sidebar__list-item-icon i fa fa-lg fa-times-circle\"></md_avatar_icon><span translate=\"sidebar.muted_threads\"></span></md_button></md_list_item><md_list_item ng-show=\"hasAnyGroups()\"><md_button ng-click=\"startThread()\" aria-label=\"{{ \'sidebar.start_thread\' | translate }}\" class=\"sidebar__list-item-button sidebar__list-item-button--start-thread\"><md_avatar_icon class=\"sidebar__list-item-icon i fa fa-lg fa-plus\"></md_avatar_icon><span translate=\"sidebar.start_thread\"></span></md_button></md_list_item></md_list><div class=\"sidebar__divider\"></div><md_list_item translate=\"common.groups\" class=\"sidebar__list-subhead\"></md_list_item><md_list ng-class=\"{\'sidebar__no-groups\': groups().length &lt; 1}\" aria-label=\"{{ \'sidebar.aria_labels.groups_list\' | translate }}\" class=\"sidebar__list sidebar__groups\"><md_list_item ng_repeat=\"group in groups() | orderBy: \'fullName\' track by group.id\"><md_button lmo-href=\"{{groupUrl(group)}}\" aria-label=\"{{group.name}}\" ng-if=\"group.isParent()\" ng-class=\"{\'sidebar__list-item--selected\': onPage(\'groupPage\', group.key)}\" class=\"sidebar__list-item-button sidebar__list-item-button--group\"><img ng_src=\"{{group.logoUrl()}}\" class=\"md-avatar lmo-box--tiny sidebar__list-item-group-logo\"><span>{{group.name}}</span></md_button><md_button lmo-href=\"{{groupUrl(group)}}\" ng-if=\"!group.isParent()\" ng-class=\"{\'sidebar__list-item--selected\': onPage(\'groupPage\', group.key)}\" class=\"sidebar__list-item-button--subgroup\">{{group.name}}</md_button><div class=\"sidebar__list-item-padding\"></div></md_list_item><md_list_item><md_button ng-click=\"startGroup()\" aria-label=\"{{ \'sidebar.start_group\' | translate }}\" class=\"sidebar__list-item-button sidebar__list-item-button--start-group\"><md_avatar_icon class=\"sidebar__list-item-icon i fa fa-lg fa-plus\"></md_avatar_icon><span translate=\"sidebar.start_group\"></span></md_button></md_list_item><md_list_item><md_button lmo-href=\"/explore\" aria-label=\"{{ \'sidebar.explore\' | translate }}\" ng-class=\"{\'sidebar__list-item--selected\': onPage(\'explorePage\')}\" class=\"sidebar__list-item-button sidebar__list-item-button--explore\"><md_avatar_icon class=\"sidebar__list-item-icon i fa fa-lg fa-globe\"></md_avatar_icon><span translate=\"sidebar.explore\"></span></md_button></md_list_item></md_list><div class=\"sidebar__divider\"></div><md_list_item translate=\"common.settings\" class=\"sidebar__list-subhead\"></md_list_item><md_list aria-label=\"{{ \'sidebar.aria_labels.user_list\' | translate }}\" class=\"sidebar__list sidebar__users\"><md_list_item><md_button lmo-href=\"/profile\" aria-label=\"{{ \'sidebar.edit_profile\' | translate }}\" ng-class=\"{\'sidebar__list-item--selected\': onPage(\'profilePage\')}\" class=\"sidebar__list-item-button sidebar__list-item-button--profile\"><md_avatar_icon class=\"sidebar__list-item-icon fa fa-lg fa-cog\"></md_avatar_icon><span translate=\"sidebar.edit_profile\"></span></md_button></md_list_item><md_list_item><md_button lmo-href=\"/email_preferences\" aria-label=\"{{ \'sidebar.email_settings\' | translate }}\" ng-class=\"{\'sidebar__list-item--selected\': onPage(\'emailSettingsPage\')}\" class=\"sidebar__list-item-button sidebar__list-item-button--email-settings\"><md_avatar_icon class=\"sidebar__list-item-icon fa fa-lg fa-envelope-o\"></md_avatar_icon><span translate=\"sidebar.email_settings\"></span></md_button></md_list_item><md_list_item><md_button href=\"{{helpLink()}}\" target=\"_blank\" aria-label=\"{{ \'sidebar.help\' | translate }}\" class=\"sidebar__list-item-button\"><md_avatar_icon class=\"sidebar__list-item-icon fa fa-lg fa-info-circle\"></md_avatar_icon><span translate=\"sidebar.help\"></span></md_button></md_list_item><md_list_item ng-if=\"showContactUs()\"><md_button ng-click=\"contactUs()\" aria-label=\"{{ \'sidebar.contact\' | translate }}\" class=\"sidebar__list-item-button\"><md_avatar_icon class=\"sidebar__list-item-icon fa fa-lg fa-question-circle\"></md_avatar_icon><span translate=\"sidebar.contact\"></span></md_button></md_list_item><md_list_item><md_button ng-click=\"signOut()\" aria-label=\"{{ \'sidebar.sign_out\' | translate }}\" class=\"sidebar__list-item-button\"><md_avatar_icon class=\"sidebar__list-item-icon fa fa-lg fa-sign-out\"></md_avatar_icon><span translate=\"sidebar.sign_out\"></span></md_button></md_list_item></md_list></md_content></md-sidenav>");
$templateCache.put("generated/components/signed_out_modal/signed_out_modal.html","<form ng-submit=\"submit()\"><div class=\"modal-header\"><h1 translate=\"signed_out_modal.title\" class=\"lmo-h1\"></h1></div><div class=\"modal-body\"><span translate=\"signed_out_modal.message\"></span></div><div class=\"modal-footer\"><button type=\"submit\" translate=\"signed_out_modal.ok\" class=\"lmo-btn--submit\"></button></div></form>");
$templateCache.put("generated/components/slack_added_modal/slack_added_modal.html","<md-dialog class=\"leave-group-form\"><md-toolbar><div class=\"md-toolbar-tools lmo-flex__space-between\"><h1 translate=\"slack_added_modal.title\" class=\"lmo-h1\"></h1><material_modal_header_cancel_button></material_modal_header_cancel_button></div></md-toolbar><div class=\"md-dialog-content\"><slack_tableau group=\"group\"></slack_tableau><p translate=\"slack_added_modal.helptext\" translate-value-name=\"{{group.name}}\"></p><div class=\"lmo-md-actions\"><md-button ng-click=\"$close()\" type=\"button\" translate=\"common.ok_got_it\"></md-button><md-button translate=\"slack_added_modal.start_poll_now\" ng-click=\"submit()\" class=\"md-primary md-raised\"></md-button></div></div></md-dialog>");
$templateCache.put("generated/components/smart_time/smart_time.html"," <abbr class=\"smart-time\"><span data-toggle=\"tooltip\" ng-attr-title=\"{{time | exactDateWithTime}}\">{{value}}</span></abbr> ");
$templateCache.put("generated/components/star_toggle/star_toggle.html","<div class=\"star-toggle\"><div ng-if=\"isLoggedIn()\" class=\"blank\"><input type=\"checkbox\" id=\"star-thread-{{thread.id}}\" ng-change=\"thread.saveStar()\" ng-model=\"thread.starred\" class=\"sr-only\"><label for=\"star-thread-{{thread.id}}\" class=\"fa\"><span translate=\"star_toggle.star\" class=\"sr-only\"></span></label></div></div>");
$templateCache.put("generated/components/start_group_page/start_group_page.html","<div class=\"lmo-one-column-layout\"><main class=\"start-group-page lmo-row\"><loading ng-if=\"!startGroupPage.group\"></loading><div ng-if=\"startGroupPage.group\" layout=\"column\" class=\"start-group-page__main-content lmo-flex lmo-card\"><h1 translate=\"group_form.start_group_heading\" class=\"lmo-card-heading\"></h1><group_form group=\"startGroupPage.group\"></group_form></div></main></div>");
$templateCache.put("generated/components/start_poll_page/start_poll_page.html","<div class=\"lmo-one-column-layout\"><main class=\"start-poll-page lmo-row\"><div layout=\"column\" class=\"start-poll-page__main-content lmo-flex lmo-card lmo-relative\"><div ng-if=\"isDisabled\" class=\"lmo-disabled-form\"></div><div ng-if=\"!startPollPage.poll.pollType\" class=\"poll-common-start-poll__header lmo-flex\"><h2 translate=\"poll_common.start_poll\" class=\"lmo-card-heading\"></h2></div><div ng-if=\"startPollPage.poll.pollType\" class=\"poll-common-start-poll__header lmo-flex\"><i class=\"material-icons\">{{ startPollPage.icon() }}</i><h2 translate=\"poll_types.{{startPollPage.poll.pollType}}\" class=\"lmo-card-heading poll-common-card-header__poll-type\"></h2></div><poll_common_start_poll poll=\"startPollPage.poll\" class=\"lmo-flex__grow\"></poll_common_start_poll></div></main></div>");
$templateCache.put("generated/components/thread_lintel/thread_lintel.html","<div ng-if=\"show()\" class=\"thread-lintel__wrapper lmo-no-print\"><div class=\"thread-lintel__content\"><div ng-class=\"{\'lmo-width-75\': !proposalButtonInView}\" class=\"thread-lintel__left\"><div ng-click=\"scrollToThread()\" class=\"lmo-truncate thread-lintel__title\">{{ discussion.title }}</div></div><div class=\"thread-lintel__progress-wrap thread-lintel__progress\"><div style=\"width: {{positionPercent}}%\" class=\"thread-lintel__progress-bar thread-lintel__progress\"></div></div></div></div>");
$templateCache.put("generated/components/thread_page/thread_page.html","<div class=\"loading-wrapper lmo-two-column-layout\"><loading ng-if=\"!threadPage.discussion\"></loading><main ng-if=\"threadPage.discussion\" class=\"thread-page lmo-row\"><group_theme group=\"threadPage.discussion.group()\" compact=\"true\"></group_theme><div class=\"thread-page__main-content\"><outlet name=\"before-thread-page-column-right\" ng-if=\"threadPage.eventsLoaded\" model=\"threadPage.discussion\" class=\"before-column-right lmo-column-right\"></outlet><poll_common_card ng-if=\"threadPage.usePolls\" poll=\"poll\" ng-repeat=\"poll in threadPage.discussion.activePolls() track by poll.id\" class=\"lmo-card--no-padding lmo-column-right\"></poll_common_card><decision_tools_card ng-if=\"threadPage.canStartPoll()\" discussion=\"threadPage.discussion\" class=\"lmo-column-right\"></decision_tools_card><poll_common_index_card model=\"threadPage.discussion\" class=\"lmo-column-right\"></poll_common_index_card><start_proposal_card in-view=\"threadPage.proposalButtonInView($inview)\" in-view-options=\"{debounce: 200}\" ng-if=\"!threadPage.usePolls &amp;&amp; threadPage.canStartProposal()\" discussion=\"threadPage.discussion\" class=\"lmo-no-print lmo-column-right\"></start_proposal_card><current_proposal_card in-view=\"threadPage.proposalInView($inview)\" in-view-options=\"{debounce: 200}\" ng-if=\"!threadPage.usePolls &amp;&amp; threadPage.discussion.hasActiveProposal()\" proposal=\"threadPage.discussion.activeProposal()\" loading=\"!threadPage.eventsLoaded\" class=\"lmo-column-right\"></current_proposal_card><previous_proposals_card discussion=\"threadPage.discussion\" ng-if=\"!threadPage.usePolls &amp;&amp; threadPage.eventsLoaded\" class=\"lmo-column-right\"></previous_proposals_card><members_card ng-if=\"threadPage.canViewMemberships()\" group=\"threadPage.discussion.group()\" class=\"lmo-column-right\"></members_card><outlet name=\"thread-page-column-right\" class=\"after-column-right lmo-column-right\"></outlet><context_panel discussion=\"threadPage.discussion\" class=\"lmo-column-left\"></context_panel><activity_card discussion=\"threadPage.discussion\" active-comment-id=\"threadPage.requestedCommentId\" loading=\"!threadPage.eventsLoaded\" class=\"lmo-column-left\"></activity_card><comment_form discussion=\"threadPage.discussion\" class=\"lmo-no-print lmo-column-left\"></comment_form></div><thread_lintel></thread_lintel><div class=\"clearfix\"></div></main></div>");
$templateCache.put("generated/components/thread_preview/thread_preview.html","<div class=\"thread-preview\"><a lmo-href-for=\"thread\" class=\"thread-preview__link\"><div ng-if=\"usePolls()\" class=\"thread-preview__icon\"><user_avatar ng-if=\"!thread.activePoll()\" user=\"thread.author()\" size=\"medium\"></user_avatar><poll_common_chart_preview ng-if=\"thread.activePoll()\" poll=\"thread.activePoll()\"></poll_common_chart_preview></div><div ng-if=\"!usePolls()\" class=\"thread-preview__icon\"><user_avatar ng-if=\"!thread.activeProposal()\" user=\"thread.author()\" size=\"medium\"></user_avatar><div ng-if=\"thread.activeProposal()\" class=\"thread-preview__pie-container\"><pie_chart votes=\"thread.activeProposal().voteCounts\" diameter=\"50\" ng-if=\"lastVoteByCurrentUser(thread)\" title=\"{{ \'dashboard_page.thread_preview.you_voted\' | translate:translationData(thread) }}\" class=\"thread-preview__pie-canvas\"></pie_chart><pie_chart votes=\"thread.activeProposal().voteCounts\" diameter=\"50\" ng-if=\"!lastVoteByCurrentUser(thread)\" title=\"{{ \'dashboard_page.thread_preview.undecided\' | translate }}\" class=\"thread-preview__pie-canvas\"></pie_chart><div class=\"thread-preview__position-icon-container\"><div ng-if=\"lastVoteByCurrentUser(thread)\" class=\"thread-preview__position-icon thread-preview__position-icon--{{lastVoteByCurrentUser(thread).position}}\"></div><div ng-if=\"!lastVoteByCurrentUser(thread)\" class=\"thread-preview__undecided-icon\"><i class=\"fa fa-question\"></i></div></div></div></div><div class=\"sr-only\"><span>{{thread.authorName()}}: {{thread.title}}.</span><span ng-if=\"thread.hasUnreadActivity()\" translate=\"dashboard_page.aria_thread.unread\" translate-value-count=\"{{ thread.unreadActivityCount() }}\"></span><span ng-if=\"thread.activeProposal()\" translate=\"dashboard_page.aria_thread.current_proposal\" translate-value-name=\"{{ thread.activeProposal().name }}\"></span><span ng-if=\"thread.activeProposal() &amp;&amp; lastVoteByCurrentUser(thread)\" translate=\"dashboard_page.aria_thread.you_voted\" translate-value-position=\" {{lastVoteByCurrentUser(thread).position}} \"></span></div><div aria-hidden=\"true\" class=\"thread-preview__screen-only screen-only\"><div class=\"thread-preview__text-container\"><div ng-class=\"{\'thread-preview--unread\': thread.isUnread() }\" class=\"thread-preview__title\">{{thread.title}}</div><div ng-if=\"thread.hasUnreadActivity()\" class=\"thread-preview__unread-count\">({{thread.unreadActivityCount()}})</div></div><div ng-if=\"thread.activeProposal()\" class=\"thread-preview__text-container\"> <div class=\"thread-preview__proposal-name\">{{ thread.activeProposal().name }}</div> <div ng-if=\"thread.activeProposal().closingSoon()\" class=\"thread-preview__proposal-closing-container\"> <proposal_closing_time proposal=\"thread.activeProposal()\" class=\"thread-preview__proposal-closing-at\"></proposal_closing_time> </div></div><div class=\"thread-preview__text-container\"><div class=\"thread-preview__group-name\">{{ thread.group().fullName }} · <smart_time time=\"thread.lastActivityAt\"></smart_time> </div></div><outlet name=\"after-thread-preview\" model=\"thread\"></outlet></div></a><div class=\"thread-preview__star\"><star_toggle thread=\"thread\" aria-hidden=\"true\"></star_toggle></div><div ng-if=\"thread.discussionReaderId\" class=\"thread-preview__actions hidden-xs\"> <button ng-click=\"dismiss()\" ng-disabled=\"!thread.isUnread()\" ng-class=\"{disabled: !thread.isUnread()}\" title=\"{{\'dashboard_page.dismiss\' | translate }}\" class=\"thread-preview__dismiss\"> <i class=\"fa fa-check\"></i> </button>  <button ng-click=\"muteThread()\" ng-show=\"!thread.isMuted()\" title=\"{{ \'volume_levels.mute\' | translate }}\" aria-label=\"{{ \'volume_levels.mute\' | translate }}\" class=\"thread-preview__mute\"><i class=\"fa fa-volume-off\"></i> <i class=\"fa fa-times\"></i> </button>  <button ng-click=\"unmuteThread()\" ng-show=\"thread.isMuted()\" title=\"{{ \'volume_levels.unmute\' | translate }}\" aria-label=\"{{ \'volume_levels.unmute\' | translate }}\" class=\"thread-preview__unmute\"> <i class=\"fa fa-volume-down\"></i> </button> </div></div>");
$templateCache.put("generated/components/thread_preview_collection/thread_preview_collection.html","<div class=\"thread-previews\"><outlet name=\"before-thread-previews\" model=\"query\"></outlet><div ng-repeat=\"thread in query.threads() | limitTo: limit | orderBy:[importance, \'-lastActivityAt\'] track by thread.key\" class=\"blank\"><thread_preview thread=\"thread\"></thread_preview></div></div>");
$templateCache.put("generated/components/time_zone_select/time_zone_select.html","<div class=\"time-zone-select\"><div ng-show=\"!isOpen\" class=\"time-zone-select__selected\"><md-button ng-click=\"open()\" aria-label=\"Change time zone\" class=\"time-zone-select__change-button\">{{currentZone()}}</md-button></div><div ng-show=\"isOpen\" class=\"time-zone-select__change\"><md-autocomplete md-search-text=\"q\" md-selected-item=\"name\" md-selected-item-change=\"change()\" md-items=\"name in names()\" placeholder=\"Enter time zone\"><md-item-template><span>{{name}}</span></md-item-template></md-autocomplete></div></div>");
$templateCache.put("generated/components/timeago/timeago.html"," <abbr class=\"timeago\"><span am-time-ago=\"timestamp\" data-toggle=\"tooltip\" ng-attr-title=\"{{timestamp | exactDateWithTime}}\"></span></abbr> ");
$templateCache.put("generated/components/translate_button/translate_button.html","<div class=\"translate-button\"> <span ng-if=\"showdot &amp;&amp; (canTranslate() || translateExecuting || translated)\" aria-hidden=\"true\">·</span>  <button ng-if=\"canTranslate()\" ng-click=\"translate()\" translate=\"common.action.translate\" class=\"thread-item__translate\"></button> <loading ng-show=\"translateExecuting\" class=\"translate-button__loading\"></loading> <span ng-if=\"translated\" translate=\"common.action.translated\" class=\"thread-item__translation\"></span> </div>");
$templateCache.put("generated/components/translation/translation.html","<div class=\"translation\"><div marked=\"translated\" class=\"translation__body\"></div></div>");
$templateCache.put("generated/components/user_avatar/user_avatar.html","<div class=\"user-avatar lmo-box--{{size}}\" aria-hidden=\"true\" ng-class=\"{\'user-avatar--coordinator\': coordinator}\" title=\"{{user.name}}\"><a lmo-href-for=\"user\" class=\"user-avatar__profile-link\"><div class=\"lmo-box--{{size}} user-avatar__initials user-avatar__initials--{{size}}\" aria-hidden=\"true\" ng-if=\"user.avatarKind == \'initials\'\">{{user.avatarInitials}}</div><img class=\"lmo-box--{{size}}\" ng-if=\"user.avatarKind == \'gravatar\'\" gravatar-src-once=\"user.gravatarMd5\" gravatar-size=\"{{gravatarSize()}}\" alt=\"{{::user.name}}\"><img class=\"lmo-box--{{size}}\" ng-if=\"user.avatarKind == \'uploaded\'\" alt=\"{{user.name}}\" ng-src=\"{{::user.avatarUrl}}\"></a></div>");
$templateCache.put("generated/components/user_page/user_page.html","<div class=\"loading-wrapper container main-container lmo-one-column-layout\"><loading ng-if=\"!userPage.user\"></loading><main ng-if=\"userPage.user\" class=\"user-page main-container lmo-row\"><div class=\"user-page__top-margin\"><div class=\"user-page__profile\"><div class=\"user-page__left\"><user_avatar user=\"userPage.user\" size=\"featured\"></user_avatar></div><div class=\"user-page__right\"><h1 class=\"user-page__name\">{{userPage.user.name}}</h1><h4 class=\"user-page__username\">@{{userPage.user.username}}</h4><p>{{userPage.user.shortBio}}</p><h2 translate=\"common.groups\" class=\"lmo-h2 user-page__groups-title\"></h2><div ng-repeat=\"group in userPage.user.groups() | orderBy: \'fullName\' track by group.id\" class=\"user-page__groups\"><a lmo-href-for=\"group\">{{group.fullName}}</a></div><loading ng-if=\"userPage.loadGroupsForExecuting\"></loading></div><div class=\"clearfix\"></div></div></div></main></div>");
$templateCache.put("generated/components/validation_errors/validation_errors.html","<div class=\"lmo-validation-error\"><label for=\"{{field}}-error\" ng-repeat=\"error in subject.errors[field] track by $index\" class=\"md-container-ignore md-no-float lmo-validation-error__message\"><span>{{error}}</span></label></div>");
$templateCache.put("generated/components/vote_icon/vote_icon.html","<div class=\"thread-item__vote-icon thread-item__vote-icon--{{position}}\"><img ng-src=\"{{positionImg()}}\" class=\"lmo-print-only\"></div>");
$templateCache.put("generated/components/auth/avatar/auth_avatar.html","<div class=\"auth-avatar\"><user_avatar user=\"avatarUser\" size=\"large-circular\" class=\"auth-avatar__avatar\"></user_avatar></div>");
$templateCache.put("generated/components/auth/complete/auth_complete.html","<div class=\"auth-complete\"><auth_avatar user=\"user\"></auth_avatar><h2 translate=\"auth_form.check_your_email\" class=\"lmo-h2\"></h2><p ng-if=\"user.sentLoginLink\" translate=\"auth_form.login_link_sent\" translate-value-email=\"{{user.email}}\" class=\"poll-common-helptext\"></p><p ng-if=\"user.sentPasswordLink\" translate=\"auth_form.password_link_sent\" translate-value-email=\"{{user.email}}\" class=\"poll-common-helptext\"></p></div>");
$templateCache.put("generated/components/auth/email_form/auth_email_form.html","<div class=\"auth-email-form\"><md-input-container class=\"md-block auth-email-form__email\"><label translate=\"auth_form.email\"></label><input type=\"email\" md-autofocus=\"true\" placeholder=\"{{ \'auth_form.email_placeholder\' | translate}}\" ng-model=\"email\" class=\"lmo-primary-form-input\"><validation_errors subject=\"user\" field=\"email\"></validation_errors></md-input-container><div class=\"lmo-actions\"><div></div><md-button ng-click=\"submit()\" ng-disabled=\"!email\" translate=\"auth_form.sign_in\" aria-label=\"{{ \'auth_form.sign_in\' | translate }}\" class=\"md-primary md-raised auth-email-form__submit\"></md-button></div></div>");
$templateCache.put("generated/components/auth/form/auth_form.html","<div class=\"auth-form lmo-slide-animation\"><div ng-show=\"isDisabled\" class=\"lmo-disabled-form\"></div><div ng-if=\"!loginComplete()\" class=\"auth-form__logging-in animated\"><div ng-if=\"!user.emailStatus\" class=\"auth-form__email-not-set animated\"><auth_provider_form user=\"user\"></auth_provider_form><auth_email_form user=\"user\"></auth_email_form></div><div ng-if=\"user.emailStatus\" class=\"auth-form__email-set animated\"><auth_identity_form ng-if=\"pendingProviderIdentity\" user=\"user\" identity=\"pendingProviderIdentity\" class=\"animated\"></auth_identity_form><div ng-if=\"!pendingProviderIdentity\" class=\"auth-form__no-pending-identity animated\"><auth_signin_form ng-if=\"user.emailStatus == \'active\'\" user=\"user\" class=\"animated\"></auth_signin_form><auth_signup_form ng-if=\"user.emailStatus == \'unused\'\" user=\"user\" class=\"animated\"></auth_signup_form><auth_inactive_form ng-if=\"user.emailStatus == \'inactive\'\" user=\"user\" class=\"animated\"></auth_inactive_form></div></div></div><auth_complete ng-if=\"loginComplete()\" user=\"user\" class=\"animated\"></auth_complete></div>");
$templateCache.put("generated/components/auth/identity_form/auth_identity_form.html","<div class=\"auth-identity-form\"><h2 translate=\"auth_form.hello\" translate-value-name=\"{{user.name || user.email}}\" class=\"lmo-h2\"></h2><auth_avatar user=\"user\"></auth_avatar><div class=\"auth-identity-form__options\"><div class=\"auth-identity-form__new-account\"><p translate=\"auth_form.new_to_loomio\" class=\"poll-common-helptext\"></p><md-button ng-click=\"createAccount()\" translate=\"auth_form.create_account\" class=\"md-primary md-raised\"></md-button></div><div class=\"auth-identity-form__existing-account\"><p translate=\"auth_form.already_a_user\" class=\"poll-common-helptext\"></p><md-input-container class=\"md-block auth-email-form__email\"><label translate=\"auth_form.email\"></label><input type=\"text\" md-autofocus=\"true\" placeholder=\"{{ \'auth_form.email_placeholder\' | translate}}\" ng-model=\"email\" class=\"lmo-primary-form-input\"><validation_errors subject=\"user\" field=\"email\"></validation_errors></md-input-container><md-button ng-click=\"submit()\" translate=\"auth_form.link_accounts\"></md-button></div></div></div>");
$templateCache.put("generated/components/auth/modal/auth_modal.html","<md-dialog class=\"auth-modal\"><div ng-show=\"isDisabled\" class=\"lmo-disabled-form\"></div><md-toolbar><div class=\"md-toolbar-tools lmo-flex__space-between\"><i ng-if=\"!showBackButton()\" class=\"material-icons\">lock_open</i><a ng-click=\"back()\" ng-if=\"showBackButton()\" class=\"auth-modal__back\"><i class=\"material-icons\">arrow_back</i></a><h1 translate=\"auth_form.sign_in_to_loomio\" class=\"lmo-h1\"></h1><material_modal_header_cancel_button ng-if=\"!preventClose\"></material_modal_header_cancel_button><div ng-if=\"preventClose\"></div></div></md-toolbar><div class=\"md-dialog-content\"><auth_form user=\"user\"></auth_form></div></md-dialog>");
$templateCache.put("generated/components/auth/provider_form/auth_provider_form.html","<div class=\"auth-provider-form\"><div translate=\"auth_form.choose_an_account\" class=\"poll-common-helptext\"></div><div layout=\"row\" class=\"auth-provider-form__providers lmo-flex\"><div ng-repeat=\"provider in providers\" class=\"auth-provider-form__provider\"><button type=\"button\" class=\"md-button md-raised {{provider.name}}\" ng-click=\"select(provider)\"> <i class=\"fa fa-lg fa-{{provider.icon || provider.name}}\"></i>  <span>{{provider.name}}</span> </button></div></div></div>");
$templateCache.put("generated/components/auth/signin_form/auth_signin_form.html","<div class=\"auth-signin-form\"><auth_avatar user=\"user\"></auth_avatar><md-input-container class=\"md-block auth-signin-form__magic-link\"><h2 translate=\"auth_form.welcome_back\" translate-value-name=\"{{user.firstName() || user.email}}\" class=\"lmo-h2\"></h2><p class=\"poll-common-helptext\"> <span translate=\"auth_form.login_link_helptext\" ng-if=\"!user.hasPassword\"></span>  <span translate=\"auth_form.login_link_helptext_with_password\" ng-if=\"user.hasPassword\"></span> </p></md-input-container><div ng-if=\"user.hasPassword\" class=\"auth-signin-form__password\"><md-input-container class=\"md-block\"><label translate=\"auth_form.password\"></label><input type=\"password\" md-autofocus=\"true\" ng-required=\"ng-required\" ng-model=\"user.password\" class=\"lmo-primary-form-input\"><validation_errors subject=\"user\" field=\"password\"></validation_errors></md-input-container><div class=\"lmo-actions\"><md-button ng-click=\"sendLoginLink()\" ng-class=\"{\'md-primary\': !user.password}\" class=\"auth-signin-form__login-link\"><span translate=\"auth_form.login_link\"></span></md-button><md-button ng-click=\"submit()\" ng-disabled=\"!user.password\" ng-if=\"user.hasPassword\" class=\"md-primary md-raised auth-signin-form__submit\"><span translate=\"auth_form.sign_in\"></span></md-button></div></div><div ng-if=\"!user.hasPassword\" class=\"auth-signin-form__no-password\"><div class=\"lmo-actions\"><md-button ng-click=\"setPassword()\" class=\"auth-signin-form__set-password\"><span translate=\"auth_form.set_password\"></span></md-button><md-button ng-click=\"sendLoginLink()\" class=\"md-primary md-raised auth-signin-form__submit\"><span translate=\"auth_form.login_link\"></span></md-button></div></div></div>");
$templateCache.put("generated/components/auth/signup_form/auth_signup_form.html","<div class=\"auth-signup-form\"><div class=\"auth-signup-form__welcome\"><auth_avatar user=\"user\"></auth_avatar><h2 translate=\"auth_form.welcome\" translate-value-email=\"{{user.name || user.email}}\" class=\"lmo-h2\"></h2><p translate=\"auth_form.sign_up_helptext\" class=\"poll-common-helptext\"></p></div><md-input-container class=\"md-block auth-signup-form__name\"><label translate=\"auth_form.name\"></label><input type=\"text\" md-autofocus=\"true\" placeholder=\"{{auth_form.name_placeholder | translate}}\" ng-model=\"name\" ng-required=\"true\" class=\"lmo-primary-form-input\"><validation_errors subject=\"user\" field=\"name\"></validation_errors></md-input-container><div vc-recaptcha=\"true\" key=\"recaptchaKey\" ng-if=\"recaptchaKey\" ng-model=\"user.recaptcha\"></div><div class=\"lmo-actions\"><div></div><md-button ng-click=\"submit()\" translate=\"auth_form.login_link\" aria-label=\"{{ \'auth_form.login_link\' | translate }}\" class=\"md-primary md-raised auth-signup-form__submit\"></md-button></div></div>");
$templateCache.put("generated/components/group/form/group_form.html","<div class=\"group-form\"><div ng-show=\"isDisabled\" class=\"lmo-disabled-form\"></div><md-input-container class=\"md-block\"><label translate=\"{{i18n.group_name}}\"></label><input type=\"text\" placeholder=\"{{\'group_form.group_name_placeholder\' | translate}}\" ng-required=\"ng-required\" ng-model=\"group.name\" md-maxlength=\"255\" class=\"lmo-primary-form-input\" id=\"group-name\"><validation_errors subject=\"group\" field=\"name\"></validation_errors></md-input-container><md-input-container class=\"md-block\"><label translate=\"group_form.description\"></label><textarea ng-model=\"group.description\" placeholder=\"{{\'group_form.description_placeholder\' | translate }}\" class=\"lmo-primary-form-input\" id=\"group-description\"></textarea></md-input-container><div class=\"group-form__privacy-statement poll-common-helptext\">{{privacyStatement()}}</div><div class=\"group-form__privacy lmo-form-group\"><label translate=\"group_form.privacy\"></label><label ng-if=\"!group.isSubgroupOfSecretParent()\" class=\"lmo-form-labelled-input\"><input type=\"radio\" ng-model=\"group.groupPrivacy\" ng-change=\"groupPrivacyChanged()\" value=\"open\" name=\"groupPrivacy\" class=\"group-form__privacy-open\"><span><strong translate=\"common.privacy.open\"></strong><span class=\"seperator\"></span>{{ privacyStringFor(\"open\") }}</span></label><label class=\"lmo-form-labelled-input\"><input type=\"radio\" ng-model=\"group.groupPrivacy\" ng-change=\"groupPrivacyChanged()\" value=\"closed\" name=\"groupPrivacy\" class=\"group-form__privacy-closed\"><span><strong translate=\"common.privacy.closed\"></strong><span class=\"seperator\"></span>{{ privacyStringFor(\"closed\") }}</span></label><label class=\"lmo-form-labelled-input\"><input type=\"radio\" ng-model=\"group.groupPrivacy\" ng-change=\"groupPrivacyChanged()\" value=\"secret\" name=\"groupPrivacy\" class=\"group-form__privacy-secret\"><span><strong translate=\"common.privacy.secret\"></strong><span class=\"seperator\"></span>{{ privacyStringFor(\"secret\") }}</span></label></div><div ng-show=\"expanded\" class=\"group-form__advanced\"><div ng-if=\"group.privacyIsOpen()\" class=\"group-form__joining lmo-form-group\"><label translate=\"group_form.how_do_people_join\"></label><label class=\"lmo-form-labelled-input\"><input type=\"radio\" ng-model=\"group.membershipGrantedUpon\" value=\"request\" name=\"membership-granted-upon\" class=\"group-form__membership-granted-upon-request\"><span translate=\"group_form.membership_granted_upon_request\"></span></label><label class=\"lmo-form-labelled-input\"><input type=\"radio\" ng-model=\"group.membershipGrantedUpon\" value=\"approval\" name=\"membership-granted-upon\" class=\"group-form__membership-granted-upon-approval\"><span translate=\"group_form.membership_granted_upon_approval\"></span></label></div><div class=\"group-form__permissions lmo-form-group\"><label translate=\"group_form.permissions\"></label><label ng-if=\"group.privacyIsClosed() &amp;&amp; !group.isSubgroupOfSecretParent()\" class=\"lmo-form-labelled-input\"><input type=\"checkbox\" ng-model=\"buh.allowPublicThreads\" ng-change=\"allowPublicThreadsClicked()\" class=\"group-form__allow-public-threads\"><span translate=\"group_form.allow_public_threads\"></span></label><label ng-if=\"group.isSubgroup() &amp;&amp; group.privacyIsClosed()\" class=\"lmo-form-labelled-input\"><input type=\"checkbox\" ng-model=\"group.parentMembersCanSeeDiscussions\" class=\"group-form__parent-members-can-see-discussions\"><span translate=\"group_form.parent_members_can_see_discussions\" translate-value-parent=\"{{group.parent().name}}\"></span></label><label class=\"lmo-form-labelled-input\"><input type=\"checkbox\" ng-model=\"group.membersCanAddMembers\" class=\"group-form__members-can-add-members\"><span translate=\"group_form.members_can_add_members\"></span></label><label ng-if=\"group.isParent()\" class=\"lmo-form-labelled-input\"><input type=\"checkbox\" ng-model=\"group.membersCanCreateSubgroups\" class=\"group-form__members-can-create-subgroups\"><span translate=\"group_form.members_can_create_subgroups\"></span></label><label class=\"lmo-form-labelled-input\"><input type=\"checkbox\" ng-model=\"group.membersCanStartDiscussions\" class=\"group-form__members-can-start-discussions\"><span translate=\"group_form.members_can_start_discussions\"></span></label><label class=\"lmo-form-labelled-input\"><input type=\"checkbox\" ng-model=\"group.membersCanEditDiscussions\" class=\"group-form__members-can-edit-discussions\"><span translate=\"group_form.members_can_edit_discussions\"></span></label><label class=\"lmo-form-labelled-input\"><input type=\"checkbox\" ng-model=\"group.membersCanEditComments\" class=\"group-form__members-can-edit-comments\"><span translate=\"group_form.members_can_edit_comments\"></span></label><label class=\"lmo-form-labelled-input\"><input type=\"checkbox\" ng-model=\"group.membersCanRaiseMotions\" class=\"group-form__members-can-raise-motions\"><span translate=\"group_form.members_can_raise_motions\"></span></label><label class=\"lmo-form-labelled-input\"><input type=\"checkbox\" ng-model=\"group.membersCanVote\" class=\"group-form__members-can-vote\"><span translate=\"group_form.members_can_vote\"></span></label></div></div><div class=\"lmo-flex lmo-flex__space-between\"><div ng-if=\"expanded\"></div><md-button type=\"button\" ng-if=\"!expanded\" ng-click=\"expandForm()\" translate=\"group_form.advanced_settings\" class=\"md-accent group-form__advanced-link\"></md-button><md-button ng-click=\"submit()\" class=\"md-primary md-raised group-form__submit-button\"><span translate=\"{{i18n.submit}}\"></span></md-button></div></div>");
$templateCache.put("generated/components/group/modal/group_modal.html","<md-dialog class=\"group-form\"><md-toolbar><div class=\"md-toolbar-tools lmo-flex__space-between\"><i class=\"material-icons\">group</i><div ng-switch=\"currentStep\" class=\"group-form__title\"><div ng-switch-when=\"create\" class=\"group-form__group-title\"><h1 ng-if=\"group.isNew()\" translate=\"group_form.start_group_heading\" class=\"lmo-h1\"></h1><h1 ng-if=\"!group.isNew()\" translate=\"group_form.edit_group_heading\" class=\"lmo-h1\"></h1></div><div ng-switch-when=\"invite\" class=\"group-form__invitation-title\"><h1 translate=\"invitation_form.invite_people\" class=\"lmo-h1\"></h1></div></div><material_modal_header_cancel_button></material_modal_header_cancel_button></div></md-toolbar><div ng-switch=\"currentStep\" class=\"md-dialog-content md-body-1 lmo-slide-animation\"><group_form ng-switch-when=\"create\" group=\"group\" class=\"animated\"></group_form><invitation_form ng-switch-when=\"invite\" group=\"group\" class=\"animated\"></invitation_form></div></md-dialog>");
$templateCache.put("generated/components/group_page/cover_photo_form/cover_photo_form.html","<div class=\"cover-photo-form\"><div ng-show=\"isDisabled\" class=\"lmo-disabled-form\"></div><md-toolbar><div class=\"md-toolbar-tools lmo-flex__space-between\"><h1 translate=\"cover_photo_form.heading\" class=\"lmo-h1\"></h1><material_modal_header_cancel_button></material_modal_header_cancel_button></div></md-toolbar><div class=\"md-dialog-content md-body-1\"><button ng-click=\"selectFile()\" class=\"lmo-btn-link\"><div class=\"lmo-box--small lmo-float--left\"><div class=\"user-avatar__initials--small\"><i class=\"fa fa-lg fa-camera\"></i></div></div><span translate=\"cover_photo_form.upload_link\" class=\"lmo-option-text\"></span></button><div class=\"lmo-clearfix\"></div><input type=\"file\" ng-model=\"files\" ng-file-select=\"upload(files)\" class=\"hidden cover-photo-form__file-input\"><p translate=\"cover_photo_form.image_size_helptext\"></p></div></div>");
$templateCache.put("generated/components/group_page/description_card/description_card.html","<section aria-labelledby=\"description-card-title\" class=\"description-card\"><h2 translate=\"description_card.title\" class=\"lmo-card-heading\" id=\"description-card-title\"></h2><div ng-hide=\"editorEnabled\"><div translate=\"description_card.placeholder\" ng-if=\"!group.description\" class=\"description-card__placeholder lmo-placeholder md-caption\"></div><div marked=\"group.description\" class=\"description-card__text lmo-last lmo-markdown-wrapper\"></div><button ng-if=\"canEditGroup()\" ng-click=\"enableEditor()\" translate=\"common.action.edit\" class=\"description-card__edit lmo-link\"></button></div><div ng-show=\"editorEnabled\"><div class=\"lmo-form-group\"><div class=\"lmo-relative lmo-textarea-wrapper\"><textarea msd-elastic=\"true\" ng-model=\"editableDescription\" ng-model-options=\"{debounce: 150}\" class=\"lmo-textarea description-card__textarea form-control lmo-primary-form-input\"></textarea></div></div><div class=\"description-card__actions\"><button ng-click=\"disableEditor()\" translate=\"common.action.cancel\" class=\"description-card__cancel lmo-btn--cancel\"></button><button ng-click=\"save()\" translate=\"common.action.save\" class=\"description-card__save lmo-btn--primary\"></button></div></div></section>");
$templateCache.put("generated/components/group_page/discussions_card/discussions_card.html","<section aria-labelledby=\"threads-card-title\" class=\"discussions-card\"><div class=\"discussions-card__header\"><h2 translate=\"group_page.discussions\" class=\"lmo-card-heading\" id=\"threads-card-title\"></h2><md-button ng-if=\"canStartThread()\" ng_click=\"openDiscussionForm()\" title=\"{{ \'navbar.start_thread\' | translate }}\" class=\"md-primary md-raised discussions-card__new-thread-button\"><span translate=\"navbar.start_thread\"></span></md-button></div><div class=\"discussions-card__placeholder\"><div translate=\"group_page.discussions_placeholder\" ng-if=\"showThreadsPlaceholder()\" class=\"lmo-placeholder\"></div></div><div ng-if=\"!loadMoreExecuting &amp;&amp; !discussions.any()\" class=\"discussions-card__list--empty\"><div translate=\"group_page.thread_list.empty.{{whyImEmpty()}}\" class=\"discussions-card__list-empty-reason\"></div><div ng-if=\"howToGainAccess()\" class=\"discussions-card__how-to-gain-access\"><span translate=\"group_page.thread_list.empty.{{howToGainAccess()}}\"></span></div></div><div class=\"discussions-card__list\"><section ng-if=\"discussions.any()\" class=\"thread-preview-collection__container\"><thread_preview_collection query=\"discussions\" limit=\"threadLimit\" class=\"thread-previews-container\"></thread_preview_collection></section><div ng-if=\"canLoadMoreDiscussions()\" class=\"lmo-show-more\"><button ng-hide=\"loadMoreExecuting\" ng-click=\"loadMore()\" translate=\"common.action.show_more\" class=\"discussions-card__show-more\"></button></div><loading ng-show=\"loadMoreExecuting\"></loading></div></section>");
$templateCache.put("generated/components/group_page/group_actions_dropdown/group_actions_dropdown.html","<div class=\"group-page-actions lmo-no-print\"><md-menu md-position-mode=\"target-right target\" class=\"lmo-dropdown-menu\"><md-button ng-click=\"$mdMenu.open()\" class=\"group-page-actions__button\"> <span translate=\"group_page.options.label\"></span> <i class=\"material-icons\">expand_more</i></md-button><md-menu-content class=\"group-actions-dropdown__menu-content\"><md-menu-item ng-if=\"canEditGroup()\" class=\"group-page-actions__edit-group-link\"><md-button ng-click=\"editGroup()\"><span translate=\"group_page.options.edit_group\"></span></md-button></md-menu-item><md-menu-item ng-if=\"canAdministerGroup()\"><a lmo-href-for=\"group\" lmo-href-action=\"memberships\" class=\"md-button\"><span translate=\"group_page.options.manage_members\"></span></a></md-menu-item><md-menu-item ng-if=\"canAddSubgroup()\" class=\"group-page-actions__add-subgroup-link\"><md-button ng-click=\"addSubgroup()\"><span translate=\"group_page.options.add_subgroup\"></span></md-button></md-menu-item><md-menu-item ng-if=\"canChangeVolume()\" class=\"group-page-actions__change-volume-link\"><md-button ng-click=\"openChangeVolumeForm()\"><span translate=\"group_page.options.email_settings\"></span></md-button></md-menu-item><outlet name=\"after-group-actions-manage-memberships\" model=\"group\"></outlet><md-menu-item class=\"group-page-actions__leave-group\"><md-button ng-click=\"leaveGroup()\"><span translate=\"group_page.options.leave_group\"></span></md-button></md-menu-item><md-menu-item ng-if=\"canArchiveGroup()\" class=\"group-page-actions__archive-group\"><md-button ng-click=\"archiveGroup()\"><span translate=\"group_page.options.deactivate_group\"></span></md-button></md-menu-item></md-menu-content></md-menu></div>");
$templateCache.put("generated/components/group_page/group_help_card/group_help_card.html","<div class=\"blank\"><section ng-if=\"showHelpCard()\" class=\"group-help-card\"><h2 translate=\"group_help_card.title\" class=\"lmo-card-heading\"></h2><div class=\"group-help-card__video-wrapper\"><div ng-if=\"showVideo\" class=\"lmo-bank\"><iframe width=\"269\" height=\"150\" ng-src=\"{{helpVideo()}}\" frameborder=\"0\" allowfullscreen></iframe></div></div><div class=\"group-help-card__articles\"><div class=\"media\"><div class=\"media-left\"><img src=\"/img/10-tips.png\" alt=\"Weaving in agenda points\" class=\"group-help-card__article-thumbnail\"></div><div class=\"media-body\"><a href=\"{{tenTipsArticleLink()}}\" title=\"{{\'group_help_card.ten_tips_article_title\' | translate}}\" target=\"_blank\" class=\"group-help-card__article-title\"><span translate=\"group_help_card.ten_tips_article_title\"></span></a></div></div><div class=\"media\"><div class=\"media-left\"><img src=\"/img/conversation-to-action.png\" alt=\"Caring work environment\" class=\"group-help-card__article-thumbnail\"></div><div class=\"media-body\"><a href=\"{{nineWaysArticleLink()}}\" title=\"{{ \'group_help_card.nine_ways_to_use_proposals\' | translate }}\" target=\"_blank\" class=\"group-help-card__article-title\"><span translate=\"group_help_card.nine_ways_to_use_proposals\"></span></a></div></div></div><a lmo-href=\"{{helpLink()}}\" target=\"_blank\" class=\"group-help-card__more-help lmo-card-minor-action\"><span translate=\"common.more_help\"></span></a></section></div>");
$templateCache.put("generated/components/group_page/group_previous_proposals_card/group_previous_proposals_card.html","<div class=\"blank\"><section ng-if=\"showPreviousProposals()\" aria-labelledby=\"group-previous-proposals-card__title\" class=\"group-previous-proposals-card\"><h2 translate=\"group_previous_proposals_card.heading\" class=\"lmo-card-heading\" id=\"group-previous-proposals-card__title\"></h2><ul class=\"group-previous-proposals-card__previous_proposals\"><li ng-repeat=\"proposal in group.closedProposals() | limitTo: 3 track by proposal.id\" class=\"group-previous-proposals-card__previous_proposal\"><pie_with_position proposal=\"proposal\" class=\"pull-left\"></pie_with_position><a lmo-href-for=\"proposal\"><div class=\"group-previous-proposals-card__proposal-title\">{{proposal.name | truncate:100:\' ...\'}}</div></a></li></ul><a ng-show=\"canShowMore()\" lmo-href-for=\"group\" lmo-href-action=\"previous_proposals\" class=\"group-previous-proposals-card__link lmo-card-minor-action\"><span translate=\"group_previous_proposals_card.see_more\"></span></a></section></div>");
$templateCache.put("generated/components/group_page/group_privacy_button/group_privacy_button.html","<button aria-label=\"{{privacyDescription()}}\" class=\"group-privacy-button md-button\"><md-tooltip class=\"group-privacy-button__tooltip\">{{privacyDescription()}}</md-tooltip><div translate=\"group_page.privacy.aria_label\" translate-value-privacy=\"{{group.groupPrivacy}}\" class=\"sr-only\"></div><div aria-hidden=\"true\" class=\"screen-only\"> <i ng-class=\"iconClass()\" class=\"fa fa-lg\"></i> <span translate=\"common.privacy.{{group.groupPrivacy}}\"></span></div></button>");
$templateCache.put("generated/components/group_page/group_theme/group_theme.html","<div class=\"group-theme\"><div class=\"group-theme__cover lmo-no-print\"><div ng-mouseover=\"themeHoverIn()\" ng-mouseleave=\"themeHoverOut()\" ng-if=\"canUploadPhotos()\" class=\"group-theme__upload-photo\"><button ng-click=\"openUploadCoverForm()\" title=\"{{ \'group_page.new_cover_photo\' | translate }}\" class=\"lmo-btn-link\"><i class=\"fa fa-lg fa-camera\"></i><span ng-show=\"themeHover\" translate=\"group_page.new_photo\" class=\"group-theme__upload-help-text\"></span></button></div></div><div ng-if=\"compact\" class=\"group-theme__header--compact\"><div aria-hidden=\"true\" class=\"group-theme__logo--compact\"><a lmo-href-for=\"group\"><img ng-src=\"{{group.logoUrl()}}\"></a></div><div aria-label=\"breadcrumb\" role=\"navigation\" class=\"group-theme__name--compact\"> <a ng-if=\"group.isSubgroup()\" lmo-href-for=\"group.parent()\" aria-level=\"1\">{{group.parentName()}}</a>  <span ng-if=\"group.isSubgroup()\">-</span>  <a lmo-href-for=\"group\" aria-level=\"2\">{{group.name}}</a>  <span ng-if=\"discussion\">-</span> <a ng-if=\"discussion\" lmo-href-for=\"discussion\" aria-level=\"3\">{{discussion.title}}</a></div></div><div ng-if=\"!compact\" class=\"group-theme__header\"><div ng-style=\"logoStyle()\" alt=\"{{ \'group_page.group_logo\' | translate }}\" class=\"group-theme__logo\"><div ng-mouseover=\"logoHoverIn()\" ng-mouseleave=\"logoHoverOut()\" ng-if=\"canUploadPhotos()\" class=\"group-theme__upload-photo\"><button ng-click=\"openUploadLogoForm()\" title=\"{{ \'group_page.new_group_logo\' | translate }}\" class=\"lmo-btn-link\"><i class=\"fa fa-lg fa-camera\"></i><span ng-show=\"logoHover\" translate=\"group_page.new_photo\" class=\"group-theme__upload-help-text\"></span></button></div></div><div class=\"group-theme__name-and-actions\"><h1 aria-label=\"breadcrumb\" role=\"navigation\" class=\"lmo-h1 group-theme__name\"><a ng-if=\"group.isSubgroup()\" lmo-href-for=\"group.parent()\" aria-level=\"1\">{{group.parentName()}}</a> <span ng-if=\"group.isSubgroup()\">-</span> <a lmo-href-for=\"group\" aria-level=\"2\">{{group.name}}</a></h1><div ng-if=\"homePage\" class=\"group-theme__actions\"><div ng-if=\"isMember()\" class=\"group-theme__member-actions\"><div class=\"pull-right\"><group_actions_dropdown group=\"group\"></group_actions_dropdown></div><div class=\"pull-right\"><outlet name=\"before-group-actions\" model=\"group\"></outlet></div><group_privacy_button group=\"group\"></group_privacy_button></div><join_group_button group=\"group\"></join_group_button></div><div class=\"clearfix\"></div></div></div></div>");
$templateCache.put("generated/components/group_page/join_group_button/join_group_button.html","<div class=\"blank\"><div ng-if=\"!isMember()\" class=\"join-group-button\"><div ng-if=\"canJoinGroup()\" class=\"blank\"><md-button ng-class=\"{\'btn-block\': block}\" translate=\"join_group_button.join_group\" ng-click=\"joinGroup()\" class=\"md-raised md-primary join-group-button__join-group\"></md-button></div><div ng-if=\"canRequestMembership()\" class=\"blank\"><md-button ng-class=\"{\'btn-block\': block}\" ng-disabled=\"hasRequestedMembership()\" translate=\"join_group_button.join_group\" ng-click=\"requestToJoinGroup()\" class=\"md-raised md-primary join-group-button__ask-to-join-group\"></md-button></div></div></div>");
$templateCache.put("generated/components/group_page/logo_photo_form/logo_photo_form.html","<div class=\"logo-photo-form\"><div ng-show=\"isDisabled\" class=\"lmo-disabled-form\"></div><md-toolbar><div class=\"md-toolbar-tools lmo-flex__space-between\"><h1 translate=\"logo_photo_form.heading\" class=\"lmo-h1 modal-title\"></h1><material_modal_header_cancel_button></material_modal_header_cancel_button></div></md-toolbar><div class=\"md-dialog-content md-body-1\"><button ng-click=\"selectFile()\" class=\"lmo-btn-link\"><div class=\"lmo-box--small lmo-float--left\"><div class=\"user-avatar__initials--small\"><i class=\"fa fa-lg fa-camera\"></i></div></div><span translate=\"logo_photo_form.upload_link\" class=\"lmo-option-text\"></span></button><div class=\"lmo-clearfix\"></div><input type=\"file\" ng-model=\"files\" ng-file-select=\"upload(files)\" class=\"hidden logo-photo-form__file-input\"><p translate=\"logo_photo_form.image_size_helptext\"></p></div></div>");
$templateCache.put("generated/components/group_page/members_card/members_card.html","<section aria-labelledby=\"members-card-title\" class=\"members-card lmo-no-print\"><h2 translate=\"group_page.members\" class=\"lmo-card-heading\" id=\"members-card-title\"></h2><div class=\"members-card__list\"><div ng-if=\"group.memberships().length &gt; 1\" ng-repeat=\"membership in group.memberships() | orderBy: \'-admin\' | limitTo:10\" class=\"members-card__avatar\"><a lmo-href-for=\"membership.user()\" class=\"members-card__avatar-link\"><user_avatar user=\"membership.user()\" coordinator=\"membership.admin\" size=\"medium\"></user_avatar></a></div><div translate=\"group_page.members_placeholder\" ng-if=\"showMembersPlaceholder()\" class=\"lmo-placeholder\"></div></div><div class=\"lmo-flex lmo-flex__space-between\"><a lmo-href-for=\"group\" lmo-href-action=\"memberships\" class=\"members-card__manage-members\"> <span translate=\"group_page.see_all_members\" translate-value-count=\"{{group.membershipsCount}}\"></span> <span ng-if=\"group.pendingInvitationsCount\" translate=\"group_page.pending_invitations\" translate-value-count=\"{{group.pendingInvitationsCount}}\"></span></a><div ng-if=\"canAddMembers()\" class=\"members-card__invite-members\"><md-button ng_click=\"invitePeople()\" class=\"md-primary md-raised members-card__invite-members-btn\"><span translate=\"group_page.invite_people\"></span></md-button></div></div></section>");
$templateCache.put("generated/components/group_page/membership_request_form/membership_request_form.html","<md-dialog class=\"membership-request-form\"><form ng-submit=\"submit()\" ng-disabled=\"membership_request.processing\"><div ng-show=\"isDisabled\" class=\"lmo-disabled-form\"></div><md-toolbar><div class=\"md-toolbar-tools lmo-flex__space-between\"><h1 translate=\"membership_request_form.heading\" class=\"lmo-h1\"></h1><material_modal_header_cancel_button></material_modal_header_cancel_button></div></md-toolbar><div class=\"md-dialog-content\"><fieldset><div class=\"lmo-form-group\"><label for=\"membership-request-name\" translate=\"membership_request_form.name_label\"></label><input ng-model=\"membershipRequest.name\" ng-required=\"true\" ng-disabled=\"isLoggedIn()\" class=\"membership-request-form__name form-control\" id=\"membership-request-name\"></div><div class=\"lmo-form-group\"><label for=\"membership-request-email\" translate=\"membership_request_form.email_label\"></label><input ng-model=\"membershipRequest.email\" ng-required=\"true\" ng-disabled=\"isLoggedIn()\" class=\"membership-request-form__email form-control\" id=\"membership-request-email\"><validation_errors subject=\"membershipRequest\" field=\"email\"></validation_errors></div><div class=\"lmo-form-group\"><label for=\"membership-request-introduction\" translate=\"membership_request_form.introduction_label\"></label><textarea ng-model=\"membershipRequest.introduction\" ng-required=\"false\" class=\"lmo-textarea membership-request-form__introduction form-control\" id=\"membership-request-introduction\"></textarea> <div ng-class=\"{ overlimit: membershipRequest.overCharLimit() }\" class=\"membership-request-form__chars-left\">{{ membershipRequest.charsLeft() }}</div> <div ng-if=\"membershipRequest.overCharLimit()\" translate=\"membership_request_form.introduction_over_limit\" class=\"membership-request-form__over-limit lmo-validation-error\"></div></div></fieldset><div class=\"lmo-md-actions\"><md-button ng-click=\"$close()\" type=\"button\" translate=\"common.action.cancel\" class=\"membership-request-form__cancel-btn\"></md-button><md-button type=\"submit\" translate=\"membership_request_form.submit_button\" class=\"md-raised md-primary membership-request-form__submit-btn\"></md-button></div></div></form></md-dialog>");
$templateCache.put("generated/components/group_page/membership_requests_card/membership_requests_card.html","<div class=\"blank\"><section ng-if=\"canManageMembershipRequests() &amp;&amp; group.hasPendingMembershipRequests()\" class=\"membership-requests-card\"><h2 translate=\"membership_requests_card.heading\" class=\"lmo-card-heading\"></h2><ul class=\"membership-requests-card__pending-requests\"><li ng-repeat=\"membershipRequest in group.pendingMembershipRequests() | orderBy: \'-createdAt\' | limitTo: 5 track by membershipRequest.id\" class=\"media\"><a lmo-href-for=\"group\" lmo-href-action=\"membership_requests\" title=\"{{ \'membership_requests_card.manage_requests\' | translate }}\"><div class=\"media-left\"><user_avatar user=\"membershipRequest.actor()\" size=\"medium\"></user_avatar></div><div class=\"media-body\"><span class=\"membership-requests-card__requestor-name\">{{membershipRequest.actor().name}}</span><div class=\"lmo-truncate membership-requests-card__requestor-introduction\">{{membershipRequest.introduction}}</div></div></a></li></ul><a lmo-href-for=\"group\" lmo-href-action=\"membership_requests\" class=\"membership-requests-card__link lmo-card-minor-action\"><span translate=\"membership_requests_card.manage_requests_with_count\" translate-value-count=\"{{group.pendingMembershipRequests().length}}\"></span></a></section></div>");
$templateCache.put("generated/components/group_page/subgroups_card/subgroups_card.html","<div class=\"blank\"><section ng-if=\"showSubgroupsCard()\" aria-labelledby=\"subgroups-card__title\" class=\"subgroups-card\"><h2 translate=\"group_page.subgroups\" class=\"lmo-card-heading\" id=\"subgroups-card__title\"></h2><ul class=\"subgroups-card__list\"><li ng-repeat=\"subgroup in group.subgroups() | orderBy: \'name\' track by subgroup.id\" class=\"subgroups-card__list-item\"><div class=\"subgroups-card__list-item-logo\"><group_avatar group=\"subgroup\" size=\"medium\"></group_avatar></div><div class=\"subgroups-card__list-item-name\"><a lmo-href-for=\"subgroup\">{{ subgroup.name }}</a></div><div class=\"subgroups-card__list-item-description\">{{ subgroup.description | truncate }}</div></li></ul><button ng-click=\"startSubgroup()\" ng-if=\"canCreateSubgroups()\" class=\"subgroups-card__add-subgroup-link\"><span translate=\"common.action.add_subgroup\"></span></button></section></div>");
$templateCache.put("generated/components/install_slack/card/install_slack_card.html","<div ng-if=\"group.slackChannelName &amp;&amp; group.slackTeamName\" class=\"install-slack-card lmo-card\"><h2 translate=\"install_slack.card.title\" class=\"lmo-card-heading\"></h2><div class=\"lmo-flex\"><img src=\"/img/slack-icon.svg\" class=\"install-slack-card__logo\"><p translate=\"install_slack.card.helptext\" translate-value-channel=\"{{group.slackChannelName}}\" translate-value-team=\"{{group.slackTeamName}}\" class=\"install-slack-card__helptext poll-common-helptext\"></p></div></div>");
$templateCache.put("generated/components/install_slack/decide_form/install_slack_decide_form.html","<div class=\"install-slack-decide-form\"><h2 translate=\"install_slack.decide.heading\" class=\"lmo-h2\"></h2><p translate=\"install_slack.decide.helptext\" ng-if=\"!poll.pollType\" class=\"poll-common-helptext\"></p><poll_common_start_poll poll=\"poll\"></poll_common_start_poll><div class=\"lmo-flex install-slack-form__actions\"><md-button ng-if=\"!poll.pollType\" ng-click=\"$emit(\'decideComplete\')\" translate=\"install_slack.common.do_it_later\"></md-button></div></div>");
$templateCache.put("generated/components/install_slack/form/install_slack_form.html","<div class=\"install-slack-form\"><div ng-show=\"isDisabled\" class=\"lmo-disabled-form\"></div><install_slack_progress slack-progress=\"progress()\"></install_slack_progress><div ng-switch=\"currentStep\" class=\"install-slack-form__form lmo-slide-animation\"><install_slack_install_form group=\"group\" ng-switch-when=\"install\"></install_slack_install_form><install_slack_invite_form group=\"group\" ng-switch-when=\"invite\" class=\"animated\"></install_slack_invite_form><install_slack_decide_form group=\"group\" ng-switch-when=\"decide\" class=\"animated\"></install_slack_decide_form></div></div>");
$templateCache.put("generated/components/install_slack/install_form/install_slack_install_form.html","<div class=\"install-slack-install-form\"><h2 translate=\"install_slack.install.heading\" class=\"lmo-h2\"></h2><div ng-if=\"group.id\" class=\"install-slack-install-form__add-to-group\"><p translate=\"install_slack.install.add_to_group_helptext\" class=\"poll-common-helptext\"></p><md-select md-autofocus=\"true\" ng-model=\"group\" ng-change=\"setSubmit(group)\" aria-label=\"{{ \'install_slack.install.group_id\' | translate }}\"><md-option ng-repeat=\"g in groups() | orderBy:\'fullName\'\" ng-value=\"g\">{{ g.fullName }}</md-option></md-select><div layout=\"column\" class=\"lmo-flex install-slack-form__actions\"><md-button translate=\"install_slack.install.install_slack\" ng-click=\"submit()\" class=\"md-primary md-raised\"></md-button><md-button translate=\"install_slack.install.start_new_group\" ng-click=\"toggleExistingGroup()\"></md-button></div></div><div ng-if=\"!group.id\" class=\"install-slack-install-form__create-new-group\"><p translate=\"install_slack.install.create_new_group_helptext\" class=\"poll-common-helptext\"></p><md-input-container class=\"md-block install-slack-install-form__group\"><label translate=\"install_slack.install.group_name\"></label><input md-autofocus=\"true\" type=\"text\" placeholder=\"{{ \'install_slack.install.group_name_placeholder\' | translate }}\" ng-model=\"group.name\" class=\"lmo-primary-form-input\"></md-input-container><div layout=\"column\" class=\"lmo-flex install-slack-form__actions\"><md-button translate=\"install_slack.install.install_slack\" ng-click=\"submit()\" class=\"md-primary md-raised install-slack-install-form__submit\"></md-button><md-button ng-if=\"groups().length\" translate=\"install_slack.install.use_existing_group\" ng-click=\"toggleExistingGroup()\"></md-button></div></div></div>");
$templateCache.put("generated/components/install_slack/invite_form/install_slack_invite_form.html","<div class=\"install-slack-invite-form\"><h2 translate=\"install_slack.invite.heading\" class=\"lmo-h2\"></h2><p translate=\"install_slack.invite.helptext\" class=\"poll-common-helptext\"></p><md-select md-autofocus=\"true\" ng-model=\"identifier\" placeholder=\"{{\'install_slack.invite.select_a_channel\' | translate}}\" md-on-open=\"fetchChannels()\"><md-option ng-value=\"channel.id\" ng-repeat=\"channel in channels\"><span>&#35;{{ channel.name }}</span></md-option></md-select><div class=\"md-list-item-text lmo-flex lmo-flex__space-between\"><div class=\"poll-common-checkbox-option__text\"><h3 translate=\"install_slack.invite.publish_group\" class=\"lmo-h3\"></h3><p translate=\"install_slack.invite.publish_group_helptext_on\" ng-if=\"group.makeAnnouncement\" class=\"md-caption\"></p><p translate=\"install_slack.invite.publish_group_helptext_off\" ng-if=\"!group.makeAnnouncement\" class=\"md-caption\"></p></div><md-checkbox ng-model=\"group.makeAnnouncement\"></md-checkbox></div><div layout=\"column\" class=\"lmo-flex install-slack-form__actions\"><md-button ng-disabled=\"!identifier\" translate=\"install_slack.invite.set_channel\" ng-click=\"submit()\" class=\"md-primary md-raised\"></md-button></div></div>");
$templateCache.put("generated/components/install_slack/invite_preview/install_slack_invite_preview.html","<div layout=\"row\" class=\"install-slack-invite-preview lmo-flex\"><div class=\"install-slack-invite-preview__avatar\"><img ng-src=\"https://s3-us-west-2.amazonaws.com/slack-files2/bot_icons/2017-03-29/161925077303_48.png\"></div><div class=\"install-slack-invite-preview__content\"><div class=\"install-slack-invite-preview__title\"> <strong class=\"install-slack-invite-preview__loomio-bot\">Loomio Bot</strong>  <span class=\"install-slack-invite-preview__app\">APP</span>  <span class=\"install-slack-invite-preview__title-timestamp\">{{ timestamp() }}</span> </div><div class=\"install-slack-invite-preview__published\"></div><div translate=\"install_slack.invite.group_published_preview\" translate-value-author=\"{{ userName }}\" translate-value-name=\"{{group.name}}\" class=\"install-slack-invite-preview__message\"></div><div layout=\"row\" class=\"install-slack-invite-preview__attachment lmo-flex\"><div class=\"install-slack-invite-preview__bar\"></div><div class=\"install-slack-invite-preview__poll\"><div class=\"install-slack-invite-preview__author\">{{ userName }}</div><div class=\"install-slack-invite-preview__poll-title\">{{ group.name }}</div><div ng-if=\"poll.details\" class=\"install-slack-invite-preview__poll-details\">{{ group.description | truncate }}</div><div translate=\"poll_common_publish_slack_preview.view_it_on_loomio\" class=\"install-slack-invite-preview__view-it\"></div><div translate=\"poll_common_publish_slack_preview.timestamp\" translate-value-timestamp=\"{{timestamp()}}\" class=\"install-slack-invite-preview__timestamp\"></div></div></div></div></div>");
$templateCache.put("generated/components/install_slack/modal/install_slack_modal.html","<md-dialog class=\"install-slack-modal\"><md-toolbar><div class=\"md-toolbar-tools lmo-flex__space-between\"><h1 translate=\"install_slack.modal_title\" class=\"lmo-h1\"></h1><material_modal_header_cancel_button ng-if=\"!preventClose\"></material_modal_header_cancel_button></div></md-toolbar><div class=\"md-dialog-content\"><install_slack_form></install_slack_form></div></md-dialog>");
$templateCache.put("generated/components/install_slack/progress/install_slack_progress.html","<div class=\"install-slack-progress\"><div class=\"install-slack-progress__bar\"></div><div ng-style=\"{\'width\': progressPercent()}\" class=\"install-slack-progress__bar active\"></div><div ng-class=\"{\'active\': slackProgress == 0, \'complete\': slackProgress &gt;= 0}\" class=\"install-slack-progress__dot\"><label translate=\"install_slack.progress.install\"></label></div><div ng-class=\"{\'active\': slackProgress == 50, \'complete\': slackProgress &gt;= 50}\" class=\"install-slack-progress__dot\"><label translate=\"install_slack.progress.invite\"></label></div><div ng-class=\"{\'active\': slackProgress == 100, \'complete\': slackProgress &gt;= 100}\" class=\"install-slack-progress__dot\"><label translate=\"install_slack.progress.decide\"></label></div></div>");
$templateCache.put("generated/components/install_slack/tableau/install_slack_tableau.html","<div class=\"install-slack-tableau\"><img ng-if=\"group()\" ng-src=\"{{group().logoUrl()}}\" class=\"install-slack-tableau__logo install-slack-tableau__loomio-logo\"><img ng-if=\"!group()\" src=\"/img/mascot.png\" class=\"install-slack-tableau__logo install-slack-tableau__loomio-logo\"><img src=\"/img/slack-icon.svg\" class=\"install-slack-tableau__logo install-slack-tableau__third-party-logo\"></div>");
$templateCache.put("generated/components/invitation/add_members_modal/add_members_modal.html","<md-dialog class=\"lmo-blank\"><loading ng-if=\"loadExecuting\"></loading><div ng-if=\"!loadExecuting\" class=\"add-members-modal\"><md-toolbar><div class=\"md-toolbar-tools lmo-flex__space-between\"><h1 translate=\"add_members_modal.heading\" translate-values=\"{name: group.parentName()}\" class=\"lmo-h1 modal-title\"></h1><material_modal_header_cancel_button></material_modal_header_cancel_button></div></md-toolbar><div class=\"md-dialog-content\"><div ng-show=\"isDisabled\" class=\"lmo-disabled-form\"></div><div ng-if=\"!canAddMembers()\" class=\"add-members-modal__empty-list\"><p translate=\"add_members_modal.no_members_to_add\" translate-value-parent=\"{{group.parentName()}}\"></p></div><div ng-if=\"canAddMembers()\" class=\"add-members-modal__list\"><label ng-repeat=\"member in members() | orderBy: \'username\' track by member.id\" class=\"add-members-modal__list-item\"><input type=\"checkbox\" checklist-model=\"selectedIds\" checklist-value=\"member.id\"><user_avatar user=\"member\" size=\"small\"></user_avatar><strong>{{member.name}}</strong>&nbsp;(@{{member.username}})</label></div><div class=\"lmo-md-actions\"><md-button type=\"button\" ng-click=\"reopenInvitationsForm()\" translate=\"common.action.back\"></md-button><md-button type=\"button\" ng-click=\"submit()\" translate=\"add_members_modal.add_members\" ng-if=\"canAddMembers()\" class=\"md-primary md-raised add-members-modal__submit\"></md-button></div></div></div></md-dialog>");
$templateCache.put("generated/components/invitation/form/invitation_form.html","<div class=\"invitation-form\"><md-input-container ng-if=\"selectGroup\" class=\"md-block\"><label for=\"group-select\" translate=\"invitation_form.group\"></label><md-select ng-model=\"form.groupId\" ng-required=\"ng-required\" ng-change=\"restoreRemoteDraft(); fetchShareableInvitation()\" id=\"group-select\"><md-option value=\"undefined\" translate=\"invitation_form.group_placeholder\"></md-option><md-option ng-repeat=\"group in availableGroups() | orderBy: \'fullName\' track by group.id\" ng-value=\"group.id\">{{group.fullName}}</md-option></md-select></md-input-container><div class=\"lmo-flex\"><label translate=\"invitation_form.shareable_link\"></label><help_bubble helptext=\"invitation_form.shareable_link_explanation\"></help_bubble></div><md-input-container class=\"lmo-flex lmo-flex__baseline\"><input value=\"{{invitationLink()}}\" ng-disabled=\"true\" class=\"invitation-form__shareable-link\"><md-button type=\"button\" clipboard=\"true\" text=\"invitationLink()\" on-copied=\"copied()\" class=\"md-raised md-primary\">{{ \'invitation_form.copy_link\' | translate}}</md-button></md-input-container><div class=\"lmo-flex\"><label for=\"email-addresses\" translate=\"invitation_form.email_addresses\"></label><help_bubble helptext=\"invitation_form.email_explanation\"></help_bubble></div><md-input-container md-no-float=\"true\" class=\"md-block\"><textarea ng-model=\"form.emails\" ng-required=\"ng-required\" msd-elastic=\"true\" rows=\"1\" placeholder=\"{{ \'invitation_form.email_addresses_placeholder\' | translate }}\" class=\"invitation-form__email-addresses\" id=\"email-addresses\"></textarea><validation_errors subject=\"form\" field=\"emails\" class=\"invitation-form__validation-errors\"></validation_errors><div ng-show=\"noInvitations\" translate=\"invitation_form.messages.no_invitations\" class=\"invitation-form__no-invitations lmo-validation-error\"></div><div ng-show=\"maxInvitations()\" translate=\"invitation_form.messages.max_invitations\" class=\"invitation-form__max-invitations lmo-validation-error\"></div></md-input-container><p ng-if=\"form.group().isSubgroup()\"> <button translate=\"invitation_form.add_members\" ng-click=\"addMembers()\" class=\"invitation-form__add-members lmo-btn-link--blue\"></button> <span translate=\"invitation_form.from_parent_group\" translate-values=\"{name: form.group().parentName()}\"></span></p><div class=\"md-dialog-actions lmo-md-action\"><md-button ng-click=\"submit()\" translate=\"{{submitText()}}\" translate-value-count=\"{{invitationsCount()}}\" ng-disabled=\"invalidEmail()\" class=\"invitation-form__submit md-raised md-primary\"></md-button></div></div>");
$templateCache.put("generated/components/invitation/invitable/invitable.html","<div class=\"invitation-form__invitable\"><div class=\"media-left\"><user_avatar ng-if=\"match.label.type == \'user\'\" user=\"match.label.user\" size=\"medium\"></user_avatar><div aria-hidden=\"true\" ng-if=\"match.label.type != \'user\'\" class=\"invitation-form__avatar-image\"><img ng-if=\"match.label.image\" alt=\"{{match.label.name}}\" ng-src=\"{{match.label.image}}\"><img ng-if=\"!match.label.image\" alt=\"{{match.label.name}}\" src=\"/img/default-user.png\"></div></div><div class=\"invitation-form__invitable-content\"> <div class=\"invitable-name\">{{match.label.name}}</div> <abbr ng-if=\"match.label.subtitle\" class=\"invitable-subtitle\">{{match.label.subtitle}}</abbr></div><div class=\"invitation-form__spacer\"></div></div>");
$templateCache.put("generated/components/invitation/modal/invitation_modal.html","<md-dialog><div ng-show=\"isDisabled\" class=\"lmo-disabled-form\"></div><md-toolbar><div class=\"md-toolbar-tools lmo-flex__space-between\"><h1 translate=\"invitation_form.invite_people\" class=\"lmo-h1 modal-title\"></h1><material_modal_header_cancel_button></material_modal_header_cancel_button></div></md-toolbar><div class=\"md-dialog-content md-body-1\"><invitation_form group=\"group\" select-group=\"true\"></invitation_form></div></md-dialog>");
$templateCache.put("generated/components/memberships_page/admin_memberships_panel/admin_memberships_panel.html","<table class=\"memberships-panel\"><tr class=\"memberships-panel__row\"><td class=\"memberships-panel__spacer-cell\"></td><td class=\"memberships-panel__spacer-cell\"></td><td translate=\"memberships_page.coordinator_label\" class=\"memberships-panel__header-cell\"></td><td translate=\"memberships_page.remove_member_label\" class=\"memberships-panel__header-cell\"></td></tr><tr ng-repeat=\"membership in memberships() | orderBy: \'-admin\' track by membership.id\" data-username=\"{{membership.user().username}}\" class=\"memberships-panel__row\"><td class=\"memberships-panel__avatar-cell\"><user_avatar user=\"membership.user()\" size=\"medium\" coordinator=\"membership.admin\"></user_avatar></td><td class=\"memberships-panel__member-info-cell\"><a lmo-href-for=\"membership.user()\">{{::membership.user().name}}</a><div>@{{::membership.user().username}}</div></td><td class=\"memberships-panel__admin-checkbox-cell\"><input type=\"checkbox\" ng-model=\"membership.admin\" ng-change=\"toggleAdmin(membership)\" ng-disabled=\"!canToggleAdmin(membership)\" class=\"memberships-page__make-coordinator\"></td><td class=\"memberships-panel__remove-member-cell\"><button ng-click=\"openRemoveForm(membership)\" ng-show=\"canRemoveMembership(membership)\" class=\"memberships-page__remove-link\"><i class=\"material-icons\">remove_circle</i></button></td></tr></table>");
$templateCache.put("generated/components/memberships_page/cancel_invitation_form/cancel_invitation_form.html","<md-dialog class=\"remove-membership-form\"><form><md-toolbar><div class=\"md-toolbar-tools lmo-flex__space-between\"><h1 translate=\"cancel_invitation_form.heading\" class=\"lmo-h1\"></h1><material_modal_header_cancel_button></material_modal_header_cancel_button></div></md-toolbar><div class=\"md-dialog-content\"><p translate=\"cancel_invitation_form.question\"></p><div class=\"lmo-md-actions\"><md-button ng-click=\"$close()\" translate=\"common.action.cancel\"></md-button><md-button ng-click=\"submit()\" translate=\"cancel_invitation_form.submit\" class=\"md-primary md-raised memberships-page__remove-membership-confirm\"></md-button></div></div></form></md-dialog>");
$templateCache.put("generated/components/memberships_page/memberships_panel/memberships_panel.html","<div class=\"memberships-panel\"><div class=\"row memberships-page__table-row\"></div><div ng-repeat=\"membership in memberships() | orderBy: \'-admin\' track by membership.id\" data-username=\"{{membership.user().username}}\" class=\"row memberships-page__table-row memberships-page__membership\"><div class=\"col-xs-8\"><div class=\"media\"><div class=\"media-left hidden-xs\"><user_avatar user=\"membership.user()\" size=\"medium\" coordinator=\"membership.admin\"></user_avatar></div><div class=\"media-body\"><div class=\"memberships-page__member-info\"><a lmo-href-for=\"membership.user()\">{{::membership.user().name}}</a><div>@{{::membership.user().username}}</div></div></div></div></div></div><div class=\"row memberships-page__table-row\"><div ng-if=\"canAddMembers()\" class=\"members-card__invite-members col-xs-6\"><a ng_click=\"invitePeople()\" class=\"lmo-btn--featured lmo-btn--block lmo-btn--icon\"> <i class=\"fa fa-lg fa-plus\"></i> <span translate=\"group_page.invite_people\"></span></a></div></div></div>");
$templateCache.put("generated/components/memberships_page/pending_invitations_card/pending_invitations_card.html","<div class=\"blank\"><section ng-if=\"canSeeInvitations() &amp;&amp; group.hasPendingInvitations()\" class=\"pending-invitations-card\"><h3 translate=\"pending_invitations_card.heading\" class=\"lmo-card-heading\"></h3><table class=\"pending-invitations-card__pending-invitations\"><thead><tr><th translate=\"pending_invitations_card.email\" class=\"pending_invitations_card__email-heading md-subhead\"></th><th translate=\"pending_invitations_card.invitation_link\" class=\"pending-invitations-card__invitation-url-heading md-subhead\"></th><th translate=\"pending_invitations_card.sent\" class=\"pending-invitations-card__sent-heading md-subhead\"></th><th></th></tr></thead><tbody><tr ng-repeat=\"invitation in group.pendingInvitations() track by invitation.id\"><td class=\"pending-invitations-card__email\">{{invitation.recipientEmail}}</td><td class=\"pending-invitations-card__invitation-url\">{{baseUrl}}invitations/{{invitation.token}}</td><td class=\"pending-invitations-card__sent\">{{invitationCreatedAt(invitation)}}</td><td class=\"align-center\"><button ng-click=\"openCancelForm(invitation)\" class=\"pending-invitations-card__cancel-link\"><i class=\"material-icons\">remove_circle</i></button></td></tr></tbody></table></section></div>");
$templateCache.put("generated/components/thread_page/activity_card/activity_card.html","<section aria-labelledby=\"activity-card-title\" class=\"activity-card\"><h2 translate=\"discussion.activity\" class=\"lmo-card-heading\" id=\"activity-card-title\"></h2><loading_content ng-if=\"loading\" block-count=\"2\"></loading_content><div ng-if=\"!loading\" class=\"activity-card__content\"><a ng-show=\"canLoadBackwards()\" ng-click=\"loadEventsBackwards()\" tabindex=\"0\" class=\"activity-card__load-backwards lmo-no-print\"> <i class=\"fa fa-refresh\"></i> <span translate=\"discussion.load_previous\" translate-value-count=\"{{beforeCount()}}\"></span></a><loading ng-show=\"loadEventsBackwardsExecuting\" class=\"activity-card__loading page-loading\"></loading><div ng-if=\"noEvents()\" translate=\"discussion.activity_placeholder\" class=\"activity-card__no-activity lmo-placeholder align-center\"></div><ul class=\"activity-card__activity-list\"><li ng_repeat=\"event in events() track by event.id\" in-view=\"($inview&amp;&amp;threadItemVisible(event)) || (!$inview&amp;&amp;threadItemHidden(event))\" in-view-options=\"{debounce: 100}\" aria-labelledby=\"event-{{event.id}}\" class=\"activity-card__activity-list-item\"><div ng-if=\"$last\" class=\"activity-card__last-item\"></div><div id=\"sequence-{{event.sequenceId}}\" ng-include=\"\'generated/components/thread_page/activity_card/\'+ event.kind + \'.html\'\" ng-if=\"!event.deleted\" class=\"activity-card-content\"></div><div ng-if=\"event.sequenceId == lastReadSequenceId\" ng-class=\"{\'activity-card__new-activity\': hasNewActivity}\" translate=\"activity_card.new_activity\" class=\"activity-card__last-read-activity\"></div></li></ul><loading ng-show=\"loadEventsForwardsExecuting\" class=\"activity-card__loading page-loading\"></loading></div></section>");
$templateCache.put("generated/components/thread_page/activity_card/discussion_edited.html","<div ng-controller=\"DiscussionEditedItemController\" class=\"thread-item\"><div class=\"thread-item__header media\"><div class=\"media-left\"><user_avatar user=\"event.actor()\" size=\"small\"></user_avatar></div><div class=\"media-body\"><h3 id=\"event-{{event.id}}\" class=\"thread-item__title\"><strong> <a lmo-href-for=\"event.actor()\" class=\"thread-item__title-link\">{{ event.actorName() }}</a> </strong><span translate=\"discussion_edited_item.{{translationKey}}\" translate-value-title=\"{{title}}\"></span><span><span ng-if=\"onlyPrivacyEdited()\" translate=\"{{privacyKey()}}\"></span></span><timeago timestamp=\"event.createdAt\" class=\"timeago--inline\"></timeago></h3></div></div></div>");
$templateCache.put("generated/components/thread_page/activity_card/discussion_moved.html","<div ng-controller=\"DiscussionMovedItemController\" class=\"thread-item\"><div class=\"thread-item__header media\"><div class=\"media-left\"><user_avatar user=\"event.actor()\" size=\"small\"></user_avatar></div><div class=\"media-body\"><h3 id=\"event-{{event.id}}\" class=\"thread-item__title\"><strong> <a lmo-href-for=\"event.actor()\" class=\"thread-item__title-link\">{{ event.actorName() }}</a> </strong><span translate=\"discussion_moved_item.discussion_moved\" translate-value-group=\"{{group.name}}\"></span><timeago timestamp=\"event.createdAt\" class=\"timeago--inline\"></timeago></h3></div></div></div>");
$templateCache.put("generated/components/thread_page/activity_card/motion_closed.html","<div ng-controller=\"MotionClosedItemController\" class=\"thread-item\"><div class=\"thread-item__header media\"><div class=\"media-left\"><div class=\"thread-item__proposal-icon\"></div></div><div class=\"media-body\"><h3 id=\"event-{{event.id}}\" class=\"thread-item__title\"><span translate=\"proposal_closed_item.proposal_closed\" translate-values=\"{title: proposal.name}\"></span><timeago timestamp=\"proposal.closingAt\" class=\"timeago--inline\"></timeago></h3></div></div></div>");
$templateCache.put("generated/components/thread_page/activity_card/motion_closed_by_user.html","<div ng-controller=\"MotionClosedItemController\" class=\"thread-item\"><div class=\"thread-item__header media\"><div class=\"media-left\"><div class=\"thread-item__proposal-icon\"></div></div><div class=\"media-body\"><h3 class=\"thread-item__title\"><strong> <a lmo-href-for=\"event.actor()\" class=\"thread-item__title-link\">{{ event.actorName() }}</a> </strong><span id=\"event-{{event.id}}\" translate=\"proposal_closed_by_user_item.proposal_closed_by_user\" translate-value-title=\"{{proposal.name}}\"></span><timeago timestamp=\"proposal.createdAt\" class=\"timeago--inline\"></timeago></h3></div></div></div>");
$templateCache.put("generated/components/thread_page/activity_card/motion_edited.html","<div ng-controller=\"MotionEditedItemController\" class=\"thread-item\"><div class=\"thread-item__header media\"><div class=\"media-left\"><div class=\"thread-item__proposal-icon\"></div></div><div class=\"media-body\"><h3 id=\"event-{{event.id}}\" class=\"thread-item__title\"><strong> <a lmo-href-for=\"event.actor()\" class=\"thread-item__title-link\">{{ event.actorName() }}</a> </strong><span translate=\"proposal_edited_item.{{translationKey}}\" translate-values=\"{time: closingAt, title: title}\"></span></h3></div></div><div class=\"thread-item__footer\"><div class=\"thread-item__actions\"><timeago timestamp=\"event.createdAt\"></timeago></div></div></div>");
$templateCache.put("generated/components/thread_page/activity_card/motion_outcome_created.html","<div ng-controller=\"MotionOutcomeCreatedItemController\" class=\"thread-item\"><div class=\"thread-item__header media\"><div class=\"media-left\"><user_avatar user=\"event.actor()\" size=\"small\"></user_avatar></div><div class=\"media-body\"><h3 id=\"event-{{event.id}}\" class=\"thread-item__title\"><strong> <a lmo-href-for=\"event.actor()\" class=\"thread-item__title-link\">{{ event.actorName() }}</a> </strong><span translate=\"proposal_outcome_created_item.proposal_outcome_created\"></span></h3></div></div><div marked=\"proposal.outcome\" class=\"thread-item__body lmo-markdown-wrapper\"></div><div class=\"thread-item__footer\"><div class=\"thread-item__actions\"><timeago timestamp=\"event.createdAt\"></timeago></div></div></div>");
$templateCache.put("generated/components/thread_page/activity_card/motion_outcome_updated.html","<div ng-controller=\"MotionOutcomeUpdatedItemController\" class=\"thread-item\"><div class=\"thread-item__header media\"><div class=\"media-left\"><user_avatar user=\"event.actor()\" size=\"small\"></user_avatar></div><div class=\"media-body\"><h3 id=\"event-{{event.id}}\" class=\"thread-item__title\"><strong> <a lmo-href-for=\"event.actor()\" class=\"thread-item__title-link\">{{ event.actorName() }}</a> </strong><span translate=\"proposal_outcome_updated_item.proposal_outcome_updated\"></span></h3></div></div><div marked=\"proposal.outcome\" class=\"thread-item__body lmo-markdown-wrapper\"></div><div class=\"thread-item__footer\"><div class=\"thread-item__actions\"><timeago timestamp=\"event.createdAt\"></timeago></div></div></div>");
$templateCache.put("generated/components/thread_page/activity_card/new_comment.html","<div ng_controller=\"NewCommentItemController\" id=\"comment-{{comment.id}}\" class=\"thread-item thread-item--comment\"><md-menu ng-if=\"showContextMenu()\" md-position-mode=\"target-right target\" class=\"lmo-dropdown-menu lmo-no-print pull-right\"><md-button ng-click=\"$mdMenu.open()\" class=\"thread-item__dropdown-button\"><div translate=\"new_comment_item.context_menu.aria_label\" class=\"sr-only\"></div><i class=\"material-icons\">expand_more</i></md-button><md-menu-content><md-menu-item ng-if=\"canEditComment()\" class=\"thread-item__edit-link\"><md-button ng-click=\"editComment()\" translate=\"common.action.edit\"></md-button></md-menu-item><md-menu-item ng-if=\"canDeleteComment()\" class=\"thread-item__delete-link\"><md-button ng-click=\"deleteComment()\" translate=\"common.action.delete\"></md-button></md-menu-item></md-menu-content></md-menu><outlet name=\"before-comment-dropdown\" model=\"comment\" class=\"lmo-relative pull-right\"></outlet><div class=\"thread-item__header media\"><div class=\"media-left\"><user_avatar user=\"comment.author()\" size=\"small\"></user_avatar></div><div class=\"media-body\"><h3 ng-if=\"comment.parentId\" id=\"event-{{event.id}}\" class=\"new-comment__in-reply-to\"><strong> <a lmo-href-for=\"comment.author()\">{{ comment.authorName() }}</a> </strong><span translate=\"new_comment_item.in_reply_to\" translate-value-recipient=\"{{comment.parentAuthorName}}\"></span></h3><h3 ng-if=\"!comment.parentId\" class=\"new-comment__author-name\"> <a lmo-href-for=\"comment.author()\">{{ comment.authorName() }}</a> </h3><div ng-if=\"!comment.parentId\" id=\"event-{{event.id}}\" translate=\"new_comment_item.aria_label\" translate-value-author=\"{{comment.authorName()}}\" class=\"sr-only\"></div></div></div><div marked=\"comment.cookedBody()\" class=\"thread-item__body new-comment__body lmo-markdown-wrapper\"></div><translation ng-if=\"translation\" translation=\"translation\" field=\"body\" class=\"thread-item__body\"></translation><outlet name=\"after-comment-body\" model=\"comment\"></outlet><div class=\"thread-item__attachments\"><div ng-repeat=\"attachment in comment.attachments() | orderBy:\'createdAt\' track by attachment.id\" class=\"thread-item__attachment\"><attachment_preview attachment=\"attachment\" mode=\"thread\"></attachment_preview></div></div><div class=\"thread-item__footer new-comment__footer\"><div ng-if=\"showCommentActions()\" class=\"thread-item__actions\"> <button tabindex=\"0\" type=\"button\" translate=\"common.action.like\" ng-show=\"!currentUserLikesIt()\" ng-click=\"like()\" class=\"thread-item__action thread-item__action--like lmo-no-print\"></button>  <button tabindex=\"0\" type=\"button\" translate=\"common.action.unlike\" ng-click=\"unlike()\" ng-show=\"currentUserLikesIt()\" class=\"thread-item__action thread-item__action--unlike lmo-no-print\"></button>  <span aria-hidden=\"true\" class=\"lmo-no-print\">·</span>  <button translate=\"common.action.reply\" type=\"button\" ng_click=\"reply()\" class=\"thread-item__action thread-item__action--reply lmo-no-print\"></button> <translate_button model=\"comment\" showdot=\"true\" class=\"lmo-no-print\"></translate_button> <span aria-hidden=\"true\" class=\"lmo-no-print\">·</span> </div><div class=\"thread-item__actions\"> <a lmo-href-for=\"comment\"> <timeago timestamp=\"comment.createdAt\"></timeago> </a> <span ng-if=\"comment.edited()\" class=\"md-caption\"><span aria-hidden=\"true\">·</span> <button ng-click=\"showRevisionHistory()\" translate=\"new_comment_item.edited\" class=\"thread-item__action--view-edits\"></button> </span></div><div class=\"clearfix\"></div><outlet name=\"after-comment-event\" model=\"comment\"></outlet><div ng_show=\"anybodyLikesIt()\" class=\"thread-item__liked-by\">{{ likedBySentence }}</div></div></div>");
$templateCache.put("generated/components/thread_page/activity_card/new_motion.html","<div ng_controller=\"NewMotionItemController\" class=\"thread-item\"><div class=\"thread-item__header media\"><div class=\"media-left\"><user_avatar user=\"event.actor()\" size=\"small\"></user_avatar></div><div class=\"media-body\"><h3 id=\"event-{{event.id}}\" class=\"thread-item__title\"><strong> <a lmo-href-for=\"event.actor()\" class=\"thread-item__title-link\">{{ event.actorName() }}</a> </strong> <span translate=\"new_proposal_item.author_created_proposal\" translate-value-title=\"{{proposal.name}}\"></span> <timeago timestamp=\"event.createdAt\" class=\"timeago--inline\"></timeago></h3></div></div></div>");
$templateCache.put("generated/components/thread_page/activity_card/new_vote.html","<div ng-controller=\"NewVoteItemController\" class=\"thread-item\"><div class=\"thread-item__header media\"><div class=\"media-left\"><vote_icon position=\"vote.position\"></vote_icon></div><div class=\"media-body\"><h3 ng-if=\"!vote.hasStatement()\" id=\"event-{{event.id}}\" class=\"thread-item__title\"><strong> <a lmo-href-for=\"vote.author()\" class=\"thread-item__title-link\">{{vote.authorName()}}</a> </strong><span translate=\"new_vote_item.{{vote.positionVerb()}}\"></span><timeago timestamp=\"vote.createdAt\" class=\"timeago--inline\"></timeago></h3><h3 ng-if=\"vote.hasStatement()\" id=\"event-{{event.id}}\" class=\"thread-item__title\"><strong> <a lmo-href-for=\"vote.author()\" class=\"thread-item__title-link\">{{vote.authorName()}}</a> </strong><span translate=\"new_vote_item.{{vote.positionVerb()}}_with_statement\"></span><span marked=\"vote.statement\" class=\"thread-item__vote-statement\"></span><translation ng-if=\"translation\" translation=\"translation\" field=\"statement\"></translation></h3></div><div class=\"thread-item__footer\"> <timeago timestamp=\"vote.createdAt\" class=\"timeago--inline\"></timeago> <translate_button ng-if=\"vote.hasStatement()\" model=\"vote\" showdot=\"true\"></translate_button></div></div></div>");
$templateCache.put("generated/components/thread_page/activity_card/outcome_created.html","<div ng_controller=\"OutcomeCreatedItemController\" class=\"thread-item\"><div class=\"thread-item__header media\"><div class=\"media-left\"><user_avatar user=\"event.actor()\" size=\"small\"></user_avatar></div><div class=\"media-body\"><h3 id=\"event-{{event.id}}\" class=\"thread-item__title\"> <span translate=\"poll_thread_item.outcome_created\" translate-values=\"{author: event.actor().name}\"></span> </h3><p>{{ outcome.statement }}</p><timeago timestamp=\"event.createdAt\" class=\"timeago--inline\"></timeago></div></div></div>");
$templateCache.put("generated/components/thread_page/activity_card/poll_closed_by_user.html","<div ng-controller=\"PollClosedByUserItemController\" class=\"thread-item\"><div class=\"thread-item__header media\"><div class=\"media-left\"><user_avatar user=\"event.actor()\" size=\"small\"></user_avatar></div><div class=\"media-body\"><h3 id=\"event-{{event.id}}\" class=\"thread-item__title\"> <span translate=\"poll_thread_item.{{poll.pollType}}.{{event.kind}}\" translate-values=\"{author: event.actor().name, title: poll.title}\"></span> <timeago timestamp=\"event.createdAt\" class=\"timeago--inline\"></timeago></h3></div></div></div>");
$templateCache.put("generated/components/thread_page/activity_card/poll_created.html","<div ng_controller=\"PollCreatedItemController\" class=\"thread-item\"><div class=\"thread-item__header media\"><div class=\"media-left\"><user_avatar user=\"event.actor()\" size=\"small\"></user_avatar></div><div class=\"media-body\"><h3 id=\"event-{{event.id}}\" class=\"thread-item__title\"> <span translate=\"poll_thread_item.{{poll.pollType}}.{{event.kind}}\" translate-values=\"{author: event.actor().name, title: poll.title}\"></span> <timeago timestamp=\"event.createdAt\" class=\"timeago--inline\"></timeago></h3></div></div></div>");
$templateCache.put("generated/components/thread_page/activity_card/poll_edited.html","<div ng-controller=\"PollEditedItemController\" class=\"thread-item\"><div class=\"thread-item__header media\"><div class=\"media-left\"><user_avatar ng-if=\"event.actor()\" user=\"event.actor()\" size=\"small\"></user_avatar></div><div class=\"media-body\"><h3 id=\"event-{{event.id}}\" class=\"thread-item__title\"> <span translate=\"poll_thread_item.{{poll.pollType}}.{{event.kind}}\" translate-values=\"{author: event.actor().name, title: poll.title}\"></span> <timeago timestamp=\"event.createdAt\" class=\"timeago--inline\"></timeago></h3></div></div></div>");
$templateCache.put("generated/components/thread_page/activity_card/poll_expired.html","<div ng-controller=\"PollExpiredItemController\" class=\"thread-item\"><div class=\"thread-item__header media\"><div class=\"media-left\"><user_avatar user=\"poll.author()\" size=\"small\"></user_avatar></div><div class=\"media-body\"><h3 id=\"event-{{event.id}}\" class=\"thread-item__title\"> <span translate=\"poll_thread_item.{{poll.pollType}}.{{event.kind}}\" translate-values=\"{title: poll.title}\"></span> <timeago timestamp=\"event.createdAt\" class=\"timeago--inline\"></timeago></h3></div></div></div>");
$templateCache.put("generated/components/thread_page/activity_card/stance_created.html","<div ng-controller=\"StanceCreatedItemController\" class=\"thread-item\"><div layout=\"row\" class=\"thread-item__header media lmo-flex\"><div class=\"media-left\"><user_avatar user=\"stance.participant()\" size=\"small\"></user_avatar></div><div layout=\"column\" class=\"media-body lmo-flex\"> <h3 id=\"event-{{event.id}}\" translate=\"poll_thread_item.{{stance.poll().pollType}}.{{event.kind}}\" translate-values=\"{author: stance.participant().name}\" class=\"thread-item__title\"></h3> <poll_common_stance_choice ng-repeat=\"choice in stance.stanceChoices()\" ng-if=\"choice.score &gt; 0\" stance_choice=\"choice\"></poll_common_stance_choice><div marked=\"stance.reason\" ng-if=\"stance.reason\" class=\"lmo-markdown-wrapper\"></div><timeago timestamp=\"event.createdAt\" class=\"timeago--inline\"></timeago></div></div></div>");
$templateCache.put("generated/components/thread_page/activity_card/thread_item.html","<div id=\"{{event.sequenceId}}\" ng_include=\"\'thread_page/activity_card/\'+ event.kind+ \'.html\'\" class=\"thread-item\"></div>");
$templateCache.put("generated/components/thread_page/close_proposal_form/close_proposal_form.html","<md-dialog class=\"close-proposal-form\"><form ng-submit=\"submit()\" ng-disabled=\"proposal.processing\"><div ng-show=\"isDisabled\" class=\"lmo-disabled-form\"></div><md-toolbar><div class=\"md-toolbar-tools lmo-flex__space-between\"><h1 translate=\"close_proposal_form.close_proposal\" class=\"lmo-h1\"></h1><material_modal_header_cancel_button></material_modal_header_cancel_button></div></md-toolbar><div class=\"md-dialog-content md-body-1\"><p translate=\"close_proposal_form.helptext\"></p><div class=\"lmo-md-actions\"><md-button type=\"button\" ng-click=\"$close()\" translate=\"common.action.cancel\"></md-button><md-button type=\"submit\" translate=\"close_proposal_form.close_proposal_submit\" class=\"md-primary md-raised close-proposal-form__submit-btn\"></md-button></div></div></form></md-dialog>");
$templateCache.put("generated/components/thread_page/comment_form/comment_form.html","<section aria-labelledby=\"comment-form-title\" class=\"comment-form\"> <h2 translate=\"comment_form.aria_label\" class=\"lmo-card-heading\" id=\"comment-form-title\"></h2> <form ng-if=\"showCommentForm()\" ng-paste=\"handlePaste($event)\" ng_submit=\"submit()\"><div ng-show=\"isDisabled\" class=\"lmo-disabled-form\"></div><input type=\"hidden\" ng-model=\"comment.usesMarkdown\"><outlet name=\"before-comment-form-textarea\" model=\"comment\"></outlet><span translate=\"comment_form.in_reply_to\" translate-value-name=\"{{comment.parent().authorName()}}\" ng-show=\"comment.parent().authorName()\" class=\"comment-form__in-reply-to\"></span><div class=\"lmo-relative comment-form__textarea-wrapper\"><textarea ng-disabled=\"isDisabled\" msd-elastic=\"true\" aria-labelledby=\"comment-form-title\" name=\"body\" placeholder=\"Say something...\" ng_model=\"comment.body\" mentio=\"mentio\" mentio-trigger-char=\"\'@\'\" mentio_items=\"mentionables\" mentio-template-url=\"generated/components/thread_page/comment_form/mentio_menu.html\" mentio-search=\"fetchByNameFragment(term)\" ng-model-options=\"{ updateOn: \'default blur\', debounce: {\'default\': 150, \'blur\': 0} }\" class=\"lmo-textarea form-control comment-form__comment-field lmo-primary-form-input\"></textarea><emoji_picker target-selector=\"bodySelector\" class=\"lmo-action-dock\"></emoji_picker></div><validation_errors subject=\"comment\" field=\"body\"></validation_errors><outlet name=\"after-comment-form-textarea\" model=\"comment\"></outlet><div ng-if=\"threadIsPublic()\" translate=\"comment_form.public_privacy_notice\" class=\"comment-form__privacy-notice\"></div><div ng-if=\"threadIsPrivate()\" translate=\"comment_form.private_privacy_notice\" translate-value-group-name=\"{{comment.group().fullName}}\" class=\"comment-form__privacy-notice\"></div><attachment_preview attachment=\"attachment\" mode=\"thumb\" ng-repeat=\"attachment in comment.newAttachments() | orderBy: \'id\' track by attachment.id\"></attachment_preview><div class=\"thread-comment-form-actions lmo-flex lmo-flex__space-between\"><div ng-if=\"submitIsDisabled\"></div><div layout=\"row\" ng-if=\"!submitIsDisabled\" class=\"comment-form__actions--left lmo-flex\"><md-button translate=\"comment_form.cancel_reply\" ng-show=\"comment.parentId\" ng-click=\"comment.parentId = null\" class=\"comment-form-button\"></md-button><md-button translate=\"common.action.cancel\" ng-click=\"cancel($event)\" ng-if=\"!comment.isNew()\" id=\"post-comment-cancel\"></md-button><a lmo-href=\"/markdown\" ng-if=\"!comment.parentId\" target=\"_blank\" title=\"{{ \'common.formatting_help.title\' | translate }}\" class=\"comment-form__formatting md-button md-accent lmo-hide-on-xs\"><span translate=\"common.formatting_help.label\"></span></a></div><div layout=\"row\" class=\"comment-form__actions--right lmo-flex\"><outlet name=\"after-comment-form\" model=\"comment\"></outlet><md-button type=\"submit\" ng-disabled=\"submitIsDisabled\" translate=\"comment_form.submit_button.label\" class=\"md-primary md-raised comment-form__submit-button\"></md-button><md_attachment_form model=\"comment\" show-label=\"true\"></md_attachment_form></div></div><validation_errors subject=\"comment\" field=\"file\"></validation_errors></form><div ng-if=\"!showCommentForm()\" class=\"comment-form__join-actions\"><join_group_button group=\"comment.discussion().group()\" ng-if=\"isLoggedIn()\" block=\"true\"></join_group_button><button translate=\"comment_form.sign_in\" ng-click=\"signIn()\" ng-if=\"!isLoggedIn()\" class=\"btn btn-block comment-form__sign-in-btn\"></button></div></section>");
$templateCache.put("generated/components/thread_page/comment_form/delete_comment_form.html","<form ng-submit=\"submit()\" ng-disabled=\"comment.processing\" name=\"deleteCommentForm\" class=\"delete-comment-form\"><md-toolbar><div class=\"md-toolbar-tools lmo-flex__space-between\"><h1 translate=\"comment_form.delete_comment\" class=\"lmo-h1\"></h1><material_modal_header_cancel_button></material_modal_header_cancel_button></div></md-toolbar><div class=\"md-dialog-content md-body-1\"><p translate=\"comment_form.confirm_delete_message\" class=\"comment-form-delete-message\"></p><div class=\"lmo-md-actions\"><md-button type=\"button\" ng-click=\"$close()\" translate=\"common.action.cancel\" class=\"lmo-btn--cancel\"></md-button><md-button type=\"submit\" translate=\"comment_form.confirm_delete\" class=\"md-raised md-primary delete-comment-form__delete-button\"></md-button></div></div></form>");
$templateCache.put("generated/components/thread_page/comment_form/edit_comment_form.html","<div class=\"edit-comment-form\"><form ng-submit=\"submit()\" ng-disabled=\"comment.processing\" name=\"commentForm\"><md-toolbar><div class=\"md-toolbar-tools lmo-flex__space-between\"><h1 translate=\"comment_form.edit_comment\" class=\"lmo-h1 modal-title\"></h1><material_modal_header_cancel_button></material_modal_header_cancel_button></div></md-toolbar><div class=\"md-dialog-content\"><div class=\"lmo-relative\"><textarea name=\"body\" msd-elastic=\"true\" ng_model=\"comment.body\" mentio=\"mentio\" mentio-trigger-char=\"\'@\'\" mentio_items=\"mentionables\" mentio-template-url=\"generated/components/thread_page/comment_form/mentio_menu.html\" mentio-search=\"fetchByNameFragment(term)\" mentio-id=\"comment-field\" ng-model-options=\"{ updateOn: \'default blur\', debounce: {\'default\': 300, \'blur\': 0} }\" class=\"lmo-textarea form-control edit-comment-form__comment-field\"></textarea><emoji_picker target-selector=\"bodySelector\" class=\"lmo-action-dock\"></emoji_picker></div><div class=\"lmo-md-action\"> <md-button type=\"submit\" translate=\"common.action.save_changes\" class=\"md-primary md-raised comment-form__submit-btn\"></md-button> </div></div></form></div>");
$templateCache.put("generated/components/thread_page/comment_form/mentio_menu.html","<ul class=\"list-group user-search\"><li mentio_menu_item=\"user\" ng_repeat=\"user in items track by user.id\" class=\"list-group-item\"><div class=\"media-left\"><user_avatar user=\"user\" size=\"small\"></user_avatar></div><div class=\"mentionable-content\"><div class=\"mentionable-name\">{{user.name}}</div><div class=\"mentionable-username\">{{user.username}}</div></div></li></ul>");
$templateCache.put("generated/components/thread_page/context_panel/context_panel.html","<section aria-label=\"{{ \'thread_context.aria_label\' | translate }}\" class=\"context-panel\"><div class=\"context-panel__top\"><div in-view=\"showLintel(!$inview)\" class=\"context-panel__h1 lmo-flex__grow\"><h1 ng-if=\"!translation\" class=\"lmo-h1 context-panel__heading\">{{discussion.title}}</h1><h1 ng-if=\"translation\" class=\"lmo-h1\"><translation translation=\"translation\" field=\"title\"></translation></h1></div><div class=\"context-panel__thread-actions pull-right lmo-no-print\"><outlet name=\"before-thread-actions\" model=\"discussion\" class=\"context-panel__before-thread-actions\"></outlet><md-menu ng-if=\"showContextMenu()\" md-position-mode=\"target-right target\" class=\"lmo-dropdown-menu\"><md-button ng-click=\"$mdMenu.open()\" class=\"context-panel__dropdown-button\"><div translate=\"thread_context.thread_options\" class=\"sr-only\"></div><i class=\"material-icons\">expand_more</i></md-button><md-menu-content><md-menu-item ng-if=\"canChangeVolume()\" class=\"context-panel__dropdown-options--email-settings\"><md-button ng-click=\"openChangeVolumeForm()\" translate=\"thread_context.email_settings\"></md-button></md-menu-item><md-menu-item ng-if=\"canEditThread()\" class=\"context-panel__dropdown-options--edit\"><md-button ng-click=\"editThread()\" translate=\"thread_context.edit\"></md-button></md-menu-item><md-menu-item ng-show=\"!discussion.isMuted()\" class=\"context-panel__dropdown-options--mute\"><md-button ng-click=\"muteThread()\" translate=\"volume_levels.mute\"></md-button></md-menu-item><md-menu-item ng-show=\"discussion.isMuted()\" class=\"context-panel__dropdown-options--unmute\"><md-button ng-click=\"unmuteThread()\" translate=\"volume_levels.unmute\"></md-button></md-menu-item><md-menu-item ng-if=\"canMoveThread()\" class=\"context-panel__dropdown-options--move\"><md-button ng-click=\"moveThread()\" translate=\"thread_context.move_thread\"></md-button></md-menu-item><md-menu-item><md-button ng-click=\"requestPagePrinted()\" translate=\"thread_context.print_thread\"></md-button></md-menu-item><md-menu-item ng-if=\"canDeleteThread()\" class=\"context-panel__dropdown-options--delete\"><md-button ng-click=\"deleteThread()\" translate=\"thread_context.delete_thread\"></md-button></md-menu-item></md-menu-content></md-menu></div></div><div class=\"context-panel__details md-body-1\"><span translate=\"discussion.started_by\" translate-value-name=\"{{::discussion.authorName()}}\"></span> <timeago timestamp=\"::discussion.createdAt\" class=\"nowrap\"></timeago>  <translate_button model=\"discussion\" showdot=\"true\" class=\"lmo-card-minor-action\"></translate_button> <span aria-hidden=\"true\">·</span> <span ng-show=\"discussion.private\" class=\"nowrap context-panel__discussion-privacy--private\"> <i class=\"fa fa-lock\"></i> <span translate=\"common.privacy.private\"></span></span>  <span ng-show=\"!discussion.private\" class=\"nowrap context-panel__discussion-privacy--public\"> <i class=\"fa fa-globe\"></i> <span translate=\"common.privacy.public\"></span></span> <outlet name=\"after-thread-title\" model=\"discussion\"></outlet></div><div ng-if=\"!translation\" marked=\"discussion.cookedDescription()\" aria-label=\"{{ \'thread_context.aria_label\' | translate }}\" class=\"context-panel__description lmo-markdown-wrapper\"></div><translation ng-if=\"translation\" translation=\"translation\" field=\"description\" class=\"lmo-markdown-wrapper\"></translation><div ng-if=\"discussion.hasAttachments()\" class=\"context-panel__attachments\"><div ng-repeat=\"attachment in discussion.attachments() | orderBy:\'createdAt\' track by attachment.id\" class=\"context-panel__attachment\"><attachment_preview attachment=\"attachment\" mode=\"context\"></attachment_preview></div></div><div class=\"context-panel__actions\"><md-button ng-click=\"scrollToCommentForm()\" translate=\"thread_context.add_comment\" ng-if=\"canAddComment()\" class=\"md-accent context-panel__add-comment lmo-no-print\"></md-button><md-button ng-click=\"editThread()\" translate=\"thread_context.edit_thread\" ng-if=\"canEditThread()\" class=\"md-accent context-panel__edit lmo-no-print\"></md-button></div></section>");
$templateCache.put("generated/components/thread_page/current_proposal_card/current_proposal_card.html","<section aria-labelledby=\"current-proposal-card-heading\" class=\"current-proposal-card\"><div class=\"lmo-card-padding\"><h2 translate=\"common.models.proposal\" tabindex=\"0\" class=\"lmo-card-heading\" id=\"current-proposal-card-heading\"></h2><loading_content ng-if=\"loading\"></loading_content><proposal_expanded proposal=\"proposal\" ng-if=\"!loading\"></proposal_expanded></div></section>");
$templateCache.put("generated/components/thread_page/decision_tools_card/decision_tools_card.html","<div class=\"decision-tools-card lmo-card lmo-no-print\"><div class=\"lmo-flex lmo-flex__space-between\"><h2 translate=\"decision_tools_card.title\" class=\"lmo-card-heading decision-tools-card__title\"></h2><div translate=\"decision_tools_card.help_text\" class=\"poll-common-helptext\"></div></div><poll_common_start_form discussion=\"discussion\"></poll_common_start_form></div>");
$templateCache.put("generated/components/thread_page/extend_proposal_form/extend_proposal_form.html","<form ng-submit=\"submit()\" ng-disabled=\"proposal.processing\" class=\"extend-proposal-form\"><div ng-show=\"isDisabled\" class=\"lmo-disabled-form\"></div><md-toolbar><div class=\"md-toolbar-tools lmo-flex__space-between\"><h1 translate=\"extend_proposal_form.title\" class=\"lmo-h1\"></h1><material_modal_header_cancel_button></material_modal_header_cancel_button></div></md-toolbar><div class=\"md-dialog-content md-body-1\"><closing_at_field proposal=\"proposal\"></closing_at_field><div class=\"lmo-md-actions\"><md-button ng-click=\"cancel($event)\" translate=\"common.action.cancel\"></md-button><md-button type=\"submit\" translate=\"extend_proposal_form.submit\" class=\"md-primary md-raised\"></md-button></div></div></form>");
$templateCache.put("generated/components/thread_page/position_buttons_panel/position_buttons_panel.html","<div class=\"blank lmo-no-print\"><div ng-if=\"showPositionButtons()\" class=\"position-buttons-panel\"><h4 translate=\"position_buttons_panel.heading\" class=\"lmo-card-subheading\"></h4><div class=\"position_buttons_panel__buttons\"><button ng-click=\"select(\'yes\')\" class=\"position-button position-button--yes\"><div translate=\"vote_form.i_agree\" class=\"sr-only\"></div></button><button ng-click=\"select(\'abstain\')\" class=\"position-button position-button--abstain\"><div translate=\"vote_form.i_abstain\" class=\"sr-only\"></div></button><button ng-click=\"select(\'no\')\" class=\"position-button position-button--no\"><div translate=\"vote_form.i_disagree\" class=\"sr-only\"></div></button><button ng-click=\"select(\'block\')\" class=\"position-button position-button--block\"><div translate=\"vote_form.i_block\" class=\"sr-only\"></div></button></div><outlet name=\"after-position-buttons\" model=\"proposal\"></outlet></div></div>");
$templateCache.put("generated/components/thread_page/previous_proposals_card/previous_proposals_card.html","<section aria-labelledby=\"previous-proposals-card-heading\" ng-show=\"anyProposals()\" class=\"previous-proposals-card\"><div class=\"lmo-card-heading-padding\"><h2 translate=\"previous_proposals_card.heading\" class=\"lmo-card-heading\" id=\"previous-proposals-card-heading\"></h2></div><proposal_accordian model=\"discussion\" selected-proposal-id=\"selectedProposalId\"></proposal_accordian></section>");
$templateCache.put("generated/components/thread_page/print_modal/print_modal.html","<div class=\"print-modal lmo-no-print\"><div class=\"modal-body\"><h1 translate=\"print_modal.load_discussion\" class=\"lmo-h1\"></h1><loading></loading></div></div>");
$templateCache.put("generated/components/thread_page/proposal_collapsed/proposal_collapsed.html","<a id=\"proposal-{{proposal.key}}\" class=\"proposal-collapsed\"><div class=\"sr-only\">{{proposal.name}}.<proposal_closing_time proposal=\"proposal\">.</proposal_closing_time></div><div aria-hidden=\"true\" class=\"screen-only proposal-collapsed__flex\"><pie_with_position proposal=\"proposal\"></pie_with_position><div class=\"proposal-collapsed__body\"><div class=\"proposal-collapsed__title\">{{proposal.name}}<proposal_closing_time proposal=\"proposal\" class=\"proposal-collapsed__closed-at\">.</proposal_closing_time></div><div ng-if=\"proposal.hasOutcome()\" class=\"proposal-collapsed__outcome\"> <span translate=\"proposal_collapsed.outcome\"></span> <span marked=\"proposal.outcome\"></span></div></div></div></a>");
$templateCache.put("generated/components/thread_page/proposal_expanded/proposal_expanded.html","<div ng-show=\"proposal\" id=\"proposal-{{proposal.key}}\" class=\"proposal-expanded\"><proposal_actions_dropdown proposal=\"proposal\" ng-if=\"showActionsDropdown()\"></proposal_actions_dropdown><a lmo-href-for=\"proposal\" class=\"proposal-expanded__proposal-name-link\"><h3 class=\"proposal-expanded__proposal-name\">{{proposal.name}}</h3><h3 ng-if=\"translation\"><translation translation=\"translation\" field=\"name\"></translation></h3></a><div class=\"proposal-expanded__started-by\"><span translate=\"proposal_expanded.started_by\" translate-values=\"{\'name\': proposal.authorName()}\"></span> <span aria-hidden=\"true\">·</span> <a lmo-href-for=\"proposal\"> <proposal_closing_time proposal=\"proposal\"></proposal_closing_time> </a><translate_button model=\"proposal\" showdot=\"true\" class=\"lmo-link-color\"></translate_button></div><div marked=\"proposal.cookedDescription()\" class=\"proposal-expanded-description lmo-markdown-wrapper\"></div><div ng-repeat=\"attachment in proposal.attachments() | orderBy:\'createdAt\' track by attachment.id\" class=\"proposal-expanded__attachments\"><attachment_preview attachment=\"attachment\" mode=\"context\"></attachment_preview></div><translation ng-if=\"translation &amp;&amp; proposal.description\" translation=\"translation\" field=\"description\"></translation><proposal_outcome_panel ng-if=\"showOutcomePanel()\" proposal=\"proposal\"></proposal_outcome_panel><translation ng-if=\"translation &amp;&amp; proposal.outcome\" translation=\"translation\" field=\"outcome\"></translation><position_buttons_panel proposal=\"proposal\"></position_buttons_panel><proposal_positions_panel proposal=\"proposal\"></proposal_positions_panel><a ng-click=\"collapse()\" translate=\"proposal_expanded.collapse\" ng-show=\"canCollapse\" class=\"proposal-expanded__collapse pull-right\"></a><div class=\"clearfix\"></div></div>");
$templateCache.put("generated/components/thread_page/proposal_outcome_form/proposal_outcome_form.html","<form ng-submit=\"submit()\" ng-disabled=\"proposal.processing\" class=\"proposal-form\"><div ng-show=\"isDisabled\" class=\"lmo-disabled-form\"></div><md-toolbar><div class=\"md-toolbar-tools lmo-flex__space-between\"><h1 ng-if=\"!hasOutcome\" translate=\"proposal_outcome_form.set_outcome\" class=\"lmo-h1\"></h1><h1 ng-if=\"hasOutcome\" translate=\"proposal_outcome_form.update_outcome\" class=\"lmo-h1\"></h1><material_modal_header_cancel_button></material_modal_header_cancel_button></div></md-toolbar><div class=\"md-dialog-content md-body-1\"><form_errors record=\"proposal\"></form_errors><fieldset><div class=\"lmo-form-group\"><label for=\"proposal-outcome-field\" translate=\"proposal_outcome_form.outcome_label\"></label><div class=\"lmo-relative\"><textarea placeholder=\"{{ \'proposal_outcome_form.outcome_placeholder\' | translate }}\" ng-model=\"proposal.outcome\" ng-required=\"true\" mention-field=\"true\" class=\"lmo-textarea proposal-form__outcome-field form-control\" id=\"proposal-outcome-field\"></textarea><emoji_picker target-selector=\"outcomeSelector\" class=\"lmo-action-dock\"></emoji_picker></div></div></fieldset><div class=\"lmo-md-actions\"><md-button ng-click=\"$close()\" type=\"button\" translate=\"common.action.cancel\" class=\"proposal-outcome-form__cancel-btn\"></md-button><md-button type=\"submit\" translate=\"proposal_outcome_form.publish_outcome\" class=\"md-raised md-primary proposal-outcome-form__publish-outcome-btn\"></md-button></div></div></form>");
$templateCache.put("generated/components/thread_page/proposal_outcome_panel/proposal_outcome_panel.html","<div class=\"proposal-outcome-panel\"><h4 translate=\"proposal_outcome_panel.heading\" class=\"lmo-card-subheading\"></h4><div ng-if=\"canCreateOutcome()\" class=\"lmo-wrap\"><p translate=\"proposal_outcome_panel.set_outcome_hint\" class=\"lmo-hint-text\"></p><md-button ng-click=\"openProposalOutcomeForm()\" translate=\"proposal_outcome_panel.set_outcome_btn\" class=\"md-primary md-raised proposal-outcome-panel__set-outcome-btn\"></md-button></div><p ng-show=\"proposal.hasOutcome()\" marked=\"proposal.outcome\" class=\"proposal-outcome-panel__outcome lmo-markdown-wrapper\"></p><md-button ng-if=\"canUpdateOutcome()\" ng-click=\"openProposalOutcomeForm()\" translate=\"proposal_outcome_panel.edit_outcome_link\" class=\"md-accent proposal-outcome-panel__edit-outcome-link\"></md-button></div>");
$templateCache.put("generated/components/thread_page/proposal_positions_panel/proposal_positions_panel.html","<div class=\"proposal-positions-panel\"><h4 translate=\"proposal_positions_panel.heading\" class=\"lmo-card-subheading\"></h4><div class=\"proposal-pie-chart media\"><div class=\"proposal-pie-chart__pie media-left\"><pie_chart votes=\"proposal.voteCounts\" diameter=\"200\" class=\"proposal-positions-panel__pie-chart\"></pie_chart></div><div class=\"proposal-pie-chart__legend media-body\"><table role=\"presentation\" id=\"proposal-current-positions\"><caption translate=\"proposal_expanded.current_votes\" class=\"sr-only\"></caption><tbody><tr ng-repeat=\"position in proposal.positions\"><td><div class=\"proposal-pie-chart__label proposal-pie-chart__label--{{position}}\">{{ proposal.voteCounts[position] }} {{ \"proposal_position_\"+position | translate}}</div></td></tr></tbody></table></div></div><div translate=\"proposal_pie_chart.participation_statement\" translate-value-percentage=\"{{proposal.percentVoted()}}\" translate-value-number=\"{{proposal.votersCount}}\" translate-value-total=\"{{proposal.membersCount}}\" class=\"proposal-pie-chart__participation-statement\"></div><ul class=\"proposal-positions-panel__list\"><li ng_repeat=\"vote in curatedVotes() track by vote.id\" class=\"proposal-positions-panel__vote media\"><div class=\"media-left\"><vote_icon position=\"vote.position\"></vote_icon></div><div class=\"media-body\"><span class=\"proposal-positions-panel__name-and-position\"><strong> <a lmo-href-for=\"vote.author()\">{{vote.authorName()}}</a> </strong><span translate=\"new_vote_item.{{vote.positionVerb()}}\"></span></span><span ng-if=\"!vote.hasStatement()\">.</span><span ng-if=\"vote.hasStatement()\" class=\"proposal-positions-panel__separator\">:</span><span marked=\"vote.statement\" ng-if=\"vote.hasStatement()\" class=\"proposal-positions-panel__vote-statement\"></span><button ng-if=\"showChangeVoteOption(vote)\" ng-click=\"changeVote()\" class=\"proposal-positions-panel__change-your-vote lmo-no-print\"> <i class=\"fa fa-exchange\"></i> <span translate=\"proposal_positions_panel.change_your_vote\"></span></button></div></li></ul><undecided_panel proposal=\"proposal\" ng-if=\"proposal.hasUndecidedMembers()\"></undecided_panel></div>");
$templateCache.put("generated/components/thread_page/proposals_card/proposals_card.html","<section aria-labelledby=\"proposals-card-heading\" class=\"proposals-card\"><h2 translate=\"discussion.proposals\" class=\"lmo-card-heading\" id=\"proposals-card-heading\"></h2><div ng_repeat=\"proposal in proposalsCard.discussion.proposals() track by proposal.id\" class=\"proposals-card__proposals\"><div ng-if=\"proposalsCard.isExpanded(proposal)\" class=\"active-proposal\"><proposal_expanded proposal=\"proposal\"></proposal_expanded></div><h3 translate=\"discussion.previous_proposals\" ng-show=\"proposalsCard.discussion.proposals().length &gt; 1\"></h3><div ng-if=\"!proposalsCard.isExpanded(proposal)\" ng_click=\"proposalsCard.selectProposal(proposal)\" class=\"inactive-proposal\"><proposal_collapsed proposal=\"proposal\"></proposal_collapsed></div></div></section>");
$templateCache.put("generated/components/thread_page/revision_history_modal/revision_history_modal.html","<div class=\"revision-history-modal\"><loading ng-if=\"loadExecuting\"></loading><md-toolbar><div class=\"md-toolbar-tools lmo-flex__space-between\"><h1 translate=\"{{header}}\" class=\"lmo-h1 modal-title lmo-flex__grow\"></h1><material_modal_header_cancel_button aria-hidden=\"true\"></material_modal_header_cancel_button></div></md-toolbar><div ng-if=\"!loadExecuting\" class=\"md-dialog-content revision-history-modal__body\"><h2 ng-if=\"commentRevision()\" class=\"lmo-h2\">{{model.authorName()}}</h2><div ng-repeat=\"version in model.versions() | orderBy: \'-createdAt\' track by version.id\" id=\"version-{{version.id}}\" class=\"lmo-wrap\"><div class=\"revision-history-modal__revision\"><h2 ng-if=\"discussionRevision()\" translate=\"{{threadTitle(version)}}\" class=\"lmo-h2\"></h2><div class=\"revision-history-modal__thread-details\"><span ng-if=\"commentRevision()\" translate=\"revision_history_modal.posted\"></span><span ng-if=\"discussionRevision()\" translate=\"{{threadDetails(version)}}\" translate-value-name=\"{{version.authorOrEditorName()}}\"></span> <span>{{versionCreatedAt(version.createdAt)}}</span>  <span ng-if=\"version.isCurrent(version)\">(current)</span> </div><div marked=\"revisionBody(version)\" class=\"revision-history-modal__revision-body lmo-markdown-wrapper\"></div></div></div></div></div>");
$templateCache.put("generated/components/thread_page/start_proposal_button/start_proposal_button.html","<div class=\"blank\"><div ng-show=\"canStartProposal()\" class=\"start-proposal-button\"><md-button type=\"button\" ng-click=\"startProposal()\" class=\"md-raised md-primary start-proposal-button__button\"><span translate=\"proposal_form.start_proposal\"></span></md-button></div></div>");
$templateCache.put("generated/components/thread_page/start_proposal_card/start_proposal_card.html","<section aria-label=\"{{ \'start_proposal_card.title\' | translate }}\" class=\"start-proposal-card lmo-no-print\"><h2 translate=\"common.models.proposal\" class=\"lmo-card-heading\"></h2><div translate=\"start_proposal_card.helptext\" class=\"lmo-placeholder lmo-hint-text table-cell\"></div><div class=\"lmo-md-action\"><start_proposal_button discussion=\"discussion\" class=\"table-cell start-proposal-card__button\"></start_proposal_button></div></section>");
$templateCache.put("generated/components/thread_page/undecided_panel/undecided_panel.html","<div class=\"undecided-panel\"><div ng-show=\"!undecidedPanelOpen\"><div class=\"undecided-panel__undecided-count lmo-card-subheading\"> <span>({{proposal.undecidedMembers().length}})</span>  <span translate=\"undecided_panel.undecided\"></span> </div><div class=\"undecided-panel__actions\"><button ng-click=\"showUndecided()\" translate=\"undecided_panel.show\" class=\"undecided-panel__show lmo-btn-link lmo-card-minor-action lmo-no-print\"></button><button ng-click=\"remindUndecided()\" translate=\"undecided_panel.remind\" class=\"undecided-panel__remind lmo-btn-link lmo-card-minor-action lmo-no-print\"></button></div></div><h3 ng-show=\"undecidedPanelOpen\" translate=\"undecided_panel.aria_label\" tabindex=\"0\" class=\"undecided-panel__heading sr-only\"></h3><ul ng-if=\"undecidedPanelOpen\" class=\"undecided-panel__list\"><li ng-repeat=\"user in proposal.undecidedMembers() track by user.id\" class=\"undecided-panel__list-item\"><i class=\"fa fa-question-circle undecided-panel__icon\"></i><span class=\"undecided-panel__user\">{{user.name}}</span></li></ul><button ng-click=\"hideUndecided()\" ng-show=\"undecidedPanelOpen\" translate=\"undecided_panel.hide_undecided\" class=\"undecided-panel__hide-undecided lmo-btn-link lmo-card-minor-action\"></button></div>");
$templateCache.put("generated/components/thread_page/vote_form/vote_form.html","<form ng-submit=\"submit()\" ng-disabled=\"vote.processing\" name=\"voteForm\" class=\"vote-form\"><div ng-show=\"isDisabled\" class=\"lmo-disabled-form\"></div><md-toolbar><div class=\"md-toolbar-tools lmo-flex__space-between\"><h1 translate=\"vote_form.heading\" class=\"lmo-h1 modal-title\"></h1><material_modal_header_cancel_button></material_modal_header_cancel_button></div></md-toolbar><div class=\"md-dialog-content\"><form_errors record=\"vote\"></form_errors><select ng-model=\"vote.position\" class=\"form-control vote-form__select-position\"><option value=\"yes\" translate=\"vote_form.i_agree\"></option><option value=\"abstain\" translate=\"vote_form.i_abstain\"></option><option value=\"no\" translate=\"vote_form.i_disagree\"></option><option value=\"block\" translate=\"vote_form.i_block\"></option></select><fieldset><div class=\"lmo-form-group\"><div class=\"lmo-relative\"><textarea ng-model=\"vote.statement\" placeholder=\"{{ \'vote_form.statement_placeholder\' | translate }}\" autofocus=\"true\" class=\"lmo-textarea vote-form__statement-field form-control lmo-primary-form-input\"></textarea><emoji_picker target-selector=\"statementSelector\" class=\"lmo-action-dock\"></emoji_picker></div><div ng-class=\"{ overlimit: vote.overCharLimit() }\" class=\"chars-left pull-right\">{{ vote.charsLeft() }}</div><div ng-if=\"vote.overCharLimit()\" translate=\"vote_form.statement_over_limit\" class=\"vote-form__statement-over-limit lmo-validation-error\"></div></div></fieldset><div class=\"lmo-md-action\"> <md-button type=\"submit\" ng-disabled=\"vote.overCharLimit()\" translate=\"vote_form.submit_position\" class=\"md-primary md-raised vote-form__submit-btn\"></md-button> </div></div></form>");
$templateCache.put("generated/components/poll/common/action_panel/poll_common_action_panel.html","<div class=\"poll-common-action-panel\"><section ng-if=\"!poll.closedAt\"><div ng-if=\"userHasVoted()\" class=\"md-block\"><div class=\"poll-common-action-panel__change-your-vote\"><h3 translate=\"poll_common.your_response\" class=\"lmo-card-subheading\"></h3><poll_common_stance_choice stance_choice=\"choice\" ng-if=\"choice.score &gt; 0\" ng-repeat=\"choice in stance.stanceChoices()\"></poll_common_stance_choice><div class=\"md-actions lmo-md-actions\"><md-button ng-click=\"openStanceForm()\" translate=\"poll_common.change_your_stance\" aria-label=\"{{ \'poll_common.change_your_stance\' | translate }}\" class=\"md-accent\"></md-button></div></div></div><div ng-if=\"!userHasVoted()\" class=\"md-block\"><poll_common_directive stance=\"stance\" name=\"vote_form\"></poll_common_directive></div></section></div>");
$templateCache.put("generated/components/poll/common/actions_dropdown/poll_common_actions_dropdown.html","<md-menu md-position-mode=\"target-right target\" class=\"lmo-dropdown-menu poll-actions-dropdown pull-right lmo-no-print\"><md-button ng-click=\"$mdMenu.open()\"><i class=\"material-icons\">expand_more</i></md-button><md-menu-content><md-menu-item><md-button ng-click=\"toggleSubscription()\"><span translate=\"common.action.unsubscribe\" ng-if=\"poll.subscribed\"></span><span translate=\"common.action.subscribe\" ng-if=\"!poll.subscribed\"></span></md-button></md-menu-item><md-menu-item ng-if=\"canSharePoll()\"><md-button ng-click=\"sharePoll()\" translate=\"common.action.share\"></md-button></md-menu-item><md-menu-item ng-if=\"canEditPoll()\"><md-button ng-click=\"editPoll()\" translate=\"common.action.edit\"></md-button></md-menu-item><md-menu-item ng-if=\"canClosePoll()\" class=\"poll-actions-dropdown__close\"><md-button ng-click=\"closePoll()\" translate=\"common.action.close\"></md-button></md-menu-item><md-menu-item ng-if=\"canDeletePoll()\"><md-button ng-click=\"deletePoll()\" translate=\"common.action.delete\"></md-button></md-menu-item></md-menu-content></md-menu>");
$templateCache.put("generated/components/poll/common/attachment_form/poll_common_attachment_form.html","<div class=\"poll-common-attachment-form\"><md_attachment_form model=\"poll\" show-label=\"true\"></md_attachment_form><validation_errors subject=\"poll\" field=\"file\"></validation_errors><label ng-if=\"poll.newAttachments().length &gt;0\" translate=\"poll_common_attachment_form.label\" class=\"poll-common-form__label\"></label><attachment_preview attachment=\"attachment\" mode=\"thumb\" ng-repeat=\"attachment in poll.newAttachments() | orderBy: \'id\' track by attachment.id\"></attachment_preview><div class=\"clearfix\"></div></div>");
$templateCache.put("generated/components/poll/common/bar_chart/poll_common_bar_chart.html","<div class=\"poll-common-bar-chart\"><div ng-repeat=\"option in poll.pollOptions() track by option.id\" ng-style=\"styleData(option)\" class=\"poll-common-bar-chart__bar\">{{barTextFor(option)}}</div></div>");
$templateCache.put("generated/components/poll/common/bar_chart_panel/poll_common_bar_chart_panel.html","<div class=\"poll-common-bar-chart-panel\"><h3 translate=\"poll_common.results\" class=\"lmo-card-subheading\"></h3><poll_common_bar_chart poll=\"poll\"></poll_common_bar_chart></div>");
$templateCache.put("generated/components/poll/common/calendar_invite/poll_common_calendar_invite.html","<div class=\"poll-common-calendar-invite lmo-drop-animation\"><div class=\"md-block poll-common-calendar-invite__checkbox poll-common-checkbox-option\"><div class=\"poll-common-checkbox-option__text md-list-item-text\"><h3 translate=\"poll_common_calendar_invite.calendar_invite\" class=\"lmo-h3\"></h3><p ng-if=\"outcome.calendarInvite\" translate=\"poll_common_calendar_invite.helptext_on\" class=\"md-caption\"></p><p ng-if=\"!outcome.calendarInvite\" translate=\"poll_common_calendar_invite.helptext_off\" class=\"md-caption\"></p></div><md-checkbox ng-model=\"outcome.calendarInvite\"></md-checkbox></div><div ng-show=\"outcome.calendarInvite\" class=\"poll-common-calendar-invite__form animated\"><md-input-container class=\"md-block poll-common-calendar-invite--pad-top\"><label translate=\"poll_common_calendar_invite.poll_option_id\"></label><md-select ng-model=\"outcome.pollOptionId\" aria-label=\"{{ \'poll_common_calendar_invite.poll_option_id\' | translate }}\" class=\"lmo-flex__grow\"><md-option ng-repeat=\"option in options\" ng-value=\"option.id\">{{option.value}}</md-option></md-select></md-input-container><md-input-container class=\"md-block poll-common-calendar-invite--pad-top\"><label translate=\"poll_common_calendar_invite.event_summary\"></label><input type=\"text\" placeholder=\"{{\'poll_common_calendar_invite.event_summary_placeholder\' | translate}}\" ng-model=\"outcome.customFields.event_summary\" class=\"poll-common-calendar-invite__summary\"><validation_errors subject=\"outcome\" field=\"event_summary\"></validation_errors></md-input-container><md-input-container class=\"md-block\"><label translate=\"poll_common_calendar_invite.location\"></label><input type=\"text\" placeholder=\"{{\'poll_common_calendar_invite.location_placeholder\' | translate}}\" ng-model=\"outcome.customFields.event_location\" class=\"poll-common-calendar-invite__location\"></md-input-container><md-input-container class=\"md-block\"><label translate=\"poll_common_calendar_invite.event_description\"></label><textarea type=\"text\" placeholder=\"{{\'poll_common_calendar_invite.event_description_placeholder\' | translate}}\" ng-model=\"outcome.customFields.event_description\" class=\"md-input lmo-primary-form-input poll-common-calendar-invite__description\"></textarea></md-input-container></div></div>");
$templateCache.put("generated/components/poll/common/card/poll_common_card.html","<section class=\"poll-common-card lmo-card-padding lmo-flex__grow lmo-relative\"><div ng-if=\"isDisabled\" class=\"lmo-disabled-form\"></div><div ng-if=\"!initExecuting\" class=\"lmo-blank\"><poll_common_card_header poll=\"poll\"></poll_common_card_header><poll_common_set_outcome_panel poll=\"poll\"></poll_common_set_outcome_panel><poll_common_summary_panel poll=\"poll\"></poll_common_summary_panel><div class=\"poll-proposal-panel\"><div ng-if=\"showResults()\" class=\"poll-common-card__results-shown\"><poll_common_directive poll=\"poll\" name=\"chart_panel\"></poll_common_directive><poll_common_percent_voted poll=\"poll\"></poll_common_percent_voted></div><poll_common_action_panel poll=\"poll\"></poll_common_action_panel><div ng-if=\"showResults()\" class=\"poll-common-card__results-shown\"><poll_common_votes_panel poll=\"poll\"></poll_common_votes_panel></div></div></div></section>");
$templateCache.put("generated/components/poll/common/card_header/poll_common_card_header.html","<poll_common_actions_dropdown poll=\"poll\" ng-if=\"pollHasActions()\" class=\"pull-right\"></poll_common_actions_dropdown><div class=\"poll-common-card-header lmo-flex\"><i class=\"material-icons\">{{icon()}}</i><h2 translate=\"poll_types.{{poll.pollType}}\" class=\"lmo-card-heading poll-common-card-header__poll-type\"></h2></div>");
$templateCache.put("generated/components/poll/common/chart_preview/poll_common_chart_preview.html","<div class=\"poll-common-chart-preview\"><bar_chart ng-if=\"chartType() == \'bar\'\" stance_counts=\"poll.stanceCounts\" size=\"50\"></bar_chart><progress_chart ng-if=\"chartType() == \'progress\'\" stance_counts=\"poll.stanceCounts\" goal=\"poll.goal()\" size=\"50\"></progress_chart><poll_proposal_chart_preview ng-if=\"chartType() == \'pie\'\" stance_counts=\"poll.stanceCounts\" my_stance=\"myStance()\"></poll_proposal_chart_preview><matrix_chart ng-if=\"chartType() == \'matrix\'\" matrix_counts=\"poll.matrixCounts\" size=\"50\"></matrix_chart></div>");
$templateCache.put("generated/components/poll/common/choose_type/poll_common_choose_type.html","<div class=\"poll-common-choose-type__select-poll-type\"><div class=\"poll-common-choose-type__poll-type--{{pollType}}\" ng-repeat=\"pollType in pollTypes() track by $index\"><md-list-item ng-click=\"choose(pollType)\" class=\"poll-common-choose-type__poll-type\"><i class=\"material-icons poll-common-choose-type__icon\">{{iconFor(pollType)}}</i><div class=\"poll-common-choose-type__content poll-common-choose-type__start-poll--{{pollType}}\"><div translate=\"decision_tools_card.{{pollType}}_title\" class=\"poll-common-choose-type__poll-type-title md-subhead\"></div><div translate=\"poll_{{pollType}}_form.tool_tip_collapsed\" class=\"poll-common-choose-type__poll-type-subtitle md-caption\"></div></div></md-list-item></div></div>");
$templateCache.put("generated/components/poll/common/close_form/poll_common_close_form.html","<form ng-submit=\"submit()\"><div ng-show=\"isDisabled\" class=\"lmo-disabled-form\"></div><md-toolbar><div class=\"md-toolbar-tools lmo-flex__space-between\"><h1 translate=\"poll_common_close_form.close_{{poll.pollType}}\" class=\"lmo-h1\"></h1><material_modal_header_cancel_button></material_modal_header_cancel_button></div></md-toolbar><md-dialog-content><div class=\"md-dialog-content\"><span translate=\"poll_common_close_form.helptext\"></span></div></md-dialog-content><md-dialog-actions><md-button ng-click=\"submit()\" translate=\"poll_common_close_form.close_{{poll.pollType}}\" class=\"md-primary poll-common-close-form__submit\"></md-button></md-dialog-actions></form>");
$templateCache.put("generated/components/poll/common/closing_at/poll_common_closing_at.html"," <abbr class=\"closing-in timeago--inline\"><span translate=\"{{translationKey()}}\" translate-value-time=\"{{time()}}\" ng-attr-title=\"{{time()}}\"></span></abbr> ");
$templateCache.put("generated/components/poll/common/closing_at_field/poll_common_closing_at_field.html","<div class=\"poll-common-closing-at-field\"><div class=\"poll-common-closing-at-field__inputs\"><md-input-container class=\"poll-common-closing-at-field-give-margin-right\"><label class=\"poll-common-closing-at-field__label\"><poll_common_closing_at poll=\"poll\"></poll_common_closing_at></label> <md-datepicker ng-model=\"closingDate\" ng-min-date=\"dateToday\" md-hide-icons=\"calendar\"></md-datepicker> </md-input-container><md-input-container><md-select ng-model=\"closingHour\" aria-label=\"{{ \'poll_common_closing_at_field.closing_hour\' | translate }}\"><md-option ng-repeat=\"hour in hours\" ng-value=\"hour\" ng-selected=\"hour == closingHour\">{{ times[hour] }}</md-option></md-select></md-input-container></div><validation_errors subject=\"poll\" field=\"closingAt\"></validation_errors></div>");
$templateCache.put("generated/components/poll/common/collapsed/poll_common_collapsed.html","<div class=\"poll-common-collapsed lmo-card-padding\"><poll_common_chart_preview poll=\"poll\"></poll_common_chart_preview><div class=\"poll-common-collapsed__content\"><div class=\"md-subhead md-primary\"> <strong>{{formattedPollType(poll.pollType)}}:</strong>  <span>{{poll.title}}</span> </div><div class=\"md-caption lmo-grey-on-white\"> <span translate=\"poll_common_collapsed.by_who\" translate-value-name=\"{{poll.author().name}}\"></span>  <span>·</span> <poll_common_closing_at poll=\"poll\"></poll_common_closing_at></div></div></div>");
$templateCache.put("generated/components/poll/common/delete_modal/poll_common_delete_modal.html","<md-dialog class=\"poll-common-delete-modal\"><md-toolbar><div class=\"md-toolbar-tools lmo-flex__space-between\"><h1 translate=\"poll_common_delete_modal.title\" class=\"lmo-h1\"></h1><material_modal_header_cancel_button></material_modal_header_cancel_button></div></md-toolbar><md-dialog-content class=\"md-dialog-content\"><p translate=\"poll_common_delete_modal.question\"></p><div class=\"lmo-flex lmo-flex__space-between\"><button ng-click=\"$close()\" translate=\"common.action.cancel\" class=\"lmo-btn--cancel\"></button><button ng-click=\"submit()\" translate=\"common.action.delete\" class=\"lmo-btn--danger\"></button></div></md-dialog-content></md-dialog>");
$templateCache.put("generated/components/poll/common/edit_vote_modal/poll_common_edit_vote_modal.html","<md-dialog class=\"poll-common-modal\"><md-toolbar><div class=\"md-toolbar-tools lmo-flex__space-between\"><i class=\"material-icons\">{{icon()}}</i><h1 ng-if=\"stance.isNew()\" translate=\"poll_common.your_response\" class=\"lmo-h1\"></h1><h1 ng-if=\"!stance.isNew()\" translate=\"poll_common.change_your_response\" class=\"lmo-h1\"></h1><material_modal_header_cancel_button></material_modal_header_cancel_button></div></md-toolbar><md-dialog-content><div class=\"md-dialog-content\"><div ng-if=\"isDisabled\" class=\"lmo-disabled-form\"></div><poll_common_directive name=\"vote_form\" stance=\"stance\"></poll_common_directive></div></md-dialog-content></md-dialog>");
$templateCache.put("generated/components/poll/common/example_card/poll_common_example_card.html","<div class=\"poll-common-example-card lmo-card lmo-flex\"><div class=\"poll-common-helptext\"> <span translate=\"poll_common_example_card.what_is_this\" translate-value-type=\"{{type()}}\"></span> <span translate=\"poll_common_example_card.helptext_before\"></span> <a lmo-href=\"/p/new/{{poll.pollType}}\" class=\"lmo-pointer\"><span translate=\"poll_common_example_card.helptext_link\" translate-value-type=\"{{type()}}\"></span></a> <span translate=\"poll_common_example_card.helptext_after\"></span></div></div>");
$templateCache.put("generated/components/poll/common/form_actions/poll_common_form_actions.html","<div class=\"poll-common-form-actions lmo-flex\"><span ng-if=\"!poll.isNew()\"></span><md-button ng-if=\"poll.isNew()\" ng-click=\"back()\" translate=\"common.action.back\" aria-label=\"{{ \'common.action.back\' | translate }}\" class=\"md-raised\"></md-button><md-button ng-click=\"submit()\" ng-if=\"!poll.isNew()\" translate=\"poll_common_form.update\" aria-label=\"{{ \'poll_common_form.update\' | translate }}\" class=\"md-primary md-raised poll-common-form__submit\"></md-button><md-button ng-click=\"submit()\" ng-if=\"poll.isNew() &amp;&amp; poll.groupId\" translate=\"poll_common_form.start\" aria-label=\"{{ \'poll_common_form.start\' | translate }}\" class=\"md-primary md-raised poll-common-form__submit\"></md-button><md-button ng-click=\"submit()\" ng-if=\"poll.isNew() &amp;&amp; !poll.groupId\" translate=\"common.action.next\" aria-label=\"{{ \'common.action.next\' | translate }}\" class=\"md-primary md-raised poll-common-form__submit\"></md-button></div>");
$templateCache.put("generated/components/poll/common/form_modal/poll_common_form_modal.html","<md-dialog class=\"poll-common-modal\"><md-toolbar><div class=\"md-toolbar-tools lmo-flex__space-between\"><i class=\"material-icons\">{{icon()}}</i><h1 ng-if=\"poll.isNew()\" translate=\"poll_{{poll.pollType}}_form.start_header\" class=\"lmo-h1\"></h1><h1 ng-if=\"!poll.isNew()\" translate=\"poll_{{poll.pollType}}_form.edit_header\" class=\"lmo-h1\"></h1><material_modal_header_cancel_button></material_modal_header_cancel_button></div></md-toolbar><div class=\"md-dialog-content\"><div ng-show=\"isDisabled\" class=\"lmo-disabled-form\"></div><poll_common_directive poll=\"poll\" name=\"form\"></poll_common_directive></div></md-dialog>");
$templateCache.put("generated/components/poll/common/form_options/poll_common_form_options.html","<md-block class=\"poll-common-form-options\"><label ng-if=\"!datesAsOptions\" translate=\"poll_common_form.options\" class=\"poll-common-form__options-label md-caption\"></label><div ng-if=\"datesAsOptions\" class=\"poll-meeting-form__label-and-timezone\"><label translate=\"poll_meeting_form.timeslots\" class=\"nowrap poll-common-form__options-label md-caption lmo-flex__grow\"></label><time_zone_select zone=\"poll.customFields.time_zone\" class=\"lmo-flex__shrink\"></time_zone_select></div><md-list class=\"md-block poll-common-form__options\"><validation_errors subject=\"poll\" field=\"pollOptions\"></validation_errors><md-list-item ng-if=\"!poll.pollOptionNames.length\"><p ng-if=\"datesAsOptions\" translate=\"poll_meeting_form.no_options\" class=\"poll-common-helptext\"></p><p ng-if=\"!datesAsOptions\" translate=\"poll_common_form.no_options\" class=\"poll-common-helptext\"></p></md-list-item><md-list-item ng-repeat=\"name in poll.pollOptionNames track by $index\" class=\"poll-common-form__list-item\"><span ng-if=\"!datesAsOptions\" class=\"poll-poll-form__option-text\">{{name}}</span><poll_meeting_time ng-if=\"datesAsOptions\" name=\"name\" zone=\"zone\" class=\"poll-meeting-form__option-text lmo-flex__grow\"></poll_meeting_time><md-button ng-if=\"!isExisting(name)\" ng-click=\"removeOption(name)\" aria-label=\"{{ \'poll_poll_form.remove_option\' | translate }}\" class=\"poll-poll-form__option-button\"><i class=\"material-icons poll-poll-form__option-icon\">clear</i></md-button></md-list-item><poll_meeting_time_field ng-if=\"datesAsOptions\" poll=\"poll\"></poll_meeting_time_field><md-list-item ng-if=\"!datesAsOptions\" flex=\"true\" layout=\"row\" class=\"poll-common-form__add-option\"><md-input-container md-no-float=\"true\" class=\"poll-poll-form__add-option-field\"><input ng-model=\"poll.newOptionName\" type=\"text\" placeholder=\"{{ \'poll_common_form.options_placeholder\' | translate }}\" class=\"poll-poll-form__add-option-input\"></md-input-container><div><md-button ng-click=\"addOption()\" aria-label=\"{{ \'poll_poll_form.add_option_placeholder\' | translate }}\" class=\"poll-poll-form__option-button\"><i class=\"material-icons poll-poll-form__option-icon\">add</i></md-button></div></md-list-item></md-list></md-block>");
$templateCache.put("generated/components/poll/common/index_card/poll_common_index_card.html","<div class=\"poll-common-index-card lmo-card\"><h2 translate=\"poll_common_index_card.title\" class=\"lmo-card-heading\"></h2><loading ng-if=\"fetchRecordsExecuting\"></loading><div ng-if=\"!fetchRecordsExecuting\" class=\"poll-common-index-card__polls\"><div ng-if=\"!polls().length\" translate=\"poll_common_index_card.no_polls\" class=\"poll-common-helptext\"></div><div ng-if=\"polls().length\" class=\"poll-common-index-card__has-polls\"><poll_common_preview poll=\"poll\" ng-repeat=\"poll in polls() track by poll.id\"></poll_common_preview><a ng-click=\"viewMore()\" ng-if=\"displayViewMore()\" class=\"poll-common-index-card__view-more\"><span translate=\"poll_common_index_card.count\" translate-value-count=\"{{model.closedPollsCount}}\"></span></a></div></div></div>");
$templateCache.put("generated/components/poll/common/notify_group/poll_common_notify_group.html","<div ng-if=\"model.announcementSize(notifyAction) &gt; 1\" class=\"md-block poll-common-notify-group poll-common-checkbox-option\"><div class=\"poll-common-checkbox-option__text md-list-item-text\"><h3 translate=\"poll_common_notify_group.notify_group\"></h3><p ng-if=\"model.makeAnnouncement\" class=\"md-caption\"><span ng-if=\"notifyAction == \'publish\'\" translate=\"poll_common_notify_group.members_count\" translate-value-count=\"{{model.announcementSize(\'publish\')}}\"></span><span ng-if=\"notifyAction == \'edit\'\" translate=\"poll_common_notify_group.participants_count\" translate-value-count=\"{{model.announcementSize(\'edit\')}}\"></span></p><p ng-if=\"!model.makeAnnouncement\" translate=\"poll_common_notify_group.members_helptext\" class=\"md-caption\"></p></div><md-checkbox ng-model=\"model.makeAnnouncement\"></md-checkbox></div>");
$templateCache.put("generated/components/poll/common/notify_on_participate/poll_common_notify_on_participate.html","<div class=\"md-block poll-common-notify-on-participate poll-common-checkbox-option\"><div class=\"poll-common-checkbox-option__text md-list-item-text\"><h3 translate=\"poll_common_notify_on_participate.notify_on_participate\"></h3><p ng-if=\"poll.notifyOnParticipate\" translate=\"poll_common_notify_on_participate.helptext_on\" class=\"md-caption\"></p><p ng-if=\"!poll.notifyOnParticipate\" translate=\"poll_common_notify_on_participate.helptext_off\" class=\"md-caption\"></p></div><md-checkbox ng-model=\"poll.notifyOnParticipate\"></md-checkbox></div>");
$templateCache.put("generated/components/poll/common/outcome_form/poll_common_outcome_form.html","<div class=\"poll-common-outcome-form\"><div ng-if=\"isDisabled\" class=\"lmo-disabled-form\"></div><md-input-container class=\"md-block\"><label translate=\"poll_common.statement\"></label><textarea placeholder=\"{{\'poll_common_outcome_form.statement_placeholder\' | translate}}\" ng-model=\"outcome.statement\" class=\"md-input lmo-primary-form-input poll-common-outcome-form__statement\"></textarea><validation_errors subject=\"outcome\" field=\"statement\"></validation_errors></md-input-container><poll_common_notify_group model=\"outcome\" notify-action=\"publish\"></poll_common_notify_group><poll_common_calendar_invite outcome=\"outcome\" ng-if=\"datesAsOptions()\"></poll_common_calendar_invite><div class=\"lmo-flex lmo-flex__space-between\"><div></div><md-button ng-click=\"submit()\" translate=\"poll_common_outcome_form.submit\" aria-label=\"{{poll_common_outcome_form.submit | translate}}\" class=\"md-raised md-primary poll-common-outcome-form__submit\"></md-button></div></div>");
$templateCache.put("generated/components/poll/common/outcome_modal/poll_common_outcome_modal.html","<md-dialog class=\"poll-common-modal\"><md-toolbar><div class=\"md-toolbar-tools lmo-flex__space-between\"><i class=\"material-icons\">thumbs_up_down</i><h1 ng-if=\"outcome.isNew()\" translate=\"poll_common_outcome_form.new_title\" class=\"lmo-h1\"></h1><h1 ng-if=\"!outcome.isNew()\" translate=\"poll_common_outcome_form.update_title\" class=\"lmo-h1\"></h1><material_modal_header_cancel_button></material_modal_header_cancel_button></div></md-toolbar><md-dialog-content><div class=\"md-dialog-content\"><poll_common_directive name=\"outcome_form\" outcome=\"outcome\"></poll_common_directive></div></md-dialog-content></md-dialog>");
$templateCache.put("generated/components/poll/common/outcome_panel/poll_common_outcome_panel.html","<div ng-if=\"poll.outcome()\" class=\"poll-common-outcome-panel\"><h3 translate=\"poll_common.outcome\" class=\"lmo-card-subheading\"></h3><div class=\"poll-common-outcome-panel__authored-by\"><span translate=\"poll_common_outcome_panel.authored_by\" translate-value-name=\"{{poll.outcome().author().name}}\"></span> <timeago timestamp=\"::poll.outcome().createdAt\"></timeago> </div><p marked=\"poll.outcome().statement\" class=\"lmo-markdown-wrapper\"></p><md-button ng-if=\"showUpdateButton()\" ng-click=\"openOutcomeForm()\" translate=\"poll_common.update_outcome\" aria-label=\"{{poll_common.update_outcome | translate}}\"></md-button></div>");
$templateCache.put("generated/components/poll/common/participant_form/poll_common_participant_form.html","<div ng-if=\"showParticipantForm()\" class=\"poll-common-participant-form\"><md-input-container class=\"md-block\"><label translate=\"poll_common.participant_name\"></label><input type=\"text\" required=\"required\" placeholder=\"{{poll_common_participant_form.participant_name_placeholder}}\" ng-model=\"stance.visitorAttributes.name\" class=\"lmo-primary-form-input poll-common-participant-form__name\"><validation_errors subject=\"stance\" field=\"participantName\"></validation_errors></md-input-container><md-input-container ng-if=\"!stance.participant().email\" class=\"md-block\"><label translate=\"poll_common.participant_email\"></label><input type=\"email\" required=\"required\" placeholder=\"{{poll_common_participant_form.participant_email_placeholder}}\" ng-model=\"stance.visitorAttributes.email\" class=\"lmo-primary-form-input poll-common-participant-form__email\"><validation_errors subject=\"stance\" field=\"participantEmail\"></validation_errors></md-input-container></div>");
$templateCache.put("generated/components/poll/common/percent_voted/poll_common_percent_voted.html","<div ng-if=\"poll.communitySize() &gt; 0\" class=\"poll-common-percent-voted poll-common-helptext\"><span translate=\"poll_common_percent_voted.sentence\" translate-value-numerator=\"{{poll.stancesCount}}\" translate-value-denominator=\"{{poll.communitySize()}}\" translate-value-percent=\"{{poll.percentVoted()}}\"></span></div>");
$templateCache.put("generated/components/poll/common/preview/poll_common_preview.html","<a lmo-href-for=\"poll\" class=\"poll-common-preview\"><poll_common_chart_preview poll=\"poll\"></poll_common_chart_preview><div class=\"poll-common-preview__body\"><div class=\"md-subhead lmo-truncate-text\"> <span>{{poll.title}}</span> </div><div class=\"md-caption lmo-grey-on-white lmo-truncate-text\"><span ng-if=\"showGroupName()\">{{ poll.group().fullName }}</span> <span ng-if=\"!showGroupName()\" translate=\"poll_common_collapsed.by_who\" translate-value-name=\"{{poll.author().name}}\"></span>  <span>·</span> <poll_common_closing_at poll=\"poll\"></poll_common_closing_at></div></div></a>");
$templateCache.put("generated/components/poll/common/school_link/poll_common_school_link.html","<div class=\"poll-common-school-link\"><i class=\"material-icons\">import_contacts</i><p> <span translate=\"poll_common_school_link.learn_about\"></span> <a lmo-href=\"{{href}}\" target=\"_blank\"><span translate=\"{{translation}}\"></span></a></p></div>");
$templateCache.put("generated/components/poll/common/set_outcome_panel/poll_common_set_outcome_panel.html","<div ng-if=\"showPanel()\" class=\"poll-common-set-outcome-panel\"><p translate=\"poll_common_set_outcome_panel.{{poll.pollType}}\" class=\"poll-common-helptext\"></p><md-dialog-actions><md-button ng-click=\"openOutcomeForm()\" translate=\"poll_common_set_outcome_panel.share_outcome\" aria-label=\"{{poll_common.set_outcome | translate}}\" class=\"md-primary poll-common-set-outcome-panel__submit\"></md-button></md-dialog-actions></div>");
$templateCache.put("generated/components/poll/common/share_modal/poll_common_share_modal.html","<md-dialog class=\"poll-common-modal poll-common-modal__narrow\"><div ng-show=\"isDisabled\" class=\"lmo-disabled-form\"></div><md-toolbar><div class=\"md-toolbar-tools lmo-flex__space-between\"><i class=\"material-icons\">{{icon()}}</i><h1 translate=\"poll_common.share_poll\" class=\"lmo-h1\"></h1><material_modal_header_cancel_button></material_modal_header_cancel_button></div></md-toolbar><md-dialog-content><div class=\"md-dialog-content\"><poll_common_share_form poll=\"poll\"></poll_common_share_form></div></md-dialog-content></md-dialog>");
$templateCache.put("generated/components/poll/common/show_results_button/show_results_button.html","<md-button ng-if=\"!clicked\" ng-click=\"press()\" translate=\"poll_common_card.show_results\" aria-label=\"{{poll_common_card.show_results | translate}}\" class=\"md-accent\"></md-button>");
$templateCache.put("generated/components/poll/common/stance_choice/poll_common_stance_choice.html","<div ng-class=\"poll-common-stance-choice--{{stanceChoice.poll().pollType}}\" class=\"poll-common-stance-choice\"><poll_common_stance_icon stance_choice=\"stanceChoice\"></poll_common_stance_icon> <span ng-if=\"hasVariableScore()\">{{stanceChoice.score}} -</span> <span ng-if=\"translateOptionName()\" translate=\"poll_{{stanceChoice.poll().pollType}}_options.{{stanceChoice.pollOption().name}}\" class=\"poll-common-stance-choice__option-name\"></span><poll_meeting_time ng-if=\"!translateOptionName() &amp;&amp; datesAsOptions()\" name=\"stanceChoice.pollOption().name\"></poll_meeting_time><span ng-if=\"!translateOptionName() &amp;&amp; !datesAsOptions()\">{{stanceChoice.pollOption().name}}</span></div>");
$templateCache.put("generated/components/poll/common/stance_icon/poll_common_stance_icon.html","<div class=\"poll-common-stance-icon\"><img ng-if=\"useOptionIcon()\" ng-src=\"/img/{{stanceChoice.pollOption().name}}.svg\" class=\"poll-common-stance-icon__svg\"><div ng-if=\"!useOptionIcon()\" ng-style=\"{\'border-color\': \'{{stanceChoice.pollOption().color}}\'}\" class=\"poll-common-stance-icon__chip\"></div></div>");
$templateCache.put("generated/components/poll/common/start_form/poll_common_start_form.html","<md-list class=\"decision-tools-card__poll-types\"><md-list-item ng-click=\"startPoll(pollType)\" ng-repeat=\"pollType in pollTypes() track by $index\" class=\"decision-tools-card__poll-type\"><i class=\"material-icons decision-tools-card__icon\">{{fieldFromTemplate(pollType, \'material_icon\')}}</i><div class=\"decision-tools-card__content decision-tools-card__poll-type--{{pollType}}\"><div translate=\"poll_types.{{pollType}}\" class=\"decision-tools-card__poll-type-title md-body-1\"></div><div translate=\"poll_{{pollType}}_form.tool_tip_collapsed\" class=\"decision-tools-card__poll-type-subtitle md-caption\"></div></div></md-list-item></md-list>");
$templateCache.put("generated/components/poll/common/start_modal/poll_common_start_modal.html","<md-dialog class=\"poll-common-modal\"><md-toolbar><div class=\"md-toolbar-tools lmo-flex__space-between\"><i class=\"material-icons\">{{icon()}}</i><h1 translate=\"poll_common.start_poll\" class=\"lmo-h1\"></h1><material_modal_header_cancel_button></material_modal_header_cancel_button></div></md-toolbar><div class=\"md-dialog-content\"><div ng-show=\"isDisabled\" class=\"lmo-disabled-form\"></div><poll_common_start_poll poll=\"poll\" class=\"lmo-slide-animation\"></poll_common_start_poll></div></md-dialog>");
$templateCache.put("generated/components/poll/common/start_poll/poll_common_start_poll.html","<div ng-switch=\"currentStep\" class=\"poll-common-start-poll\"><poll_common_choose_type ng-switch-when=\"choose\" poll=\"poll\" class=\"animated\"></poll_common_choose_type><poll_common_directive ng-switch-when=\"save\" poll=\"poll\" name=\"form\" class=\"animated\"></poll_common_directive><poll_common_share_form ng-switch-when=\"share\" poll=\"poll\" class=\"animated\"></poll_common_share_form></div>");
$templateCache.put("generated/components/poll/common/summary_panel/poll_common_summary_panel.html","<div class=\"poll-common-summary-panel\"><h3 ng-if=\"!translation\" class=\"poll-common-summary-panel__title\">{{poll.title}}</h3><h3 ng-if=\"translation\" class=\"lmo-h1 poll-common-summary-panel__title\"><translation translation=\"translation\" field=\"title\"></translation></h3><poll_common_outcome_panel poll=\"poll\"></poll_common_outcome_panel><h3 translate=\"poll_common.details\" ng-if=\"poll.outcome()\" class=\"lmo-card-subheading\"></h3><div class=\"poll-common-summary-panel__started-by\"><span translate=\"poll_card.started_by\" translate-value-name=\"{{poll.author().name}}\"></span> <span aria-hidden=\"true\">·</span> <poll_common_closing_at poll=\"poll\"></poll_common_closing_at> <translate_button model=\"poll\" showdot=\"true\" class=\"lmo-card-minor-action\"></translate_button> </div><div ng-if=\"!translation\" marked=\"poll.cookedDetails()\" class=\"poll-common-summary-panel__details lmo-markdown-wrapper\"></div><div ng-if=\"translation\" class=\"poll-common-summary-panel__details\"><translation translation=\"translation\" field=\"details\" class=\"lmo-markdown-wrapper\"></translation></div><div ng-if=\"poll.hasAttachments()\" class=\"poll-common-summary-panel__attachments\"><div ng-repeat=\"attachment in poll.attachments() | orderBy:\'createdAt\' track by attachment.id\" class=\"poll-common-summary-panel__attachment\"><attachment_preview attachment=\"attachment\" mode=\"context\"></attachment_preview></div></div></div>");
$templateCache.put("generated/components/poll/common/tool_tip/poll_common_tool_tip.html","<div class=\"poll-common-tool-tip\"><div ng-if=\"!collapsed\" class=\"poll-common-tool-tip__expanded\"><div translate=\"poll_{{poll.pollType}}_form.tool_tip_expanded\" class=\"poll-common-tool-tip__expanded-body md-body-1\"></div><div class=\"lmo-flex lmo-flex__space-around\"><md-button translate=\"common.ok_got_it\" ng-click=\"hide()\" class=\"md-accent poll-common-tool-tip__collapse\"></md-button></div></div><div ng-if=\"collapsed\" class=\"poll-common-tool-tip__collapsed md-caption\"><span translate=\"poll_{{poll.pollType}}_form.tool_tip_collapsed\" class=\"poll-common-tool-tip__collapsed-body\"></span><span translate=\"common.expand\" ng-click=\"show()\" class=\"poll-common-tool-tip__learn-more\"></span></div></div>");
$templateCache.put("generated/components/poll/common/voter_add_options/poll_common_voter_add_options.html","<div ng-if=\"validPollType()\" class=\"md-block poll-common-notify-group poll-common-checkbox-option\"><div class=\"poll-common-checkbox-option__text md-list-item-text\"><h3 translate=\"poll_common_voter_add_options.add_options\"></h3><p ng-if=\"poll.voterCanAddOptions\" translate=\"poll_common_voter_add_options.helptext_on\" class=\"md-caption\"></p><p ng-if=\"!poll.voterCanAddOptions\" translate=\"poll_common_voter_add_options.helptext_off\" class=\"md-caption\"></p></div><md-checkbox ng-model=\"poll.voterCanAddOptions\"></md-checkbox></div>");
$templateCache.put("generated/components/poll/common/votes_panel/poll_common_votes_panel.html","<div class=\"poll-common-votes-panel\"><div class=\"poll-common-votes-panel__header\"><h3 translate=\"poll_common.votes\" class=\"lmo-card-subheading\"></h3><md-select ng-model=\"fix.votesOrder\" ng-change=\"changeOrder()\" aria-label=\"{{ \'poll_common_votes_panel.change_results_order\' | translate }}\" class=\"md-no-underline\"><md-option ng-repeat=\"opt in sortOptions\" ng-value=\"opt\" translate=\"poll_common_votes_panel.{{opt}}\"></md-option></md-select></div><div ng-if=\"!hasSomeVotes()\" translate=\"poll_common_votes_panel.no_votes_yet\" class=\"poll-common-votes-panel__no-votes\"></div><div ng-if=\"hasSomeVotes()\" class=\"poll-common-votes-panel__has-votes\"><poll_common_directive stance=\"stance\" name=\"votes_panel_stance\" ng-repeat=\"stance in stances() track by stance.id\"></poll_common_directive><md-button ng-if=\"moreToLoad()\" translate=\"common.action.load_more\" ng-click=\"loader.loadMore()\"></md-button></div><poll_common_undecided_panel poll=\"poll\"></poll_common_undecided_panel></div>");
$templateCache.put("generated/components/poll/common/votes_panel_stance/poll_common_votes_panel_stance.html","<div class=\"poll-common-votes-panel__stance\"><user_avatar user=\"stance.participant()\" size=\"small\" class=\"lmo-flex__no-shrink\"></user_avatar><div class=\"poll-common-votes-panel__stance-content\"><div class=\"poll-common-votes-panel__stance-name-and-option\"> <strong>{{stance.participant().name}}</strong>  <span translate=\"poll_common_votes_panel.none_of_the_above\" ng-if=\"!stance.stanceChoices().length\" class=\"poll-common-helptext\"></span> <poll_common_stance_choice stance_choice=\"choice\" ng-if=\"choice.score &gt; 0\" ng-repeat=\"choice in stance.stanceChoices()\"></poll_common_stance_choice> <translate_button ng-if=\"stance.reason\" model=\"stance\" showdot=\"true\" class=\"lmo-card-minor-action\"></translate_button> </div><div ng-if=\"stance.reason\" class=\"poll-common-votes-panel__stance-reason\"><span ng-if=\"!translation\" marked=\"stance.reason\" class=\"lmo-markdown-wrapper\"></span><translation ng-if=\"translation\" translation=\"translation\" field=\"reason\"></translation></div></div></div>");
$templateCache.put("generated/components/poll/count/chart_panel/poll_count_chart_panel.html","<div class=\"poll-count-chart-panel\"><h3 translate=\"poll_common.results\" class=\"lmo-card-subheading\"></h3><div class=\"poll-count-chart-panel__chart-container\"><div class=\"poll-count-chart-panel__progress\"><div class=\"poll-count-chart-panel__incomplete\"></div><div ng-style=\"{\'flex-basis\': percentComplete(1), \'background-color\': colors[1]}\" class=\"poll-count-chart-panel__no\"></div><div ng-style=\"{\'flex-basis\': percentComplete(0), \'background-color\': colors[0]}\" class=\"poll-count-chart-panel__yes\"></div></div><div class=\"poll-count-chart-panel__data\"><div class=\"poll-count-chart-panel__numerator\">{{poll.stanceCounts[0]}}</div><div translate=\"poll_count_chart_panel.out_of\" translate-value-goal=\"{{poll.goal()}}\" class=\"poll-count-chart-panel__denominator\"></div></div></div></div>");
$templateCache.put("generated/components/poll/count/form/poll_count_form.html","<div class=\"poll-count-form\"><poll_common_tool_tip poll=\"poll\"></poll_common_tool_tip><md-input-container class=\"md-block\"><label translate=\"poll_common_form.title\"></label><input type=\"text\" placeholder=\"{{\'poll_count_form.title_placeholder\' | translate}}\" ng-model=\"poll.title\" class=\"lmo-primary-form-input poll-count-form__title\"><validation_errors subject=\"poll\" field=\"title\"></validation_errors></md-input-container><md-input-container class=\"md-block\"><label translate=\"poll_common_form.details\"></label><textarea ng-model=\"poll.details\" placeholder=\"{{\'poll_count_form.details_placeholder\' | translate}}\" mention-field=\"true\" class=\"lmo-primary-form-input poll-count-form__details\"></textarea><outlet name=\"after-count-form-textarea\" model=\"poll\"></outlet></md-input-container><div class=\"md-input-compensate\"><poll_common_attachment_form poll=\"poll\"></poll_common_attachment_form></div><poll_common_closing_at_field poll=\"poll\"></poll_common_closing_at_field><div class=\"md-input-compensate\"><poll_common_notify_group model=\"poll\" notify-action=\"{{poll.notifyAction()}}\"></poll_common_notify_group><poll_common_notify_on_participate poll=\"poll\"></poll_common_notify_on_participate></div><poll_common_form_actions poll=\"poll\" submit=\"submit\" back=\"back\"></poll_common_form_actions></div>");
$templateCache.put("generated/components/poll/count/vote_form/poll_count_vote_form.html","<form class=\"poll-count-vote-form\"><poll_common_participant_form stance=\"stance\"></poll_common_participant_form><div class=\"poll-common-vote-form__reason\"><md-input-container class=\"md-block\"><input type=\"text\" placeholder=\"{{\'poll_count_vote_form.reason_placeholder\' | translate}}\" ng-model=\"stance.reason\" md-maxlength=\"250\" class=\"lmo-primary-form-input\"><validation_errors subject=\"stance\" field=\"reason\"></validation_errors></md-input-container><div class=\"poll-count-vote-form__options\"><md-button ng-click=\"submit(yes)\" ng-style=\"{background: yes.color}\" class=\"poll-count-vote-form__option poll-count-vote-form__option--yes\"><i class=\"material-icons\">done</i></md-button><md-button ng-click=\"submit(no)\" ng-style=\"{\'border-color\': no.color}\" class=\"poll-count-vote-form__option poll-count-vote-form__option--no\"><i ng-style=\"{color: no.color}\" class=\"material-icons\">clear</i></md-button></div></div></form>");
$templateCache.put("generated/components/poll/dot_vote/chart_panel/poll_dot_vote_chart_panel.html","<div class=\"poll-dot-vote-chart-panel\"><poll_common_bar_chart_panel poll=\"poll\"></poll_common_bar_chart_panel></div>");
$templateCache.put("generated/components/poll/dot_vote/form/poll_dot_vote_form.html","<div class=\"poll-dot-vote-form\"><poll_common_tool_tip poll=\"poll\"></poll_common_tool_tip><md-input-container class=\"md-block\"><label translate=\"poll_common_form.title\"></label><input type=\"text\" placeholder=\"{{\'poll_dot_vote_form.title_placeholder\' | translate}}\" ng-model=\"poll.title\" class=\"lmo-primary-form-input poll-dot-vote-form__title\"><validation_errors subject=\"poll\" field=\"title\"></validation_errors></md-input-container><md-input-container class=\"md-block\"><label translate=\"poll_common_form.details\"></label><textarea ng-model=\"poll.details\" placeholder=\"{{\'poll_dot_vote_form.details_placeholder\' | translate}}\" mention-field=\"true\" class=\"lmo-primary-form-input poll-dot-vote-form__details\"></textarea><outlet name=\"after-poll-form-textarea\" model=\"poll\"></outlet></md-input-container><div class=\"md-input-compensate\"><poll_common_attachment_form poll=\"poll\"></poll_common_attachment_form></div><poll_common_form_options poll=\"poll\"></poll_common_form_options><div class=\"md-input-compensate\"><poll_common_closing_at_field poll=\"poll\" class=\"md-block\"></poll_common_closing_at_field></div><div class=\"md-input-compensate\"><md-input-container><label translate=\"poll_dot_vote_form.dots_per_person\"></label><input type=\"number\" min=\"0\" ng-model=\"poll.customFields.dots_per_person\"><validation_errors subject=\"poll\" field=\"dotsPerPerson\"></validation_errors></md-input-container></div><div class=\"md-input-compensate\"><poll_common_notify_group model=\"poll\" notify-action=\"{{poll.notifyAction()}}\"></poll_common_notify_group><poll_common_voter_add_options poll=\"poll\"></poll_common_voter_add_options><poll_common_notify_on_participate poll=\"poll\"></poll_common_notify_on_participate></div><poll_common_form_actions poll=\"poll\" submit=\"submit\" back=\"back\"></poll_common_form_actions></div>");
$templateCache.put("generated/components/poll/dot_vote/vote_form/poll_dot_vote_vote_form.html","<form ng-submit=\"submit()\" class=\"poll-dot-vote-vote-form\"><poll_common_participant_form stance=\"stance\"></poll_common_participant_form><h3 translate=\"poll_common.your_response\" class=\"lmo-card-subheading\"></h3><div class=\"poll-common-helptext\"><div ng-if=\"tooManyDots()\" translate=\"poll_dot_vote_vote_form.too_many_dots\" class=\"poll-dot-vote-vote-form__too-many-dots\"></div><div ng-if=\"!tooManyDots()\" translate=\"poll_dot_vote_vote_form.dots_remaining\" translate-value-count=\"{{dotsRemaining()}}\" class=\"poll-dot-vote-vote-form__dots-remaining\"></div></div><md-list class=\"poll-common-vote-form__options\"><md-list-item ng-repeat=\"choice in stanceChoices\" class=\"poll-dot-vote-vote-form__option poll-common-vote-form__option\"><md-input-container class=\"poll-dot-vote-vote-form__input-container\"><p ng-style=\"styleData(choice)\" class=\"poll-dot-vote-vote-form__chosen-option--name poll-common-vote-form__border-chip poll-common-bar-chart__bar\">{{ optionFor(choice).name }}</p><div class=\"poll-dot-vote-vote-form__dot-input-field\"><button type=\"button\" ng-click=\"adjust(choice, -1)\" ng-disabled=\"choice.score == 0\" class=\"poll-dot-vote-vote-form__dot-button\"><div class=\"material-icons\">remove</div></button><input type=\"number\" ng-model=\"choice.score\" min=\"0\" step=\"1\" class=\"poll-dot-vote-vote-form__dot-input\"><button type=\"button\" ng-click=\"adjust(choice, 1)\" ng-disabled=\"dotsRemaining() == 0\" class=\"poll-dot-vote-vote-form__dot-button\"><div class=\"material-icons\">add</div></button></div></md-input-container></md-list-item></md-list><validation_errors subject=\"stance\" field=\"stanceChoices\"></validation_errors><poll_common_add_option_button ng-if=\"stance.isNew() &amp;&amp; stance.poll().voterCanAddOptions\" poll=\"stance.poll()\"></poll_common_add_option_button><div class=\"poll-common-vote-form__reason\"><md-input-container class=\"md-block\"><label translate=\"poll_common.reason\"></label><input type=\"text\" placeholder=\"{{\'poll_common.reason_placeholder\' | translate}}\" ng-model=\"stance.reason\" md-maxlength=\"250\" class=\"lmo-primary-form-input\"><validation_errors subject=\"stance\" field=\"reason\"></validation_errors></md-input-container></div><div class=\"poll-common-form-actions lmo-flex lmo-flex__space-between\"><show_results_button ng-if=\"stance.isNew()\"></show_results_button><div ng-if=\"!stance.isNew()\"></div><md-button type=\"submit\" ng-disabled=\"tooManyDots()\" translate=\"poll_common.vote\" aria-label=\"{{ \'poll_poll_vote_form.vote\' | translate }}\" class=\"md-primary md-raised\"></md-button></div></form>");
$templateCache.put("generated/components/poll/dot_vote/votes_panel_stance/poll_dot_vote_votes_panel_stance.html","<div class=\"poll-dot-vote-votes-panel-stance\"><div class=\"poll-dot-vote-votes-panel-stance__head\"><user_avatar user=\"stance.participant()\" size=\"small\" class=\"lmo-flex__no-shrink\"></user_avatar><div class=\"poll-dot-vote-votes-panel-stance__name\"> <strong>{{stance.participant().name}}</strong>  <span translate=\"poll_common_votes_panel.none_of_the_above\" ng-if=\"!stance.stanceChoices().length\" class=\"poll-common-helptext\"></span> </div></div><div class=\"poll-dot-vote-votes-panel-stance__body\"><div ng-if=\"stance.reason\" marked=\"stance.reason\" class=\"poll-common-votes-panel__stance-reason lmo-markdown-wrapper\"></div><div ng-if=\"choice.score &gt; 0\" ng-repeat=\"choice in stanceChoices()\" ng-style=\"styleData(choice)\" class=\"poll-dot-vote-votes-panel__stance-choice\">{{barTextFor(choice)}}</div></div></div>");
$templateCache.put("generated/components/poll/meeting/chart_panel/poll_meeting_chart_panel.html","<div class=\"poll-meeting-chart-panel\"><table><thead><tr><td><time_zone_select></time_zone_select></td><td ng-repeat=\"option in poll.pollOptions() | orderBy:\'name\' track by option.id\" class=\"poll-meeting-chart-panel__cell\"><poll_meeting_time name=\"option.name\" zone=\"zone\"></poll_meeting_time></td></tr></thead><tbody><tr ng-repeat=\"stance in poll.uniqueStances() track by stance.id\"><td class=\"poll-meeting-chart-panel__participant-name\">{{ stance.participant().name }}</td><td ng-class=\"{\'poll-meeting-chart-panel--active\': stance.votedFor(option), \'poll-meeting-chart-panel--inactive\': !stance.votedFor(option)}\" ng-repeat=\"option in poll.pollOptions() | orderBy: \'name\' track by option.id\" class=\"poll-meeting-chart-panel__cell\"><i ng-if=\"stance.votedFor(option)\" class=\"material-icons\">check</i></td></tr><tr class=\"poll-meeting-chart-panel__bold\"><td translate=\"poll_meeting_chart_panel.total\"></td><td ng-repeat=\"option in poll.pollOptions() | orderBy:\'name\' track by option.id\" class=\"poll-meeting-chart-panel__cell\">{{option.stances().length}}</td></tr></tbody></table></div>");
$templateCache.put("generated/components/poll/meeting/form/poll_meeting_form.html","<div class=\"poll-meeting-form\"><poll_common_tool_tip poll=\"poll\"></poll_common_tool_tip><md-input-container class=\"md-block\"><label translate=\"poll_common_form.title\"></label><input type=\"text\" placeholder=\"{{\'poll_meeting_form.title_placeholder\' | translate}}\" ng-model=\"poll.title\" class=\"lmo-primary-form-input poll-meeting-form__title\"><validation_errors subject=\"poll\" field=\"title\"></validation_errors></md-input-container><md-input-container class=\"md-block\"><label translate=\"poll_common_form.details\"></label><textarea ng-model=\"poll.details\" placeholder=\"{{\'poll_meeting_form.details_placeholder\' | translate}}\" mention-field=\"true\" class=\"lmo-primary-form-input poll-meeting-form__details\"></textarea><outlet name=\"after-poll-form-textarea\" model=\"poll\"></outlet></md-input-container><md-input-container class=\"md-block\"><label translate=\"poll_meeting_form.meeting_duration\"></label><md-select ng-model=\"poll.customFields.meeting_duration\"><md-option ng-repeat=\"duration in durations\" ng-value=\"duration.minutes\">{{duration.label}}</md-option></md-select></md-input-container><poll_common_attachment_form poll=\"poll\"></poll_common_attachment_form><poll_common_form_options poll=\"poll\"></poll_common_form_options><poll_common_closing_at_field poll=\"poll\" class=\"md-block\"></poll_common_closing_at_field><poll_common_notify_group model=\"poll\" notify-action=\"{{poll.notifyAction()}}\"></poll_common_notify_group><poll_common_voter_add_options poll=\"poll\"></poll_common_voter_add_options><poll_common_notify_on_participate poll=\"poll\"></poll_common_notify_on_participate><poll_common_form_actions poll=\"poll\" submit=\"submit\" back=\"back\"></poll_common_form_actions></div>");
$templateCache.put("generated/components/poll/meeting/time_field/poll_meeting_time_field.html","<md-list-item flex=\"true\" layout=\"row\" class=\"poll-meeting-time-field\"><md-input-container class=\"poll-meeting-time-field__datepicker-container\"><label translate=\"poll_meeting_time_field.new_time_slot\" class=\"poll-common-closing-at-field__label\"></label> <md-datepicker ng-model=\"option.date\" md-placeholder=\"{{ \'poll_meeting_form.add_option_placeholder\' | translate }}\" ng-min-date=\"dateToday\" md-hide-icons=\"calendar\" class=\"lmo-flex lmo-flex__baseline poll-meeting-time-field__datepicker\"></md-datepicker> </md-input-container><md-input-container ng-if=\"poll.customFields.meeting_duration\" class=\"poll-meeting-time-field__timepicker-container\"><md-select ng-model=\"option.time\" aria-label=\"{{ \'poll_meeting_time_field.closing_hour\' | translate }}\"><md-option ng-repeat=\"time in times track by $index\" ng-value=\"time\">{{ time }}</md-option></md-select></md-input-container><div class=\"lmo-flex__grow\"></div><div class=\"poll-meeting-time-field__add\"><md-button ng-click=\"addOption()\" aria-label=\"{{ \'poll_meeting_form.add_option_placeholder\' | translate }}\" class=\"poll-meeting-form__option-button lmo-inline-action\"><i class=\"material-icons poll-meeting-form__option-icon\">add</i></md-button></div></md-list-item>");
$templateCache.put("generated/components/poll/meeting/vote_form/poll_meeting_vote_form.html","<form ng-submit=\"submit()\" class=\"poll-meeting-vote-form\"><poll_common_participant_form stance=\"stance\"></poll_common_participant_form><h3 translate=\"poll_meeting_vote_form.your_response\" class=\"lmo-card-subheading\"></h3><div class=\"lmo-flex lmo-flex__flex-end\"><time_zone_select></time_zone_select></div><md-list class=\"poll-common-vote-form__options\"><md-list-item ng-repeat=\"option in stance.poll().pollOptions() | orderBy:\'name\' track by option.id\" class=\"poll-common-vote-form__option\"><p ng-style=\"{\'border-color\': option.color}\" class=\"poll-poll-vote-form__option--name poll-common-vote-form__border-chip\"><poll_meeting_time name=\"option.name\" zone=\"zone\"></poll_meeting_time></p><md-checkbox ng-model=\"pollOptionIdsChecked[option.id]\" class=\"md-block poll-poll-vote-form__checkbox\"></md-checkbox></md-list-item></md-list><validation_errors subject=\"stance\" field=\"stanceChoices\"></validation_errors><poll_common_add_option_button ng-if=\"stance.isNew() &amp;&amp; stance.poll().voterCanAddOptions\" poll=\"stance.poll()\"></poll_common_add_option_button><div class=\"poll-common-vote-form__reason\"><md-input-container class=\"md-block\"><label translate=\"poll_common.message\"></label><input type=\"text\" placeholder=\"{{\'poll_meeting_vote_form.reason_placeholder\' | translate}}\" ng-model=\"stance.reason\" md-maxlength=\"250\" class=\"lmo-primary-form-input\"><validation_errors subject=\"stance\" field=\"reason\"></validation_errors></md-input-container></div><div class=\"poll-common-form-actions lmo-flex lmo-flex__space-between\"><show_results_button ng-if=\"stance.isNew()\"></show_results_button><div ng-if=\"!stance.isNew()\"></div><md-button type=\"submit\" translate=\"poll_common.vote\" aria-label=\"{{ \'poll_meeting_vote_form.vote\' | translate }}\" class=\"md-primary md-raised\"></md-button></div></form>");
$templateCache.put("generated/components/poll/meeting/votes_panel_stance/poll_meeting_votes_panel_stance.html","<div ng-if=\"stance.reason\" class=\"poll-common-votes-panel__stance\"><user_avatar user=\"stance.participant()\" size=\"small\" class=\"lmo-flex__no-shrink\"></user_avatar><div class=\"poll-common-votes-panel__stance-content\"><div class=\"poll-common-votes-panel__stance-name-and-option\"> <strong>{{stance.participant().name}}</strong>  <translate_button ng-if=\"stance.reason\" model=\"stance\" showdot=\"true\" class=\"lmo-card-minor-action\"></translate_button> </div><div ng-if=\"stance.reason\" class=\"poll-common-votes-panel__stance-reason\"><span ng-if=\"!translation\" marked=\"stance.reason\" class=\"lmo-markdown-wrapper\"></span><translation ng-if=\"translation\" translation=\"translation\" field=\"reason\"></translation></div></div></div>");
$templateCache.put("generated/components/poll/poll/chart_panel/poll_poll_chart_panel.html","<div class=\"poll-poll-chart-panel\"><poll_common_bar_chart_panel poll=\"poll\"></poll_common_bar_chart_panel></div>");
$templateCache.put("generated/components/poll/poll/form/poll_poll_form.html","<div class=\"poll-poll-form\"><poll_common_tool_tip poll=\"poll\"></poll_common_tool_tip><md-input-container class=\"md-block\"><label translate=\"poll_common_form.title\"></label><input type=\"text\" placeholder=\"{{\'poll_poll_form.title_placeholder\' | translate}}\" ng-model=\"poll.title\" class=\"lmo-primary-form-input poll-poll-form__title\"><validation_errors subject=\"poll\" field=\"title\"></validation_errors></md-input-container><md-input-container class=\"md-block\"><label translate=\"poll_common_form.details\"></label><textarea ng-model=\"poll.details\" placeholder=\"{{\'poll_poll_form.details_placeholder\' | translate}}\" mention-field=\"true\" class=\"lmo-primary-form-input poll-poll-form__details\"></textarea><outlet name=\"after-poll-form-textarea\" model=\"poll\"></outlet></md-input-container><div class=\"md-input-compensate\"><poll_common_attachment_form poll=\"poll\"></poll_common_attachment_form></div><poll_common_form_options poll=\"poll\"></poll_common_form_options><div class=\"md-input-compensate\"><poll_common_closing_at_field poll=\"poll\" class=\"md-block\"></poll_common_closing_at_field></div><div class=\"md-input-compensate\"><div class=\"md-block poll-poll-form__multiple-choice poll-common-checkbox-option\"><div class=\"poll-common-checkbox-option__text md-list-item-text\"><h3 translate=\"poll_poll_form.multiple_choice\"></h3><p translate=\"poll_poll_form.multiple_choice_explained\" class=\"md-caption\"></p></div><md-checkbox ng-model=\"poll.multipleChoice\" aria-label=\"{{ \'poll_poll_form.multiple_choice\' | translate }}\"></md-checkbox></div></div><poll_common_notify_group model=\"poll\" notify-action=\"{{poll.notifyAction()}}\"></poll_common_notify_group><poll_common_voter_add_options poll=\"poll\"></poll_common_voter_add_options><poll_common_notify_on_participate poll=\"poll\"></poll_common_notify_on_participate><poll_common_form_actions poll=\"poll\" submit=\"submit\" back=\"back\"></poll_common_form_actions></div>");
$templateCache.put("generated/components/poll/poll/vote_form/poll_poll_vote_form.html","<form ng-submit=\"submit()\" class=\"poll-poll-vote-form\"><poll_common_participant_form stance=\"stance\"></poll_common_participant_form><h3 translate=\"poll_common.your_response\" class=\"lmo-card-subheading\"></h3><div class=\"poll-common-vote-form__options\"><div ng-if=\"!stance.poll().multipleChoice\" class=\"poll-poll-vote-form__options\"><md-radio-group ng-model=\"vars.pollOptionId\"><md-radio-button ng-repeat=\"option in stance.poll().pollOptions()\" ng-style=\"{\'border-color\': option.color}\" ng-value=\"option.id\" class=\"poll-common-vote-form__option poll-common-vote-form__radio-button poll-common-vote-form__border-chip\"><span class=\"poll-common-vote-form__option-name\">{{option.name}}</span></md-radio-button></md-radio-group></div><md-list ng-if=\"stance.poll().multipleChoice\" class=\"poll-poll-vote-form__options\"><md-list-item ng-repeat=\"option in stance.poll().pollOptions()\" class=\"poll-common-vote-form__option\"><p ng-style=\"{\'border-color\': option.color}\" class=\"poll-common-vote-form__option-name poll-common-vote-form__border-chip\">{{option.name}}</p><md-checkbox ng-model=\"pollOptionIdsChecked[option.id]\" class=\"md-block poll-common-vote-form__checkbox\"></md-checkbox></md-list-item></md-list></div><validation_errors subject=\"stance\" field=\"stanceChoices\"></validation_errors><poll_common_add_option_button ng-if=\"stance.isNew() &amp;&amp; stance.poll().voterCanAddOptions\" poll=\"stance.poll()\"></poll_common_add_option_button><div class=\"poll-common-vote-form__reason\"><md-input-container class=\"md-block\"><label translate=\"poll_common.reason\"></label><input type=\"text\" placeholder=\"{{\'poll_common.reason_placeholder\' | translate}}\" ng-model=\"stance.reason\" md-maxlength=\"250\" class=\"lmo-primary-form-input\"><validation_errors subject=\"stance\" field=\"reason\"></validation_errors></md-input-container></div><div class=\"poll-common-form-actions lmo-flex lmo-flex__space-between\"><show_results_button ng-if=\"stance.isNew()\"></show_results_button><div ng-if=\"!stance.isNew()\"></div><md-button type=\"submit\" translate=\"poll_common.vote\" aria-label=\"{{ \'poll_poll_vote_form.vote\' | translate }}\" class=\"md-primary md-raised\"></md-button></div></form>");
$templateCache.put("generated/components/poll/proposal/edit_vote_modal/poll_proposal_edit_vote_modal.html","<md-dialog class=\"poll-common-modal\"><div ng-show=\"isDisabled\" class=\"lmo-disabled-form\"></div><div class=\"modal-header poll-common-modal-header\"><i class=\"material-icons\">thumbs_up_down</i><h1 ng-if=\"stance.isNew()\" translate=\"poll_common.your_response\" class=\"lmo-h1\"></h1><h1 ng-if=\"!stance.isNew()\" translate=\"poll_common.change_your_response\" class=\"lmo-h1\"></h1><modal_header_cancel_button></modal_header_cancel_button></div><div class=\"modal-body\"><poll_proposal_vote_form stance=\"stance\"></poll_proposal_vote_form></div></md-dialog>");
$templateCache.put("generated/components/poll/proposal/form/poll_proposal_form.html","<div class=\"poll-proposal-form\"><poll_common_tool_tip poll=\"poll\"></poll_common_tool_tip><md-input-container class=\"md-block\"><label translate=\"poll_common_form.title\"></label><input type=\"text\" placeholder=\"{{\'poll_proposal_form.title_placeholder\' | translate}}\" ng-model=\"poll.title\" class=\"lmo-primary-form-input poll-proposal-form__title\"><validation_errors subject=\"poll\" field=\"title\"></validation_errors></md-input-container><md-input-container class=\"md-block\"><label translate=\"poll_common_form.details\"></label><textarea ng-model=\"poll.details\" placeholder=\"{{\'poll_proposal_form.details_placeholder\' | translate}}\" mention-field=\"true\" class=\"lmo-primary-form-input poll-proposal-form__details\"></textarea><outlet name=\"after-poll-proposal-form-textarea\" model=\"poll\"></outlet></md-input-container><poll_common_attachment_form poll=\"poll\" class=\"md-input-compensate\"></poll_common_attachment_form><poll_common_closing_at_field poll=\"poll\" class=\"md-block\"></poll_common_closing_at_field><div class=\"md-input-compensate\"><poll_common_notify_group model=\"poll\" notify-action=\"{{poll.notifyAction()}}\"></poll_common_notify_group><poll_common_notify_on_participate poll=\"poll\"></poll_common_notify_on_participate></div><poll_common_form_actions poll=\"poll\" submit=\"submit\" back=\"back\"></poll_common_form_actions></div>");
$templateCache.put("generated/components/poll/proposal/vote_form/poll_proposal_vote_form.html","<form ng-submit=\"submit()\" class=\"poll-proposal-vote-form\"><poll_common_participant_form stance=\"stance\"></poll_common_participant_form><h3 translate=\"poll_common.your_response\" class=\"lmo-card-subheading\"></h3><div class=\"poll-common-vote-form__options\"><md-radio-group ng-model=\"stance.selectedOption\"><md-radio-button class=\"poll-common-vote-form__radio-button poll-common-vote-form__radio-button--{{option.name}}\" ng-repeat=\"option in stance.poll().pollOptions()\" ng-value=\"option\" aria-label=\"{{option.name}}\"><img ng-src=\"/img/{{option.name}}.svg\" class=\"poll-proposal-form__icon\"><span translate=\"poll_proposal_options.{{option.name}}\" class=\"poll-proposal-vote-form__chosen-option--name\"></span></md-radio-button></md-radio-group></div><validation_errors subject=\"stance\" field=\"stanceChoices\"></validation_errors><div class=\"poll-common-vote-form__reason\"><md-input-container class=\"md-block\"><label translate=\"poll_common.reason\"></label><textarea placeholder=\"{{reasonPlaceholder() | translate}}\" ng-model=\"stance.reason\" md-max-length=\"250\" rows=\"2\" class=\"lmo-primary-form-input poll-proposal-vote-form__reason\"></textarea><validation_errors subject=\"stance\" field=\"reason\"></validation_errors></md-input-container></div><div class=\"poll-common-form-actions lmo-flex lmo-flex__space-between\"><show_results_button ng-if=\"stance.isNew()\"></show_results_button><div ng-if=\"!stance.isNew()\"></div><md-button type=\"submit\" translate=\"poll_common.vote\" aria-label=\"{{ \'poll_proposal_vote_form.vote\' | translate }}\" class=\"md-primary md-raised poll-proposal-vote-form__submit\"></md-button></div></form>");
$templateCache.put("generated/components/poll/proposal/chart_panel/poll_proposal_chart_panel.html","<div class=\"poll-proposal-chart-panel\"><h3 translate=\"poll_common.results\" class=\"lmo-card-subheading\"></h3><div class=\"poll-proposal-chart-panel__chart-container\"><poll_proposal_chart stance_counts=\"poll.stanceCounts\" diameter=\"200\" class=\"poll-proposal-chart-panel__chart\"></poll_proposal_chart><table role=\"presentation\" class=\"poll-proposal-chart-panel__legend\"><tbody><tr ng-repeat=\"name in pollOptionNames() track by $index\"><td><div class=\"poll-proposal-chart-panel__label poll-proposal-chart-panel__label--{{name}}\">{{ countFor(name) }} {{ translationFor(name) }}</div></td></tr></tbody></table></div></div>");
$templateCache.put("generated/components/poll/proposal/chart_preview/poll_proposal_chart_preview.html","<div class=\"poll-proposal-chart-preview\"><poll_proposal_chart stance_counts=\"stanceCounts\" diameter=\"50\" class=\"poll-common-collapsed__pie-chart\"></poll_proposal_chart><div class=\"poll-proposal-chart-preview__stance-container\"><div ng-if=\"myStance\" class=\"poll-proposal-chart-preview__stance poll-proposal-chart-preview__stance--{{myStance.pollOption().name}}\"></div><div ng-if=\"!myStance\" class=\"poll-proposal-chart-preview__stance poll-proposal-chart-preview__stance--undecided\"><i class=\"fa fa-question\"></i></div></div></div>");
$templateCache.put("generated/components/thread_page/current_proposal_card/proposal_actions_dropdown/proposal_actions_dropdown.html","<div uib-dropdown=\"true\" class=\"proposal-actions-dropdown pull-right lmo-no-print\"><button uib-dropdown-toggle=\"true\" class=\"lmo-btn--nude proposal-actions-dropdown__btn\"><i class=\"fa fa-chevron-down\"></i></button><div role=\"menu\" class=\"uib-dropdown-menu lmo-dropdown-menu\"><ul class=\"lmo-dropdown-menu-items\"><li ng-if=\"canEditProposal()\" class=\"lmo-dropdown-menu-item\"><a ng_click=\"editProposal()\" class=\"lmo-dropdown-menu-item-label proposal-actions-dropdown__edit-link\"><span translate=\"proposal_expanded.edit_proposal\"></span></a></li><li ng-if=\"!canEditProposal() &amp;&amp; canCloseOrExtendProposal()\" class=\"lmo-dropdown-menu-item\"><a href=\"#\" ng_click=\"extendProposal()\" class=\"lmo-dropdown-menu-item-label proposal-actions-dropdown__extend-link\"><span translate=\"proposal_expanded.extend_proposal\"></span></a></li><li ng-if=\"canCloseOrExtendProposal()\" class=\"lmo-dropdown-menu-item\"><a href=\"#\" ng_click=\"closeProposal()\" class=\"lmo-dropdown-menu-item-label proposal-actions-dropdown__close-link\"><span translate=\"proposal_expanded.close_proposal\"></span></a></li></ul></div></div>");
$templateCache.put("generated/components/poll/common/add_option/button/poll_common_add_option_button.html","<div class=\"poll-common-add-option-button lmo-flex lmo-flex__space-between\"><div></div><md-button ng-click=\"open()\" translate=\"poll_common_add_option.button.add_option\"></md-button></div>");
$templateCache.put("generated/components/poll/common/add_option/form/poll_common_add_option_form.html","<div class=\"poll-common-add-option-form\"><poll_common_form_options poll=\"poll\"></poll_common_form_options><div class=\"lmo-flex lmo-flex__space-between md-input-compensate\"><div></div><md-button ng-click=\"submit()\" translate=\"poll_common_add_option.form.add_options\" class=\"md-raised md-primary\"></md-button></div></div>");
$templateCache.put("generated/components/poll/common/add_option/modal/poll_common_add_option_modal.html","<md-dialog class=\"poll-common-modal\"><md-toolbar><div class=\"md-toolbar-tools lmo-flex__space-between\"><i class=\"material-icons\">lightbulb_outline</i><h1 translate=\"poll_common_add_option.modal.title\" class=\"lmo-h1\"></h1><material_modal_header_cancel_button></material_modal_header_cancel_button></div></md-toolbar><div class=\"md-dialog-content\"><div ng-show=\"isDisabled\" class=\"lmo-disabled-form\"></div><poll_common_add_option_form poll=\"poll\"></poll_common_add_option_form></div></md-dialog>");
$templateCache.put("generated/components/poll/common/publish/facebook_preview/poll_common_publish_facebook_preview.html","<div class=\"poll-common-publish-facebook-preview\"><div layout=\"row\" class=\"poll-common-publish-facebook-preview__title lmo-flex\"><img ng-src=\"{{community.identity().logo}}\" class=\"poll-common-publish-facebook-preview__image\"><div layout=\"column\" class=\"lmo-flex\"><div> <span class=\"poll-common-publish-facebook-preview__link\">{{community.identity().name}}</span>  <span translate=\"poll_common_publish_facebook_preview.shared_a\"></span>  <span translate=\"poll_common_publish_facebook_preview.link\" class=\"poll-common-publish-facebook-preview__link\"></span> </div><div> <abbr translate=\"poll_common_publish_facebook_preview.just_now\" class=\"poll-common-publish-facebook-preview__abbr\"></abbr> <abbr class=\"poll-common-publish-facebook-preview__abbr\">· Loomio</abbr></div></div></div><div class=\"poll-common-publish-facebook-preview__message\">{{ message }}</div><div class=\"poll-common-publish-facebook-preview__preview\"><div class=\"poll-common-publish-facebook-preview__poll-title\">{{ poll.title }}</div><div class=\"poll-common-publish-facebook-preview__poll-details\">{{ poll.details }}</div><div class=\"poll-common-publish-facebook-preview__host\">{{ host() }}</div></div></div>");
$templateCache.put("generated/components/poll/common/publish/form/poll_common_publish_form.html","<div class=\"poll-common-publish-form\"><div ng-if=\"submitExecuting\" class=\"lmo-disabled-form\"></div><h3 translate=\"poll_common_publish_form.community\" class=\"lmo-h3\"></h3><div layout=\"row\" class=\"lmo-flex__grow lmo-flex\"><img class=\"poll-common-share-form__community-icon poll-common-share-form__community-icon--{{community.communityType}}\" ng-src=\"/img/{{community.communityType}}-icon.svg\"><div class=\"poll-common-share-form__h3 lmo-inline-action\">{{community.displayName()}}</div></div><md-input-container class=\"md-block\"><label translate=\"poll_common_publish_form.message\"></label><textarea ng-model=\"message\" placeholder=\"{{\'poll_common_publish_form.message_placeholder\' | translate}}\" class=\"lmo-primary-form-input\"></textarea></md-input-container><div class=\"poll-common-publish-form__preview\"><poll_common_publish_facebook_preview ng-if=\"community.communityType == \'facebook\'\" poll=\"poll\" community=\"community\" message=\"message\"></poll_common_publish_facebook_preview><poll_common_publish_slack_preview ng-if=\"community.communityType == \'slack\'\" poll=\"poll\" community=\"community\" message=\"message\"></poll_common_publish_slack_preview></div><div class=\"lmo-flex lmo-flex__space-between\"><md-button ng-click=\"back()\"><span translate=\"common.action.back\"></span></md-button><md-button ng-click=\"submit()\" class=\"md-raised md-primary\"><span translate=\"common.action.ok\"></span></md-button></div></div>");
$templateCache.put("generated/components/poll/common/publish/modal/poll_common_publish_modal.html","<md-dialog class=\"poll-common-modal\"><div ng-show=\"isDisabled\" class=\"lmo-disabled-form\"></div><md-toolbar><div class=\"md-toolbar-tools lmo-flex__space-between\"><i class=\"material-icons\">{{icon()}}</i><h1 translate=\"poll_common.share_poll\" class=\"lmo-h1\"></h1><material_modal_header_cancel_button></material_modal_header_cancel_button></div></md-toolbar><md-dialog-content><div class=\"md-dialog-content\"><poll_common_publish_form poll=\"poll\" community=\"community\" back=\"back\"></poll_common_publish_form></div></md-dialog-content></md-dialog>");
$templateCache.put("generated/components/poll/common/publish/slack_preview/poll_common_publish_slack_preview.html","<div layout=\"row\" class=\"poll-common-publish-slack-preview lmo-flex\"><div class=\"poll-common-publish-slack-preview__avatar\"><img ng-src=\"https://s3-us-west-2.amazonaws.com/slack-files2/bot_icons/2017-03-29/161925077303_48.png\"></div><div class=\"poll-common-publish-slack-preview__content\"><div class=\"poll-common-publish-slack-preview__title\"> <strong class=\"poll-common-publish-slack-preview__loomio-bot\">Loomio Bot</strong>  <span class=\"poll-common-publish-slack-preview__app\">APP</span>  <span class=\"poll-common-publish-slack-preview__title-timestamp\">{{ timestamp() }}</span> </div><div class=\"poll-common-publish-slack-preview__published\"></div><div class=\"poll-common-publish-slack-preview__message\">{{ message }}</div><div layout=\"row\" class=\"poll-common-publish-slack-preview__attachment lmo-flex\"><div class=\"poll-common-publish-slack-preview__bar\"></div><div class=\"poll-common-publish-slack-preview__poll\"><div class=\"poll-common-publish-slack-preview__author\">{{ userName }}</div><div class=\"poll-common-publish-slack-preview__poll-title\">{{ poll.title }}</div><div ng-if=\"poll.details\" class=\"poll-common-publish-slack-preview__poll-details\">{{ poll.details | truncate }}</div><div translate=\"poll_common_publish_slack_preview.view_it_on_loomio\" class=\"poll-common-publish-slack-preview__view-it\"></div><div translate=\"poll_common_publish_slack_preview.timestamp\" translate-value-timestamp=\"{{timestamp()}}\" class=\"poll-common-publish-slack-preview__timestamp\"></div><div layout=\"row\" class=\"poll-common-publish-slack-preview__options lmo-flex\"><div ng-repeat=\"option in poll.pollOptions()\" class=\"poll-common-publish-slack-preview__option\">{{ option.displayName }}</div></div></div></div></div></div>");
$templateCache.put("generated/components/poll/common/share/email_form/poll_common_share_email_form.html","<section><div class=\"lmo-flex\"><h3 translate=\"poll_common_share_form.invite_visitor\" class=\"lmo-h3\"></h3><help_bubble helptext=\"poll_common_share_form.invite_visitor_helptext\"></help_bubble></div><md-list class=\"md-block\"><md-list-item ng-if=\"poll.customFields.pending_emails.length\" layout=\"column\" class=\"poll-common-share-form__emails\"><div ng-repeat=\"email in poll.customFields.pending_emails track by $index\" class=\"poll-common-share-form__visitor lmo-flex\"><div class=\"poll-common-share-form__email lmo-flex__grow\">{{email}}</div><md-button ng-click=\"remove(email)\" class=\"lmo-inline-action\"><i class=\"material-icons\">clear</i></md-button></div></md-list-item><md-list-item flex=\"true\" layout=\"row\" class=\"poll-common-share-form__add-option\"><md-input-container md-no-float=\"true\" class=\"lmo-flex__grow\"><input type=\"text\" placeholder=\"{{ \'poll_common_share_form.enter_email\' | translate }}\" ng-model=\"newEmail\" class=\"poll-common-share-form__add-option-input\"></md-input-container><md-button ng-click=\"addIfValid()\" ng-disabled=\"poll.customFields.pending_emails.length == 0\" aria-label=\"{{ \'poll_common_share_form.enter_email\' | translate }}\" class=\"poll-common-share-form__option-button poll-common-share-form__add-button\"><i class=\"material-icons\">add</i></md-button></md-list-item><div class=\"clearfix\"></div><div class=\"lmo-validation-error\">{{ emailValidationError }}</div></md-list><div class=\"lmo-flex lmo-flex__space-between\"><div></div><md-button ng-disabled=\"poll.processing || poll.customFields.pending_emails.length == 0\" ng-click=\"submit()\" aria-label=\"{{ \'poll_common_share_form.send_email\' | translate }}\" class=\"md-primary md-raised poll-common-share-form__button poll-common-share-form__option-button\"><span translate=\"poll_common_share_form.send_email\"></span></md-button></div></section>");
$templateCache.put("generated/components/poll/common/share/form/poll_common_share_form.html","<div class=\"poll-common-share-form\"><poll_common_share_group_form poll=\"poll\" ng-if=\"hasGroups()\" class=\"poll-common-share-form__form\"></poll_common_share_group_form><poll_common_share_link_form poll=\"poll\" class=\"poll-common-share-form__form\"></poll_common_share_link_form><poll_common_share_email_form poll=\"poll\" ng-if=\"hasPendingEmails()\" class=\"poll-common-share-form__form\"></poll_common_share_email_form><poll_common_share_visitor_form poll=\"poll\" ng-if=\"!hasPendingEmails()\" class=\"poll-common-share-form__form\"></poll_common_share_visitor_form></div>");
$templateCache.put("generated/components/poll/common/share/group_form/poll_common_share_group_form.html","<section><div class=\"lmo-flex\"><h3 translate=\"poll_common_share_form.share_group\" class=\"lmo-h3\"></h3><help_bubble helptext=\"poll_common_share_form.share_group_helptext\"></help_bubble></div><div class=\"lmo-flex\"><md-input-container class=\"md-block lmo-flex__grow poll-common-share-form__group-select\"><md-select ng-model=\"poll.groupId\" ng-disabled=\"poll.discussionId\" aria-label=\"{{ \'poll_common_share_group_form.loomio_group\' | translate }}\"><md-option ng-value=\"null\">{{ \'poll_common_select_group.loomio_group_placeholder\' | translate }}</md-option><md-option ng-repeat=\"group in groups() | orderBy:\'fullName\'\" ng-value=\"group.id\">{{ group.fullName }}</md-option></md-select></md-input-container><md-button ng-disabled=\"poll.groupId == groupId\" ng-click=\"submit()\" aria-label=\"{{ \'poll_common_share_form.set_group\' | translate }}\" class=\"md-primary md-raised poll-common-share-form__button poll-common-share-form__option-button\"><span translate=\"poll_common_share_form.set_group\"></span></md-button></div><poll_common_notify_group model=\"poll\" notify-action=\"publish\" ng-if=\"poll.groupId != groupId\"></poll_common_notify_group><p ng-if=\"groupId &amp;&amp; groupId != poll.groupId\" translate=\"poll_common_share_form.move_group_helptext\" class=\"poll-common-helptext\"></p><p ng-if=\"poll.discussionId\" translate=\"poll_common_share_form.cannot_move_poll_helptext\" translate-value-discussion=\"{{poll.discussion().title}}\" translate-value-group=\"{{poll.group().name}}\" class=\"poll-common-helptext\"></p></section>");
$templateCache.put("generated/components/poll/common/share/link_form/poll_common_share_link_form.html","<section><div class=\"lmo-flex\"><h3 translate=\"poll_common_share_form.share_a_link\" class=\"lmo-h3\"></h3><help_bubble helptext=\"poll_common_share_form.share_a_link_helptext\"></help_bubble></div><md-list class=\"md-block\"><div class=\"lmo-flex\"><md-list-item class=\"lmo-flex__grow lmo-flex lmo-flex__center\"><md-checkbox ng-model=\"poll.anyoneCanParticipate\" ng-change=\"setAnyoneCanParticipate()\"></md-checkbox><p translate=\"poll_common_share_form.anyone_can_participate\"></p></md-list-item><md-button type=\"button\" title=\"{{ \'common.copy\' | translate }}\" clipboard=\"true\" text=\"shareableLink\" on-copied=\"copied()\" ng-disabled=\"!poll.anyoneCanParticipate\" class=\"md-primary md-raised poll-common-share-form__button\"><span translate=\"poll_common_share_form.get_link\"></span></md-button></div></md-list></section>");
$templateCache.put("generated/components/poll/common/share/visitor_form/poll_common_share_visitor_form.html","<section><div class=\"lmo-flex\"><h3 translate=\"poll_common_share_form.invite_visitor\" class=\"lmo-h3\"></h3><help_bubble helptext=\"poll_common_share_form.invite_visitor_helptext\"></help_bubble></div><md-list class=\"md-block\"><md-list-item ng-if=\"visitors().length\" layout=\"column\" class=\"poll-common-share-form__emails\"><div ng-repeat=\"visitor in visitors() | orderBy: \'updatedAt\'\" class=\"poll-common-share-form__visitor lmo-flex\"><div class=\"poll-common-share-form__email lmo-flex__grow\">{{visitor.email}}</div><md-button ng-click=\"remind(visitor)\" ng-if=\"!visitor.reminded &amp;&amp; !visitor.processing\" class=\"lmo-inline-action\"><i class=\"material-icons\">redo</i></md-button><loading ng-if=\"visitor.processing\"></loading><span ng-if=\"visitor.reminded\" translate=\"poll_common_share_form.reminded\" class=\"poll-common-share-form__reminded\"></span><md-button ng-if=\"!poll.anyoneCanParticipate\" ng-click=\"revoke(visitor)\" class=\"lmo-inline-action\"><i class=\"material-icons\">clear</i></md-button></div></md-list-item><md-list-item flex=\"true\" layout=\"row\" class=\"poll-common-share-form__add-option\"><md-input-container md-no-float=\"true\" class=\"lmo-flex__grow\"><input type=\"text\" ng-disabled=\"newVisitor.processing\" placeholder=\"{{ \'poll_common_share_form.enter_email\' | translate }}\" ng-model=\"newVisitor.email\" class=\"poll-common-share-form__add-option-input\"></md-input-container><div><loading ng-if=\"newVisitor.processing\" class=\"lmo-inline-action\"></loading><md-button ng-if=\"!newVisitor.processing\" ng-disabled=\"!newVisitor.email\" ng-click=\"invite()\" aria-label=\"{{ \'poll_common_share_form.add_email_placeholder\' | translate }}\" class=\"md-primary md-raised poll-common-share-form__button poll-common-share-form__option-button\"><span translate=\"poll_common_share_form.send_email\"></span></md-button></div></md-list-item><div class=\"clearfix\"></div><div class=\"lmo-validation-error\">{{ emailValidationError }}</div></md-list></section>");
$templateCache.put("generated/components/poll/common/undecided/panel/poll_common_undecided_panel.html","<div class=\"poll-common-undecided-panel\"><md-button ng-if=\"canShowUndecided()\" ng-click=\"showUndecided()\" translate=\"poll_common_undecided_panel.show_undecided\" translate-value-count=\"{{poll.undecidedCount()}}\" aria-label=\"poll_common_undecided_panel.show_undecided\" class=\"poll-common-undecided-panel__button\"></md-button><div ng-if=\"showingUndecided &amp;&amp; poll.undecidedUserCount &gt; 0\" class=\"poll-common-undecided-panel__panel poll-common-undecided-panel__users\"><h3 translate=\"poll_common_undecided_panel.undecided_users\" translate-value-count=\"{{poll.undecidedUserCount}}\" class=\"lmo-card-subheading\"></h3><poll_common_undecided_user user=\"user\" ng-repeat=\"user in poll.undecidedUsers()\"></poll_common_undecided_user><md-button translate=\"common.action.load_more\" aria-label=\"common.action.load_more\" ng-click=\"usersLoader.loadMore()\" ng-if=\"moreUsersToLoad()\"></md-button></div><div ng-if=\"showingUndecided &amp;&amp; poll.undecidedVisitorCount &gt; 0\" class=\"poll-common-undecided-panel__panel poll-common-undecided-panel__visitors\"><h3 translate=\"poll_common_undecided_panel.undecided_visitors\" translate-value-count=\"{{poll.undecidedVisitorCount}}\" class=\"lmo-card-subheading\"></h3><poll_common_undecided_user ng-if=\"canViewVisitors()\" user=\"visitor\" ng-repeat=\"visitor in poll.undecidedVisitors()\"></poll_common_undecided_user><md-button ng-if=\"moreVisitorsToLoad()\" translate=\"common.action.load_more\" aria-label=\"common.action.load_more\" ng-click=\"visitorsLoader.loadMore()\"></md-button><p translate=\"poll_common_undecided_panel.hidden_visitors\" class=\"poll-common-helptext\"></p></div></div>");
$templateCache.put("generated/components/poll/common/undecided/user/poll_common_undecided_user.html","<div layout=\"row\" class=\"poll-common-undecided-user lmo-flex lmo-flex__center\"><user_avatar user=\"user\" size=\"small\"></user_avatar><span class=\"poll-common-undecided-user__name\">{{user.name}}</span></div>");
$templateCache.put("generated/components/poll/common/unsubscribe/form/poll_common_unsubscribe_form.html","<div class=\"poll-common-unsubscribe-form\"><div class=\"md-list-item-text lmo-flex lmo-flex__space-between\"><div class=\"poll-common-checkbox-option__text\"><h3 class=\"lmo-h3\"><span translate=\"poll_common_unsubscribe_form.label\"></span></h3><p translate=\"poll_common_unsubscribe_form.helptext_on\" ng-if=\"poll.subscribed\" class=\"md-caption\"></p><p translate=\"poll_common_unsubscribe_form.helptext_off\" ng-if=\"!poll.subscribed\" class=\"md-caption\"></p></div><md-checkbox ng-model=\"poll.subscribed\" ng-change=\"toggle()\"></md-checkbox></div></div>");
$templateCache.put("generated/components/poll/common/unsubscribe/modal/poll_common_unsubscribe_modal.html","<md-dialog class=\"poll-common-modal\"><md-toolbar><div class=\"md-toolbar-tools lmo-flex__space-between\"><i class=\"material-icons\">email</i><h1 translate=\"poll_common_unsubscribe_form.title\" class=\"lmo-h1\"></h1><material_modal_header_cancel_button></material_modal_header_cancel_button></div></md-toolbar><div class=\"md-dialog-content\"><poll_common_unsubscribe_form poll=\"poll\"></poll_common_unsubscribe_form></div></md-dialog>");}]);